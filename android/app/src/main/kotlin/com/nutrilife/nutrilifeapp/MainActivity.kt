package com.nutrilife.nutrilifeapp

import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.content.Context
import android.content.Intent
import android.location.LocationManager
import android.widget.Toast
import androidx.annotation.NonNull
import cn.net.aicare.pabulumlibrary.PabulumSDK
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugins.GeneratedPluginRegistrant

class MainActivity : FlutterActivity() {
    private val bleSupportAdapter: BluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
    private var isSupportBle: Int = 0 // 0: notSupport    1: BLE off   2: BLE on
    private val CHANNEL = "com.ble.support/testChannel"
    private var mResult: MethodChannel.Result? = null
    private val ENABLE_BLUETOOTH: Int = 1
    private val LAUNCH_SECOND_ACTIVITY: Int = 2

    override fun configureFlutterEngine(@NonNull flutterEngine: FlutterEngine) {
        GeneratedPluginRegistrant.registerWith(flutterEngine);
        //PabulumSDK.getInstance().init(this)
        //key : fd9bee5bf18627f1
        //secret : 00d3a8f9fe2d7c8ed124db4933325ed9
        PabulumSDK.getInstance().init(this)
        MethodChannel(flutterEngine.dartExecutor, CHANNEL).setMethodCallHandler { call, result ->
            val any = if (call.method.equals("getBleSupportStatus")) {
                var flag: Int = getEnsureBLESupport()
                val returnArg: HashMap<Int, String> = HashMap()
                returnArg.put(0, flag.toString())
                result.success(returnArg);
            } else if (call.method.equals("bleTurnOn")) {
                /*var flag : Int = turnOnBLE()
                val returnArg: HashMap<Int, String> = HashMap()
                returnArg.put(0, flag.toString())
                result.success(returnArg);*/
                mResult = result
                turnOnBLE()
            } else if (call.method.equals("getBLEDeviceList")) {
                mResult = result
                getDeviceList(call.arguments())
            } else {
                result.notImplemented()
            }
            any
        }
    }

    private  fun getEnsureBLESupport(): Int {
        if (bleSupportAdapter == null) {
            isSupportBle = 0
        } else {
            if (!bleSupportAdapter.isEnabled) {
                isSupportBle = 1
            } else if (!isLocServiceEnable(applicationContext)) {
                isSupportBle = 1
                Toast.makeText(applicationContext, this.getString(R.string.permissions_server), Toast.LENGTH_LONG).show()
                //ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION), LOCATION_PERMISSION)
            } else {
                isSupportBle = 2
            }
        }
        return isSupportBle
    }

    private fun turnOnBLE(): Int {
        if (bleSupportAdapter == null) {
            isSupportBle = 0
            val returnArg: HashMap<Int, String> = HashMap()
            returnArg.put(0, "0")
            mResult!!.success(returnArg);
        } else {
            if (!bleSupportAdapter.isEnabled) {
                isSupportBle = 1
                startActivityForResult(Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE), ENABLE_BLUETOOTH)
            } else {
                if(!isLocServiceEnable(applicationContext)) {
                    isSupportBle = 1
                } else {
                    isSupportBle = 2
                }
                val returnArg: HashMap<Int, String> = HashMap()
                returnArg.put(0, isSupportBle.toString())
                mResult!!.success(returnArg);
            }
        }
        return isSupportBle
    }

    private fun getDeviceList(intentData: Map<String, String>) {
        val intent = Intent(this, WeightMeterActivity::class.java)
        intent.putExtra("foodName", intentData["food"])
        intent.putExtra("unitType", intentData["unit"])
        startActivityForResult(intent, LAUNCH_SECOND_ACTIVITY)
    }

    fun isLocServiceEnable(context: Context): Boolean {
        val locationManager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        val gps = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
        val network = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
        return if (gps || network) {
            true
        } else false
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == ENABLE_BLUETOOTH) {
            if (bleSupportAdapter.isEnabled) {
                val returnArg: HashMap<Int, String> = HashMap()
                if (!isLocServiceEnable(this)) {
                    isSupportBle = 1
                    Toast.makeText(applicationContext, this.getString(R.string.permissions_server), Toast.LENGTH_SHORT).show()
                } else {
                    isSupportBle = 2
                }
                returnArg.put(0, isSupportBle.toString())
                mResult!!.success(returnArg);
                Toast.makeText(applicationContext, "Bluetooth on", Toast.LENGTH_LONG).show()
            } else {
                isSupportBle = 1;
                val returnArg: HashMap<Int, String> = HashMap()
                returnArg.put(0, "1")
                mResult!!.success(returnArg);
            }
        } else if(requestCode == LAUNCH_SECOND_ACTIVITY) {
            if (resultCode == Activity.RESULT_OK) {
                // 0 = fruit | 1 = weight value | 2 = unitType in String (not as flag)
                val returnArg: HashMap<String, String> = HashMap()
                data!!.getStringExtra("0")?.let { returnArg.put("0", it) }
                data!!.getStringExtra("1")?.let { returnArg.put("1", it) }
                data!!.getStringExtra("2")?.let { returnArg.put("2", it) }
                mResult!!.success(returnArg);
            } else {
                mResult!!.success(null);
            }

        }
    }

}
