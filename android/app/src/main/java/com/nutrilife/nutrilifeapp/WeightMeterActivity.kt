package com.nutrilife.nutrilifeapp

import android.Manifest
import android.app.Activity
import android.bluetooth.BluetoothDevice
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.*
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import cn.net.aicare.pabulumlibrary.PabulumSDK
import cn.net.aicare.pabulumlibrary.bleprofile.BleProfileService
import cn.net.aicare.pabulumlibrary.bleprofile.BleProfileServiceReadyActivity
import cn.net.aicare.pabulumlibrary.entity.FoodData
import cn.net.aicare.pabulumlibrary.pabulum.PabulumService.PabulumBinder
import cn.net.aicare.pabulumlibrary.utils.PabulumBleConfig
import cn.net.aicare.pabulumlibrary.utils.ParseData


class WeightMeterActivity : BleProfileServiceReadyActivity() {

    private val TAG: String = "WeightMeterActivity"
    private val handler = Handler()
    private var smallLog: TextView? = null
    private var binder: PabulumBinder? = null
    private var weightTextView: TextView? = null
    private var foodNameTextView: TextView? = null
    private var staticSpinner: Spinner? = null
    private var foodName: String? = ""
    private var unitType: String? = ""
    private var weightValue: String? = ""
    private var preWeightValue: String? = "0"
    private var preUnit = PabulumBleConfig.UNIT_G
    private var progressCircular: ProgressBar? = null
    private var deBounser: Handler? = null
    val MY_PERMISSIONS_REQUEST_LOCATION = 99

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_weight_meter)
        PabulumSDK.getInstance().init(this)
        smallLog = findViewById(R.id.smallLog) as TextView
        weightTextView = findViewById(R.id.weight_scale) as TextView
        foodNameTextView = findViewById(R.id.food_name) as TextView
        staticSpinner = findViewById(R.id.static_spinner) as Spinner
        progressCircular = findViewById(R.id.progress_circular) as ProgressBar
        foodName = intent.getStringExtra("foodName")
        unitType = intent.getStringExtra("unitType")
        //showToast(unitType.toString())

        foodNameTextView!!.text = foodName

        // setUp dropdown items
        val staticAdapter: ArrayAdapter<CharSequence> = ArrayAdapter
                .createFromResource(this, R.array.scale_unit_type_array,
                        android.R.layout.simple_spinner_item)
        staticAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        staticSpinner!!.setAdapter(staticAdapter)
        // click listen for the clicked item
        staticSpinner!!.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View?,
                                        position: Int, id: Long) {
                Log.v("item", parent.getItemAtPosition(position) as String)
                if(binder != null) {
                    if(parent.getItemAtPosition(position) as String == "g") {
                        binder!!.setUnit(PabulumBleConfig.UNIT_G);
                    } else if(parent.getItemAtPosition(position) as String == "ml") {
                        binder!!.setUnit(PabulumBleConfig.UNIT_ML);
                    } else if (parent.getItemAtPosition(position) as String == "lb") {
                        binder!!.setUnit(PabulumBleConfig.UNIT_LB);
                    } else if(parent.getItemAtPosition(position) as String == "oz") {
                        binder!!.setUnit(PabulumBleConfig.UNIT_OZ);
                    } else if (parent.getItemAtPosition(position) as String == "kg") {
                        binder!!.setUnit(PabulumBleConfig.UNIT_KG);
                    } else {
                        binder!!.setUnit(PabulumBleConfig.UNIT_G);
                    }
                }
            }
            override fun onNothingSelected(parent: AdapterView<*>?) {
                // TODO Auto-generated method stub
            }
        })
        //checkLocationPermission()
        bluetoothStateOn()
    }

    fun checkLocationPermission(): Boolean {
        return if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.ACCESS_FINE_LOCATION)
                !== PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION),
                        MY_PERMISSIONS_REQUEST_LOCATION)
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION),
                        MY_PERMISSIONS_REQUEST_LOCATION)
            }
            false
        } else {
            true
        }
    }

    fun onClickGoBack(view: View) {
        if(isDeviceConnected) {
            val data = Intent()
            data.putExtra("0", foodName)
            data.putExtra("1", weightValue)
            data.putExtra("2", unitType)
            setResult(Activity.RESULT_OK, data)
            onPause()
        } else {
            showToast("Please wait, Until connection exist")
        }
    }

    fun onClickCancel(view: View) {
        val data = Intent()
        data.putExtra("0", "")
        data.putExtra("1", "")
        data.putExtra("2", "")
        setResult(Activity.RESULT_CANCELED, data)
        onPause()
    }

    override fun onStateChanged(state: Int) {
        super.onStateChanged(state)
        when (state) {
            BleProfileService.STATE_CONNECTED -> {
                Log.e(TAG, "onDeviceConnected")
                progressCircular!!.visibility = View.INVISIBLE

                updateLog("Device Connected Successfully")
                updateLog(java.lang.String.format(resources.getString(R.string.current_device),
                        binder!!.deviceName))

                if(binder != null) {
                    when {
                        unitType == "g" -> {
                            binder!!.setUnit(PabulumBleConfig.UNIT_G);
                        }
                        unitType == "ml" -> {
                            binder!!.setUnit(PabulumBleConfig.UNIT_ML);
                        }
                        unitType == "lb" -> {
                            binder!!.setUnit(PabulumBleConfig.UNIT_LB);
                        }
                        unitType == "oz" -> {
                            binder!!.setUnit(PabulumBleConfig.UNIT_OZ);
                        }
                        unitType == "kg" -> {
                            binder!!.setUnit(PabulumBleConfig.UNIT_KG);
                        }
                        else -> {
                            binder!!.setUnit(PabulumBleConfig.UNIT_G);
                        }
                    }
                }

                /*setState("Device Connected Successfully")
                setState(java.lang.String.format(resources.getString(R.string.current_device),
                            binder!!.deviceName))*/

            }

            BleProfileService.STATE_DISCONNECTED -> {
                progressCircular!!.visibility = View.VISIBLE
                Log.e(TAG, "onDeviceDisconnected")
                updateLog("onDeviceDisconnected")
                /*setState(R.string.disconnected)
                preWeight = "0"
                reset()*/
                Handler().postDelayed(Runnable { startScan() }, 1000)
            }
            BleProfileService.STATE_INDICATION_SUCCESS -> {
                Log.e(TAG, "onIndicationSuccess")
                updateLog("onIndicationSuccess")
            }
        }
    }

    override fun bluetoothStateOn() {
        super.bluetoothStateOn()
        /*setState("BluetoothStates On")
        Log.e(TAG, "bluetoothStateOn")*/
        updateLog("bluetoothState : On")
        handler.postDelayed(Runnable { startScan() }, 500)
    }

    override fun onStartScan() {
        progressCircular!!.visibility = View.VISIBLE
        /*setState("Scanning...")*/
        updateLog("Scanning...")
    }

    // on connection done : we will get connection device info from here "onLeScanCallback"
    override fun onLeScanCallback(device: BluetoothDevice?, rssi: Int) {
        connectDevice(device)
        if(isDeviceConnected) {
            progressCircular!!.visibility = View.INVISIBLE
            /*setState("DeviceConnected")
            updateLog("DeviceConnected : " + binder!!.deviceName)*/
            updateLog("Device : " + device.toString())
        } else {
            progressCircular!!.visibility = View.VISIBLE
                    setState("Not Connected")
            updateLog("Not Connected")
        }
    }

    override fun connectDevice(device: BluetoothDevice?) {
        super.connectDevice(device)
        updateLog("connectDevice")
    }

    override fun getFoodData(foodData: FoodData?) {
        Log.d(TAG, foodData.toString())
        if(foodData != null) {
            //updateLog("FoodData : " + foodData!!.toString())
            //updateLog("Food Weight : " + foodData!!.weight)
            var weightText : String = foodData!!.data
//            if(unitType == "lb" && weightText.contains(":")) {
//                val arrayString: List<String> = weightText.split(":")
//                val pounds: Double = arrayString[0].toDouble()
//                val ounce: Double = arrayString[1].toDouble()
//                val convertToPoundValue: Double = ounce * (1 / 16)
//                weightText = (pounds + convertToPoundValue).toString()
//            }
            weightTextView!!.text = weightText + " " + unitType
            weightValue = weightText
            preUnit = foodData!!.unit
            /*weightTextView!!.text = foodData!!.data + " " + unitType
            weightValue = foodData!!.data
            preUnit = foodData!!.unit*/
            //unitType = getUnitStr(preUnit)

            if(preWeightValue == null || preWeightValue!!.isEmpty()) {
                preWeightValue = weightValue
            }

            if (preWeightValue == weightValue) {
                // nothing to do
            } else {
                preWeightValue = weightValue
                if(deBounser != null) {
                    deBounser!!.removeCallbacksAndMessages(null)
                }
                goBackToHome()
            }
        }

    }

    fun goBackToHome() {
        deBounser = Handler()
        deBounser!!.postDelayed( Runnable() {
             run() {
                 if(isDeviceConnected) {
                     if(weightValue != "0") {
                         val data = Intent()
                         data.putExtra("0", foodName)
                         data.putExtra("1", weightValue)
                         data.putExtra("2", unitType)
                         setResult(Activity.RESULT_OK, data)
                         onPause()
                     } else {
                         deBounser = Handler()
                     }
                 } else {
                     //showToast("Please wait, Until connection exist")
                 }
            }
        }, 4000);
    }

    //////////////////////////////
    /// helper methods
    /////////////////////////////
    private fun setState(`object`: Any) {
        if (`object` is Int) {
         //   showToast(`object`.toString())
        } else if (`object` is String) {
         //   showToast(`object`)
        }
    }

    private  fun showToast(content: String) {
        Toast.makeText(applicationContext, content, Toast.LENGTH_LONG).show()
    }
    
    private fun updateLog(content: String) {
        //smallLog!!.text = smallLog!!.text.toString() + "\n" + content
        smallLog!!.text = content
    }

    private fun getUnitStr(preUnit: Byte): String? {
        var unitStr = getString(R.string.unit_g)
        when (preUnit) {
            PabulumBleConfig.UNIT_G -> unitStr = getString(R.string.unit_g)
            PabulumBleConfig.UNIT_ML -> unitStr = getString(R.string.unit_ml)
            PabulumBleConfig.UNIT_LB -> unitStr = getString(R.string.unit_lb_oz)
            PabulumBleConfig.UNIT_OZ -> unitStr = getString(R.string.unit_oz)
            PabulumBleConfig.UNIT_KG -> unitStr = getString(R.string.unit_kg)
            PabulumBleConfig.UNIT_FG -> unitStr = getString(R.string.unit_fg)
            PabulumBleConfig.UNIT_ML_MILK -> unitStr = getString(R.string.unit_ml_milk)
            PabulumBleConfig.UNIT_ML_WATER -> unitStr = getString(R.string.unit_ml_water)
            PabulumBleConfig.UNIT_FL_OZ_MILK -> unitStr = getString(R.string.unit_oz_milk)
            PabulumBleConfig.UNIT_FL_OZ_WATER -> unitStr = getString(R.string.unit_oz_water)
            PabulumBleConfig.UNIT_LB_LB -> unitStr = getString(R.string.unit_lb)
        }
        return unitStr
    }
    override fun onDestroy() {
        super.onDestroy()
        Log.e(TAG,"onDestroy")
        stopScan();
    }

    override fun onPause() {
        super.onPause()
        if (mIsScanning) {
            stopScan()
        }
        if(binder != null) {
            binder!!.disconnect ()
        }
        onBackPressed()
    }


    /////////////////////////////////////////////////////////////
    //////            Un used methods
    /////////////////////////////////////////////////////////////
    override fun getUnit(p0: Byte) {
        TODO("Not yet implemented")
    }

    private var countRssi = 0

    override fun onReadRssi(p0: Int) {
        Log.e(TAG, "onReadRssi rssi: " + p0)
        setCurrentRssi(p0)
        if (Math.abs(p0) > Math.abs(-70)) {
            countRssi += 1;
        } else {
            countRssi = 0;
        }
        if (countRssi >= 20) {
            if (binder != null) {
                binder!!.disconnect();
            }
        }
    }

    private fun setCurrentRssi(`object`: Any?) {
        if (`object` == null) {
            //tvShowRssi.setText(R.string.no_rssi)
          //  updateLog("Current signal：--")
        } else {
            if (`object` is Int) {
            //    updateLog(String.format(resources.getString(R.string.current_rssi), `object` as Int?))
            }
        }
    }

    override fun getErrCodes(p0: IntArray?) {
        Log.e(TAG, "getErrorCodes : " + p0.toString())
    }

    override fun onError(message: String?, errorCode: Int) {
        Log.d(TAG, "msg = " + message + "; code = " + errorCode)
    }

    override fun getStopAlarm() {
        Log.e(TAG, "getStopAlarm")
    }

    override fun getBleDID(p0: Int) {
        Log.e(TAG, "BLE DID : " + p0)
    }

    override fun getSynTime(cmdType: Byte, timeS: Int) {
        var tyepName = ""
        when (cmdType) {
            PabulumBleConfig.SYN_TIME ->                 //时间同步
                tyepName = "Syn time"
            PabulumBleConfig.SYN_TIME_LESS ->                 //倒计时时间同步
                tyepName = "Less syn time"
            PabulumBleConfig.TIMING_PAUSE -> {
                tyepName = "Timing Pause"
                Toast.makeText(this, "Timing Pause", Toast.LENGTH_SHORT).show()
            }
            PabulumBleConfig.TIMING_PAUSE_LESS -> {
                tyepName = "Less Time Pause"
                Toast.makeText(this, "Less Time Pause", Toast.LENGTH_SHORT).show()
            }
        }
        Log.e(TAG, "Type : "  + timeS + "||" + tyepName)
    }

    override fun getCountdownStart(time: Int) {
        Toast.makeText(this, "CountdownStart:" + time, Toast.LENGTH_SHORT).show();
    }

    override fun getPenetrateData(data: ByteArray?) {
       Log.i(TAG, "PenetrateData:" + ParseData.arr2Str(data))
    }

    override fun getTimeStatus(status: Int) {
        Toast.makeText(this, "TimeStatus:" + status, Toast.LENGTH_SHORT).show();
    }

    override fun onWriteSuccess(p0: ByteArray?) {
        Log.e(TAG, "onWriteSuccess " + ParseData.arr2Str(p0))
    }

    override fun getBleVersion(p0: String?) {
        Log.e(TAG, "getBleVersion " + p0)
    }

    override fun getUnits(p0: IntArray?) {
        TODO("Not yet implemented")
    }

    override fun onServiceBinded(p0: BleProfileService.LocalBinder?) {
        this.binder = p0 as PabulumBinder?
        this.binder!!.deviceAddress
        this.binder!!.deviceName
        Log.d(TAG, "onServiceBinded : $binder")
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            MY_PERMISSIONS_REQUEST_LOCATION -> {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.size > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                                    Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {
                    }
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return
            }
        }
    }
}