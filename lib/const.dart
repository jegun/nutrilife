import 'package:flutter/material.dart';
import 'package:nutrilifeapp/screens/login_screen.dart';
const String kUrl = 'http://applications.nutrilifeco.com/public/api/';

  void wrongAlert(BuildContext context, String message) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: Row(children: <Widget>[
            Icon(Icons.error, color: Colors.red),
            SizedBox(width:5),
            Text("Error", style: TextStyle(color: Colors.red)),
          ]),
          content: new Text(message),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            FlatButton(
              child: new Text("Close", style: TextStyle(color: kGreenColor)),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
}

void wrongAlertWithLogin(BuildContext context, String message) {
  // flutter defined function
  showDialog(
    context: context,
    builder: (BuildContext context) {
      // return object of type Dialog
      return AlertDialog(
        title: Row(children: <Widget>[
          Icon(Icons.error, color: Colors.red),
          SizedBox(width:5),
          Text("Error", style: TextStyle(color: Colors.red)),
        ]),
        content: new Text(message),
        actions: <Widget>[
          // usually buttons at the bottom of the dialog
          FlatButton(
            child: new Text("Login", style: TextStyle(color: kGreenColor)),
            onPressed: () {
              Navigator.pushReplacement(context, FadeRoute(
                page:LoginScreen(),
              ));
              },
          ),
          FlatButton(
            child: new Text("Close", style: TextStyle(color: kGreenColor)),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}

  void holdToEdit(BuildContext context) {
    Scaffold.of(context).showSnackBar(
        SnackBar(
         // shape: RoundedRectangleBorder(side: BorderSide() ,borderRadius: BorderRadius.circular(14),),
          content: Container(
              height: 30,
              child: Center(child: Text('Hold to edit', style: TextStyle(fontSize: 15, fontWeight: FontWeight.w600)))),
          backgroundColor: kGreenColor,
          duration: Duration(seconds: 1),
        ),
    );
  }


const Color kGreenColor = Color(0xFF76BB59);
const Color kGreyColor = Color(0xFFF5F2F2);
const List<Map> kFactList = [];
const   Map<String, dynamic> kInitialData = {
  "name": "Select your food",
  "id": 0,
  "Facts": kFactList,
};

const kInputTextStyle = InputDecoration(
//  hintText: 'Enter value.',
  hintStyle: TextStyle(color: Colors.grey),
  //contentPadding:
 // EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
  enabledBorder: UnderlineInputBorder(
  borderSide: BorderSide(color: kGreenColor),
  ),
  focusedBorder: UnderlineInputBorder(
  borderSide: BorderSide(color: kGreenColor),
  ),
  border: UnderlineInputBorder(
  borderSide: BorderSide(color: kGreenColor),
  ),
);

const   Map<int, String> kNutritionsNames = {
  1: "Calories",
  2: "Protein",
  3: "Fat",
  31: "Saturated fats",
  32: "Trans fats",
  4: "Carbohydrates",
  41: "Sugars",
  5: "Fibre",
  6: "Vitamin A",
  7: "Carotene",
  8: "Vitamin B6",
  9: "Vitamin B12",
  10: "Vitamin B3",
  11: "Vitamin C",
  12: "Vitamin E",
  13: "Cholesterol",
  14: "Potassium",
  15: "Sodium",
  16: "Calcium",
  17: "Magnesium",
  18: "Iron",
  19: "Manganese",
  20: "Zinc",
  21: "Copper",
  22: "Phosphorus",
  23: "Selenium",
  24: "Water",
};

const   Map<int, String> kNutritionsUnits = {
  1: "(kcal)",
  2: "(g)",
  3: "(g)",
  31: "(g)",
  32: "(g)",
  4: "(g)",
  41: "(g)",
  5: "(g)",
  6: "(IU)",
  7: "(µg)",
  8: "(mg)",
  9: "(µg)",
  10: "(mg)",
  11: "(mg)",
  12: "(IU)",
  13: "(mg)",
  14: "(mg)",
  15: "(mg)",
  16: "(mg)",
  17: "(mg)",
  18: "(mg)",
  19: "(mg)",
  20: "(mg)",
  21: "(mg)",
  22: "(mg)",
  23: "(µg)",
  24: "(ml)",
};

const Map<String, String> kFacts = {
  "Calories": "(kcal)",
  "Protein": "(g)",
  "Fat": "(g)",
  "Saturated fats": "(g)",
  "Trans fats": "(g)",
  "Carbohydrates": "(g)",
  "Sugars": "(g)",
  "Fibre": "(g)",
  "Vitamin A": "(IU)",
  "Carotene": "(µg)",
  "Vitamin B6": "(mg)",
  "Vitamin B12": "(µg)",
  "Vitamin B3": "(mg)",
  "Vitamin C": "(mg)",
  "Vitamin E": "(IU)",
  "Cholesterol": "(mg)",
  "Potassium": "(mg)",
  "Sodium":"(mg)",
  "Calcium":"(mg)",
  "Magnesium":"(mg)",
  "Iron":"(mg)",
  "Manganese":"(mg)",
  "Zinc":"(mg)",
  "Copper":"(mg)",
  "Phosphorus":"(mg)",
  "Selenium":"(µg)",
};

class FadeRoute extends PageRouteBuilder {
  final Widget page;
  FadeRoute({this.page})
      : super(
    pageBuilder: (
        BuildContext context,
        Animation<double> animation,
        Animation<double> secondaryAnimation,
        ) =>
    page,
    transitionsBuilder: (
        BuildContext context,
        Animation<double> animation,
        Animation<double> secondaryAnimation,
        Widget child,
        ) =>
        FadeTransition(
          opacity: animation,
          child: child,
        ),
  );
}