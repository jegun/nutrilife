import 'package:flutter/foundation.dart';

class Food {
  String _foodName;
  double _calories;
  int _id;
  double _fat;
  double _protein;

  Food(this._id, this._foodName, this._calories, this._protein,this._fat);

  Food.map(dynamic obj){
    this._foodName = obj['foodname'];
    this._id = obj['id'];
    this._calories = obj['calories'];
    this._protein = obj['protein'];
    this._fat = obj['fat'];
  }

  String get foodname => _foodName;
  int get id => _id;
  double get calories => _calories;
  double get protein => _protein;
  double get fat => _fat;

  Map<String, dynamic> toMap(){
    var map = Map<String, dynamic>();
    map["foodname"] = _foodName;
    map["calories"] = _calories;
    map["protein"] = _protein;
    map["fat"] = _fat;
    if(["id"] != null){
      map["id"] = _id;
    }
    return map;
  }

  Food.fromMap(Map<String, dynamic> map){
    this._foodName = map["foodname"];
    this._calories = map["calories"];
    this._protein = map["protein"];
    this._fat = map["fat"];
    this._id = map["id"];
  }

}