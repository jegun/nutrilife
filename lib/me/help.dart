import 'package:flutter/material.dart';
import 'package:nutrilifeapp/const.dart';
import 'package:nutrilifeapp/me/update_basic_info.dart';
import 'package:nutrilifeapp/me/units_setting.dart';
import 'package:url_launcher/url_launcher.dart';


class HelpScreen extends StatefulWidget {
  @override
  _HelpScreenState createState() => _HelpScreenState();
}

class _HelpScreenState extends State<HelpScreen> {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => true,
      child: SafeArea(
        child: Scaffold(
          appBar: AppBar(
            leading: GestureDetector(
              onTap: (){Navigator.pop(context);},
              child: Icon(Icons.arrow_back, size: 30),),
            backgroundColor: kGreenColor,
            title: Text('Help'),
          ),
          body: Padding(
              padding: EdgeInsets.all(10),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  //todo: insert 3 widgets (weight input, actions and nutrition facts)
                  Container(
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(
                          color: Colors.grey[300],
                        ),
                      ),
                      margin: EdgeInsets.all(5),
                      child: Column(
                        children: <Widget>[
                          GestureDetector(
                            onTap: () => launch('https://nutrilifeco.com/a/faq'),
                            child: ListTile(
                              title: Text('FAQs'),
                              leading: Icon(Icons.help, color: kGreenColor),
                              trailing: Container(
                                child: Icon(Icons.keyboard_arrow_right),
                              ),
                            ),
                          ),
                          GestureDetector(
                            onTap: () => launch('https://nutrilifeco.com/pages/contact-us'),
                            child: ListTile(
                              title: Text('Contact Us'),
                              leading: Icon(Icons.email, color: kGreenColor),
                              trailing: Container(
                                child: Icon(Icons.keyboard_arrow_right),
                              ),
                            ),
                          ),
                        ],
                      )
                  ),
                ],
              )
          ),
        ),
      ),
    );
  }
}
