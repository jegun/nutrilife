import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:nutrilifeapp/const.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter/services.dart';
import 'package:configurable_expansion_tile/configurable_expansion_tile.dart';
import 'package:nutrilifeapp/services/shared_preferences.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:http/http.dart' as http;
import 'package:nutrilifeapp/screens/me_loading.dart';

NutrilifeSharedPreferences pref = NutrilifeSharedPreferences();

List<String> gender = ['Male', 'Female', 'Other'];
List<String> weight = ['0.5 lbs/week', '1 lb/week (recommended)', '1.5 lbs/week','2 lbs/week'];
double userHeight;
double userWeight;
Map<String, double> dailyGoalAll;

class UpdateBasicInfo extends StatefulWidget {
  String userGender;
  String name;
  int height;
  int weight;
  String birthDateShown;
  int levelChoice;
  int goalChoice;
  String gainWeight;
  String loseWeight;
  int userId;
  String token;
  String heightUnit;
  String weightUnit;
  String email;
  UpdateBasicInfo(this.name, this.userId, this.token, this.userGender, this.height, this.weight, this.birthDateShown, this.levelChoice,
      this.goalChoice, this.gainWeight, this.loseWeight, this.heightUnit, this.weightUnit, this.email);
  @override
  _UpdateBasicInfoState createState() => _UpdateBasicInfoState();
}

class _UpdateBasicInfoState extends State<UpdateBasicInfo> {

  DateTime birthDate;

  double dailyGoal(){
    double value;
    double w = (widget.weightUnit=='kg') ? userWeight : (userWeight * 0.45359237);
    double h = (widget.heightUnit == 'cm') ? userHeight : (userHeight * 2.54);
    String birthYear = birthDate.year.toString();
    String nowYear = DateTime.now().year.toString();
    double age = double.parse(nowYear) - double.parse(birthYear);
    double bmr;
    double af;

    if (widget.userGender == 'Male'){
      bmr = 66 + (13.7 * w) + (5 * h) - (6.8 * age);
    } else {
      bmr = 655 + (9.6 * w) + (1.8 * h) - (4.7 * age);
    }

    if(widget.levelChoice == 1){
      af = 1.2;
    } else if(widget.levelChoice == 2){
      af = 1.375;
    } else if(widget.levelChoice == 3){
      af = 1.55;
    } else if(widget.levelChoice == 4){
      af = 1.725;
    }

    if (widget.goalChoice == 1){
      if(widget.loseWeight == weight[0]){
        value = ((bmr * af) - 250);
      }
      else if(widget.loseWeight == weight[1]){
        value = ((bmr * af) - 500);
      }
      else if(widget.loseWeight == weight[2]){
        value = ((bmr * af) - 750);
      }
      else if(widget.loseWeight == weight[3]){
        value = ((bmr * af) - 1000);
      }
    } else if(widget.goalChoice == 2){
      value = (bmr*af);
    } else if(widget.goalChoice == 3){
      if(widget.gainWeight == weight[0]){
        value = ((bmr * af) + 250);
      }
      else if(widget.gainWeight == weight[1]){
        value = ((bmr * af) + 500);
      }
      else if(widget.gainWeight == weight[2]){
        value = ((bmr * af) + 750);
      }
      else if(widget.gainWeight == weight[3]){
        value = ((bmr * af) + 1000);
      }
    }

    dailyGoalAll = {
      'calories' : value,
      'protein' : (0.8 * w),
      'fat' : (0.3 * value / 9),
      'saturated fat' : ((0.1 * value)/9),
      'trans fat' : ((0.01 * value)/9),
      'carbohydrates' : (0.55 * value / 4),
      'sugars' : (widget.userGender == "Male") ? 37.5 : 25,
      'fiber' : (widget.userGender == "Male") ? 38 : 25,
      'vitamin a' : (widget.userGender == "Male") ? 3000 : 2333,
      'carotene' : 6000,
      'vitamin b6' : 1.3,
      'vitamin b12' : 2.4,
      'vitamin b3': (widget.userGender == "Male") ? 16 : 14,
      'vitamin c' : (widget.userGender == "Male") ? 90 : 75,
      'vitamin e' : 22,
      'cholestrol' : 300,
      'potassium' : 4700,
      'sodium' : 1500,
      'calcium' : 1000,
      'magnesium' : (widget.userGender == "Male") ? 420 : 320,
      'iron' : (widget.userGender == "Male") ? 8 : 18,
      'manganese' : (widget.userGender == "Male") ? 2.3 : 1.8,
      'zinc' : (widget.userGender == "Male") ? 11 : 8,
      'copper' : 0.9,
      'phosphorus' : 700,
      'selenium' : 55,
      'water' : (widget.userGender == "Male") ? 3.7 : 2.7,
    };

    return value;
  }


  bool isExpanded = false;
  bool isExpanded2 = false;
  TextEditingController _nameController = new TextEditingController();
  TextEditingController _heightController = new TextEditingController();
  TextEditingController _weightController = new TextEditingController();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print(widget.height);
    print(widget.weight);
    _heightController.text = widget.height.toString();
    _weightController.text = widget.weight.toString();
    _nameController.text = widget.name;
    userHeight = widget.height.toDouble();
    userWeight = widget.weight.toDouble();
    String dateFormat = '${widget.birthDateShown} 00:00:00';
    birthDate = DateTime.parse(dateFormat);
  }

  @override
  Widget build(BuildContext context) {
    ProgressDialog pr = ProgressDialog(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: kGreenColor,
        leading: GestureDetector(child: Icon(Icons.arrow_back), onTap:(){
          Navigator.pushReplacement(context, FadeRoute(
            page:MeLoading(),
          ));        }),
        title: Text('Profile Basic Info'),
      ),
      body: Container(
          child: Column(
            children: <Widget>[
              SizedBox(height: 15),
            Expanded(
             child: ListView(
              children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(13.0),
                child: Row(
                  children: <Widget>[
                    Container(
                      width: 110,
                      child: Text('Your Name:'),
                    )              ,
                    Expanded(
                      child: TextField(
                        onChanged:(value){
                          setState(() {
                            widget.name = value;
                          });
                        },
                        controller: _nameController,
                        maxLength: 10,
                        decoration: kInputTextStyle.copyWith(hintText: 'Enter your name'),
                      ),
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(13),
                child: Row(
                  children: <Widget>[
                    Container(
                      width: 110,
                      child: Text('Birth date:'),
                    )              ,
                    Expanded(
                      child: GestureDetector(
                        child: Row(
                          children: <Widget>[
                            Text(
                              '${widget.birthDateShown}',
                              style: TextStyle(
                                color: kGreenColor,
                                fontSize: 19,
                              ),
                            ),
                            Icon(Icons.keyboard_arrow_down, color: kGreenColor, size: 30),
                          ],
                        ),
                        onTap: (){
                          DatePicker.showDatePicker(context,
                              showTitleActions: true,
                              minTime: DateTime(1900, 1, 1),
                              maxTime: DateTime(2020, 12, 31), onChanged: (date) {
                                //todo: add date variable to have the value
                              }, onConfirm: (date) {
                                setState(() {
                                  birthDate = date;
                                  widget.birthDateShown = '${birthDate.year}-${birthDate.month}-${birthDate.day}';
                                });
                                //todo: return date value
                              }, currentTime: birthDate, locale: LocaleType.en,
                              theme: DatePickerTheme(
                                doneStyle: TextStyle(color: kGreenColor),
                                itemStyle: TextStyle(color: Colors.black54),
                              ));
                        },
                      ),
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(13.0),
                child: Row(
                  children: <Widget>[
                    Container(
                      width: 110,
                      child: Text('Height:'),
                    )              ,
                    Expanded(
                      child: TextField(
                        onChanged:(value){
                          setState(() {
                            userHeight = double.parse(value);
                          });
                        },
                        controller: _heightController,
                        keyboardType: TextInputType.number,
                        inputFormatters: [WhitelistingTextInputFormatter.digitsOnly],
                        maxLength: 5,
                        decoration: kInputTextStyle.copyWith(hintText: 'Enter your height', suffixText: '${widget.heightUnit}'),
                      ),
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(13.0),
                child: Row(
                  children: <Widget>[
                    Container(
                      width: 110,
                      child: Text('Gender:'),
                    )              ,
                    Expanded(
                      child: DropdownButton<String>(
                        value: widget.userGender,
                        icon: Icon(Icons.keyboard_arrow_down, color: kGreenColor),
                        iconSize: 30,
                        elevation: 16,
                        style: TextStyle(
                          color: kGreenColor,
                          fontSize: 19,
                        ),
                        underline: Container(),
                        onChanged: (String newValue) {
                        setState(() {
                          widget.userGender = newValue;
                        });
                        },
                        items: gender.map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(value),
                          );
                        })
                            .toList(),
                      ),
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(13.0),
                child: Row(
                  children: <Widget>[
                    Container(
                      width: 110,
                      child: Text('Weight:'),
                    )              ,
                    Expanded(
                      child: TextField(
                        onChanged:(value){
                          setState(() {
                            userWeight = double.parse(value);
                          });
                        },
                        controller: _weightController,
                        maxLength: 5,
                        keyboardType: TextInputType.number,
                        inputFormatters: <TextInputFormatter>[WhitelistingTextInputFormatter.digitsOnly],// Only numbers can be entered
                        decoration: kInputTextStyle.copyWith(hintText: 'Enter your weight', suffixText: '${widget.weightUnit}'),
                      ),
                    )
                  ],
                ),
              ),
              Divider(),
              SizedBox(height:5),
              Container(
                margin: EdgeInsets.all(10),
                decoration: BoxDecoration(
                  border: Border.all(
                    color: Colors.grey[300],
                  ),
                  // color: kGreyColor,
                  //borderRadius: BorderRadius.circular(10),
                ),
                child: ConfigurableExpansionTile(
                  header: SizedBox(
                    child: Center(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          Text(
                            'Weekly Goal',
                            style: TextStyle(color:kGreenColor, fontSize: 15),
                          ),
                          Icon(Icons.keyboard_arrow_down, color:kGreenColor),
                        ],
                      ),
                    ),
                    height: 35,
                  ),
                  headerExpanded: SizedBox(
                    child: Center(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          Text(
                            'Weekly Goal',
                            style: TextStyle(color: kGreenColor, fontSize: 15),
                          ),
                          Icon(Icons.keyboard_arrow_up, color:kGreenColor),
                        ],
                      ),
                    ),
                    height: 35,
                  ),
                  onExpansionChanged: (bool isExpand){
                    setState(() {
                      isExpanded = isExpand;
                    });
                  },
                  children: <Widget>[
                    SizedBox(
                      height: 250,
                      child: Scrollbar(
                        child: ListView(
                          children: <Widget>[
                            SizedBox(height:10),
                            Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Text(
                                  'What is your goal?',
                                  style: TextStyle(
                                    fontSize: 19,
                                  )
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(5.0),
                              child: Row(
                                children: <Widget>[
                                  Radio(
                                    activeColor: kGreenColor,
                                    focusColor: kGreenColor,
                                    value: 1,
                                    groupValue: widget.goalChoice,
                                    onChanged: (T){
                                      setState(() {
                                        widget.goalChoice = T;
                                      });
                                    },
                                  ),
                                  SizedBox(width:5),
                                  Container(
                                    width: 80,
                                    child: Text('Lose weight'),
                                  )              ,
                                  Expanded(
                                    child: DropdownButton<String>(
                                      value: widget.loseWeight,
                                      icon: Icon(Icons.keyboard_arrow_down, color: kGreenColor),
                                      iconSize: 24,
                                      elevation: 16,
                                      style: TextStyle(
                                        color: kGreenColor,
                                      ),
                                      underline: Container(),
                                      onChanged: (String newValue) {
                                        setState(() {
                                          widget.loseWeight = newValue;
                                        });
                                      },
                                      items: weight.map<DropdownMenuItem<String>>((String value) {
                                        return DropdownMenuItem<String>(
                                          value: value,
                                          child: Text(value),
                                        );
                                      })
                                          .toList(),
                                    ),
                                  )
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(5.0),
                              child: Row(
                                children: <Widget>[
                                  Radio(
                                    focusColor: kGreenColor,
                                    activeColor: kGreenColor,
                                    value: 2,
                                    groupValue: widget.goalChoice,
                                    onChanged: (T){
                                      setState(() {
                                        widget.goalChoice = T;
                                      });
                                    },
                                  ),
                                  SizedBox(width:5),
                                  Container(
                                    width: 120,
                                    child: Text('Maintain weight'),
                                  )              ,
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(5.0),
                              child: Row(
                                children: <Widget>[
                                  Radio(
                                    activeColor: kGreenColor,
                                    focusColor: kGreenColor,
                                    value: 3,
                                    groupValue: widget.goalChoice,
                                    onChanged: (T){
                                      setState(() {
                                        widget.goalChoice = T;
                                      });
                                    },
                                  ),
                                  SizedBox(width:5),
                                  Container(
                                    width: 80,
                                    child: Text('Gain weight'),
                                  )              ,
                                  Expanded(
                                    child: DropdownButton<String>(
                                      value: widget.gainWeight,
                                      icon: Icon(Icons.keyboard_arrow_down, color: kGreenColor),
                                      iconSize: 24,
                                      elevation: 16,
                                      style: TextStyle(
                                        color: kGreenColor,
                                      ),
                                      underline: Container(),
                                      onChanged: (String newValue) {
                                        setState(() {
                                          widget.gainWeight = newValue;
                                        });
                                      },
                                      items: weight.map<DropdownMenuItem<String>>((String value) {
                                        return DropdownMenuItem<String>(
                                          value: value,
                                          child: Text(value),
                                        );
                                      })
                                          .toList(),
                                    ),
                                  )
                                ],
                              ),
                            ),
                            SizedBox(height:10),
                          ],
                        ),
                      ),
                    ),
                  ],
                  initiallyExpanded: isExpanded,
                ),
              ),
              SizedBox(height:5),
              Container(
                margin: EdgeInsets.all(10),
                decoration: BoxDecoration(
                  border: Border.all(
                    color: Colors.grey[300],
                  ),
                  // color: kGreyColor,
                  //borderRadius: BorderRadius.circular(10),
                ),
                child: ConfigurableExpansionTile(
                  header: SizedBox(
                    child: Center(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          Text(
                            'Activity Level',
                            style: TextStyle(color:kGreenColor, fontSize: 15),
                          ),
                          Icon(Icons.keyboard_arrow_down, color:kGreenColor),
                        ],
                      ),
                    ),
                    height: 35,
                  ),
                  headerExpanded: SizedBox(
                    child: Center(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          Text(
                            'Activity Level',
                            style: TextStyle(color: kGreenColor, fontSize: 15),
                          ),
                          Icon(Icons.keyboard_arrow_up, color:kGreenColor),
                        ],
                      ),
                    ),
                    height: 35,
                  ),
                  onExpansionChanged: (bool isExpand){
                    setState(() {
                      isExpanded2 = isExpand;
                    });
                  },
                  children: <Widget>[
                    SizedBox(
                      height: 340,
                      child: Scrollbar(
                        child: ListView(
                          children: <Widget>[
                            SizedBox(height:10),
                            Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Text(
                                  'How active are you?',
                                  style: TextStyle(
                                    fontSize: 19,
                                  )
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: <Widget>[
                                  Radio(
                                    activeColor: kGreenColor,
                                    focusColor: kGreenColor,
                                    value: 1,
                                    groupValue: widget.levelChoice,
                                    onChanged: (T){
                                      setState(() {
                                        widget.levelChoice = T;
                                      });
                                    },
                                  ),
                                  SizedBox(width:5),
                                  Container(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text('Little/No activity', style: TextStyle(fontWeight: FontWeight.w700)),
                                        Text(
                                          'Spend most of the day sitting (e.g., full time ',
                                          style: TextStyle(color: Colors.grey, fontSize: 13),
                                        ),

                                        Text(
                                          'student, desk job)',
                                          style: TextStyle(color: Colors.grey, fontSize: 13),
                                        )
                                      ],
                                    ),
                                  )              ,
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: <Widget>[
                                  Radio(
                                    activeColor: kGreenColor,
                                    focusColor: kGreenColor,
                                    value: 2,
                                    groupValue: widget.levelChoice,
                                    onChanged: (T){
                                      setState(() {
                                        widget.levelChoice = T;
                                      });
                                    },
                                  ),
                                  SizedBox(width:5),
                                  Container(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text('Lightly active', style: TextStyle(fontWeight: FontWeight.w700)),
                                        Text(
                                          'Spend most of the day on your feet (e.g.,',
                                          style: TextStyle(color: Colors.grey, fontSize: 13),
                                        ),

                                        Text(
                                          'teacher, salesperson)',
                                          style: TextStyle(color: Colors.grey, fontSize: 13),
                                        )
                                      ],
                                    ),
                                  )              ,
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: <Widget>[
                                  Radio(
                                    activeColor: kGreenColor,
                                    focusColor: kGreenColor,
                                    value: 3,
                                    groupValue: widget.levelChoice,
                                    onChanged: (T){
                                      setState(() {
                                        widget.levelChoice = T;
                                      });
                                    },
                                  ),
                                  SizedBox(width:5),
                                  Container(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text('Active', style: TextStyle(fontWeight: FontWeight.w700)),
                                        Text(
                                          'Spend most of the day doing some physical',
                                          style: TextStyle(color: Colors.grey, fontSize: 13),
                                        ),

                                        Text(
                                          'activity (e.g., daily workouts, food server)',
                                          style: TextStyle(color: Colors.grey, fontSize: 13),
                                        )
                                      ],
                                    ),
                                  )              ,
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: <Widget>[
                                  Radio(
                                    activeColor: kGreenColor,
                                    focusColor: kGreenColor,
                                    value: 4,
                                    groupValue: widget.levelChoice,
                                    onChanged: (T){
                                      setState(() {
                                        widget.levelChoice = T;
                                      });
                                    },
                                  ),
                                  SizedBox(width:5),
                                  Container(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text('Very active', style: TextStyle(fontWeight: FontWeight.w700)),
                                        Text(
                                          'Spend most of the day doing heavy physical',
                                          style: TextStyle(color: Colors.grey, fontSize: 13),
                                        ),

                                        Text(
                                          'activity (e.g., construction, athlete)',
                                          style: TextStyle(color: Colors.grey, fontSize: 13),
                                        )
                                      ],
                                    ),
                                  )              ,
                                ],
                              ),
                            ),
                            SizedBox(height:10),
                          ],
                        ),
                      ),
                    ),
                  ],
                  initiallyExpanded: isExpanded2,
                ),
              ),
            ],
          ),
        ),
              SizedBox(height: 10),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: FlatButton(
                        color: kGreenColor,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8.0),
                          side: BorderSide(color: kGreenColor),
                        ),
                        child: Container(
                          height:50,
                          child: Center(
                            child: Text('Update Basic Info',
                                style: TextStyle(
                                  color: Colors.white,
                                )),
                          ),
                        ),
                        onPressed: () async{

                          try{
                            pr.update(
                              message: "Updating",
                              progressWidget: Container(
                                  padding: EdgeInsets.all(8.0), child: CircularProgressIndicator(
                                valueColor: AlwaysStoppedAnimation<Color>(kGreenColor),
                              )),
                            );
                            var x = dailyGoal();
                            print(x);
                            pr.show();
                            print(userHeight);
                            print(userWeight);
                            double h = widget.height.toDouble();
                            double w = widget.weight.toDouble();
                            var response = await http.put(
                                '${kUrl}edit/user/${widget.userId}?name=${widget.name}&email=${widget.email}&activityLevel=${widget.levelChoice}&gender=${widget.userGender}&birthDate=$birthDate&height=$userHeight&currentWeight=$userWeight&goalWeight=${widget.goalChoice}&weeklyGoal=${widget.goalChoice}&dailyGoal=${jsonEncode(dailyGoalAll)}&gainWeight=${widget.gainWeight}&loseWeight=${widget.loseWeight}',
                                headers: {
                                  "Accept": "application/json",
                                  "Authorization" : "Bearer ${widget.token}",
                                },
                                );
                            print('Response status: ${response.statusCode}');
                            print('Response body: ${response.body}');
                            await pref.setWeeklyGoal(widget.goalChoice);
                            await pref.setUserActivityLevel(widget.levelChoice);
                            if(birthDate.month <10) {
                              if(birthDate.day <10) {
                                await pref.setUserBirthDate('${birthDate.year}-0${birthDate.month}-0${birthDate.day}');
                              } else {
                                await pref.setUserBirthDate('${birthDate.year}-0${birthDate.month}-${birthDate.day}');
                              }
                            } else {
                              if(birthDate.day <10) {
                                await pref.setUserBirthDate('${birthDate.year}-${birthDate.month}-0${birthDate.day}');
                              } else {
                                await pref.setUserBirthDate('${birthDate.year}-${birthDate.month}-${birthDate.day}');
                              }
                            }

                            await pref.setUserDailyCalories(dailyGoal());
                            await pref.setUserGender(widget.userGender);
                            await pref.setUserHeight(userHeight.round());
                            await pref.setUserWeight(userWeight.round());
                            await pref.setUserName(widget.name);
                            await pref.setUserGainWeight(widget.gainWeight);
                            await pref.setUserLoseWeight(widget.loseWeight);
                            pr.hide();
                            showDialog(
                              context: context,
                              builder: (BuildContext context) {
                                // return object of type Dialog
                                return AlertDialog(
                                  title: Row(children: <Widget>[
                                    Icon(Icons.check_circle, color: kGreenColor),
                                    SizedBox(width:5),
                                    Text("Nutrilife", style: TextStyle(color: kGreenColor)),
                                  ]),
                                  content: new Text('Personal data has been updated successfully, your new daily calorie goal is: ${dailyGoal().round()}'),
                                  actions: <Widget>[
                                    // usually buttons at the bottom of the dialog
                                    FlatButton(
                                      child: new Text("Ok", style: TextStyle(color: kGreenColor)),
                                      onPressed: () {
                                        Navigator.of(context).pop();
                                      },
                                    ),
                                  ],
                                );
                              },
                            );
                          } catch (e){
                            print(e);
                          }
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ],
          )
      ),
    );
  }
}


