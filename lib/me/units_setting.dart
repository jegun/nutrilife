import 'package:flutter/material.dart';
import 'package:nutrilifeapp/const.dart';
import 'package:http/http.dart' as http;
import 'package:nutrilifeapp/services/shared_preferences.dart';
import 'package:progress_dialog/progress_dialog.dart';

NutrilifeSharedPreferences pref = NutrilifeSharedPreferences();

class UnitSetting extends StatefulWidget {
  int weightGroup;
  int heightGroup;
  int measurementGroup;
  final int userId;
  final String token;
  final String email;
  final String name;
  final int height;
  final int weight;
  final String heightUnit;
  final String weightUnit;
  UnitSetting(
      this.weightGroup,
      this.heightGroup,
      this.measurementGroup,
      this.userId,
      this.token,
      this.name,
      this.email,
      this.height,
      this.weight,
      this.heightUnit,
      this.weightUnit);
  @override
  _UnitSettingState createState() => _UnitSettingState();
}

class _UnitSettingState extends State<UnitSetting> {
  getWeightUnit() {
    String value;
    if (widget.weightGroup == 1) {
      value = 'kg';
    } else {
      value = 'lbs';
    }
    return value;
  }

  getHeightUnit() {
    String value;
    if (widget.heightGroup == 1) {
      value = 'cm';
    } else {
      value = 'inch';
    }
    return value;
  }

  getMeasureUnit() {
    String value;
    if (widget.measurementGroup == 1) {
      value = 'g';
    } else if (widget.measurementGroup == 2) {
      value = 'ml';
    } else if (widget.measurementGroup == 3) {
      value = 'lb';
    } else if (widget.measurementGroup == 4) {
      value = 'oz';
    } else if (widget.measurementGroup == 5) {
      value = 'kg';
    }
    return value;
  }

  @override
  Widget build(BuildContext context) {
    final ProgressDialog pr = ProgressDialog(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: kGreenColor,
        leading: GestureDetector(
            child: Icon(Icons.arrow_back),
            onTap: () {
              Navigator.pop(context);
            }),
        title: Text('Unit Setting'),
      ),
      body: Container(
          child: Column(
        children: <Widget>[
          SizedBox(height: 15),
          Expanded(
              child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: ListView(
              children: <Widget>[
                Text('Weight unit',
                    style: TextStyle(color: kGreenColor, fontSize: 16)),
                SizedBox(height: 5),
                ListTile(
                    title: Text('kg'),
                    leading: Radio(
                      value: 1,
                      groupValue: widget.weightGroup,
                      focusColor: kGreenColor,
                      activeColor: kGreenColor,
                      onChanged: (T) {
                        setState(() {
                          widget.weightGroup = T;
                        });
                      },
                    )),
                ListTile(
                    title: Text('lb'),
                    leading: Radio(
                      value: 2,
                      groupValue: widget.weightGroup,
                      focusColor: kGreenColor,
                      activeColor: kGreenColor,
                      onChanged: (T) {
                        setState(() {
                          widget.weightGroup = T;
                        });
                      },
                    )),
                Divider(),
                SizedBox(height: 15),
                Text('Height unit',
                    style: TextStyle(color: kGreenColor, fontSize: 16)),
                SizedBox(height: 5),
                ListTile(
                    title: Text('cm'),
                    leading: Radio(
                      value: 1,
                      groupValue: widget.heightGroup,
                      focusColor: kGreenColor,
                      activeColor: kGreenColor,
                      onChanged: (T) {
                        setState(() {
                          widget.heightGroup = T;
                        });
                      },
                    )),
                ListTile(
                    title: Text('inch'),
                    leading: Radio(
                      value: 2,
                      groupValue: widget.heightGroup,
                      focusColor: kGreenColor,
                      activeColor: kGreenColor,
                      onChanged: (T) {
                        setState(() {
                          widget.heightGroup = T;
                        });
                      },
                    )),
                Divider(),
                SizedBox(height: 15),
                Text('Unit of measurement',
                    style: TextStyle(color: kGreenColor, fontSize: 16)),
                SizedBox(height: 5),
                ListTile(
                    title: Text('g'),
                    leading: Radio(
                      value: 1,
                      groupValue: widget.measurementGroup,
                      focusColor: kGreenColor,
                      activeColor: kGreenColor,
                      onChanged: (T) {
                        setState(() {
                          widget.measurementGroup = T;
                        });
                      },
                    )),
                /*ListTile(
                          title: Text('ml'),
                          leading: Radio(
                            value: 2,
                            groupValue: widget.measurementGroup,
                            focusColor: kGreenColor,
                            activeColor: kGreenColor,
                            onChanged: (T){
                              setState(() {
                                widget.measurementGroup = T;
                              });
                            },
                          )
                      ),*/
                ListTile(
                    title: Text('lb:oz'),
                    leading: Radio(
                      value: 3,
                      groupValue: widget.measurementGroup,
                      focusColor: kGreenColor,
                      activeColor: kGreenColor,
                      onChanged: (T) {
                        setState(() {
                          widget.measurementGroup = T;
                        });
                      },
                    )),
                ListTile(
                    title: Text('oz'),
                    leading: Radio(
                      value: 4,
                      groupValue: widget.measurementGroup,
                      focusColor: kGreenColor,
                      activeColor: kGreenColor,
                      onChanged: (T) {
                        setState(() {
                          widget.measurementGroup = T;
                        });
                      },
                    )),
                ListTile(
                    title: Text('kg'),
                    leading: Radio(
                      value: 5,
                      groupValue: widget.measurementGroup,
                      focusColor: kGreenColor,
                      activeColor: kGreenColor,
                      onChanged: (T) {
                        setState(() {
                          widget.measurementGroup = T;
                        });
                      },
                    )),
                Divider(),
                SizedBox(height: 15),
              ],
            ),
          )),
          Padding(
            padding: const EdgeInsets.all(13.0),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: FlatButton(
                    color: kGreenColor,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8.0),
                      side: BorderSide(color: kGreenColor),
                    ),
                    child: Text('Update Unit Setting',
                        style: TextStyle(
                          color: Colors.white,
                        )),
                    onPressed: () async {
                      try {
                        pr.update(
                          message: "Updating",
                          progressWidget: Container(
                              padding: EdgeInsets.all(8.0),
                              child: CircularProgressIndicator(
                                valueColor:
                                    AlwaysStoppedAnimation<Color>(kGreenColor),
                              )),
                        );
                        pr.show();

                        getWeight() {
                          double w;
                          if (widget.weightGroup == 1 &&
                              widget.weightUnit == 'kg') {
                            w = widget.weight.toDouble();
                          } else if (widget.weightGroup == 1 &&
                              widget.weightUnit == 'lbs') {
                            w = widget.weight * 0.45359237;
                          } else if (widget.weightGroup == 2 &&
                              widget.weightUnit == 'kg') {
                            w = widget.weight * 2.204;
                          } else if (widget.weightGroup == 2 &&
                              widget.weightUnit == 'lbs') {
                            w = widget.weight.toDouble();
                          }
                          return w;
                        }

                        getHeight() {
                          double h;
                          if (widget.heightGroup == 1 &&
                              widget.heightUnit == 'cm') {
                            h = widget.height.toDouble();
                          } else if (widget.heightGroup == 1 &&
                              widget.heightUnit == 'inch') {
                            h = widget.height * 2.54;
                          } else if (widget.heightGroup == 2 &&
                              widget.heightUnit == 'cm') {
                            h = widget.height * 0.393700787;
                          } else if (widget.heightGroup == 2 &&
                              widget.heightUnit == 'inch') {
                            h = widget.height.toDouble();
                          }
                          return h;
                        }

                        var response = await http.put(
                          '${kUrl}edit/user/${widget.userId}?name=${widget.name}&email=${widget.email}&height=${getHeight()}&currentWeight=${getWeight()}&heightUnit=${getHeightUnit()}&weightUnit=${getWeightUnit()}&measurementUnit=${getMeasureUnit()}',
                          headers: {
                            "Accept": "application/json",
                            "Authorization": "Bearer ${widget.token}",
                          },
                        );
                        print('Response status: ${response.statusCode}');
                        print('Response body: ${response.body}');
                        await pref.setUserWeightUnit(getWeightUnit());
                        await pref.setUserHeightUnit(getHeightUnit());
                        await pref.setUserMeasureUnit(getMeasureUnit());
                        await pref.setUserHeight(getHeight().round());
                        await pref.setUserWeight(getWeight().round());
                        pr.hide();
                        showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            // return object of type Dialog
                            return AlertDialog(
                              title: Row(children: <Widget>[
                                Icon(Icons.check_circle, color: kGreenColor),
                                SizedBox(width: 5),
                                Text("Nutrilife",
                                    style: TextStyle(color: kGreenColor)),
                              ]),
                              content: Text(
                                  'Unit setting has been updated successfully!'),
                              actions: <Widget>[
                                // usually buttons at the bottom of the dialog
                                FlatButton(
                                  child: new Text("Ok",
                                      style: TextStyle(color: kGreenColor)),
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                ),
                              ],
                            );
                          },
                        );
                      } catch (e) {
                        print('Error is $e');
                      }
                    },
                  ),
                ),
              ],
            ),
          ),
        ],
      )),
    );
  }
}
