import 'package:flutter/material.dart';
import 'package:nutrilifeapp/const.dart';
import 'package:nutrilifeapp/icons.dart';

class AboutUs extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
            appBar: AppBar(
                backgroundColor: kGreenColor,
                leading: Icon(MyFlutterApp.apple_alt, size: 30),
                title: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Expanded(child: Text('About Nutrilife'),),
                    GestureDetector(
                      child: Container( height: 50, child: Center(child: Icon(Icons.close, size: 25, color: kGreyColor))),
                      onTap: (){
                       Navigator.pop(context);
                       },
                    )
                  ],
                )

            ),
            body: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
              ),
              margin: EdgeInsets.all(5),
              padding: EdgeInsets.all(20),
              child: ListView(
                children: <Widget>[
                  SizedBox(height: 8,),
                  Text('At Nutrilife, we believe in a helping you reach your health and fitness goals, whatever they may be. Diet is the most important factor in bettering and maintaining one’s holistic health. The innovative technology of our Fresh+ Nutrition Scale helps you understand your food easier than ever before. Whether you are body building, trying to lose weight, developing balanced recipes or just trying to lead a healthier lifestyle, we strive to help you accomplish that. We’re dedicated to providing you the very best product, with an emphasis on quality, consistency and customer satisfaction. Our products help motivate you to be a better, healthier version of yourself. We have a strong passion for health and fitness and are committed to the personal growth of our customers.'),
                  SizedBox(height: 20),
                  Text('Here at Nutrilife, we represent and value loyalty, authenticity and sustainability. We hope you enjoy our product as much as we enjoy offering it to you. If you have any questions or comments, please don\'t hesitate to contact us'),
                  SizedBox(height:8),
                ],
              ),
            )
        )
    );
  }
}



