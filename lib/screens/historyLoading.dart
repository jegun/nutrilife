import 'package:flutter/material.dart';
import 'package:nutrilifeapp/const.dart';
import 'package:http/http.dart' as http;
import 'package:nutrilifeapp/services/shared_preferences.dart';
import 'dart:convert';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:nutrilifeapp/screens/history.dart';
import 'package:nutrilifeapp/services/global.dart';
import 'package:provider/provider.dart' as provider;


NutrilifeSharedPreferences pref = NutrilifeSharedPreferences();

class HistoryLoading extends StatefulWidget {
  final DateTime firstDate;
  final DateTime secondDate;
  String token;
  HistoryLoading(this.firstDate, this.secondDate, this.token);
  @override
  _HistoryLoadingState createState() => _HistoryLoadingState();
}

class _HistoryLoadingState extends State<HistoryLoading> {

  getHistory()async{
    final ProgressDialog pr = ProgressDialog(context);
    pr.update(
      message: "Updating..",
      progressWidget: Container(
          padding: EdgeInsets.all(8.0), child: CircularProgressIndicator(
        valueColor: AlwaysStoppedAnimation<Color>(kGreenColor),
      )),
    );
    pr.show();
    provider.Provider.of<GlobalVariables>(context, listen: false).updateDateFrom(widget.firstDate);
    provider.Provider.of<GlobalVariables>(context, listen: false).updateDateTo(widget.secondDate);
    await provider.Provider.of<GlobalVariables>(context, listen: false).getHistoryAll(widget.firstDate, widget.secondDate);
    pr.hide();
    Navigator.pushReplacement(context, FadeRoute(
        page: HistoryScreen()
    ));
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(
        Duration(milliseconds: 200),
            () {
          getHistory();
        }
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: kGreenColor,
        ),
        body: Container(
          child: Column(
            children: <Widget>[
              SizedBox(height: 45),
              Container(
                height: 100,
                decoration: BoxDecoration(
                  color: kGreenColor,
                  borderRadius: BorderRadius.circular(10),
                  border: Border.all(
                    color: Colors.grey[300],
                  ),
                ),
                margin: EdgeInsets.all(5),
                padding: EdgeInsets.all(7),
              ),
              SizedBox(height:15),
              Expanded(
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                      color: Colors.grey[300],
                    ),
                  ),
                  margin: EdgeInsets.all(5),
                ),
              ),
            ],
          ),
        )
    );
  }
}


