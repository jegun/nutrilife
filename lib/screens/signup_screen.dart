import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nutrilifeapp/const.dart';
import 'package:nutrilifeapp/icons.dart';
import 'package:nutrilifeapp/screens/login_screen.dart';
import 'package:nutrilifeapp/screens/personal_data_wizard.dart';
import 'package:http/http.dart' as http;

class SignUpScreen extends StatefulWidget {
  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  String email;
  String password;

  bool isEmail(String em) {

    String p = r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+";
    RegExp regExp = RegExp(p);
    return regExp.hasMatch(em);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async => true,
        child: Scaffold(
          appBar: AppBar(
            leading: Icon(MyFlutterApp.apple_alt, size: 30),
            title: Text('Sign Up'),
            backgroundColor: kGreenColor,
          ),
          body: Center(
            // color: kGreenColor,
              child: SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(MyFlutterApp.apple_alt, size: 70, color: kGreenColor),
                    SizedBox(height:15),
                    Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: TextField(
                        textAlign: TextAlign.start,
                        keyboardType: TextInputType.emailAddress,
                        onChanged: (value) {
                          setState(() {
                            email = value;
                          });
                        },
                        decoration: kInputTextStyle.copyWith(
                          prefixIcon: Icon(Icons.person, color: Colors.grey[400]),
                          labelText: 'Email',
                          labelStyle: TextStyle(color: Colors.grey[400]),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: TextField(
                        obscureText: true,
                        textAlign: TextAlign.start,
                        onChanged: (value) {
                          setState(() {
                            password = value;
                          });
                        },
                        decoration: kInputTextStyle.copyWith(
                          prefixIcon: Icon(Icons.lock, color: Colors.grey[400]),
                          labelText: 'Password',
                          labelStyle: TextStyle(color: Colors.grey[400]),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 14.0,
                    ),
                    Material(
                      color: kGreenColor,
                      borderRadius: BorderRadius.circular(10.0),
                      elevation: 5.0,
                      child: MaterialButton(
                        onPressed: () async{

                          if(email != null && password != null){
                            if(isEmail(email)){
                              if(password.length >= 6){
                                try{
                                  var response = await http.post(
                                      '${kUrl}cheek',
                                    headers: {"Accept" : "application/json"},
                                    body:{
                                        'email': email,
                                    },
                                  );
                                if(response.body == 'true') {
                                  Navigator.push(context, FadeRoute(
                                    page: PersonalDataWizard(email, password),
                                  ));
                                } else{
                                  wrongAlertWithLogin(context, 'There is already an account with this email address');
                                }
                                } catch (e) {
                                  print(e);
                                }

                              }
                              else{
                                wrongAlert(context, 'Password should be at least 6 digits');
                              }
                            }
                            else {
                              wrongAlert(context, 'Invalid email format');
                            }
                          } else {
                            wrongAlert(context, 'Empty email or password');
                          }




                        },
                        minWidth: 200.0,
                        height: 42.0,
                        child: Text(
                          'Sign up', style: TextStyle(color: Colors.white,),
                        ),
                      ),
                    ),
                    SizedBox(height:20),
                    GestureDetector(
                      child: Text('Already have an account? Login Here',
                        style: TextStyle(fontSize: 10, color: kGreenColor, fontWeight: FontWeight.w600),
                      ),
                      onTap: (){
                        Navigator.pushReplacement(context, FadeRoute(
                          page:LoginScreen(),
                        ));
                        },
                    ),
                  ],
                ),
              )
          ),
        )
    );
  }
}
