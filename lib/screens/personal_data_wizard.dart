import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nutrilifeapp/const.dart';
import 'package:nutrilifeapp/icons.dart';
import 'package:nutrilifeapp/screens/home_loading.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:nutrilifeapp/services/shared_preferences.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:nutrilifeapp/services/global.dart';
import 'package:provider/provider.dart' as provider;

NutrilifeSharedPreferences pref = NutrilifeSharedPreferences();

String name;
DateTime birthDate = DateTime.now();
double userHeight = 0;
double userWeight = 0;
String userGender = gender[0];
int receive = 0;
int policy = 0;
Map<String, double> dailyGoalAll;

List<String> gender = ['Male', 'Female', 'Other'];
List<String> activityLevel = [
  'Not very active',
  'Lightly active',
  'Active',
  'Very active'
];
int count = 0;
int goalChoice = 1;
int levelChoice = 1;
String loseChoice = weight[1];
String gainChoice = weight[1];
String userEmail;
String userPassword;

List<String> weight = [
  '0.5 lbs/week',
  '1 lb/week (recommended)',
  '1.5 lbs/week',
  '2 lbs/week'
];

class PersonalDataWizard extends StatefulWidget {
  final String email;
  final String password;
  PersonalDataWizard(this.email, this.password);

  @override
  _PersonalDataWizardState createState() => _PersonalDataWizardState();
}

class _PersonalDataWizardState extends State<PersonalDataWizard> {
//   String token;
//   getToken() async{
//     token = await pref.getToken();
// }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    userEmail = widget.email;
    userPassword = widget.password;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: kGreenColor,
          leading: (count == 0)
              ? Icon(MyFlutterApp.apple_alt, size: 30)
              : GestureDetector(
                  child: Icon(Icons.arrow_back),
                  onTap: () {
                    setState(() {
                      count--;
                    });
                  }),
          title: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Expanded(
                child: Text('Personal Info'),
              ),
              (count != 2)
                  ? GestureDetector(
                      child: Container(
                          height: 50,
                          child: Center(
                              child: Text('Next >',
                                  style: TextStyle(fontSize: 17)))),
                      onTap: () {
                        if (count < 2) {
                          setState(() {
                            count++;
                          });
                        } else {
                          // getToken();
                          // provider.Provider.of<GlobalVariables>(context).getThemAll(DateTime.now(), token);
                          // await provider.Provider.of<GlobalVariables>(context).getThemAll(DateTime.now(), token);
                          // Navigator.pushReplacement(context, FadeRoute(
                          //   page: HomeLoading(kInitialData,token, 0),
                          // ));
                        }
                      },
                    )
                  : Container(),
            ],
          )),
      body: Container(
          child: Column(
        children: <Widget>[
          SizedBox(
            height: 10,
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Container(height: 4, color: kGreenColor),
                ),
                SizedBox(width: 2),
                Expanded(
                  child: Container(
                    height: 4,
                    color: (count < 1) ? Colors.grey[300] : kGreenColor,
                  ),
                ),
                SizedBox(width: 2),
                Expanded(
                  child: Container(
                    height: 4,
                    color: (count < 2) ? Colors.grey[300] : kGreenColor,
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 15),
          (count == 0)
              ? PersonalDataFirst()
              : (count == 1)
                  ? PersonalDataSecond()
                  : PersonalDataThird(),
        ],
      )),
    );
  }
}

class PersonalDataFirst extends StatefulWidget {
  @override
  _PersonalDataFirstState createState() => _PersonalDataFirstState();
}

class _PersonalDataFirstState extends State<PersonalDataFirst> {
  TextEditingController _nameController = new TextEditingController();
  TextEditingController _heightController = new TextEditingController();
  TextEditingController _weightController = new TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _nameController.text = name;
    _heightController.text = userHeight.toString();
    _weightController.text = userWeight.toString();
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: ListView(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(13.0),
            child: Row(
              children: <Widget>[
                Container(
                  width: 90,
                  child: Text('Your Name:'),
                ),
                Expanded(
                  child: TextField(
                      controller: _nameController,
                      maxLength: 10,
                      decoration: kInputTextStyle.copyWith(hintText: 'Name'),
                      onChanged: (value) {
                        setState(() {
                          name = value;
                        });
                      }),
                )
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(13),
            child: Row(
              children: <Widget>[
                Container(
                  width: 90,
                  child: Text('Birth date:'),
                ),
                Expanded(
                  child: GestureDetector(
                    child: Row(
                      children: <Widget>[
                        Text(
                          (birthDate != null)
                              ? '${birthDate.year}-${birthDate.month}-${birthDate.day}'
                              : '01/01/1900',
                          style: TextStyle(
                            color: kGreenColor,
                            fontSize: 19,
                          ),
                        ),
                        Icon(Icons.keyboard_arrow_down,
                            color: kGreenColor, size: 30),
                      ],
                    ),
                    onTap: () {
                      DatePicker.showDatePicker(context,
                          showTitleActions: true,
                          minTime: DateTime(1900, 1, 1),
                          maxTime: DateTime(2020, 12, 31), onChanged: (date) {
                        //todo: add date variable to have the value
                      }, onConfirm: (date) {
                        setState(() {
                          birthDate = date;
                        });
                        //todo: return date value
                      },
                          currentTime:
                              (birthDate != null) ? birthDate : DateTime.now(),
                          locale: LocaleType.en,
                          theme: DatePickerTheme(
                            doneStyle: TextStyle(color: kGreenColor),
                            itemStyle: TextStyle(color: Colors.black54),
                          ));
                    },
                  ),
                )
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(13.0),
            child: Row(
              children: <Widget>[
                Container(
                  width: 90,
                  child: Text('Height:'),
                ),
                Expanded(
                  child: TextField(
                    controller: _heightController,
                    onTap: () {
                      if (_heightController.text == '0.0') {
                        _heightController.clear();
                      }
                    },
                    onChanged: (value) {
                      setState(() {
                        if (value.isEmpty) {
                          userHeight = 0;
                        } else {
                          userHeight = double.parse(value);
                        }
                      });
                    },
                    maxLength: 3,
                    keyboardType: TextInputType.number,
                    inputFormatters: <TextInputFormatter>[
                      WhitelistingTextInputFormatter.digitsOnly
                    ], // Only numbers can be entered
                    decoration: kInputTextStyle.copyWith(
                        hintText: 'Height', suffixText: 'cm'),
                  ),
                )
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(13.0),
            child: Row(
              children: <Widget>[
                Container(
                  width: 90,
                  child: Text('Gender:'),
                ),
                Expanded(
                  child: DropdownButton<String>(
                    value: userGender,
                    icon: Icon(Icons.keyboard_arrow_down, color: kGreenColor),
                    iconSize: 30,
                    elevation: 16,
                    style: TextStyle(
                      color: kGreenColor,
                      fontSize: 19,
                    ),
                    underline: Container(),
                    onChanged: (String newValue) {
                      setState(() {
                        userGender = newValue;
                      });
                    },
                    items: gender.map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                  ),
                )
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(13.0),
            child: Row(
              children: <Widget>[
                Container(
                  width: 90,
                  child: Text('Weight:'),
                ),
                Expanded(
                  child: TextField(
                    controller: _weightController,
                    onTap: () {
                      if (_weightController.text == '0.0') {
                        _weightController.clear();
                      }
                    },
                    onChanged: (value) {
                      setState(() {
                        userWeight = double.parse(value);
                      });
                    },
                    maxLength: 3,
                    keyboardType: TextInputType.number,
                    inputFormatters: <TextInputFormatter>[
                      WhitelistingTextInputFormatter.digitsOnly
                    ], // Only numbers can be entered
                    decoration: kInputTextStyle.copyWith(
                        hintText: 'Weight', suffixText: 'lbs'),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class PersonalDataSecond extends StatefulWidget {
  @override
  _PersonalDataSecondState createState() => _PersonalDataSecondState();
}

class _PersonalDataSecondState extends State<PersonalDataSecond> {
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: ListView(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Text('What is your goal?',
                style: TextStyle(
                  fontSize: 19,
                )),
          ),
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Row(
              children: <Widget>[
                Radio(
                  activeColor: kGreenColor,
                  focusColor: kGreenColor,
                  value: 1,
                  groupValue: goalChoice,
                  onChanged: (T) {
                    setState(() {
                      goalChoice = T;
                    });
                  },
                ),
                SizedBox(width: 5),
                Container(
                  width: 80,
                  child: Text('Lose weight'),
                ),
                Expanded(
                  child: DropdownButton<String>(
                    value: loseChoice,
                    icon: Icon(Icons.keyboard_arrow_down, color: kGreenColor),
                    iconSize: 24,
                    elevation: 16,
                    style: TextStyle(
                      color: kGreenColor,
                    ),
                    underline: Container(),
                    onChanged: (String newValue) {
                      setState(() {
                        loseChoice = newValue;
                      });
                    },
                    items: weight.map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                  ),
                )
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Row(
              children: <Widget>[
                Radio(
                  focusColor: kGreenColor,
                  activeColor: kGreenColor,
                  value: 2,
                  groupValue: goalChoice,
                  onChanged: (T) {
                    setState(() {
                      goalChoice = T;
                    });
                  },
                ),
                SizedBox(width: 5),
                Container(
                  width: 120,
                  child: Text('Maintain weight'),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Row(
              children: <Widget>[
                Radio(
                  activeColor: kGreenColor,
                  focusColor: kGreenColor,
                  value: 3,
                  groupValue: goalChoice,
                  onChanged: (T) {
                    setState(() {
                      goalChoice = T;
                    });
                  },
                ),
                SizedBox(width: 5),
                Container(
                  width: 80,
                  child: Text('Gain weight'),
                ),
                Expanded(
                  child: DropdownButton<String>(
                    value: gainChoice,
                    icon: Icon(Icons.keyboard_arrow_down, color: kGreenColor),
                    iconSize: 24,
                    elevation: 16,
                    style: TextStyle(
                      color: kGreenColor,
                    ),
                    underline: Container(),
                    onChanged: (String newValue) {
                      setState(() {
                        gainChoice = newValue;
                      });
                    },
                    items: weight.map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                  ),
                )
              ],
            ),
          ),
          Divider(),
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Text('How active are you?',
                style: TextStyle(
                  fontSize: 19,
                )),
          ),
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                Radio(
                  activeColor: kGreenColor,
                  focusColor: kGreenColor,
                  value: 1,
                  groupValue: levelChoice,
                  onChanged: (T) {
                    setState(() {
                      levelChoice = T;
                    });
                  },
                ),
                SizedBox(width: 5),
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text('Little/No activity',
                          style: TextStyle(fontWeight: FontWeight.w700)),
                      Text(
                        'Spend most of the day sitting (e.g., full time ',
                        style: TextStyle(color: Colors.grey, fontSize: 13),
                      ),
                      Text(
                        'student, desk job)',
                        style: TextStyle(color: Colors.grey, fontSize: 13),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                Radio(
                  activeColor: kGreenColor,
                  focusColor: kGreenColor,
                  value: 2,
                  groupValue: levelChoice,
                  onChanged: (T) {
                    setState(() {
                      levelChoice = T;
                    });
                  },
                ),
                SizedBox(width: 5),
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text('Lightly active',
                          style: TextStyle(fontWeight: FontWeight.w700)),
                      Text(
                        'Spend most of the day on your feet (e.g.,',
                        style: TextStyle(color: Colors.grey, fontSize: 13),
                      ),
                      Text(
                        'teacher, salesperson)',
                        style: TextStyle(color: Colors.grey, fontSize: 13),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                Radio(
                  activeColor: kGreenColor,
                  focusColor: kGreenColor,
                  value: 3,
                  groupValue: levelChoice,
                  onChanged: (T) {
                    setState(() {
                      levelChoice = T;
                    });
                  },
                ),
                SizedBox(width: 5),
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text('Active',
                          style: TextStyle(fontWeight: FontWeight.w700)),
                      Text(
                        'Spend most of the day doing some physical',
                        style: TextStyle(color: Colors.grey, fontSize: 13),
                      ),
                      Text(
                        'activity (e.g., daily workouts, food server)',
                        style: TextStyle(color: Colors.grey, fontSize: 13),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                Radio(
                  activeColor: kGreenColor,
                  focusColor: kGreenColor,
                  value: 4,
                  groupValue: levelChoice,
                  onChanged: (T) {
                    setState(() {
                      levelChoice = T;
                    });
                  },
                ),
                SizedBox(width: 5),
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text('Very active',
                          style: TextStyle(fontWeight: FontWeight.w700)),
                      Text(
                        'Spend most of the day doing heavy physical',
                        style: TextStyle(color: Colors.grey, fontSize: 13),
                      ),
                      Text(
                        'activity (e.g., construction, athlete)',
                        style: TextStyle(color: Colors.grey, fontSize: 13),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class PersonalDataThird extends StatefulWidget {
  @override
  _PersonalDataThirdState createState() => _PersonalDataThirdState();
}

bool check1 = false;
bool check2 = false;

class _PersonalDataThirdState extends State<PersonalDataThird> {
// String token;
// getToken()async{
//   token = await pref.getToken();
// }

  @override
  Widget build(BuildContext context) {
    final ProgressDialog pr = ProgressDialog(context);
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: ListView(
          children: <Widget>[
            SizedBox(
              height: 15,
            ),
            Text('Congratulations!',
                style: TextStyle(fontSize: 23, fontWeight: FontWeight.w600)),
            Text('Your personalized plan is ready.',
                style: TextStyle(fontSize: 16)),
            SizedBox(
              height: 20,
            ),
            Text('Your daily goal is:', style: TextStyle(fontSize: 16)),
            SizedBox(
              height: 10,
            ),
            Row(
              children: <Widget>[
                Text('${dailyGoal().round()}',
                    style: TextStyle(
                        fontSize: 30,
                        color: kGreenColor,
                        fontWeight: FontWeight.w600)),
                SizedBox(
                  width: 10,
                ),
                Text('Calories', style: TextStyle(fontSize: 16)),
              ],
            ),
            SizedBox(height: 20),
            Text(
                'Daily goals have been created for 23 other nutritional measures to help you achieve nutritional needs.',
                style: TextStyle(fontSize: 16)),
            SizedBox(height: 10),
            Divider(),
            SizedBox(height: 20),
            Row(
              children: <Widget>[
                Checkbox(
                  value: check1,
                  onChanged: (t) {
                    setState(() {
                      check1 = t;
                    });
                    if (check1) {
                      policy = 1;
                    } else {
                      policy = 0;
                    }
                  },
                  activeColor: kGreenColor,
                ),
                SizedBox(width: 5),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text('I have read and agree to Nutrilife\'s'),
                      Row(
                        children: <Widget>[
                          GestureDetector(
                            child: Text(
                              'Privacy Policy',
                              style: TextStyle(
                                fontSize: 14,
                                color: kGreenColor,
                                fontWeight: FontWeight.w400,
                                decoration: TextDecoration.underline,
                              ),
                            ),
                            onTap: () => launch(
                                'https://nutrilifeco.com/pages/privacy-policy-1'),
                          ),
                          Text(' and '),
                          GestureDetector(
                            child: Text(
                              'Terms of Service',
                              style: TextStyle(
                                fontSize: 14,
                                color: kGreenColor,
                                fontWeight: FontWeight.w400,
                                decoration: TextDecoration.underline,
                              ),
                            ),
                            onTap: () => launch(
                                'https://nutrilifeco.com/pages/terms-of-service-1'),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
            SizedBox(height: 20),
            Row(
              children: <Widget>[
                Checkbox(
                  value: check2,
                  onChanged: (t) {
                    setState(() {
                      check2 = t;
                    });
                    if (check2) {
                      receive = 1;
                    } else {
                      receive = 0;
                    }
                  },
                  activeColor: kGreenColor,
                ),
                SizedBox(width: 5),
                Expanded(
                  child: Text(
                      'Yes, I want to receive tips and exclusive offers from Nutrilife. You can unsubscribe at any time.'),
                ),
              ],
            ),
            SizedBox(height: 30),
            Material(
              color: (check1) ? kGreenColor : kGreyColor,
              borderRadius: BorderRadius.circular(10.0),
              elevation: 5.0,
              child: MaterialButton(
                onPressed: () async {
                  if (check1) {
                    try {
                      pr.update(
                        message: "Signing Up",
                        progressWidget: Container(
                            padding: EdgeInsets.all(8.0),
                            child: CircularProgressIndicator(
                              valueColor:
                                  AlwaysStoppedAnimation<Color>(kGreenColor),
                            )),
                      );
                      var response =
                          await http.post('${kUrl}register', headers: {
                        "Accept": "application/json"
                      }, body: {
                        'name': name,
                        'email': userEmail,
                        'activityLevel': '$levelChoice',
                        'gender': userGender,
                        'birthDate': '$birthDate',
                        'height': '$userHeight',
                        'currentWeight': '$userWeight',
                        'heightUnit': 'cm',
                        'weightUnit': 'lbs',
                        'measurementUnit': 'g',
                        'goalWeight': '$goalChoice',
                        'dailyGoal': json.encode(dailyGoalAll),
                        'weeklyGoal': '$goalChoice',
                        'policy': '$policy',
                        'receive': '$receive',
                        'password': userPassword,
                        'gainWeight': gainChoice,
                        'loseWeight': loseChoice,
                      });
                      pr.show();
                      print('Response status: ${response.statusCode}');
                      print('Response body: ${response.body}');
                      var data = jsonDecode(response.body);
                      await pref.setIsLogged(data['status']);
                      await pref.setUserID(data['user']['id']);
                      await pref.setToken(data['access_token']);
                      await pref.setWeeklyGoal(goalChoice);
                      List<String> emptyList = [];
                      await pref.setLastChosen(emptyList);
                      await pref.setUserActivityLevel(levelChoice);
                      if (birthDate.month < 10) {
                        if (birthDate.day < 10) {
                          await pref.setUserBirthDate(
                              '${birthDate.year}-0${birthDate.month}-0${birthDate.day}');
                        } else {
                          await pref.setUserBirthDate(
                              '${birthDate.year}-0${birthDate.month}-${birthDate.day}');
                        }
                      } else {
                        if (birthDate.day < 10) {
                          await pref.setUserBirthDate(
                              '${birthDate.year}-${birthDate.month}-0${birthDate.day}');
                        } else {
                          await pref.setUserBirthDate(
                              '${birthDate.year}-${birthDate.month}-${birthDate.day}');
                        }
                      }
                      await pref.setUserDailyCalories(dailyGoal());
                      await pref.setUserGender(userGender);
                      await pref.setUserHeight(userHeight.round());
                      await pref.setUserName(name);
                      await pref.setUserWeight(userWeight.round());
                      await pref.setUserGainWeight(gainChoice);
                      await pref.setUserLoseWeight(loseChoice);
                      await pref.setUserWeightUnit('lbs');
                      await pref.setUserHeightUnit('cm');
                      await pref.setUserMeasureUnit('g');
                      await pref.setUserEmail(userEmail);
                      await pref.setUserPassword(userPassword);
                      await pref.setUserPolicy(policy);
                      await pref.setUserReceive(receive);
                      provider.Provider.of<GlobalVariables>(context,
                              listen: false)
                          .updateToken(data['access_token']);
                      await provider.Provider.of<GlobalVariables>(context,
                              listen: false)
                          .getThemAll(DateTime.now(), data['access_token']);
                      await provider.Provider.of<GlobalVariables>(context,
                              listen: false)
                          .getHistoryAll(
                              DateTime.now().subtract(Duration(days: 7)),
                              DateTime.now());
                      pr.hide();
                      Future.delayed(Duration(milliseconds: 200), () {
                        Navigator.push(
                            context,
                            FadeRoute(
                              page: HomeLoading(
                                  kInitialData, data['access_token'], 0),
                            ));
                      });
                    } catch (e) {
                      print('Error is $e');
                    }
                  }
                },
                minWidth: 200.0,
                height: 42.0,
                child: Text(
                  'Start tracking',
                  style:
                      TextStyle(color: (check1) ? Colors.white : Colors.grey),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

double dailyGoal() {
  double value;
  double w = 0.45359237 * userWeight;
  String birthYear = birthDate.year.toString();
  String nowYear = DateTime.now().year.toString();
  double age = double.parse(nowYear) - double.parse(birthYear);
  double bmr;
  double af;

  if (userGender == 'Male') {
    bmr = 66 + (13.7 * w) + (5 * userHeight) - (6.8 * age);
  } else {
    bmr = 655 + (9.6 * w) + (1.8 * userHeight) - (4.7 * age);
  }

  if (levelChoice == 1) {
    af = 1.2;
  } else if (levelChoice == 2) {
    af = 1.375;
  } else if (levelChoice == 3) {
    af = 1.55;
  } else if (levelChoice == 4) {
    af = 1.725;
  }

  if (goalChoice == 1) {
    if (loseChoice == weight[0]) {
      value = ((bmr * af) - 250);
    } else if (loseChoice == weight[1]) {
      value = ((bmr * af) - 500);
    } else if (loseChoice == weight[2]) {
      value = ((bmr * af) - 750);
    } else if (loseChoice == weight[3]) {
      value = ((bmr * af) - 1000);
    }
  } else if (goalChoice == 2) {
    value = (bmr * af);
  } else if (goalChoice == 3) {
    if (gainChoice == weight[0]) {
      value = ((bmr * af) + 250);
    } else if (gainChoice == weight[1]) {
      value = ((bmr * af) + 500);
    } else if (gainChoice == weight[2]) {
      value = ((bmr * af) + 750);
    } else if (gainChoice == weight[3]) {
      value = ((bmr * af) + 1000);
    }
  }

  dailyGoalAll = {
    'calories': value,
    'protein': (0.8 * w),
    'fat': (0.3 * value / 9),
    'saturated fat': ((0.1 * value) / 9),
    'trans fat': ((0.01 * value) / 9),
    'carbohydrates': (0.55 * value / 4),
    'sugars': (userGender == "Male") ? 37.5 : 25,
    'fiber': (userGender == "Male") ? 38 : 25,
    'vitamin a': (userGender == "Male") ? 3000 : 2333,
    'carotene': 6000,
    'vitamin b6': 1.3,
    'vitamin b12': 2.4,
    'vitamin b3': (userGender == "Male") ? 16 : 14,
    'vitamin c': (userGender == "Male") ? 90 : 75,
    'vitamin e': 22,
    'cholestrol': 300,
    'potassium': 4700,
    'sodium': 1500,
    'calcium': 1000,
    'magnesium': (userGender == "Male") ? 420 : 320,
    'iron': (userGender == "Male") ? 8 : 18,
    'manganese': (userGender == "Male") ? 2.3 : 1.8,
    'zinc': (userGender == "Male") ? 11 : 8,
    'copper': 0.9,
    'phosphorus': 700,
    'selenium': 55,
    'water': (userGender == "Male") ? 3.7 : 2.7,
  };

  return value;
}

int goalWeight() {
  int value;
  if (goalChoice == 1) {
    if (loseChoice == weight[0]) {
      value = 0;
    } else if (loseChoice == weight[1]) {
      value = 1;
    } else if (loseChoice == weight[2]) {
      value = 2;
    } else if (loseChoice == weight[3]) {
      value = 3;
    }
  } else if (goalChoice == 2) {
    value = 4;
  } else if (goalChoice == 3) {
    if (loseChoice == weight[0]) {
      value = 5;
    } else if (loseChoice == weight[1]) {
      value = 6;
    } else if (loseChoice == weight[2]) {
      value = 7;
    } else if (loseChoice == weight[3]) {
      value = 8;
    }
  }
  return value;
}
