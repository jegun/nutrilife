import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nutrilifeapp/icons.dart';
import 'package:nutrilifeapp/const.dart';
import 'package:nutrilifeapp/screens/diaryLoading.dart';
import 'package:nutrilifeapp/widgets/navigation_bar.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:nutrilifeapp/widgets/addexercise_bottomsheet.dart';
import 'package:nutrilifeapp/screens/nutritional_analysis.dart';
import 'package:nutrilifeapp/widgets/addwater_bottomsheet.dart';
import 'package:nutrilifeapp/widgets/edit_meal.dart';
import 'package:nutrilifeapp/widgets/edit_water.dart';
import 'package:nutrilifeapp/widgets/edit_exercise.dart';
import 'package:nutrilifeapp/services/shared_preferences.dart';
import 'package:nutrilifeapp/services/global.dart';
import 'package:provider/provider.dart' as provider;

NutrilifeSharedPreferences pref = NutrilifeSharedPreferences();

class DiaryScreen extends StatefulWidget {
  @override
  _DiaryScreenState createState() => _DiaryScreenState();
}

class _DiaryScreenState extends State<DiaryScreen> {
  List<dynamic> allFood = [];
  String unitType = '';

  setAllFood() {
    allFood = [];
    allFood.addAll(provider.Provider.of<GlobalVariables>(context, listen: false)
        .breakfastList);
    allFood.addAll(provider.Provider.of<GlobalVariables>(context, listen: false)
        .lunchList);
    allFood.addAll(provider.Provider.of<GlobalVariables>(context, listen: false)
        .dinnerList);
    allFood.addAll(provider.Provider.of<GlobalVariables>(context, listen: false)
        .snacksList);
  }

  getWaterSum(List<dynamic> list) {
    double x = 0;
    if (list.length != 0) {
      for (int s = 0; s < list.length; s++) {
        Map y = list[s];
        double z = double.parse(y["water_amount"]);
        x = x + z;
      }
    }
    return x;
  }

  getExerciseSum(List<dynamic> list) {
    double x = 0;
    if (list.length != 0) {
      for (int s = 0; s < list.length; s++) {
        Map y = list[s];
        double z = double.parse(y["calories"]);
        x = x + z;
      }
    }
    return x;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    loadUnitType();
  }

  loadUnitType() async {
    unitType = await pref.getUserMeasureUnit();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        appBar: AppBar(
          title: Text('Diary'),
          leading: Icon(MyFlutterApp.apple_alt, size: 30),
          backgroundColor: kGreenColor,
        ),
        body: Padding(
            padding: EdgeInsets.all(5),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                //todo: insert 3 widgets (weight input, actions and nutrition facts)
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: GestureDetector(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                (provider.Provider.of<GlobalVariables>(context)
                                                .chosenDate
                                                .day ==
                                            DateTime.now().day &&
                                        provider.Provider.of<GlobalVariables>(
                                                    context)
                                                .chosenDate
                                                .month ==
                                            DateTime.now().month &&
                                        provider.Provider.of<GlobalVariables>(
                                                    context)
                                                .chosenDate
                                                .year ==
                                            DateTime.now().year)
                                    ? 'Today'
                                    : '${provider.Provider.of<GlobalVariables>(context).chosenDate.year}-${provider.Provider.of<GlobalVariables>(context).chosenDate.month}-${provider.Provider.of<GlobalVariables>(context).chosenDate.day}',
                                style: TextStyle(
                                    fontSize: 15, fontWeight: FontWeight.w600),
                              ),
                              Icon(
                                Icons.keyboard_arrow_down,
                              ),
                            ],
                          ),
                          onTap: () {
                            DatePicker.showDatePicker(context,
                                showTitleActions: true,
                                minTime: DateTime(2000, 1, 1),
                                maxTime: DateTime(2100, 12, 31),
                                onChanged: (date) {
                              //todo: add date variable to have the value
                            }, onConfirm: (date) async {
                              Navigator.pushReplacement(
                                  context,
                                  FadeRoute(
                                      page: DiaryLoading(
                                          date,
                                          provider.Provider.of<GlobalVariables>(
                                                  context,
                                                  listen: false)
                                              .token)));
                            },
                                currentTime:
                                    provider.Provider.of<GlobalVariables>(
                                            context,
                                            listen: false)
                                        .chosenDate,
                                locale: LocaleType.en,
                                theme: DatePickerTheme(
                                  doneStyle: TextStyle(color: kGreenColor),
                                  itemStyle: TextStyle(color: Colors.black54),
                                ));
                          },
                        ),
                      ),
                    ],
                  ),
                ),

                SizedBox(
                  height: 5,
                ),
                DiaryCalculations(
                    provider.Provider.of<GlobalVariables>(context).caloriesAll,
                    provider.Provider.of<GlobalVariables>(context).caloriesGoal,
                    getExerciseSum(
                        provider.Provider.of<GlobalVariables>(context)
                            .exerciseList)),
                SizedBox(
                  height: 5,
                ),
                Expanded(
                  child: Scrollbar(
                    child: ListView(children: <Widget>[
                      Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: Colors.grey[300],
                            ),
                          ),
                          margin: EdgeInsets.all(5),
                          child: Column(
                            children: <Widget>[
                              ListTile(
                                title: Text('Breakfast',
                                    style:
                                        TextStyle(fontWeight: FontWeight.w700)),
                                trailing: Text(
                                    '${provider.Provider.of<GlobalVariables>(context).caloriesBreakfast.round()} kcal',
                                    style:
                                        TextStyle(fontWeight: FontWeight.w500)),
                              ),
                              (provider.Provider.of<GlobalVariables>(context)
                                          .breakfastList
                                          .length ==
                                      0)
                                  ? Container()
                                  : Divider(),
                              Container(
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10),
                                ),
                                height: (provider.Provider.of<GlobalVariables>(
                                                context)
                                            .breakfastList
                                            .length ==
                                        0)
                                    ? 0
                                    : provider.Provider.of<GlobalVariables>(
                                                context)
                                            .breakfastList
                                            .length
                                            .toDouble() *
                                        60,
                                child: ListView.builder(
                                    physics: NeverScrollableScrollPhysics(),
                                    itemCount:
                                        provider.Provider.of<GlobalVariables>(
                                                context)
                                            .breakfastList
                                            .length,
                                    itemBuilder:
                                        (BuildContext context, int position) {
                                      return GestureDetector(
                                        onTap: () {
                                          holdToEdit(context);
                                        },
                                        onLongPress: () {
                                          showModalBottomSheet(
                                              context: context,
                                              isScrollControlled: true,
                                              builder: (context) =>
                                                  SingleChildScrollView(
                                                      child: Container(
                                                    padding: EdgeInsets.only(
                                                        bottom: MediaQuery.of(
                                                                context)
                                                            .viewInsets
                                                            .bottom),
                                                    child: EditMeal(
                                                        provider.Provider.of<
                                                                        GlobalVariables>(
                                                                    context)
                                                                .breakfastList[
                                                            position],
                                                        provider.Provider.of<
                                                                    GlobalVariables>(
                                                                context)
                                                            .chosenDate,
                                                        provider.Provider.of<
                                                                    GlobalVariables>(
                                                                context)
                                                            .token,
                                                        unitType),
                                                  )));
                                        },
                                        child: ListTile(
                                          title: Text(
                                              '${provider.Provider.of<GlobalVariables>(context).breakfastList[position]['food_name']}',
                                              style: TextStyle(fontSize: 13)),
                                          trailing: Text(
                                              '${(double.parse(provider.Provider.of<GlobalVariables>(context).breakfastList[position]['weight']) / 100 * double.parse(provider.Provider.of<GlobalVariables>(context).breakfastList[position]['calories'])).round()} kcal'),
                                        ),
                                      );
                                    }),
                              ),
                            ],
                          )),
                      SizedBox(height: 5),
                      Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: Colors.grey[300],
                            ),
                          ),
                          margin: EdgeInsets.all(5),
                          child: Column(
                            children: <Widget>[
                              ListTile(
                                title: Text('Lunch',
                                    style:
                                        TextStyle(fontWeight: FontWeight.w700)),
                                trailing: Text(
                                    '${provider.Provider.of<GlobalVariables>(context).caloriesLunch.round()} kcal',
                                    style:
                                        TextStyle(fontWeight: FontWeight.w500)),
                              ),
                              (provider.Provider.of<GlobalVariables>(context)
                                          .lunchList
                                          .length ==
                                      0)
                                  ? Container()
                                  : Divider(),
                              Container(
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10),
                                ),
                                height: (provider.Provider.of<GlobalVariables>(
                                                context)
                                            .lunchList
                                            .length ==
                                        0)
                                    ? 0
                                    : provider.Provider.of<GlobalVariables>(
                                                context)
                                            .lunchList
                                            .length
                                            .toDouble() *
                                        60,
                                child: ListView.builder(
                                    physics: NeverScrollableScrollPhysics(),
                                    itemCount:
                                        provider.Provider.of<GlobalVariables>(
                                                context)
                                            .lunchList
                                            .length,
                                    itemBuilder:
                                        (BuildContext context, int position) {
                                      return GestureDetector(
                                        onTap: () {
                                          holdToEdit(context);
                                        },
                                        onLongPress: () {
                                          showModalBottomSheet(
                                              context: context,
                                              isScrollControlled: true,
                                              builder: (context) =>
                                                  SingleChildScrollView(
                                                      child: Container(
                                                    padding: EdgeInsets.only(
                                                        bottom: MediaQuery.of(
                                                                context)
                                                            .viewInsets
                                                            .bottom),
                                                    child: EditMeal(
                                                        provider.Provider.of<
                                                                        GlobalVariables>(
                                                                    context)
                                                                .lunchList[
                                                            position],
                                                        provider.Provider.of<
                                                                    GlobalVariables>(
                                                                context)
                                                            .chosenDate,
                                                        provider.Provider.of<
                                                                    GlobalVariables>(
                                                                context)
                                                            .token,
                                                        unitType),
                                                  )));
                                        },
                                        child: ListTile(
                                          title: Text(
                                              '${provider.Provider.of<GlobalVariables>(context).lunchList[position]['food_name']}',
                                              style: TextStyle(fontSize: 13)),
                                          trailing: Text(
                                              '${(double.parse(provider.Provider.of<GlobalVariables>(context).lunchList[position]['weight']) / 100 * double.parse(provider.Provider.of<GlobalVariables>(context).lunchList[position]['calories'])).round()} kcal'),
                                        ),
                                      );
                                    }),
                              ),
                            ],
                          )),
                      SizedBox(height: 5),
                      Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: Colors.grey[300],
                            ),
                          ),
                          margin: EdgeInsets.all(5),
                          child: Column(
                            children: <Widget>[
                              ListTile(
                                title: Text('Dinner',
                                    style:
                                        TextStyle(fontWeight: FontWeight.w700)),
                                trailing: Text(
                                    '${provider.Provider.of<GlobalVariables>(context).caloriesDinner.round()} kcal',
                                    style:
                                        TextStyle(fontWeight: FontWeight.w500)),
                              ),
                              (provider.Provider.of<GlobalVariables>(context)
                                          .dinnerList
                                          .length ==
                                      0)
                                  ? Container()
                                  : Divider(),
                              Container(
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10),
                                ),
                                height: (provider.Provider.of<GlobalVariables>(
                                                context)
                                            .dinnerList
                                            .length ==
                                        0)
                                    ? 0
                                    : provider.Provider.of<GlobalVariables>(
                                                context)
                                            .dinnerList
                                            .length
                                            .toDouble() *
                                        60,
                                child: ListView.builder(
                                    physics: NeverScrollableScrollPhysics(),
                                    itemCount:
                                        provider.Provider.of<GlobalVariables>(
                                                context)
                                            .dinnerList
                                            .length,
                                    itemBuilder:
                                        (BuildContext context, int position) {
                                      return GestureDetector(
                                        onTap: () {
                                          holdToEdit(context);
                                        },
                                        onLongPress: () {
                                          showModalBottomSheet(
                                              context: context,
                                              isScrollControlled: true,
                                              builder: (context) =>
                                                  SingleChildScrollView(
                                                      child: Container(
                                                    padding: EdgeInsets.only(
                                                        bottom: MediaQuery.of(
                                                                context)
                                                            .viewInsets
                                                            .bottom),
                                                    child: EditMeal(
                                                        provider.Provider.of<
                                                                        GlobalVariables>(
                                                                    context)
                                                                .dinnerList[
                                                            position],
                                                        provider.Provider.of<
                                                                    GlobalVariables>(
                                                                context)
                                                            .chosenDate,
                                                        provider.Provider.of<
                                                                    GlobalVariables>(
                                                                context)
                                                            .token,
                                                        unitType),
                                                  )));
                                        },
                                        child: ListTile(
                                          title: Text(
                                              '${provider.Provider.of<GlobalVariables>(context).dinnerList[position]['food_name']}',
                                              style: TextStyle(fontSize: 13)),
                                          trailing: Text(
                                              '${(double.parse(provider.Provider.of<GlobalVariables>(context).dinnerList[position]['weight']) / 100 * double.parse(provider.Provider.of<GlobalVariables>(context).dinnerList[position]['calories'])).round()} kcal'),
                                        ),
                                      );
                                    }),
                              ),
                            ],
                          )),
                      SizedBox(height: 5),
                      Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: Colors.grey[300],
                            ),
                          ),
                          margin: EdgeInsets.all(5),
                          child: Column(
                            children: <Widget>[
                              ListTile(
                                title: Text('Snacks',
                                    style:
                                        TextStyle(fontWeight: FontWeight.w700)),
                                trailing: Text(
                                    '${provider.Provider.of<GlobalVariables>(context).caloriesSnacks.round()} kcal',
                                    style:
                                        TextStyle(fontWeight: FontWeight.w500)),
                              ),
                              (provider.Provider.of<GlobalVariables>(context)
                                          .snacksList
                                          .length ==
                                      0)
                                  ? Container()
                                  : Divider(),
                              Container(
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10),
                                ),
                                height: (provider.Provider.of<GlobalVariables>(
                                                context)
                                            .snacksList
                                            .length ==
                                        0)
                                    ? 0
                                    : provider.Provider.of<GlobalVariables>(
                                                context)
                                            .snacksList
                                            .length
                                            .toDouble() *
                                        60,
                                child: ListView.builder(
                                    physics: NeverScrollableScrollPhysics(),
                                    itemCount:
                                        provider.Provider.of<GlobalVariables>(
                                                context)
                                            .snacksList
                                            .length,
                                    itemBuilder:
                                        (BuildContext context, int position) {
                                      return GestureDetector(
                                        onTap: () {
                                          holdToEdit(context);
                                        },
                                        onLongPress: () {
                                          showModalBottomSheet(
                                              context: context,
                                              isScrollControlled: true,
                                              builder: (context) =>
                                                  SingleChildScrollView(
                                                      child: Container(
                                                    padding: EdgeInsets.only(
                                                        bottom: MediaQuery.of(
                                                                context)
                                                            .viewInsets
                                                            .bottom),
                                                    child: EditMeal(
                                                        provider.Provider.of<
                                                                        GlobalVariables>(
                                                                    context)
                                                                .snacksList[
                                                            position],
                                                        provider.Provider.of<
                                                                    GlobalVariables>(
                                                                context)
                                                            .chosenDate,
                                                        provider.Provider.of<
                                                                    GlobalVariables>(
                                                                context)
                                                            .token,
                                                        unitType),
                                                  )));
                                        },
                                        child: ListTile(
                                          title: Text(
                                              '${provider.Provider.of<GlobalVariables>(context).snacksList[position]['food_name']}',
                                              style: TextStyle(fontSize: 13)),
                                          trailing: Text(
                                              '${(double.parse(provider.Provider.of<GlobalVariables>(context).snacksList[position]['weight']) / 100 * double.parse(provider.Provider.of<GlobalVariables>(context).snacksList[position]['calories'])).round()} kcal'),
                                        ),
                                      );
                                    }),
                              ),
                            ],
                          )),
                      SizedBox(height: 5),
                      Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: Colors.grey[300],
                            ),
                          ),
                          margin: EdgeInsets.all(5),
                          child: Column(
                            children: <Widget>[
                              ListTile(
                                title: Text('Water',
                                    style:
                                        TextStyle(fontWeight: FontWeight.w700)),
                                trailing: Text(
                                    '${getWaterSum(provider.Provider.of<GlobalVariables>(context).waterList).round()} ml',
                                    style:
                                        TextStyle(fontWeight: FontWeight.w500)),
                              ),
                              (provider.Provider.of<GlobalVariables>(context)
                                          .waterList
                                          .length ==
                                      0)
                                  ? Container()
                                  : Divider(),
                              Container(
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10),
                                ),
                                height: (provider.Provider.of<GlobalVariables>(
                                                context)
                                            .waterList
                                            .length ==
                                        0)
                                    ? 0
                                    : provider.Provider.of<GlobalVariables>(
                                                context)
                                            .waterList
                                            .length
                                            .toDouble() *
                                        60,
                                child: ListView.builder(
                                    physics: NeverScrollableScrollPhysics(),
                                    itemCount:
                                        provider.Provider.of<GlobalVariables>(
                                                context)
                                            .waterList
                                            .length,
                                    itemBuilder:
                                        (BuildContext context, int position) {
                                      return GestureDetector(
                                        onTap: () {
                                          holdToEdit(context);
                                        },
                                        onLongPress: () {
                                          showModalBottomSheet(
                                              context: context,
                                              isScrollControlled: true,
                                              builder: (context) =>
                                                  SingleChildScrollView(
                                                      child: Container(
                                                    padding: EdgeInsets.only(
                                                        bottom: MediaQuery.of(
                                                                context)
                                                            .viewInsets
                                                            .bottom),
                                                    child: EditWater(
                                                        provider.Provider.of<
                                                                        GlobalVariables>(
                                                                    context)
                                                                .waterList[
                                                            position],
                                                        provider.Provider.of<
                                                                    GlobalVariables>(
                                                                context)
                                                            .chosenDate,
                                                        provider.Provider.of<
                                                                    GlobalVariables>(
                                                                context)
                                                            .token),
                                                  )));
                                        },
                                        child: ListTile(
                                          title: Text(
                                              '${double.parse(provider.Provider.of<GlobalVariables>(context).waterList[position]['water_amount']).round()} ml',
                                              style: TextStyle(fontSize: 13)),
                                        ),
                                      );
                                    }),
                              ),
                              GestureDetector(
                                onTap: () {
                                  showModalBottomSheet(
                                      context: context,
                                      isScrollControlled: true,
                                      builder: (context) =>
                                          SingleChildScrollView(
                                              child: Container(
                                            padding: EdgeInsets.only(
                                                bottom: MediaQuery.of(context)
                                                    .viewInsets
                                                    .bottom),
                                            child: AddWater(
                                                provider.Provider.of<
                                                            GlobalVariables>(
                                                        context)
                                                    .chosenDate,
                                                provider.Provider.of<
                                                            GlobalVariables>(
                                                        context)
                                                    .token),
                                          )));
                                },
                                child: ListTile(
                                  leading: Icon(Icons.add, color: Colors.grey),
                                  title: Text('Add water',
                                      style: TextStyle(color: Colors.grey)),
                                ),
                              )
                            ],
                          )),
                      SizedBox(height: 5),
                      Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: Colors.grey[300],
                            ),
                          ),
                          margin: EdgeInsets.all(5),
                          child: Column(
                            children: <Widget>[
                              ListTile(
                                title: Text('Exercise',
                                    style:
                                        TextStyle(fontWeight: FontWeight.w700)),
                                trailing: Text(
                                    '${getExerciseSum(provider.Provider.of<GlobalVariables>(context).exerciseList).round()} kcal',
                                    style:
                                        TextStyle(fontWeight: FontWeight.w500)),
                              ),
                              (provider.Provider.of<GlobalVariables>(context)
                                          .exerciseList
                                          .length ==
                                      0)
                                  ? Container()
                                  : Divider(),
                              Container(
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10),
                                ),
                                height: (provider.Provider.of<GlobalVariables>(
                                                context)
                                            .exerciseList
                                            .length ==
                                        0)
                                    ? 0
                                    : provider.Provider.of<GlobalVariables>(
                                                context)
                                            .exerciseList
                                            .length
                                            .toDouble() *
                                        60,
                                child: ListView.builder(
                                    physics: NeverScrollableScrollPhysics(),
                                    itemCount:
                                        provider.Provider.of<GlobalVariables>(
                                                context)
                                            .exerciseList
                                            .length,
                                    itemBuilder:
                                        (BuildContext context, int position) {
                                      return GestureDetector(
                                        onTap: () {
                                          holdToEdit(context);
                                        },
                                        onLongPress: () {
                                          showModalBottomSheet(
                                              context: context,
                                              isScrollControlled: true,
                                              builder: (context) =>
                                                  SingleChildScrollView(
                                                      child: Container(
                                                    padding: EdgeInsets.only(
                                                        bottom: MediaQuery.of(
                                                                context)
                                                            .viewInsets
                                                            .bottom),
                                                    child: EditExercise(
                                                        provider.Provider.of<
                                                                        GlobalVariables>(
                                                                    context)
                                                                .exerciseList[
                                                            position],
                                                        provider.Provider.of<
                                                                    GlobalVariables>(
                                                                context)
                                                            .chosenDate,
                                                        provider.Provider.of<
                                                                    GlobalVariables>(
                                                                context)
                                                            .token),
                                                  )));
                                        },
                                        child: ListTile(
                                          title: Text(
                                              '${provider.Provider.of<GlobalVariables>(context).exerciseList[position]['name']}',
                                              style: TextStyle(fontSize: 13)),
                                          trailing: Text(
                                              '${double.parse(provider.Provider.of<GlobalVariables>(context).exerciseList[position]['calories']).round()} kcal'),
                                        ),
                                      );
                                    }),
                              ),
                              GestureDetector(
                                onTap: () {
                                  showModalBottomSheet(
                                      context: context,
                                      isScrollControlled: true,
                                      builder: (context) =>
                                          SingleChildScrollView(
                                              child: Container(
                                            padding: EdgeInsets.only(
                                                bottom: MediaQuery.of(context)
                                                    .viewInsets
                                                    .bottom),
                                            child: AddExercise(
                                                provider.Provider.of<
                                                            GlobalVariables>(
                                                        context)
                                                    .chosenDate,
                                                provider.Provider.of<
                                                            GlobalVariables>(
                                                        context)
                                                    .token),
                                          )));
                                },
                                child: ListTile(
                                  leading: Icon(Icons.add, color: Colors.grey),
                                  title: Text('Add exercise',
                                      style: TextStyle(color: Colors.grey)),
                                ),
                              )
                            ],
                          )),
                      Container(
                        height: 50,
                        margin: EdgeInsets.all(5),
                        child: FlatButton(
                          color: kGreenColor,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8.0),
                            side: BorderSide(color: kGreyColor),
                          ),
                          child: Text('Nutritional Analysis',
                              style: TextStyle(
                                color: Colors.white,
                              )),
                          onPressed: () {
                            setAllFood();
                            Navigator.push(
                              context,
                              FadeRoute(
                                  page: NutritionalAnalysis(
                                      allFood,
                                      provider.Provider.of<GlobalVariables>(
                                              context,
                                              listen: false)
                                          .chosenDate,
                                      getWaterSum(
                                          provider.Provider.of<GlobalVariables>(
                                                  context,
                                                  listen: false)
                                              .waterList),
                                      provider.Provider.of<GlobalVariables>(
                                              context,
                                              listen: false)
                                          .dailyGoal)),
                            );
                          },
                        ),
                      ),
                    ]),
                  ),
                ),
                NavigationBar(1),
              ],
            )),
      ),
    );
  }
}

/*class Meal extends StatefulWidget {
  DateTime date;
  final String mealName;
  List<dynamic> mealList;
  double caloriesTotal;
  final String token;
  Meal(this.mealName, this.mealList, this.caloriesTotal, this.date, this.token);
  @override
  _MealState createState() => _MealState();
}

class _MealState extends State<Meal> {
  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          border: Border.all(
            color: Colors.grey[300],
          ),
        ),
        margin: EdgeInsets.all(5),
        child: Column(
          children: <Widget>[
            ListTile(
              title: Text(widget.mealName,
                  style: TextStyle(fontWeight: FontWeight.w700)),
              trailing: Text('${widget.caloriesTotal.round()} kcal',
                  style: TextStyle(fontWeight: FontWeight.w500)),
            ),
            (widget.mealList.length == 0) ? Container() : Divider(),
            Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
              ),
              height: (widget.mealList.length == 0)
                  ? 0
                  : widget.mealList.length.toDouble() * 60,
              child: ListView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  itemCount: widget.mealList.length,
                  itemBuilder: (BuildContext context, int position) {
                    return GestureDetector(
                      onTap: () {
                        holdToEdit(context);
                      },
                      onLongPress: () {
                        showModalBottomSheet(
                            context: context,
                            isScrollControlled: true,
                            builder: (context) => SingleChildScrollView(
                                    child: Container(
                                  padding: EdgeInsets.only(
                                      bottom: MediaQuery.of(context)
                                          .viewInsets
                                          .bottom),
                                  child: EditMeal(widget.mealList[position],
                                      widget.date, widget.token, unitType),
                                )));
                      },
                      child: ListTile(
                        title: Text('${widget.mealList[position]['food_name']}',
                            style: TextStyle(fontSize: 13)),
                        trailing: Text(
                            '${double.parse(widget.mealList[position]['calories']).round()} kcal'),
                      ),
                    );
                  }),
            ),
          ],
        ));
  }
}*/

class DiaryCalculations extends StatefulWidget {
  double foodCalories;
  double caloriesGoal;
  double exercise;
  DiaryCalculations(this.foodCalories, this.caloriesGoal, this.exercise);
  @override
  _DiaryCalculationsState createState() => _DiaryCalculationsState();
}

class _DiaryCalculationsState extends State<DiaryCalculations> {
  @override
  Widget build(BuildContext context) {
    return Container(
        height: 100,
        decoration: BoxDecoration(
          color: kGreenColor,
          borderRadius: BorderRadius.circular(10),
          border: Border.all(
            color: Colors.grey[300],
          ),
        ),
        margin: EdgeInsets.all(5),
        padding: EdgeInsets.all(7),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Text('Calories Remaining:',
                textAlign: TextAlign.left,
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 15,
                    fontWeight: FontWeight.w600)),
            SizedBox(
              height: 8,
            ),
            Row(
              children: <Widget>[
                Expanded(
                  child: Column(
                    children: <Widget>[
                      Text('${widget.caloriesGoal.round()}',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 15,
                              fontWeight: FontWeight.w600)),
                      Text('Goal',
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.white, fontSize: 11)),
                    ],
                  ),
                ),
                Text('-',
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.white)),
                Expanded(
                    child: Column(
                  children: <Widget>[
                    Text('${widget.foodCalories.round()}',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 15,
                            fontWeight: FontWeight.w600)),
                    Text('Food',
                        textAlign: TextAlign.center,
                        style: TextStyle(color: Colors.white, fontSize: 11)),
                  ],
                )),
                Text('+',
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.white)),
                Expanded(
                    child: Column(
                  children: <Widget>[
                    Text('${widget.exercise.round()}',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 15,
                            fontWeight: FontWeight.w600)),
                    Text('Exercise',
                        textAlign: TextAlign.center,
                        style: TextStyle(color: Colors.white, fontSize: 11)),
                  ],
                )),
                Text('=',
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.white)),
                Expanded(
                    child: Column(
                  children: <Widget>[
                    Text(
                        '${(widget.caloriesGoal - widget.foodCalories + widget.exercise).round()}',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 15,
                            fontWeight: FontWeight.w600)),
                    Text(
                      'Remaining',
                      style: TextStyle(color: Colors.white, fontSize: 11),
                      textAlign: TextAlign.center,
                    ),
                  ],
                ))
              ],
            ),
          ],
        ));
  }
}

class Water extends StatefulWidget {
  final DateTime date;
  final String token;
  final double waterSum;
  final List<dynamic> waterList;
  Water(this.date, this.token, this.waterList, this.waterSum);
  @override
  _WaterState createState() => _WaterState();
}

class _WaterState extends State<Water> {
  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          border: Border.all(
            color: Colors.grey[300],
          ),
        ),
        margin: EdgeInsets.all(5),
        child: Column(
          children: <Widget>[
            ListTile(
              title:
                  Text('Water', style: TextStyle(fontWeight: FontWeight.w700)),
              trailing: Text('${widget.waterSum.round()} ml',
                  style: TextStyle(fontWeight: FontWeight.w500)),
            ),
            (widget.waterList.length == 0) ? Container() : Divider(),
            Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
              ),
              height: (widget.waterList.length == 0)
                  ? 0
                  : widget.waterList.length.toDouble() * 60,
              child: ListView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  itemCount: widget.waterList.length,
                  itemBuilder: (BuildContext context, int position) {
                    return GestureDetector(
                      onTap: () {
                        holdToEdit(context);
                      },
                      onLongPress: () {
                        showModalBottomSheet(
                            context: context,
                            isScrollControlled: true,
                            builder: (context) => SingleChildScrollView(
                                    child: Container(
                                  padding: EdgeInsets.only(
                                      bottom: MediaQuery.of(context)
                                          .viewInsets
                                          .bottom),
                                  child: EditWater(widget.waterList[position],
                                      widget.date, widget.token),
                                )));
                      },
                      child: ListTile(
                        title: Text(
                            '${double.parse(widget.waterList[position]['water_amount']).round()} ml',
                            style: TextStyle(fontSize: 13)),
                      ),
                    );
                  }),
            ),
            GestureDetector(
              onTap: () {
                showModalBottomSheet(
                    context: context,
                    isScrollControlled: true,
                    builder: (context) => SingleChildScrollView(
                            child: Container(
                          padding: EdgeInsets.only(
                              bottom: MediaQuery.of(context).viewInsets.bottom),
                          child: AddWater(widget.date, widget.token),
                        )));
              },
              child: ListTile(
                leading: Icon(Icons.add, color: Colors.grey),
                title: Text('Add water', style: TextStyle(color: Colors.grey)),
              ),
            )
          ],
        ));
  }
}

class Exercise extends StatefulWidget {
  final DateTime date;
  final String token;
  final double exerciseSum;
  final List<dynamic> exerciseList;
  Exercise(this.date, this.token, this.exerciseList, this.exerciseSum);
  @override
  _ExerciseState createState() => _ExerciseState();
}

class _ExerciseState extends State<Exercise> {
  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          border: Border.all(
            color: Colors.grey[300],
          ),
        ),
        margin: EdgeInsets.all(5),
        child: Column(
          children: <Widget>[
            ListTile(
              title: Text('Exercise',
                  style: TextStyle(fontWeight: FontWeight.w700)),
              trailing: Text('${widget.exerciseSum.round()} kcal',
                  style: TextStyle(fontWeight: FontWeight.w500)),
            ),
            (widget.exerciseList.length == 0) ? Container() : Divider(),
            Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
              ),
              height: (widget.exerciseList.length == 0)
                  ? 0
                  : widget.exerciseList.length.toDouble() * 60,
              child: ListView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  itemCount: widget.exerciseList.length,
                  itemBuilder: (BuildContext context, int position) {
                    return GestureDetector(
                      onTap: () {
                        holdToEdit(context);
                      },
                      onLongPress: () {
                        showModalBottomSheet(
                            context: context,
                            isScrollControlled: true,
                            builder: (context) => SingleChildScrollView(
                                    child: Container(
                                  padding: EdgeInsets.only(
                                      bottom: MediaQuery.of(context)
                                          .viewInsets
                                          .bottom),
                                  child: EditExercise(
                                      widget.exerciseList[position],
                                      widget.date,
                                      widget.token),
                                )));
                      },
                      child: ListTile(
                        title: Text('${widget.exerciseList[position]['name']}',
                            style: TextStyle(fontSize: 13)),
                        trailing: Text(
                            '${double.parse(widget.exerciseList[position]['calories']).round()} kcal'),
                      ),
                    );
                  }),
            ),
            GestureDetector(
              onTap: () {
                showModalBottomSheet(
                    context: context,
                    isScrollControlled: true,
                    builder: (context) => SingleChildScrollView(
                            child: Container(
                          padding: EdgeInsets.only(
                              bottom: MediaQuery.of(context).viewInsets.bottom),
                          child: AddExercise(widget.date, widget.token),
                        )));
              },
              child: ListTile(
                leading: Icon(Icons.add, color: Colors.grey),
                title:
                    Text('Add exercise', style: TextStyle(color: Colors.grey)),
              ),
            )
          ],
        ));
  }
}
//
//Widget buildBottomSheet(BuildContext context){
//  return AddExercise();
//}
