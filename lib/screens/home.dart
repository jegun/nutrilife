import 'dart:async';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:nutrilifeapp/icons.dart';
import 'package:nutrilifeapp/screens/home_loading.dart';
import 'package:nutrilifeapp/widgets/nutrition_home.dart';
import 'package:nutrilifeapp/services/shared_preferences.dart';
import 'package:nutrilifeapp/widgets/choose_food_widget.dart';
import 'package:nutrilifeapp/const.dart';
import 'package:nutrilifeapp/widgets/navigation_bar.dart';
import 'package:nutrilifeapp/widgets/addplate_bottomsheet.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:flutter/services.dart';

NutrilifeSharedPreferences pref = NutrilifeSharedPreferences();
var controller = TextEditingController();

class HomeScreen extends StatefulWidget {
  Map<String, dynamic> selectedFood;
  double inputWeight;
  int state;
  final String userName;
  String userUnit;
  HomeScreen(this.selectedFood, this.state, this.userName, this.userUnit,
      [this.inputWeight]);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final controller = TextEditingController();
  final _keyScaffold = new GlobalKey<ScaffoldState>();
  Map scaleData;
  String weightData;

  String foodName;
  List facts;
  static const platform = const MethodChannel('com.ble.support/testChannel');
  int flag = 0;

  String previousString = '';
 Timer timer;
  bool isConnectionDone = true;
  var scaleVal;

  @override
  void initState() {
    super.initState();
    weightData = widget.inputWeight.toString();
    controller.text = (widget.inputWeight.round()).toString();
    foodName = widget.selectedFood["name"];
    facts = widget.selectedFood["Facts"];
if(Platform.isIOS){
  // timer = Timer.periodic(Duration(seconds: 0), (Timer t) => setState(() {
  //   initSetUp();
  // }

  // ));
}
initSetUp();
  }




  void initSetUp() async {
    if (Platform.isAndroid) {
      flag = await _checkBLEStatus();
    }
    else if (Platform.isIOS) {

      flag = await _checkBLEStatus();

    }
    print("Flag status : " + flag.toString());
    setState(() {
      widget.state = flag;
    });
  }

timerControl(){
    if(isConnectionDone && scaleVal == 0){
      timer = Timer.periodic(Duration(seconds: 0), (Timer t) => setState(() {
        _openBLEMachineList();
      }

      ));
    }
    else if (isConnectionDone && scaleVal == 1){
      timer.cancel();

    }

}

  Widget buildBottomSheet(BuildContext context) {
    return AddPlate(widget.selectedFood["id"], widget.selectedFood["name"],
        widget.inputWeight, widget.userUnit, facts, controller.text);
  }

  @override
  Widget build(BuildContext context) {
    final ProgressDialog pr = ProgressDialog(context);
    pr.hide();
    return WillPopScope(
      onWillPop: () async => false,
      child: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(FocusNode());
        },
        child: Scaffold(
          key: _keyScaffold,
          appBar: AppBar(
            title: Text('Home'),
            leading: Icon(MyFlutterApp.apple_alt, size: 30),
            backgroundColor: kGreenColor,
          ),
          body: Padding(
              padding: EdgeInsets.all(5),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  //todo: insert 3 widgets (weight input, actions and nutrition facts)
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ListTile(
                      title: Row(
                        children: <Widget>[
                          Text('Hello '),
                          Text('${widget.userName}',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontStyle: FontStyle.italic)),
                        ],
                      ),
                      trailing: InkWell(
                        onTap: () async {
                          if (Platform.isAndroid) {
                            await initSetUp();
                            if (flag == 2) {
                              if (widget.selectedFood["name"] !=
                                  "Select your food") {
                                var status = await Permission.location.status;
                                if (status.isGranted) {
                                  setState(() {
                                    scaleVal = 0;
                                    timerControl();

                                  });
                                  _openBLEMachineList();
                                } else {
                                  await Permission.location.request();
                                }
                              } else {
                                ScaffoldMessenger.of(context)
                                    .showSnackBar(SnackBar(
                                  content: Text("Please select a food"),
                                ));
                              }
                            } else {
                              flag = await _bleTurnOn();
                              setState(() {
                                widget.state = flag;
                              });
                              print("OnClick button : " + flag.toString());
                            }
                          }
                          else if (Platform.isIOS){
                            await initSetUp();
                            if (flag == 2) {
                              if (widget.selectedFood["name"] !=
                                  "Select your food") {
                                var status = await Permission.location.status;
                                if (status.isGranted) {
                                  _openBLEMachineList();
                                } else {
                                  await Permission.location.request();
                                }
                              } else {
                                ScaffoldMessenger.of(context)
                                    .showSnackBar(SnackBar(
                                  content: Text("Please select a food"),
                                ));
                              }
                            } else {
                              flag = await _bleTurnOn();
                              setState(() {
                                widget.state = flag;
                              });
                              print("OnClick button : " + flag.toString());
                            }
                          }
                        },
                        child: CircleAvatar(
                          child: Icon(
                              widget.state == 2
                                  ? Icons.bluetooth_connected
                                  : Icons.bluetooth_disabled,
                              color: widget.state == 2
                                  ? Colors.white
                                  : Colors.black),
                          radius: 35,
                          backgroundColor:
                              widget.state == 2 ? kGreenColor : kGreyColor,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Expanded(
                    child: ListView(children: <Widget>[
                      Container(
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: Colors.grey[300],
                          ),
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10),
                        ),
                        margin: EdgeInsets.all(2),
                        child: Column(children: <Widget>[
                          SizedBox(height: 20),
                          ChooseFoodWidget(
                              widget.selectedFood["name"], widget.inputWeight),
                          SizedBox(height: 10),
                          Visibility(
                            visible: false, //widget.state == 2,
                            child: Container(
                              margin: EdgeInsets.only(right: 20),
                              alignment: Alignment.centerRight,
                              child: ElevatedButton(
                                  onPressed: () {
                                    if (widget.selectedFood["name"] !=
                                        "Select your food") {
                                      _openBLEMachineList();

                                    } else {
                                      ScaffoldMessenger.of(context)
                                          .showSnackBar(SnackBar(
                                        content: Text("Please select the food"),
                                      ));
                                    }
                                  },
                                  style: ButtonStyle(
                                      backgroundColor:
                                          MaterialStateProperty.all<Color>(
                                              kGreyColor),
                                      textStyle: MaterialStateProperty.all(
                                          TextStyle(
                                              color: kGreenColor,
                                              fontWeight: FontWeight.bold))),
                                  child: Text(
                                    "Connect Scale",
                                    style: TextStyle(color: kGreenColor),
                                  )),
                            ),
                          ),
                          //WeightInput(widget.inputWeight,getWeightBack, TextEditingController(text: widget.inputWeight.toString())),
                          Container(
                              margin: EdgeInsets.symmetric(horizontal: 20),
                              child: Column(
                                children: <Widget>[
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    children: <Widget>[
                                      SizedBox(height: 20),
                                      Expanded(
                                        child: TextField(
                                          maxLength: 6,
                                          keyboardType: widget.userUnit == 'lb'
                                              ? TextInputType.text
                                              : TextInputType.numberWithOptions(
                                                  decimal: true),
                                          inputFormatters: widget.userUnit ==
                                                  'lb'
                                              ? [
                                                  FilteringTextInputFormatter
                                                      .allow(RegExp(r'[0-9:.]'))
                                                ]
                                              : [
                                                  FilteringTextInputFormatter
                                                      .allow(RegExp(r'[0-9.]'))
                                                ],
                                          onChanged: (value) {
                                            print("value: " + value);
                                            double w = 0;
                                            if (widget.userUnit == 'lb') {
                                              if (previousString == "" &&
                                                  value == ":") {
                                                controller.text = "0:";
                                                controller.selection =
                                                    TextSelection.collapsed(
                                                        offset: controller
                                                            .text.length);
                                                return;
                                              }
                                              if (!previousString
                                                      .contains(":") &&
                                                  value.contains(".")) {
                                                controller.text = "0:0.";
                                                previousString = "0:0.";
                                                controller.selection =
                                                    TextSelection.collapsed(
                                                        offset: controller
                                                            .text.length);
                                                return;
                                              }
                                              if (value.isNotEmpty) {
                                                List<String> lbAoz =
                                                    value.split(":");
                                                print("lbAoz('${value}'): " +
                                                    lbAoz.length.toString());
                                                if (lbAoz.length == 1) {
                                                  try {
                                                    w = double.parse(value) ??
                                                        "0";
                                                  } catch (e) {
                                                    w = 0;
                                                  }
                                                  print(
                                                      "lb => " + w.toString());
                                                } else if (lbAoz.length == 2) {
                                                  double pounds = 0;
                                                  double convertToPoundValue =
                                                      0;
                                                  try {
                                                    pounds =
                                                        double.parse(lbAoz[0]);
                                                  } catch (e) {
                                                    pounds = 0;
                                                  }
                                                  try {
                                                    String oz = lbAoz[1] ?? "0";
                                                    convertToPoundValue =
                                                        double.parse(oz) *
                                                            (1 / 16);
                                                  } catch (e) {
                                                    convertToPoundValue = 0;
                                                  }
                                                  w = pounds +
                                                      convertToPoundValue;
                                                  print("lb&oz => " +
                                                      w.toString());
                                                } else {
                                                  w = 0;
                                                }
                                              }
                                            } else {
                                              try {
                                                w = double.parse(value);
                                              } catch (e) {
                                                w = 0;
                                              }
                                            }
                                            previousString = value;
                                            if (value == "") {
                                              setState(() {
                                                widget.inputWeight = 0;
                                              });
                                            } else {
                                              setState(() {
                                                widget.inputWeight = w;
                                              });
                                            }
                                          },
                                          onTap: () {
                                            try {
                                              if (double.parse(
                                                      controller.text) ==
                                                  0) {
                                                controller.clear();
                                                setState(() {
                                                  timer.cancel();

                                                });
                                              }
                                            } catch (e) {
                                              controller.clear();
                                              setState(() {
                                                timer.cancel();

                                              });
                                            }
                                          },
                                          controller: controller,
                                          textAlign: TextAlign.center,
                                          decoration: InputDecoration(
                                            enabledBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: kGreenColor),
                                            ),
                                            focusedBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: kGreenColor),
                                            ),
                                            border: UnderlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: kGreenColor),
                                            ),

                                            hintText: '0',
                                            suffixText: widget.userUnit == 'lb'
                                                ? 'lb:oz'
                                                : '${widget.userUnit}',
                                            suffixStyle: widget.userUnit == 'lb'
                                                ? TextStyle(
                                                    color: Colors.black54,
                                                    fontSize: 40,
                                                  )
                                                : TextStyle(
                                                    color: Colors.black54,
                                                    fontSize: 60,
                                                  ),
                                          ),
                                          style: TextStyle(
                                            color: Colors.black54,
                                            fontSize: 60,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                ],
                              )),
                          SizedBox(height: 10),
                        ]),
                      ),
                      SizedBox(height: 15),
                      NutritionHome(facts, widget.inputWeight, widget.userUnit),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Expanded(
                            child: Container(
                              decoration: BoxDecoration(
                                border: Border.all(
                                  color: Colors.grey[300],
                                ),
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(10),
                              ),
                              margin: EdgeInsets.all(5),
                              child: GestureDetector(
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Icon(Icons.clear,
                                        size: 50, color: kGreenColor),
                                    Text('Reset'),
                                  ],
                                ),
                                onTap: () async {
                                  String token = await pref.getToken();
                                  setState(() {
                                    Navigator.pushReplacement(
                                        context,
                                        FadeRoute(
                                          page: HomeLoading(
                                              kInitialData, token, 0),
                                        ));
                                  });
//                                      clearTextInput();
                                },
                              ),
                            ),
                          ),
                          Expanded(
                            child: Container(
                              decoration: BoxDecoration(
                                border: Border.all(
                                  color: Colors.grey[300],
                                ),
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(10),
                              ),
                              margin: EdgeInsets.all(5),
                              child: GestureDetector(
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Icon(Icons.add,
                                        size: 50,
                                        color: (widget.selectedFood != null &&
                                                widget.inputWeight != 0)
                                            ? kGreenColor
                                            : Colors.grey),
                                    Text('Add to Diary',
                                        style: TextStyle(
                                            color:
                                                (widget.selectedFood != null &&
                                                        widget.inputWeight != 0)
                                                    ? Colors.black
                                                    : Colors.grey)),
                                  ],
                                ),
                                onTap: () {
                                  if (widget.selectedFood != null &&
                                      widget.inputWeight != 0) {
                                    showModalBottomSheet(
                                        context: context,
                                        builder: buildBottomSheet);
                                  }
                                },
                              ),
                            ),
                          ),
                        ],
                      ),
                    ]),
                  ),
                  NavigationBar(0),
                ],
              )),
        ),
      ),
    );
  }

  Future<int> _checkBLEStatus() async {
    try {
      var sendHashMap = <int, String>{};
      var result =
          await platform.invokeMethod('getBleSupportStatus', sendHashMap);
      debugPrint('Result: $result ');
      Map<int, String> response = Map<int, String>.from(result);
      return int.parse(response[0]);
    } on PlatformException catch (e) {
      debugPrint("Error: '${e.message}'.");
      return null;
    }
  }

  Future<int> _bleTurnOn() async {
    try {
      var result = await platform.invokeMethod('bleTurnOn');
      debugPrint('Result: $result ');
      Map<int, String> response = Map<int, String>.from(result);
      return int.parse(response[0]);
    } on PlatformException catch (e) {
      debugPrint("Error: '${e.message}'.");
      return null;
    }
  }

  Future<Map<String, String>> _openBLEMachineList() async {
    try {
      Map<String, String> sendDetail = Map();
      sendDetail['food'] = foodName;
      sendDetail['unit'] = widget.userUnit;
      var result = await platform.invokeMethod('getBLEDeviceList', sendDetail);
      debugPrint('Result Data from weight activity: $result ');
      if (result != null) {
        Map<String, String> response = Map<String, String>.from(result);
        controller.text = response["1"];
        previousString = response["1"];
        widget.userUnit = response["2"];
        if(widget.userUnit == 'lb') {
          double w = 0;
          if (response["1"].isNotEmpty) {
            List<String> lbAoz =
            response["1"].split(":");
            print("lbAoz('${response["1"]}'): " +
                lbAoz.length.toString());
            if (lbAoz.length == 1) {
              try {
                w = double.parse(response["1"]) ??
                    "0";
              } catch (e) {
                w = 0;
              }
              print(
                  "lb => " + w.toString());
            } else if (lbAoz.length == 2) {
              double pounds = 0;
              double convertToPoundValue =
              0;
              try {
                pounds =
                    double.parse(lbAoz[0]);
              } catch (e) {
                pounds = 0;
              }
              try {
                String oz = lbAoz[1] ?? "0";
                convertToPoundValue =
                    double.parse(oz) *
                        (1 / 16);
              } catch (e) {
                convertToPoundValue = 0;
              }
              w = pounds +
                  convertToPoundValue;
              print("lb&oz => " +
                  w.toString());
            } else {
              w = 0;
            }
          }
          setState(() {
            widget.inputWeight = w;
          });
        } else {
          widget.inputWeight = double.parse(response["1"]);
        }
        print('onResponse: ' + controller.text);
        initSetUp();
        return response;
      } else {
        return null;
      }
    } on PlatformException catch (e) {
      debugPrint("Error: '${e.message}'.");
      return null;
    }
  }

  void showMessage(String content) {
    _keyScaffold.currentState.showSnackBar(SnackBar(content: Text(content),));
  }
}
