import 'package:flutter/material.dart';
import 'package:nutrilifeapp/const.dart';
import 'package:nutrilifeapp/screens/diary.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:nutrilifeapp/services/shared_preferences.dart';
import 'package:nutrilifeapp/services/global.dart';
import 'package:provider/provider.dart' as provider;

NutrilifeSharedPreferences pref = NutrilifeSharedPreferences();

class DiaryLoading extends StatefulWidget {
  DateTime date = DateTime.now();
  String token;
  DiaryLoading(this.date, this.token);
  @override
  _DiaryLoadingState createState() => _DiaryLoadingState();
}

class _DiaryLoadingState extends State<DiaryLoading> {
  getDiaryScreen() async{
    final ProgressDialog pr = ProgressDialog(context);
    pr.update(
      message: "Updating..",
      progressWidget: Container(
          padding: EdgeInsets.all(8.0), child: CircularProgressIndicator(
        valueColor: AlwaysStoppedAnimation<Color>(kGreenColor),
      )),
    );
   pr.show();
    provider.Provider.of<GlobalVariables>(context, listen: false).updateDate(widget.date);
    await provider.Provider.of<GlobalVariables>(context, listen: false).getThemAll(widget.date, widget.token);
   pr.hide();
    Navigator.pushReplacement(context, FadeRoute(
        page: DiaryScreen()
    ));
  }


  @override
  void initState() {
    super.initState();
    Future.delayed(
        Duration(milliseconds: 200),
            () async{
          await     getDiaryScreen();
        }
    );

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
          color:Colors.white,
        )
    );
  }
}

