import 'dart:convert';
import 'package:nutrilifeapp/screens/diary.dart';
import 'package:nutrilifeapp/screens/reset_pass.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nutrilifeapp/const.dart';
import 'package:nutrilifeapp/icons.dart';
import 'package:nutrilifeapp/screens/home_loading.dart';
import 'package:nutrilifeapp/screens/signup_screen.dart';
import 'package:nutrilifeapp/services/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:nutrilifeapp/services/global.dart';
import 'package:provider/provider.dart' as provider;

String email;
String password;

NutrilifeSharedPreferences pref = NutrilifeSharedPreferences();

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  bool isTapped = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    email = "0";
    password = "0";
  }

  @override
  Widget build(BuildContext context) {
    final ProgressDialog pr = ProgressDialog(context);
    return WillPopScope(
        onWillPop: () async => true,
        child: Scaffold(
          //resizeToAvoidBottomPadding: false,
          appBar: AppBar(
            leading: Icon(MyFlutterApp.apple_alt, size: 30),
            title: Text('Login'),
            backgroundColor: kGreenColor,
          ),
          body: Center(
            // color: kGreenColor,
            child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(height: 15),
                  Icon(MyFlutterApp.apple_alt, size: 70, color: kGreenColor),
                  SizedBox(height: 15),
                  Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: TextField(
                      textAlign: TextAlign.start,
                      keyboardType: TextInputType.emailAddress,
                      onChanged: (value) {
                        setState(() {
                          email = value;
                        });
                      },
                      decoration: kInputTextStyle.copyWith(
                        prefixIcon: Icon(Icons.person, color: Colors.grey[400]),
                        labelText: 'Email',
                        labelStyle: TextStyle(color: Colors.grey[400]),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: TextField(
                      obscureText: true,
                      textAlign: TextAlign.start,
                      onChanged: (value) {
                        setState(() {
                          password = value;
                        });
                      },
                      decoration: kInputTextStyle.copyWith(
                        prefixIcon: Icon(Icons.lock, color: Colors.grey[400]),
                        labelText: 'Password',
                        labelStyle: TextStyle(color: Colors.grey[400]),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 14.0,
                  ),
                  Material(
                    color: kGreenColor,
                    borderRadius: BorderRadius.circular(10.0),
                    elevation: 5.0,
                    child: MaterialButton(
                      onPressed: () async {
                        pr.update(
                          message: "Signing In",
                          progressWidget: Container(
                              padding: EdgeInsets.all(8.0),
                              child: CircularProgressIndicator(
                                valueColor:
                                    AlwaysStoppedAnimation<Color>(kGreenColor),
                              )),
                        );
                        //try {
                        var response = await http.post('${kUrl}login', body: {
                          'email': '$email',
                          'password': '$password',
                        });
                        var data = jsonDecode(response.body);
                        if (data['status']) {
                          print(data['access_token']);
                          pr.show();
                          var dailyGoal = jsonDecode(data['user']['dailyGoal']);
                          await pref.setIsLogged(data['status']);
                          await pref.setUserID(data['user']['id']);
                          await pref.setToken(data['access_token']);
                          await pref.setWeeklyGoal(data['user']['weeklyGoal']);
                          await pref.setUserActivityLevel(
                              data['user']['activityLevel']);
                          DateTime date =
                              DateTime.parse(data['user']['birthDate']);
                          if (date.month < 10) {
                            if (date.day < 10) {
                              await pref.setUserBirthDate(
                                  '${date.year}-0${date.month}-0${date.day}');
                            } else {
                              await pref.setUserBirthDate(
                                  '${date.year}-0${date.month}-${date.day}');
                            }
                          } else {
                            if (date.day < 10) {
                              await pref.setUserBirthDate(
                                  '${date.year}-${date.month}-0${date.day}');
                            } else {
                              await pref.setUserBirthDate(
                                  '${date.year}-${date.month}-${date.day}');
                            }
                          }
                          await pref
                              .setUserDailyCalories(dailyGoal['calories']);
                          await pref.setUserGender(data['user']['gender']);
                          int userHeight = (data['user']['height']).round();
                          await pref.setUserHeight(userHeight);
                          await pref.setUserName(data['user']['name']);
                          int userWeight =
                              (data['user']['currentWeight']).round();
                          await pref.setUserWeight(userWeight);
                          await pref
                              .setUserGainWeight(data['user']['gainWeight']);
                          await pref
                              .setUserLoseWeight(data['user']['loseWeight']);
                          await pref.setUserMeasureUnit(
                              data['user']['measurementUnit']);
                          await pref
                              .setUserHeightUnit(data['user']['heightUnit']);
                          await pref
                              .setUserWeightUnit(data['user']['weightUnit']);
                          await pref.setUserEmail(data['user']['email']);
                          await pref.setUserPassword(data['user']['password']);
                          await pref.setUserPolicy(data['user']['policy']);
                          await pref.setUserReceive(data['user']['receive']);
                          List<String> emptyList = [];
                          await pref.setLastChosen(emptyList);
                          provider.Provider.of<GlobalVariables>(context,
                                  listen: false)
                              .updateToken(data['access_token']);
                          await provider.Provider.of<GlobalVariables>(context,
                                  listen: false)
                              .getThemAll(DateTime.now(), data['access_token']);
                          await provider.Provider.of<GlobalVariables>(context,
                                  listen: false)
                              .getHistoryAll(
                                  DateTime.now().subtract(Duration(days: 7)),
                                  DateTime.now());
                          await provider.Provider.of<GlobalVariables>(context,
                                  listen: false)
                              .getFavFoodDa(1);
                          await provider.Provider.of<GlobalVariables>(context,
                                  listen: false)
                              .getMyFoodDa();
                          pr.hide();
                          Future.delayed(Duration(milliseconds: 200), () {
                            Navigator.push(
                                context,
                                FadeRoute(
                                  page: HomeLoading(
                                      kInitialData, data['access_token'], 0),
                                ));
                          });
                        } else {
                          wrongAlert(context, 'Invalid username or password');
                        }
                        // } catch (e) {
                        //   print('Error is $e');
                        // }
                      },
                      minWidth: 200.0,
                      height: 42.0,
                      child: Text(
                        'Login',
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 20),
                  Row(
                    children: <Widget>[
                      Expanded(child: Container()),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          GestureDetector(
                            child: Text(
                              'Do not have an account? Register Now',
                              style: TextStyle(
                                  fontSize: 10,
                                  color: kGreenColor,
                                  fontWeight: FontWeight.w600),
                            ),
                            onTap: () {
                              Navigator.pushReplacement(
                                  context,
                                  FadeRoute(
                                    page: SignUpScreen(),
                                  ));
                            },
                          ),
                          SizedBox(height: 15),
                          Visibility(
                            visible: false,
                            child: GestureDetector(
                              child: Text(
                                'Forgot password?',
                                style: TextStyle(
                                    fontSize: 10,
                                    color: kGreenColor,
                                    fontWeight: FontWeight.w600),
                              ),
                              onTap: () {
                                Navigator.pushReplacement(
                                    context,
                                    FadeRoute(
                                      page: ResetPass(),
                                    ));
                              },
                            ),
                          ),
                        ],
                      ),
                      Expanded(child: Container()),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}
