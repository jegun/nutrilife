import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:nutrilifeapp/const.dart';
import 'package:nutrilifeapp/screens/me_screen.dart';
import 'package:nutrilifeapp/services/shared_preferences.dart';

class MeLoading extends StatefulWidget {
  @override
  _MeLoadingState createState() => _MeLoadingState();
}

class _MeLoadingState extends State<MeLoading> {

  getUserName() async{
    NutrilifeSharedPreferences preference = NutrilifeSharedPreferences();
    String userName = await preference.getUserName();
    double dailyCalories = await preference.getUserDailyCalories();
    String token = await preference.getToken();
    Navigator.push(context, FadeRoute(
        page: MeScreen(userName, dailyCalories, token)
    ));
  }


  @override
  void initState() {
    super.initState();
    getUserName();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
          color:Colors.white,
            )
    );
  }
}

