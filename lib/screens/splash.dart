import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nutrilifeapp/const.dart';
import 'package:nutrilifeapp/icons.dart';
import 'package:nutrilifeapp/screens/home_loading.dart';
import 'package:nutrilifeapp/screens/welcome.dart';
import 'dart:async';
import 'package:nutrilifeapp/services/shared_preferences.dart';
import 'package:nutrilifeapp/services/global.dart';
import 'package:provider/provider.dart' as provider;

NutrilifeSharedPreferences pref = NutrilifeSharedPreferences();

class Splash extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyStatefulWidget(),
    );
  }
}

class SpinningContainer extends AnimatedWidget {
  const SpinningContainer({Key key, AnimationController controller})
      : super(key: key, listenable: controller);

  Animation<double> get _scale => listenable;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: kGreenColor,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon( MyFlutterApp.apple_alt,
                  size: 90,
                  color: Colors.white),
            SizedBox(height: 10,),
            ],
        )
    );
  }
}

class MyStatefulWidget extends StatefulWidget {
  MyStatefulWidget({Key key}) : super(key: key);

  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<MyStatefulWidget>
    with TickerProviderStateMixin {
  AnimationController _controller;

  isLoggedCheck() async{
    bool isLogged = await pref.getIsLogged();
    return isLogged;
  }

  navigation() async{
    bool x = await isLoggedCheck();
    print(x);
    if(x) {
      String token = await pref.getToken();
      print('Token is $token');
      List<String> laChosen = await pref.getLastChosen();
      Future.delayed(
          Duration(seconds: 2),
              () async{
            provider.Provider.of<GlobalVariables>(context, listen: false).updateToken(token);
            await provider.Provider.of<GlobalVariables>(context, listen: false).getThemAll(DateTime.now(), token);
            await provider.Provider.of<GlobalVariables>(context, listen: false).getHistoryAll(DateTime.now().subtract(Duration(days: 7)), DateTime.now());
            await provider.Provider.of<GlobalVariables>(context, listen: false).getFavFoodDa(1);
            await provider.Provider.of<GlobalVariables>(context, listen: false).getMyFoodDa();
            await  provider.Provider.of<GlobalVariables>(context, listen: false).getRecentFood(laChosen);
            Navigator.push(context, FadeRoute(
              page: HomeLoading(kInitialData,token, 0),
            ));
          }
      );
    } else {
      Future.delayed(
          Duration(seconds: 2),
              () {
            Navigator.push(context, FadeRoute(
              page: WeclomeScreen(),
            ));
          }
      );
    }
  }

  @override
  void initState() {
    super.initState();
    navigation();
    _controller = AnimationController(
      duration: const Duration(seconds: 5),
      vsync: this,
    )..repeat();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SpinningContainer(controller: _controller);
  }
}
