import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nutrilifeapp/widgets/singleNutrition_chart.dart';
import 'package:nutrilifeapp/const.dart';

class NutritionalAnalysis extends StatefulWidget {
  final List<dynamic> allFood;
  final DateTime chosenDate;
  final double waterSum;
  Map<String, dynamic> dailyGoal;
  NutritionalAnalysis(this.allFood, this.chosenDate, this.waterSum, this.dailyGoal);
  @override
  _NutritionalAnalysisState createState() => _NutritionalAnalysisState();
}

class _NutritionalAnalysisState extends State<NutritionalAnalysis> {

  getGoal(String fact){
    String value;
    value = widget.dailyGoal['$fact'].toString();
    return value;
  }

  getValue(String fact){
    double value = 0;
    for(int i = 0; i < widget.allFood.length; i++){
      Map singleFood = widget.allFood[i];
      //value = value + double.parse(singleFood['$fact']);
      value = value +
          (double.parse(singleFood['weight']) /
              100 *
              double.parse(singleFood['$fact']));
    }
    return value;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print(widget.dailyGoal);
  }
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          leading: GestureDetector(
            onTap: (){Navigator.pop(context);},
            child: Icon(Icons.clear, size: 30),),
          backgroundColor: kGreenColor,
        ),
        body: Padding(
            padding: EdgeInsets.all(5),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                //todo: insert 3 widgets (weight input, actions and nutrition facts)
                SizedBox(height: 8,),
                Text('Nutritional Analysis', style: TextStyle(color: kGreenColor, fontSize: 20, fontWeight: FontWeight.w400)),
                SizedBox(height: 5,),
                Text(
                    (widget.chosenDate.day == DateTime.now().day && widget.chosenDate.month == DateTime.now().month && widget.chosenDate.year == DateTime.now().year) ? 'Today' : '${widget.chosenDate.year}-${widget.chosenDate.month}-${widget.chosenDate.day}',
                    style: TextStyle(fontSize: 15, fontWeight: FontWeight.w400)),
                SizedBox(height: 5,),
                Expanded(
                  child: Scrollbar(
                    child: ListView(
                      children: <Widget>[
                        NutritionProgress(widget.allFood,  widget.dailyGoal, 'Calories (kcal)',double.parse(getGoal('calories')),getValue('calories'),true),
                        NutritionProgress(widget.allFood, widget.dailyGoal,'Protein (g)',double.parse(getGoal('protein')),getValue('protein'),true),
                        NutritionProgress(widget.allFood, widget.dailyGoal,'Fat (g)',double.parse(getGoal('fat')),getValue('fat'),true),
                        NutritionProgress(widget.allFood, widget.dailyGoal,'Carbohydrates (g)',double.parse(getGoal('carbohydrates')),getValue('carbohydrates'),true),
                        NutritionProgress(widget.allFood, widget.dailyGoal,'Fibre (g)',double.parse(getGoal('fiber')),getValue('fibre'),true),
                        NutritionProgress(widget.allFood, widget.dailyGoal,'Vitamin A (IU)',double.parse(getGoal('vitamin a')),getValue('vitaminA'),true),
                        NutritionProgress(widget.allFood, widget.dailyGoal,'Carotene (µg)',double.parse(getGoal('carotene')),getValue('carotene'),true),
                        NutritionProgress(widget.allFood, widget.dailyGoal,'Vitamin B6 (mg)',double.parse(getGoal('vitamin b6')),getValue('vitaminB6'),true),
                        NutritionProgress(widget.allFood, widget.dailyGoal,'Vitamin B12 (µg)',double.parse(getGoal('vitamin b12')),getValue('vitaminB12'),true),
                        NutritionProgress(widget.allFood, widget.dailyGoal,'Vitamin B3 (mg)',double.parse(getGoal('vitamin b3')),getValue('vitaminB3'),true),
                        NutritionProgress(widget.allFood, widget.dailyGoal,'Vitamin C (mg)',double.parse(getGoal('vitamin c')),getValue('vitaminC'),true),
                        NutritionProgress(widget.allFood, widget.dailyGoal,'Vitamin E (IU)',double.parse(getGoal('vitamin e')),getValue('vitaminE'),true),
                        NutritionProgress(widget.allFood, widget.dailyGoal,'Cholesterol (mg)',double.parse(getGoal('cholestrol')),getValue('cholesterol'),true),
                        NutritionProgress(widget.allFood, widget.dailyGoal,'Potassium (mg)',double.parse(getGoal('potassium')),getValue('potassium'),true),
                        NutritionProgress(widget.allFood, widget.dailyGoal,'Sodium (mg)',double.parse(getGoal('sodium')),getValue('sodium'),true),
                        NutritionProgress(widget.allFood, widget.dailyGoal,'Calcium (mg)',double.parse(getGoal('calcium')),getValue('calcium'),true),
                        NutritionProgress(widget.allFood, widget.dailyGoal,'Magnesium (mg)',double.parse(getGoal('magnesium')),getValue('magnesium'),true),
                        NutritionProgress(widget.allFood, widget.dailyGoal,'Iron (mg)',double.parse(getGoal('iron')),getValue('iron'),true),
                        NutritionProgress(widget.allFood, widget.dailyGoal,'Manganese (mg)',double.parse(getGoal('manganese')),getValue('manganese'),true),
                        NutritionProgress(widget.allFood, widget.dailyGoal,'Zinc (mg)', double.parse(getGoal('zinc')),getValue('zinc'),true),
                        NutritionProgress(widget.allFood, widget.dailyGoal,'Copper (mg)',double.parse(getGoal('copper')),getValue('copper'),true),
                        NutritionProgress(widget.allFood, widget.dailyGoal,'Phosphorus (mg)',double.parse(getGoal('phosphorus')),getValue('phosphorus'),true),
                        NutritionProgress(widget.allFood, widget.dailyGoal,'Selenium (µg)',double.parse(getGoal('selenium')),getValue('selenium'),true),
                        NutritionProgress(widget.allFood, widget.dailyGoal,'Water (ml)',double.parse(getGoal('water'))*1000,widget.waterSum,false),
                      ],
                    ),
                  ),
                ),
              ],
            )
        ),
      ),
    );
  }
}

class NutritionProgress extends StatefulWidget {
  final List<dynamic> allFood;
  Map<String, dynamic> dailyGoal;
  final double total;
  final double goal;
  final String name;
  final bool hasPie;
  NutritionProgress(this.allFood, this.dailyGoal, this.name, this.goal, this.total, this.hasPie);

  @override
  _NutritionProgressState createState() => _NutritionProgressState();
}

class _NutritionProgressState extends State<NutritionProgress> {

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        if(widget.hasPie) {
          Navigator.push(
            context,
            FadeRoute(
                page:SingleNutritionChart(widget.allFood, widget.dailyGoal, widget.name, widget.total, widget.goal)),
          );
        }
      },
      child: Container(
        height: 60,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          border: Border.all(
            color: Colors.grey[300],
          ),
        ),
        margin: EdgeInsets.all(5),
        padding: EdgeInsets.all(7),
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Expanded(
                  child: Text(''),
                ),
                Expanded(
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: Text('Total', style: TextStyle(fontSize:11)),
                        ),
                        Expanded(
                          child: Text('Goal', style: TextStyle(fontSize:11)),
                        ),
                        Expanded(
                          child: Text('Left', style: TextStyle(fontSize:11)),
                        ),
                      ],
                    )
                ),
              ],
            ),
            Row(
              children: <Widget>[
                Expanded(
                  child: Text('${widget.name}', style: TextStyle(fontSize: 11)),
                ),
                Expanded(
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: Text('${widget.total.round()}'),
                      ),
                      Expanded(
                        child: (widget.name == "Cholesterol (mg)" || widget.name == "Trans Fats (g)" || widget.name == "Saturated Fats (g)") ? Text('<${widget.goal.round()}') : Text('${widget.goal.round()}'),
                      ),
                      Expanded(
                        child: Text('${(widget.goal - widget.total).round()}', style: TextStyle(color: (widget.goal - widget.total <0) ? Colors.red : Colors.black)),
                      ),
                    ],
                  )
                ),
              ],
            ),
            SizedBox(height: 3,),
            LinearProgressIndicator(
              backgroundColor: kGreyColor,
              value: widget.total/widget.goal,
              valueColor: (widget.total/widget.goal <= 1) ? AlwaysStoppedAnimation<Color>(kGreenColor) : AlwaysStoppedAnimation<Color>(Colors.red),
            ),
          ],
        )
      ),
    );
  }
}
