import 'package:flutter/material.dart';
import 'package:nutrilifeapp/const.dart';
import 'package:nutrilifeapp/screens/home.dart';
import 'package:nutrilifeapp/services/shared_preferences.dart';

NutrilifeSharedPreferences pref = NutrilifeSharedPreferences();

class HomeLoading extends StatefulWidget {
  Map<String, dynamic> selectedFood;
  double inputWeight;
  DateTime date = DateTime.now();
  String token;
  HomeLoading(this.selectedFood, this.token, [this.inputWeight]);

  @override
  _HomeLoadingState createState() => _HomeLoadingState();
}

class _HomeLoadingState extends State<HomeLoading> {
//  FlutterBlue flutterBlue = FlutterBlue.instance;

  getUserName() async {
    NutrilifeSharedPreferences preference = NutrilifeSharedPreferences();
    String userName = await preference.getUserName();
    String userUnit = await preference.getUserMeasureUnit();
    Navigator.push(
        context,
        FadeRoute(
            page: HomeScreen(widget.selectedFood, 0, userName, userUnit,
                widget.inputWeight)));
  }

  @override
  void initState() {
    super.initState();
    getUserName();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            // color:Colors.white,
            ));
  }
}
