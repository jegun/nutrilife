import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nutrilifeapp/const.dart';
import 'package:nutrilifeapp/icons.dart';
import 'package:nutrilifeapp/screens/home_loading.dart';
import 'package:nutrilifeapp/widgets/addmyfood_bottomsheet.dart';
import 'package:nutrilifeapp/widgets/editmyfood_bottomsheet.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:nutrilifeapp/services/shared_preferences.dart';
import 'package:nutrilifeapp/services/global.dart';
import 'package:provider/provider.dart' as provider;


NutrilifeSharedPreferences pref = NutrilifeSharedPreferences();

Map<String, dynamic> foodItem;
bool isTabbed = false;
List foodDa = [];
double weightDa;
int listPage = 1;
int favListPage = 1;
String searchValue = "";
bool isNotSearch = true;

class ChooseFood extends StatefulWidget {
  @override
  _ChooseFoodState createState() => _ChooseFoodState();
  final double weight;
  int page;
  ChooseFood(this.weight, this.page);
}


getFoodBrand(int position){
  foodItem = foodDa[position];
  String brand = foodItem['brand_owner'];
    return brand;
  }


String getFoodName(int foodId){
  foodItem = foodDa[foodId];
  return foodItem["name"];
}


String getMyFoodName(BuildContext context, int foodId){
  foodItem = provider.Provider.of<GlobalVariables>(context, listen: false).myFood[foodId];
  return foodItem["name"];
}


String getFavName(BuildContext context, int foodId){
  foodItem = provider.Provider.of<GlobalVariables>(context, listen: false).favFood[foodId];
  return foodItem["name"];
}


String getFavBrand(BuildContext context, int foodId){
  foodItem = provider.Provider.of<GlobalVariables>(context, listen: false).favFood[foodId];
  return foodItem["brand_owner"];
}

String getRecentName(BuildContext context, int foodId){
  foodItem = provider.Provider.of<GlobalVariables>(context, listen: false).recentFood[foodId];
  return foodItem["name"];
}


String getRecentBrand(BuildContext context, int foodId){
  foodItem = provider.Provider.of<GlobalVariables>(context, listen: false).recentFood[foodId];
  return foodItem["brand_owner"];
}

class _ChooseFoodState extends State<ChooseFood> {

  final _scaffoldKey = GlobalKey<ScaffoldState>();

  getFavColor(int position){
    Color color;
    if(foodDa[position]['in_favorite'] == 'false'){
      color = Colors.grey;
    } else {
      color = kGreenColor;
    }
    return color;
  }

  clearHistory() async{
    ProgressDialog pr = ProgressDialog(context);
    pr.update(
      message: "Loading..",
      progressWidget: Container(
          padding: EdgeInsets.all(8.0),
          child: CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation<Color>(
                kGreenColor),
          )),
    );
    pr.show();
    List<String> emptyList =[];
    await pref.setLastChosen(emptyList);
    Future.delayed(
        Duration(milliseconds: 1500),
            () {
          pr.hide();
        }
    );
  }

getSearchResults(String searchVal, int listIndex)async{
  ProgressDialog pr = ProgressDialog(context);
  String x = searchVal;
  foodDa = [];
  if (x.length >= 3) {
    pr.update(
      message: "Loading..",
      progressWidget: Container(
          padding: EdgeInsets.all(8.0),
          child: CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation<Color>(
                kGreenColor),
          )),
    );
    pr.show();
    String token = await pref.getToken();
    List<String> dataList = searchVal.split(' ');
    print(dataList);
    var response;
    if(dataList.length == 1){
      response = await http.get(
          '${kUrl}getOneFood?data1=$searchVal&page=$listIndex',
          headers: {
            'Accept': 'application/json',
            'Authorization' : 'Bearer $token',
          }
      );
    }
    else if(dataList.length == 2){
      response = await http.get(
          '${kUrl}getTowFood?data1=${dataList[0]}&data2=${dataList[1]}&page=$listIndex',
          headers: {
            'Accept': 'application/json',
            'Authorization' : 'Bearer $token',
          }
      );

    } else if(dataList.length == 3){
      response = await http.get(
          '${kUrl}getThreeFood?data1=${dataList[0]}&data2=${dataList[1]}&data3=${dataList[2]}&page=$listIndex',
          headers: {
            'Accept': 'application/json',
            'Authorization' : 'Bearer $token',
          }
      );
    } else if(dataList.length >= 4) {
      response = await http.get(
          '${kUrl}getFourFood?data1=${dataList[0]}&data2=${dataList[1]}&data3=${dataList[2]}&data4=${dataList[3]}&page=$listIndex',
          headers: {
            'Accept': 'application/json',
            'Authorization' : 'Bearer $token',
          }
      );
    }
    var data = jsonDecode(response.body);
    print(response.body);
    List<dynamic> foodItem = data['data']['data'];
    if (foodItem.isEmpty){
      pr.hide();
      print('this is done');
      final snackBar = SnackBar(
        content: Container(
            height: 30,
            child: Center(child: Text((listIndex != 1) ? 'No more results!' : 'No results were found!', style: TextStyle(fontSize: 15, fontWeight: FontWeight.w600)))),
        backgroundColor: Colors.red,
        duration: Duration(seconds: 2),
      );
      _scaffoldKey.currentState.showSnackBar(snackBar);
    } else {
      setState(() {
        isNotSearch = false;
        foodDa.addAll(foodItem);
        pr.hide();
      });
    }
  } else if(x.length == 0) {
   setState(() {
     isNotSearch = true;
   });
  }
  else {
    final snackBar = SnackBar(
      content: Container(
          height: 30,
          child: Center(child: Text('Enter at least 3 letters!', style: TextStyle(fontSize: 15, fontWeight: FontWeight.w600)))),
      backgroundColor: Colors.red,
      duration: Duration(seconds: 1),
    );
    _scaffoldKey.currentState.showSnackBar(snackBar);
//
  }
}


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if(widget.page == 0){
      foodDa = [];
      isNotSearch = true;
    }
  weightDa = widget.weight;
 }

  @override
  Widget build(BuildContext context){
    ProgressDialog pr = ProgressDialog(context);
    return WillPopScope(
      onWillPop: () async => false,
      child: SafeArea(
        child: Scaffold(
          key: _scaffoldKey,
          appBar: AppBar(
            leading: Icon(MyFlutterApp.apple_alt, size: 30),
            backgroundColor: kGreenColor,
          ),
          body: Column(
            children: <Widget>[
              Padding(
                  padding: EdgeInsets.only(top:10),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: GestureDetector(
                          onTap: () async{
                            setState(() {
                              widget.page=0;
                            });
                          },
                          child: Container(
                            child: Column(
                              // crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Icon(Icons.format_list_bulleted, color: (widget.page==0)? Colors.black: Colors.grey),
                                  Text('All Food', style: TextStyle(color: (widget.page==0)? Colors.black : Colors.grey)),
                                ]
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        child: GestureDetector(
                          onTap: () async{
                            setState(() {
                              widget.page=1;
                              favListPage = 1;
                            });
                          },
                          child: Container(
                            child: Column(
                              // crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Icon(Icons.favorite, color: (widget.page==1) ? Colors.black: Colors.grey),
                                  Text('Favorites', style: TextStyle(color: (widget.page==1) ? Colors.black : Colors.grey)),
                                ]
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        child: GestureDetector(
                          onTap: () async{
                            setState(() {
                              widget.page=2;
                            });
                          },
                          child: Container(
                            child: Column(
                              //  crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Icon(Icons.person, color: (widget.page==2) ? Colors.black: Colors.grey),
                                  Text('My Food', style: TextStyle(color: (widget.page==2) ? Colors.black: Colors.grey)),
                                ]
                            ),
                          ),
                        ),
                      ),
                    ]

                  ),
                ),
              (widget.page==0) ? Expanded(
                  child: Column(
                   // crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.all(7),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all(
                            color: Colors.grey[300],
                          ),
                        ),
                        margin: EdgeInsets.all(15),
                        child: TextField(
                          onSubmitted: (value) async {
                            setState(() {
                              isNotSearch = false;
                              searchValue = value;
                              foodDa = [];
                              listPage = 1;
                            });
                           await getSearchResults(searchValue, 1);
                          },
                          decoration: InputDecoration(
                            icon: Icon(Icons.search),
                            hintText: 'Type food name',
                            border: InputBorder.none,
                          ),
                        ),
                      ),
                      Expanded(
                        child: Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: Colors.grey[300],
                            ),
                          ),
                          margin: EdgeInsets.all(15),
                          child: (isNotSearch) ?
                          ListView.builder
                            (
                              itemCount: provider.Provider.of<GlobalVariables>(context).recentFood.length,
                              itemBuilder: (BuildContext context, int position) {
                                return  ListTile(
                                  title: GestureDetector(
                                      onTap: () async{
                                        String token = await pref.getToken();
                                        Navigator.pushReplacement(context, FadeRoute(
                                            page: HomeLoading(provider.Provider.of<GlobalVariables>(context, listen: false).recentFood[position],token,weightDa)
                                        ));
                                      },
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Text('${getRecentName(context, position)}', style: TextStyle(color: kGreenColor, fontSize: 14)),
                                          Text('${getRecentBrand(context, position)}', style: TextStyle(color: Colors.grey, fontSize: 12)),
                                        ],)
                                  ),
                                  trailing: GestureDetector(
                                    onTap: () async{
                                      if(!provider.Provider.of<GlobalVariables>(context).recentFood[position]["in_favorite"]) {
                                        provider.Provider.of<GlobalVariables>(context, listen: false).clearFavFood();
                                        pr.update(
                                          message: "Adding..",
                                          progressWidget: Container(
                                              padding: EdgeInsets.all(8.0),
                                              child: CircularProgressIndicator(
                                                valueColor: AlwaysStoppedAnimation<
                                                    Color>(
                                                    kGreenColor),
                                              )),
                                        );
                                        pr.show();
                                        String token = await pref.getToken();
                                        var response = await http.post(
                                          '${kUrl}addFavorite/food_id/${provider.Provider.of<GlobalVariables>(context, listen: false).recentFood[position]['id']}',
                                          headers: {
                                            'Accept': 'application/json',
                                            'Authorization': 'Bearer $token',
                                          },
                                        );
                                        await provider.Provider.of<GlobalVariables>(context, listen: false).getFavFoodDa(1);
                                        List<String> recentList = await pref.getLastChosen();
                                        await  provider.Provider.of<GlobalVariables>(context, listen: false).getRecentFood(recentList);
                                        pr.hide();
                                        showDialog(
                                          context: context,
                                          builder: (BuildContext context) {
                                            // return object of type Dialog
                                            return AlertDialog(
                                              title: Row(children: <Widget>[
                                                Icon(Icons.check,
                                                    color: kGreenColor),
                                                SizedBox(width: 5),
                                                Text("Nutrilife",
                                                    style: TextStyle(
                                                        color: kGreenColor)),
                                              ]),
                                              content: new Text(
                                                  'Added to your favorite foods successfully!'),
                                              actions: <Widget>[
                                                // usually buttons at the bottom of the dialog
                                                FlatButton(
                                                  child: new Text("Close",
                                                      style: TextStyle(
                                                          color: kGreenColor)),
                                                  onPressed: () async {
                                                    int count = 0;
                                                    Navigator.popUntil(context, (route) { return count++ == 1; });
                                                    setState(() {
                                                      favListPage = 1;
                                                      widget.page =1;
                                                      // favFood = [];
                                                    });
                                                  },
                                                ),
                                              ],
                                            );
                                          },
                                        );
                                        print(response.body);
                                      } else {
                                        showDialog(
                                          context: context,
                                          builder: (BuildContext context) {
                                            // return object of type Dialog
                                            return AlertDialog(
                                              title: Row(children: <Widget>[
                                                Icon(Icons.live_help, color: kGreenColor),
                                                SizedBox(width:5),
                                                Text("Nutrilife", style: TextStyle(color: kGreenColor)),
                                              ]),
                                              content: new Text('Remove ${getRecentName(context, position)} from your favorite list?'),
                                              actions: <Widget>[
                                                // usually buttons at the bottom of the dialog
                                                FlatButton(
                                                  child: new Text("Yes", style: TextStyle(color: kGreenColor)),
                                                  onPressed: () async{
                                                    provider.Provider.of<GlobalVariables>(context, listen: false).clearFavFood();
                                                    pr.update(
                                                      message: "Removing..",
                                                      progressWidget: Container(
                                                          padding: EdgeInsets.all(8.0),
                                                          child: CircularProgressIndicator(
                                                            valueColor: AlwaysStoppedAnimation<Color>(
                                                                kGreenColor),
                                                          )),
                                                    );
                                                    pr.show();
                                                    String token = await pref.getToken();
                                                    var response = await http.post(
                                                      '${kUrl}deleteFavorite/food_id/${provider.Provider.of<GlobalVariables>(context, listen: false).recentFood[position]['id']}',
                                                      headers: {
                                                        'Accept' : 'application/json',
                                                        'Authorization' : 'Bearer $token',
                                                      },
                                                    );
                                                    await provider.Provider.of<GlobalVariables>(context, listen: false).getFavFoodDa(1);
                                                    List<String> recentList2 = await pref.getLastChosen();
                                                    await  provider.Provider.of<GlobalVariables>(context, listen: false).getRecentFood(recentList2);
                                                    pr.hide();
                                                    showDialog(
                                                      context: context,
                                                      builder: (BuildContext context) {
                                                        // return object of type Dialog
                                                        return AlertDialog(
                                                          title: Row(children: <Widget>[
                                                            Icon(Icons.check, color: kGreenColor),
                                                            SizedBox(width:5),
                                                            Text("Nutrilife", style: TextStyle(color: kGreenColor)),
                                                          ]),
                                                          content: Text('Removed from your favorite foods successfully!'),
                                                          actions: <Widget>[
                                                            // usually buttons at the bottom of the dialog
                                                            FlatButton(
                                                              child: Text("Close", style: TextStyle(color: kGreenColor)),
                                                              onPressed: () async {
                                                                int count = 0;
                                                                Navigator.popUntil(context, (route) { return count++ == 2; });
                                                                setState(() {
                                                                  favListPage = 1;
                                                                  // favFood = [];
                                                                  widget.page =1;
                                                                });
                                                              },
                                                            ),
                                                          ],
                                                        );
                                                      },
                                                    );
                                                    print(response.body);
                                                  },
                                                ),
                                                FlatButton(
                                                  child: new Text("Cancel", style: TextStyle(color: Colors.grey)),
                                                  onPressed: () {
                                                    Navigator.pop(context);
                                                  },
                                                ),
                                              ],
                                            );
                                          },
                                        );
                                      }
                                    },
                                    child: Icon(Icons.favorite,
                                      color: (provider.Provider.of<GlobalVariables>(context).recentFood[position]['in_favorite']) ? kGreenColor : Colors.grey,
                                    ),
                                  ),
                                );
                              }
                          )

                           : ListView.builder
                            (
                              itemCount: foodDa.length +1,
                              itemBuilder: (BuildContext context, int position) {
                              return  (position == foodDa.length) ?
                              Container(
                                height: (foodDa.length != 0 && int.tryParse((foodDa.length/10).toString().split('.')[1].substring(0,1)) == 0) ? 50 : 0,
                                padding: EdgeInsets.all(5),
                                color: Colors.white,
                                child: FlatButton(
                                  child: Text("Load More", style: TextStyle(color: kGreenColor)),
                                  onPressed: () async{
                                    setState(() {
                                      listPage++;
                                    });
                                    await getSearchResults(searchValue, listPage);
                                  },
                                ),
                              )
                                  : ListTile(
                                  title: GestureDetector(
                                      onTap: () async{
                                        pr.update(
                                          message: "Adding..",
                                          progressWidget: Container(
                                              padding: EdgeInsets.all(8.0),
                                              child: CircularProgressIndicator(
                                                valueColor: AlwaysStoppedAnimation<
                                                    Color>(
                                                    kGreenColor),
                                              )),
                                        );
                                        pr.show();
                                         String val = '${foodDa[position]['id']}';
                                         List<String> laChosen = await pref.getLastChosen();
                                         laChosen.insert(0,val);
                                         List<String> lastChosenData = [];
                                         if(laChosen.length >=10) {
                                           for (int i = 0; i < 10; i++) {
                                             lastChosenData.insert(
                                                 i, laChosen[i]);
                                           }
                                           await  pref.setLastChosen(lastChosenData);
                                         } else {
                                           await  pref.setLastChosen(laChosen);
                                         }
                                         await  provider.Provider.of<GlobalVariables>(context, listen: false).addToRecent(val);
                                       String token = await pref.getToken();
                                       pr.hide();
                                        Navigator.pushReplacement(context, FadeRoute(
                                            page: HomeLoading(foodDa[position],token,weightDa)
                                        ));
                                      },
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: <Widget>[
                                        Text('${getFoodName(position)}', style: TextStyle(color: kGreenColor, fontSize: 14)),
                                        Text('${getFoodBrand(position)}', style: TextStyle(color: Colors.grey, fontSize: 12)),
                                      ],)
                                  ),
                                  trailing: GestureDetector(
                                    onTap: () async{
                                      if(!foodDa[position]["in_favorite"]) {
                                        provider.Provider.of<GlobalVariables>(context, listen: false).clearFavFood();
                                        pr.update(
                                          message: "Adding..",
                                          progressWidget: Container(
                                              padding: EdgeInsets.all(8.0),
                                              child: CircularProgressIndicator(
                                                valueColor: AlwaysStoppedAnimation<
                                                    Color>(
                                                    kGreenColor),
                                              )),
                                        );
                                        pr.show();
                                        String token = await pref.getToken();
                                        var response = await http.post(
                                          '${kUrl}addFavorite/food_id/${foodDa[position]['id']}',
                                          headers: {
                                            'Accept': 'application/json',
                                            'Authorization': 'Bearer $token',
                                          },
                                        );
                                        await provider.Provider.of<GlobalVariables>(context, listen: false).getFavFoodDa(1);
                                        pr.hide();
                                        showDialog(
                                          context: context,
                                          builder: (BuildContext context) {
                                            // return object of type Dialog
                                            return AlertDialog(
                                              title: Row(children: <Widget>[
                                                Icon(Icons.check,
                                                    color: kGreenColor),
                                                SizedBox(width: 5),
                                                Text("Nutrilife",
                                                    style: TextStyle(
                                                        color: kGreenColor)),
                                              ]),
                                              content: new Text(
                                                  'Added to your favorite foods successfully!'),
                                              actions: <Widget>[
                                                // usually buttons at the bottom of the dialog
                                                FlatButton(
                                                  child: new Text("Close",
                                                      style: TextStyle(
                                                          color: kGreenColor)),
                                                  onPressed: () async {
                                                    int count = 0;
                                                    Navigator.popUntil(context, (route) { return count++ == 1; });
                                                    setState(() {
                                                      favListPage = 1;
                                                      widget.page =1;
                                                      // favFood = [];
                                                    });
                                                  },
                                                ),
                                              ],
                                            );
                                          },
                                        );
                                        print(response.body);
                                      } else {
                                        showDialog(
                                          context: context,
                                          builder: (BuildContext context) {
                                            // return object of type Dialog
                                            return AlertDialog(
                                              title: Row(children: <Widget>[
                                                Icon(Icons.live_help, color: kGreenColor),
                                                SizedBox(width:5),
                                                Text("Nutrilife", style: TextStyle(color: kGreenColor)),
                                              ]),
                                              content: new Text('Remove ${getFoodName(position)} from your favorite list?'),
                                              actions: <Widget>[
                                                // usually buttons at the bottom of the dialog
                                                FlatButton(
                                                  child: new Text("Yes", style: TextStyle(color: kGreenColor)),
                                                  onPressed: () async{
                                                    provider.Provider.of<GlobalVariables>(context, listen: false).clearFavFood();
                                                    pr.update(
                                                      message: "Removing..",
                                                      progressWidget: Container(
                                                          padding: EdgeInsets.all(8.0),
                                                          child: CircularProgressIndicator(
                                                            valueColor: AlwaysStoppedAnimation<Color>(
                                                                kGreenColor),
                                                          )),
                                                    );
                                                    pr.show();
                                                    String token = await pref.getToken();
                                                    var response = await http.post(
                                                      '${kUrl}deleteFavorite/food_id/${foodDa[position]['id']}',
                                                      headers: {
                                                        'Accept' : 'application/json',
                                                        'Authorization' : 'Bearer $token',
                                                      },
                                                    );
                                                    await provider.Provider.of<GlobalVariables>(context, listen: false).getFavFoodDa(1);
                                                    pr.hide();
                                                    showDialog(
                                                      context: context,
                                                      builder: (BuildContext context) {
                                                        // return object of type Dialog
                                                        return AlertDialog(
                                                          title: Row(children: <Widget>[
                                                            Icon(Icons.check, color: kGreenColor),
                                                            SizedBox(width:5),
                                                            Text("Nutrilife", style: TextStyle(color: kGreenColor)),
                                                          ]),
                                                          content: Text('Removed from your favorite foods successfully!'),
                                                          actions: <Widget>[
                                                            // usually buttons at the bottom of the dialog
                                                            FlatButton(
                                                              child: Text("Close", style: TextStyle(color: kGreenColor)),
                                                              onPressed: () async {
                                                                int count = 0;
                                                                Navigator.popUntil(context, (route) { return count++ == 2; });
                                                               setState(() {
                                                                 favListPage = 1;
                                                                 // favFood = [];
                                                                 widget.page =1;
                                                               });
                                                              },
                                                            ),
                                                          ],
                                                        );
                                                      },
                                                    );
                                                    print(response.body);
                                                  },
                                                ),
                                                FlatButton(
                                                  child: new Text("Cancel", style: TextStyle(color: Colors.grey)),
                                                  onPressed: () {
                                                    Navigator.pop(context);
                                                  },
                                                ),
                                              ],
                                            );
                                          },
                                        );
                                      }
                                    },
                                      child: Icon(Icons.favorite,
                                          color: (foodDa[position]['in_favorite']) ? kGreenColor : Colors.grey,
                                      ),
                                  ),
                                );
                              }
                          ),
                        ),
                      ),
                      Row(
                        children: <Widget>[
                          Expanded(child:Container()),
                          (isNotSearch && provider.Provider.of<GlobalVariables>(context).recentFood.length > 0 ) ? GestureDetector(
                            onTap:()async{
                              await clearHistory();
                              provider.Provider.of<GlobalVariables>(context, listen: false).clearRecent();
                            },
                              child: Container(
                                padding: EdgeInsets.symmetric(vertical:0, horizontal: 25),
                                child: Row(
                                  children: <Widget>[
                                    Icon(Icons.history, size: 12, color: Colors.grey),
                                    SizedBox(width:3),
                                    Text('Clear history',
                                      textAlign: TextAlign.right,
                                      style: TextStyle(color: Colors.grey, fontSize: 12),
                                    ),
                                  ],
                                )
                              )
                          ) : Container(),
                        ],
                      ),
                    ],
                  )
              ) : (widget.page==1) ? Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Expanded(
                        child: Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: Colors.grey[300],
                            ),
                          ),
                          margin: EdgeInsets.all(15),
                          padding: EdgeInsets.symmetric(vertical: 10, horizontal:0),
                          child: ListView.builder
                            (
                            itemCount: provider.Provider.of<GlobalVariables>(context).favFood.length,
                            itemBuilder: (BuildContext context, int position) {
                              return  (position == provider.Provider.of<GlobalVariables>(context).favFood.length) ?
                              Container(
                                height: (provider.Provider.of<GlobalVariables>(context).favFood.length != 0 && int.tryParse((provider.Provider.of<GlobalVariables>(context).favFood.length/10).toString().split('.')[1].substring(0,1)) == 0) ? 50 : 0,
                                padding: EdgeInsets.all(5),
                                color: Colors.white,
                                child: FlatButton(
                                  child: Text("Load More", style: TextStyle(color: kGreenColor)),
                                  onPressed: () async{
                                    setState(() {
                                      favListPage++;
                                    });
                                    await provider.Provider.of<GlobalVariables>(context, listen: false).getFavFoodDa(favListPage);
                                  },
                                ),
                              )
                                  :  ListTile(
                                title: GestureDetector(
                                    onTap: ()async{
                                      String token = await pref.getToken();
                                      Navigator.pushReplacement(context, FadeRoute(
                                          page: HomeLoading(provider.Provider.of<GlobalVariables>(context, listen: false).favFood[position],token, weightDa)
                                      ));
                                    },
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text('${getFavName(context, position)}', style: TextStyle(color: kGreenColor, fontSize: 14)),
                                        Text('${getFavBrand(context, position)}', style: TextStyle(color: Colors.grey, fontSize: 12)),
                                      ],)
                                ),
                                trailing: GestureDetector(
                                  onTap: () async{
                                    showDialog(
                                      context: context,
                                      builder: (BuildContext context) {
                                        // return object of type Dialog
                                        return AlertDialog(
                                          title: Row(children: <Widget>[
                                            Icon(Icons.live_help, color: kGreenColor),
                                            SizedBox(width:5),
                                            Text("Nutrilife", style: TextStyle(color: kGreenColor)),
                                          ]),
                                          content: new Text('Remove ${getFavName(context, position)} from your favorite list?'),
                                          actions: <Widget>[
                                            // usually buttons at the bottom of the dialog
                                            FlatButton(
                                              child: new Text("Yes", style: TextStyle(color: kGreenColor)),
                                              onPressed: () async{
                                                pr.update(
                                                  message: "Removing..",
                                                  progressWidget: Container(
                                                      padding: EdgeInsets.all(8.0),
                                                      child: CircularProgressIndicator(
                                                        valueColor: AlwaysStoppedAnimation<Color>(
                                                            kGreenColor),
                                                      )),
                                                );
                                                pr.show();
                                                String token = await pref.getToken();
                                                var response = await http.post(
                                                  '${kUrl}deleteFavorite/food_id/${provider.Provider.of<GlobalVariables>(context, listen: false).favFood[position]['id']}',
                                                  headers: {
                                                    'Accept' : 'application/json',
                                                    'Authorization' : 'Bearer $token',
                                                  },
                                                );
                                                provider.Provider.of<GlobalVariables>(context, listen: false).clearFavFood();
                                                await provider.Provider.of<GlobalVariables>(context, listen: false).getFavFoodDa(1);
                                                pr.hide();
                                                showDialog(
                                                  context: context,
                                                  builder: (BuildContext context) {
                                                    // return object of type Dialog
                                                    return AlertDialog(
                                                      title: Row(children: <Widget>[
                                                        Icon(Icons.check, color: kGreenColor),
                                                        SizedBox(width:5),
                                                        Text("Nutrilife", style: TextStyle(color: kGreenColor)),
                                                      ]),
                                                      content: new Text('Removed from your favorite foods successfully!'),
                                                      actions: <Widget>[
                                                        // usually buttons at the bottom of the dialog
                                                        FlatButton(
                                                          child: new Text("Close", style: TextStyle(color: kGreenColor)),
                                                          onPressed: () async{
                                                            setState(() {
                                                              favListPage = 1;
                                                              // favFood = [];
                                                            });
                                                            int count = 0;
                                                            Navigator.popUntil(context, (route) { return count++ == 2; });
                                                          },
                                                        ),
                                                      ],
                                                    );
                                                  },
                                                );
                                                print(response.body);
                                              },
                                            ),
                                            FlatButton(
                                              child: new Text("Cancel", style: TextStyle(color: Colors.grey)),
                                              onPressed: () {
                                                Navigator.pop(context);
                                              },
                                            ),
                                          ],
                                        );
                                      },
                                    );
                                  },
                                  child: Icon(Icons.favorite, color: kGreenColor),
                                ),
                              );
                            }
                        ),
                        ),
                      ),
                    ],
                  )
              ) : Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Expanded(
                        child: Container(
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(10),
                              border: Border.all(
                                color: Colors.grey[300],
                              ),
                            ),
                            margin: EdgeInsets.all(15),
                            child: ListView.builder
                              (
                                itemCount: provider.Provider.of<GlobalVariables>(context).myFood.length,
                                itemBuilder: (BuildContext context, int position) {
                                  return  GestureDetector(
                                    onLongPress: (){
                                      showModalBottomSheet(
                                          isScrollControlled: true,
                                          context: context,
                                          builder: (context) => SingleChildScrollView(
                                              child: Container(
                                                padding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
                                                child: EditMyFood(widget.weight, provider.Provider.of<GlobalVariables>(context, listen: false).myFood[position]),
                                              )
                                          )
                                      );
                                    },
                                    onTap: ()async{
                                      String token = await pref.getToken();
                                      Navigator.pushReplacement(context, FadeRoute(
                                          page: HomeLoading(provider.Provider.of<GlobalVariables>(context, listen: false).myFood[position],token, weightDa)
                                      ));
                                    },
                                    onHorizontalDragDown: (x){
                                      final snackBar = SnackBar(
                                        content: Container(
                                            height: 30,
                                            child: Center(child: Text('Hold to edit', style: TextStyle(fontSize: 15, fontWeight: FontWeight.w600)))),
                                        backgroundColor: kGreenColor,
                                        duration: Duration(seconds: 1),
                                      );
                                      _scaffoldKey.currentState.showSnackBar(snackBar);
                                    },
                                    child: ListTile(
                                      title: Text(provider.Provider.of<GlobalVariables>(context).myFood[position]['name']),
                                      trailing: Icon(Icons.add),
                                    ),
                                  );
                                }
                            ),
                        ),
                      ),
                      GestureDetector(
                        onTap: (){
                          showModalBottomSheet(
                              isScrollControlled: true,
                              context: context,
                              builder: (context) => SingleChildScrollView(
                                  child:Container(
                                    padding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
                                    child: AddMyFood(widget.weight),
                                  )
                              )
                          );
                        },
                        child: ListTile(
                          leading: Icon(Icons.add, color: Colors.grey),
                          title: Text('Add Food', style: TextStyle(color: Colors.grey)),
                        ),
                      ),

                    ],
                  )
              ),
              Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10),
                  border: Border.all(
                    color: Colors.grey[300],
                  ),
                ),
                margin: EdgeInsets.all(15),
                padding: EdgeInsets.all(10),
                child: GestureDetector(
                    onTap: ()async {
                      String token = await pref.getToken();
                      Navigator.pushReplacement(context, FadeRoute(
                          page: HomeLoading(kInitialData,token, weightDa)
                      ));
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(Icons.cancel, size: 40, color: kGreenColor),
                        SizedBox(width: 10),
                        Text('Cancel', style: TextStyle(color: kGreenColor, fontSize: 25)),
                      ],
                    )
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

