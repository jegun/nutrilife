import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nutrilifeapp/const.dart';
import 'package:nutrilifeapp/icons.dart';
import 'package:nutrilifeapp/screens/login_screen.dart';
import 'package:http/http.dart' as http;
import 'package:progress_dialog/progress_dialog.dart';


class ResetPass extends StatefulWidget {
  @override
  _ResetPassState createState() => _ResetPassState();
}

class _ResetPassState extends State<ResetPass> {
  String email;
  bool isOk = false;
  postEmail(String email) async {
    var response = await http.post(
        '${kUrl}submitForgotPassword',
        body: {
        'email' : '$email',
        }
    );
    var data = jsonDecode(response.body);
    print(data);
    if(data['status'] == true){
      setState(() {
        isOk = true;
      });
    } else {
      isOk = false;
    }
  }

  bool isEmail(String em) {
    String p = r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+";
    RegExp regExp = RegExp(p);
    return regExp.hasMatch(em);
  }

  @override
  Widget build(BuildContext context) {
    final ProgressDialog pr = ProgressDialog(context);
    return WillPopScope(
        onWillPop: () async => true,
        child: Scaffold(
          appBar: AppBar(
            leading: Icon(MyFlutterApp.apple_alt, size: 30),
            title: Text('Reset Your Password'),
            backgroundColor: kGreenColor,
          ),
          body: Center(
            // color: kGreenColor,
              child: SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(MyFlutterApp.apple_alt, size: 70, color: kGreenColor),
                    SizedBox(height:15),
                    Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: TextField(
                        textAlign: TextAlign.start,
                        keyboardType: TextInputType.emailAddress,
                        onChanged: (value) {
                          setState(() {
                            email = value;
                          });
                        },
                        decoration: kInputTextStyle.copyWith(
                          prefixIcon: Icon(Icons.person, color: Colors.grey[400]),
                          labelText: 'Email',
                          labelStyle: TextStyle(color: Colors.grey[400]),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 14.0,
                    ),
                    Material(
                      color: kGreenColor,
                      borderRadius: BorderRadius.circular(10.0),
                      elevation: 5.0,
                      child: MaterialButton(
                        onPressed: () async{
                          if(email != null){
                            if(isEmail(email)){
                                try{
                                  pr.update(
                                    message: "Checking..",
                                    progressWidget: Container(
                                        padding: EdgeInsets.all(8.0), child: CircularProgressIndicator(
                                      valueColor: AlwaysStoppedAnimation<Color>(kGreenColor),
                                    )),
                                  );
                                  pr.show();
                                  var response = await http.post(
                                    '${kUrl}cheek',
                                    headers: {"Accept" : "application/json"},
                                    body:{
                                      'email': email,
                                    },
                                  );
                                  pr.hide();
                                  if(response.body == 'false') {
                                    pr.update(
                                      message: "Sending..",
                                      progressWidget: Container(
                                          padding: EdgeInsets.all(8.0), child: CircularProgressIndicator(
                                        valueColor: AlwaysStoppedAnimation<Color>(kGreenColor),
                                      )),
                                    );
                                    pr.show();
                                   await postEmail(email);
                                   pr.hide();
                                   if(isOk){
                                     showDialog(
                                       context: context,
                                       builder: (BuildContext context) {
                                         // return object of type Dialog
                                         return AlertDialog(
                                           title: Row(children: <Widget>[
                                             Icon(Icons.check_circle, color: kGreenColor),
                                             SizedBox(width:5),
                                             Text("Nutrilife", style: TextStyle(color: kGreenColor)),
                                           ]),
                                           content: new Text('An email of your password reset link has been sent to your email address.'),
                                           actions: <Widget>[
                                             // usually buttons at the bottom of the dialog
                                             FlatButton(
                                               child: new Text("Ok", style: TextStyle(color: kGreenColor)),
                                               onPressed: () {
                                                 Navigator.of(context).pop();
                                               },
                                             ),
                                           ],
                                         );
                                       },
                                     );
                                   } else{
                                     wrongAlert(context, 'Something went wrong! Please try again later.');
                                   }
                                  } else{
                                    wrongAlert(context, 'There is no account with this email address');
                                  }
                                } catch (e) {
                                  print(e);
                                }
                            }
                            else {
                              wrongAlert(context, 'Invalid email format');
                          }
                          }
                          else {
                            wrongAlert(context, 'Empty email');
                          }
                        },
                        minWidth: 200.0,
                        height: 42.0,
                        child: Text(
                          'Reset Password', style: TextStyle(color: Colors.white,),
                        ),
                      ),
                    ),
                    SizedBox(height:20),
                    GestureDetector(
                      child: Text('Login Here',
                        style: TextStyle(fontSize: 10, color: kGreenColor, fontWeight: FontWeight.w600),
                      ),
                      onTap: (){
                        Navigator.pushReplacement(context, FadeRoute(
                          page:LoginScreen(),
                        ));
                      },
                    ),
                  ],
                ),
              )
          ),
        )
    );
  }
}
