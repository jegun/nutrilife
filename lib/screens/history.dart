import 'package:flutter/material.dart';
import 'package:nutrilifeapp/icons.dart';
import 'package:nutrilifeapp/const.dart';
import 'package:nutrilifeapp/screens/historyLoading.dart';
import 'package:nutrilifeapp/widgets/navigation_bar.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:nutrilifeapp/widgets/history_chart.dart';
import 'package:nutrilifeapp/services/shared_preferences.dart';
import 'package:nutrilifeapp/services/global.dart';
import 'package:provider/provider.dart' as provider;

NutrilifeSharedPreferences pref = NutrilifeSharedPreferences();

int selectedNutrition = 1;

class HistoryScreen extends StatefulWidget {
  @override
  _HistoryScreenState createState() => _HistoryScreenState();
}

class _HistoryScreenState extends State<HistoryScreen> {
  int nutrition = 1;
  double goal;

  getGoal(String fact) {
    String value;
    value = provider.Provider.of<GlobalVariables>(context, listen: false)
        .dailyGoal['$fact']
        .toString();
    return value;
  }

  getDifference(DateTime d1, DateTime d2) {
    DateTime dateX = d1;
    DateTime dateY = d2;
    int period = 0;
    while (
        '${dateX.subtract(Duration(days: 1)).year}-${dateX.subtract(Duration(days: 1)).month}-${dateX.subtract(Duration(days: 1)).day}' !=
            '${dateY.year}-${dateY.month}-${dateY.day}') {
      period++;
      dateX = dateX.add(Duration(days: 1));
    }
    return period;
  }

  getWaterAverage() {
    double x = 0;
    if (provider.Provider.of<GlobalVariables>(context, listen: false)
            .waterListHistory !=
        null) {
      for (int s = 0;
          s <
              provider.Provider.of<GlobalVariables>(context, listen: false)
                  .waterListHistory
                  .length;
          s++) {
        Map y = provider.Provider.of<GlobalVariables>(context, listen: false)
            .waterListHistory[s];
        double z = double.parse(y["water_amount"]);
        x = x + z;
      }
    }
    return x /
        provider.Provider.of<GlobalVariables>(context, listen: false).period;
  }

  getValue(String fact) {
    double value = 0;
    if (provider.Provider.of<GlobalVariables>(context, listen: false)
            .foodList
            .length !=
        0) {
      for (int i = 0;
          i <
              provider.Provider.of<GlobalVariables>(context, listen: false)
                  .foodList
                  .length;
          i++) {
        Map singleFood =
            provider.Provider.of<GlobalVariables>(context, listen: false)
                .foodList[i];
        value = value +
            (double.parse(singleFood['weight']) /
                100 *
                double.parse(singleFood['$fact']));
      }
      return value /
          provider.Provider.of<GlobalVariables>(context, listen: false).period;
    } else {
      return value;
    }
  }

  getNutritionList(String fact) {
    double value = 0;
    List nutritionList = [];
    if (provider.Provider.of<GlobalVariables>(context, listen: false)
            .foodList
            .length !=
        0) {
      for (int i = 0;
          i <
              provider.Provider.of<GlobalVariables>(context, listen: false)
                  .foodList
                  .length;
          i++) {
        Map singleFood =
            provider.Provider.of<GlobalVariables>(context, listen: false)
                .foodList[i];
        value = value + double.parse(singleFood['$fact']);
        nutritionList.add(value);
      }
      return nutritionList;
    } else {
      return nutritionList;
    }
  }

  DateTime date1;
  DateTime date2;
  updateDates() {
    date1 =
        provider.Provider.of<GlobalVariables>(context, listen: false).dateFrom;
    date2 =
        provider.Provider.of<GlobalVariables>(context, listen: false).dateTo;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    nutrition = 1;
    selectedNutrition = 1;
    goal = double.parse(double.parse(getGoal('calories')).round().toString());
    updateDates();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        appBar: AppBar(
          leading: Icon(MyFlutterApp.apple_alt, size: 30),
          backgroundColor: kGreenColor,
          title: Text('History'),
        ),
        body: Padding(
            padding: EdgeInsets.all(5),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                //todo: insert 3 widgets (weight input, actions and nutrition facts)
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Expanded(
                          child: Row(
                        children: <Widget>[
                          Text('From: '),
                          GestureDetector(
                            child: Row(
                              children: <Widget>[
                                Text(
                                  '${date1.year}-${date1.month}-${date1.day}',
                                  style: TextStyle(
                                    color: kGreenColor,
                                    fontSize: 14,
                                  ),
                                ),
                                Icon(Icons.keyboard_arrow_down,
                                    color: kGreenColor, size: 30),
                              ],
                            ),
                            onTap: () {
                              DatePicker.showDatePicker(context,
                                  showTitleActions: true,
                                  minTime: DateTime(2020, 1, 1),
                                  maxTime: date2.subtract(Duration(days: 1)),
                                  onChanged: (date) {
                                //todo: add date variable to have the value
                              }, onConfirm: (date) {
                                setState(() {
                                  date1 = date;
                                });
                              },
                                  currentTime: date1,
                                  locale: LocaleType.en,
                                  theme: DatePickerTheme(
                                    doneStyle: TextStyle(color: kGreenColor),
                                    itemStyle: TextStyle(color: Colors.black54),
                                  ));
                            },
                          ),
                        ],
                      )),
                      Expanded(
                        child: Row(
                          children: <Widget>[
                            Text('To: '),
                            GestureDetector(
                              child: Row(
                                children: <Widget>[
                                  Text(
                                    (date2.year == DateTime.now().year &&
                                            date2.month ==
                                                DateTime.now().month &&
                                            date2.day == DateTime.now().day)
                                        ? 'Today'
                                        : '${date2.year}-${date2.month}-${date2.day}',
                                    style: TextStyle(
                                      color: kGreenColor,
                                      fontSize: 14,
                                    ),
                                  ),
                                  Icon(Icons.keyboard_arrow_down,
                                      color: kGreenColor, size: 30),
                                ],
                              ),
                              onTap: () {
                                DatePicker.showDatePicker(context,
                                    showTitleActions: true,
                                    minTime: date1.add(Duration(days: 1)),
                                    maxTime: DateTime.now(), onChanged: (date) {
                                  //todo: add date variable to have the value
                                }, onConfirm: (date) {
                                  setState(() {
                                    date2 = date;
                                  });
                                },
                                    currentTime: date2,
                                    locale: LocaleType.en,
                                    theme: DatePickerTheme(
                                      doneStyle: TextStyle(color: kGreenColor),
                                      itemStyle:
                                          TextStyle(color: Colors.black54),
                                    ));
                              },
                            ),
                          ],
                        ),
                      ),
                      Material(
                        color: (date1.day ==
                                    provider.Provider.of<GlobalVariables>(context, listen: false)
                                        .dateFrom
                                        .day &&
                                date1.month ==
                                    provider.Provider.of<GlobalVariables>(context,
                                            listen: false)
                                        .dateFrom
                                        .month &&
                                date1.year ==
                                    provider.Provider.of<GlobalVariables>(context,
                                            listen: false)
                                        .dateFrom
                                        .year &&
                                date2.day ==
                                    provider.Provider.of<GlobalVariables>(context,
                                            listen: false)
                                        .dateTo
                                        .day &&
                                date2.month ==
                                    provider.Provider.of<GlobalVariables>(context,
                                            listen: false)
                                        .dateTo
                                        .month &&
                                date2.year == provider.Provider.of<GlobalVariables>(context, listen: false).dateTo.year)
                            ? Colors.grey
                            : kGreenColor,
                        borderRadius: BorderRadius.circular(10.0),
                        elevation: 0,
                        child: MaterialButton(
                          onPressed: () async {
                            if (date1.day ==
                                    provider.Provider.of<GlobalVariables>(context, listen: false)
                                        .dateFrom
                                        .day &&
                                date1.month ==
                                    provider.Provider.of<GlobalVariables>(context, listen: false)
                                        .dateFrom
                                        .month &&
                                date1.year ==
                                    provider.Provider.of<GlobalVariables>(context,
                                            listen: false)
                                        .dateFrom
                                        .year &&
                                date2.day ==
                                    provider.Provider.of<GlobalVariables>(context,
                                            listen: false)
                                        .dateTo
                                        .day &&
                                date2.month ==
                                    provider.Provider.of<GlobalVariables>(context,
                                            listen: false)
                                        .dateTo
                                        .month &&
                                date2.year ==
                                    provider.Provider.of<GlobalVariables>(
                                            context,
                                            listen: false)
                                        .dateTo
                                        .year) {
                            } else {
                              if (getDifference(date1, date2) <= 90) {
                                String token = await pref.getToken();
                                Navigator.pushReplacement(
                                    context,
                                    FadeRoute(
                                        page: HistoryLoading(
                                            date1, date2, token)));
                              } else {
                                wrongAlert(context,
                                    'The maximum time range is 90 days!');
                              }
                            }
                          },
                          minWidth: 50.0,
                          height: 22.0,
                          child: Text(
                            'Go',
                            style: TextStyle(
                              color: Colors.white,
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Divider(),
                Text(
                  '${kNutritionsNames[nutrition]}  ${kNutritionsUnits[nutrition]}',
                  style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                Container(
                  height: 200,
                  padding: const EdgeInsets.all(8.0),
                  child: historyChart(
                      context,
                      kNutritionsUnits[nutrition],
                      goal,
                      provider.Provider.of<GlobalVariables>(context,
                              listen: false)
                          .dateFrom,
                      provider.Provider.of<GlobalVariables>(context,
                              listen: false)
                          .dateTo,
                      provider.Provider.of<GlobalVariables>(context,
                              listen: false)
                          .foodList,
                      provider.Provider.of<GlobalVariables>(context,
                              listen: false)
                          .waterListHistory,
                      nutrition),
                ),
                Divider(),
                Text(
                    '----------------- The following is avg. daily data -----------------',
                    style: TextStyle(
                      color: Colors.grey[400],
                      fontSize: 13,
                    )),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Scrollbar(
                      child: ListView(
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Expanded(
                                child: SingleNutritionBox(
                                  provider.Provider.of<GlobalVariables>(context,
                                          listen: false)
                                      .gender,
                                  1,
                                  double.parse(double.parse(getGoal('calories'))
                                      .round()
                                      .toString()),
                                  getValue('calories'),
                                  updateName: (v) {
                                    setState(() {
                                      nutrition = v;
                                      selectedNutrition = v;
                                      int x = double.parse(getGoal('calories'))
                                          .round();
                                      goal = double.parse(x.toString());
                                    });
                                  },
                                ),
                              ),
                              Expanded(
                                child: SingleNutritionBox(
                                  provider.Provider.of<GlobalVariables>(context,
                                          listen: false)
                                      .gender,
                                  2,
                                  double.parse(double.parse(getGoal('protein'))
                                      .round()
                                      .toString()),
                                  getValue('protein'),
                                  updateName: (v) {
                                    setState(() {
                                      nutrition = v;
                                      selectedNutrition = v;
                                      int x = double.parse(getGoal('protein'))
                                          .round();
                                      goal = double.parse(x.toString());
                                    });
                                  },
                                ),
                              ),
                              Expanded(
                                child: SingleNutritionBox(
                                  provider.Provider.of<GlobalVariables>(context,
                                          listen: false)
                                      .gender,
                                  3,
                                  double.parse(double.parse(getGoal('fat'))
                                      .round()
                                      .toString()),
                                  getValue('fat'),
                                  updateName: (v) {
                                    setState(() {
                                      nutrition = v;
                                      selectedNutrition = v;
                                      int x =
                                          double.parse(getGoal('fat')).round();
                                      goal = double.parse(x.toString());
                                    });
                                  },
                                ),
                              ),
                            ],
                          ),
                          Row(
                            children: <Widget>[
                              Expanded(
                                child: SingleNutritionBox(
                                  provider.Provider.of<GlobalVariables>(context,
                                          listen: false)
                                      .gender,
                                  4,
                                  double.parse(
                                      double.parse(getGoal('carbohydrates'))
                                          .round()
                                          .toString()),
                                  getValue('carbohydrates'),
                                  updateName: (v) {
                                    setState(() {
                                      nutrition = v;
                                      selectedNutrition = v;
                                      int x =
                                          double.parse(getGoal('carbohydrates'))
                                              .round();
                                      goal = double.parse(x.toString());
                                    });
                                  },
                                ),
                              ),
                              Expanded(
                                child: SingleNutritionBox(
                                  provider.Provider.of<GlobalVariables>(context,
                                          listen: false)
                                      .gender,
                                  5,
                                  double.parse(double.parse(getGoal('fiber'))
                                      .round()
                                      .toString()),
                                  getValue('fibre'),
                                  updateName: (v) {
                                    setState(() {
                                      nutrition = v;
                                      selectedNutrition = v;
                                      int x = double.parse(getGoal('fiber'))
                                          .round();
                                      goal = double.parse(x.toString());
                                    });
                                  },
                                ),
                              ),
                              Expanded(
                                child: SingleNutritionBox(
                                  provider.Provider.of<GlobalVariables>(context,
                                          listen: false)
                                      .gender,
                                  6,
                                  double.parse(
                                      double.parse(getGoal('vitamin a'))
                                          .round()
                                          .toString()),
                                  getValue('vitaminA'),
                                  updateName: (v) {
                                    setState(() {
                                      nutrition = v;
                                      selectedNutrition = v;
                                      int x = double.parse(getGoal('vitamin a'))
                                          .round();
                                      goal = double.parse(x.toString());
                                    });
                                  },
                                ),
                              ),
                            ],
                          ),
                          Row(
                            children: <Widget>[
                              Expanded(
                                child: SingleNutritionBox(
                                  provider.Provider.of<GlobalVariables>(context,
                                          listen: false)
                                      .gender,
                                  7,
                                  double.parse(double.parse(getGoal('carotene'))
                                      .round()
                                      .toString()),
                                  getValue('carotene'),
                                  updateName: (v) {
                                    setState(() {
                                      nutrition = v;
                                      selectedNutrition = v;
                                      int x = double.parse(getGoal('carotene'))
                                          .round();
                                      goal = double.parse(x.toString());
                                    });
                                  },
                                ),
                              ),
                              Expanded(
                                child: SingleNutritionBox(
                                  provider.Provider.of<GlobalVariables>(context,
                                          listen: false)
                                      .gender,
                                  8,
                                  double.parse(
                                      double.parse(getGoal('vitamin b6'))
                                          .round()
                                          .toString()),
                                  getValue('vitaminB6'),
                                  updateName: (v) {
                                    setState(() {
                                      nutrition = v;
                                      selectedNutrition = v;
                                      int x =
                                          double.parse(getGoal('vitamin b6'))
                                              .round();
                                      goal = double.parse(x.toString());
                                    });
                                  },
                                ),
                              ),
                              Expanded(
                                child: SingleNutritionBox(
                                  provider.Provider.of<GlobalVariables>(context,
                                          listen: false)
                                      .gender,
                                  9,
                                  double.parse(
                                      double.parse(getGoal('vitamin b12'))
                                          .round()
                                          .toString()),
                                  getValue('vitaminB12'),
                                  updateName: (v) {
                                    setState(() {
                                      nutrition = v;
                                      selectedNutrition = v;
                                      int x =
                                          double.parse(getGoal('vitamin b12'))
                                              .round();
                                      goal = double.parse(x.toString());
                                    });
                                  },
                                ),
                              ),
                            ],
                          ),
                          Row(
                            children: <Widget>[
                              Expanded(
                                child: SingleNutritionBox(
                                  provider.Provider.of<GlobalVariables>(context,
                                          listen: false)
                                      .gender,
                                  10,
                                  double.parse(
                                      double.parse(getGoal('vitamin b3'))
                                          .round()
                                          .toString()),
                                  getValue('vitaminB3'),
                                  updateName: (v) {
                                    setState(() {
                                      nutrition = v;
                                      selectedNutrition = v;
                                      int x =
                                          double.parse(getGoal('vitamin b3'))
                                              .round();
                                      goal = double.parse(x.toString());
                                    });
                                  },
                                ),
                              ),
                              Expanded(
                                child: SingleNutritionBox(
                                  provider.Provider.of<GlobalVariables>(context,
                                          listen: false)
                                      .gender,
                                  11,
                                  double.parse(
                                      double.parse(getGoal('vitamin c'))
                                          .round()
                                          .toString()),
                                  getValue('vitaminC'),
                                  updateName: (v) {
                                    setState(() {
                                      nutrition = v;
                                      selectedNutrition = v;
                                      int x = double.parse(getGoal('vitamin c'))
                                          .round();
                                      goal = double.parse(x.toString());
                                    });
                                  },
                                ),
                              ),
                              Expanded(
                                child: SingleNutritionBox(
                                  provider.Provider.of<GlobalVariables>(context,
                                          listen: false)
                                      .gender,
                                  12,
                                  double.parse(
                                      double.parse(getGoal('vitamin e'))
                                          .round()
                                          .toString()),
                                  getValue('vitaminE'),
                                  updateName: (v) {
                                    setState(() {
                                      nutrition = v;
                                      selectedNutrition = v;
                                      int x = double.parse(getGoal('vitamin e'))
                                          .round();
                                      goal = double.parse(x.toString());
                                    });
                                  },
                                ),
                              ),
                            ],
                          ),
                          Row(
                            children: <Widget>[
                              Expanded(
                                child: SingleNutritionBox(
                                  provider.Provider.of<GlobalVariables>(context,
                                          listen: false)
                                      .gender,
                                  13,
                                  double.parse(
                                      double.parse(getGoal('cholestrol'))
                                          .round()
                                          .toString()),
                                  getValue('cholesterol'),
                                  updateName: (v) {
                                    setState(() {
                                      nutrition = v;
                                      selectedNutrition = v;
                                      int x =
                                          double.parse(getGoal('cholestrol'))
                                              .round();
                                      goal = double.parse(x.toString());
                                    });
                                  },
                                ),
                              ),
                              Expanded(
                                child: SingleNutritionBox(
                                  provider.Provider.of<GlobalVariables>(context,
                                          listen: false)
                                      .gender,
                                  14,
                                  double.parse(
                                      double.parse(getGoal('potassium'))
                                          .round()
                                          .toString()),
                                  getValue('potassium'),
                                  updateName: (v) {
                                    setState(() {
                                      nutrition = v;
                                      selectedNutrition = v;
                                      int x = double.parse(getGoal('potassium'))
                                          .round();
                                      goal = double.parse(x.toString());
                                    });
                                  },
                                ),
                              ),
                              Expanded(
                                child: SingleNutritionBox(
                                  provider.Provider.of<GlobalVariables>(context,
                                          listen: false)
                                      .gender,
                                  15,
                                  double.parse(double.parse(getGoal('sodium'))
                                      .round()
                                      .toString()),
                                  getValue('sodium'),
                                  updateName: (v) {
                                    setState(() {
                                      nutrition = v;
                                      selectedNutrition = v;
                                      int x = double.parse(getGoal('sodium'))
                                          .round();
                                      goal = double.parse(x.toString());
                                    });
                                  },
                                ),
                              ),
                            ],
                          ),
                          Row(
                            children: <Widget>[
                              Expanded(
                                child: SingleNutritionBox(
                                  provider.Provider.of<GlobalVariables>(context,
                                          listen: false)
                                      .gender,
                                  16,
                                  double.parse(double.parse(getGoal('calcium'))
                                      .round()
                                      .toString()),
                                  getValue('calcium'),
                                  updateName: (v) {
                                    setState(() {
                                      nutrition = v;
                                      selectedNutrition = v;
                                      int x = double.parse(getGoal('calcium'))
                                          .round();
                                      goal = double.parse(x.toString());
                                    });
                                  },
                                ),
                              ),
                              Expanded(
                                child: SingleNutritionBox(
                                  provider.Provider.of<GlobalVariables>(context,
                                          listen: false)
                                      .gender,
                                  17,
                                  double.parse(
                                      double.parse(getGoal('magnesium'))
                                          .round()
                                          .toString()),
                                  getValue('magnesium'),
                                  updateName: (v) {
                                    setState(() {
                                      nutrition = v;
                                      selectedNutrition = v;
                                      int x = double.parse(getGoal('magnesium'))
                                          .round();
                                      goal = double.parse(x.toString());
                                    });
                                  },
                                ),
                              ),
                              Expanded(
                                child: SingleNutritionBox(
                                  provider.Provider.of<GlobalVariables>(context,
                                          listen: false)
                                      .gender,
                                  18,
                                  double.parse(double.parse(getGoal('iron'))
                                      .round()
                                      .toString()),
                                  getValue('iron'),
                                  updateName: (v) {
                                    setState(() {
                                      nutrition = v;
                                      selectedNutrition = v;
                                      int x =
                                          double.parse(getGoal('iron')).round();
                                      goal = double.parse(x.toString());
                                    });
                                  },
                                ),
                              ),
                            ],
                          ),
                          Row(
                            children: <Widget>[
                              Expanded(
                                child: SingleNutritionBox(
                                  provider.Provider.of<GlobalVariables>(context,
                                          listen: false)
                                      .gender,
                                  19,
                                  double.parse(
                                      double.parse(getGoal('manganese'))
                                          .round()
                                          .toString()),
                                  getValue('manganese'),
                                  updateName: (v) {
                                    setState(() {
                                      nutrition = v;
                                      selectedNutrition = v;
                                      int x = double.parse(getGoal('manganese'))
                                          .round();
                                      goal = double.parse(x.toString());
                                    });
                                  },
                                ),
                              ),
                              Expanded(
                                child: SingleNutritionBox(
                                  provider.Provider.of<GlobalVariables>(context,
                                          listen: false)
                                      .gender,
                                  20,
                                  double.parse(double.parse(getGoal('zinc'))
                                      .round()
                                      .toString()),
                                  getValue('zinc'),
                                  updateName: (v) {
                                    setState(() {
                                      nutrition = v;
                                      selectedNutrition = v;
                                      int x =
                                          double.parse(getGoal('zinc')).round();
                                      goal = double.parse(x.toString());
                                    });
                                  },
                                ),
                              ),
                              Expanded(
                                child: SingleNutritionBox(
                                  provider.Provider.of<GlobalVariables>(context,
                                          listen: false)
                                      .gender,
                                  21,
                                  double.parse(double.parse(getGoal('copper'))
                                      .round()
                                      .toString()),
                                  getValue('copper'),
                                  updateName: (v) {
                                    setState(() {
                                      nutrition = v;
                                      selectedNutrition = v;
                                      int x = double.parse(getGoal('copper'))
                                          .round();
                                      goal = double.parse(x.toString());
                                    });
                                  },
                                ),
                              ),
                            ],
                          ),
                          Row(
                            children: <Widget>[
                              Expanded(
                                child: SingleNutritionBox(
                                  provider.Provider.of<GlobalVariables>(context,
                                          listen: false)
                                      .gender,
                                  22,
                                  double.parse(
                                      double.parse(getGoal('phosphorus'))
                                          .round()
                                          .toString()),
                                  getValue('phosphorus'),
                                  updateName: (v) {
                                    setState(() {
                                      nutrition = v;
                                      selectedNutrition = v;
                                      int x =
                                          double.parse(getGoal('phosphorus'))
                                              .round();
                                      goal = double.parse(x.toString());
                                    });
                                  },
                                ),
                              ),
                              Expanded(
                                child: SingleNutritionBox(
                                  provider.Provider.of<GlobalVariables>(context,
                                          listen: false)
                                      .gender,
                                  23,
                                  double.parse(double.parse(getGoal('selenium'))
                                      .round()
                                      .toString()),
                                  getValue('selenium'),
                                  updateName: (v) {
                                    setState(() {
                                      nutrition = v;
                                      selectedNutrition = v;
                                      int x = double.parse(getGoal('selenium'))
                                          .round();
                                      goal = double.parse(x.toString());
                                    });
                                  },
                                ),
                              ),
                              Expanded(
                                child: SingleNutritionBox(
                                  provider.Provider.of<GlobalVariables>(context,
                                          listen: false)
                                      .gender,
                                  24,
                                  double.parse(
                                      (double.parse(getGoal('water')) * 1000)
                                          .round()
                                          .toString()),
                                  getWaterAverage(),
                                  updateName: (v) {
                                    setState(() {
                                      nutrition = v;
                                      selectedNutrition = v;
                                      int x = (double.parse(getGoal('water')) *
                                              1000)
                                          .round();
                                      goal = double.parse(x.toString());
                                    });
                                  },
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                NavigationBar(2),
              ],
            )),
      ),
    );
  }
}

typedef NameCallBack = void Function(int nutrition);

class SingleNutritionBox extends StatefulWidget {
  final double average;
  final double goal;
  final int nutrition;
  final NameCallBack updateName;
  final String gender;
  SingleNutritionBox(this.gender, this.nutrition, this.goal, this.average,
      {this.updateName});

  @override
  _SingleNutritionBoxState createState() => _SingleNutritionBoxState();
}

class _SingleNutritionBoxState extends State<SingleNutritionBox> {
  String getResult(double av, double g, int nutrition) {
    double v = av - g;

    switch (nutrition) {
      case 13:
        {
          if (av < 300) {
            return 'Goal Met';
          } else if (av >= 300 && av <= 315) {
            return 'Slightly Over';
          } else if (av > 315) {
            return 'In Excess';
          }
        }
        break;
      case 7:
        {
          if (av < 5700) {
            return 'Insufficient';
          } else if (av < 6000 && av >= 5700) {
            return 'Slightly Under';
          } else if (av < 15000 && av >= 6000) {
            return 'Goal Met';
          } else if (av < 17000 && av >= 15000) {
            return 'Slightly Over';
          } else if (av >= 17000) {
            return 'In Excess';
          }
        }
        break;
      case 8:
        {
          if (av < 0.8) {
            return 'Insufficient';
          } else if (av < 1 && av >= 0.8) {
            return 'Slightly Under';
          } else if (av < 4 && av >= 1) {
            return 'Goal Met';
          } else if (av < 9 && av >= 4) {
            return 'Slightly Over';
          } else if (av >= 9) {
            return 'In Excess';
          }
        }
        break;
      case 21:
        {
          if (av < 0.5) {
            return 'Insufficient';
          } else if (av < 0.8 && av >= 0.5) {
            return 'Slightly Under';
          } else if (av < 1.5 && av >= 0.8) {
            return 'Goal Met';
          } else if (av < 4.9 && av >= 1.5) {
            return 'Slightly Over';
          } else if (av >= 4.9) {
            return 'In Excess';
          }
        }
        break;
      case 9:
        {
          if (av < 1.4) {
            return 'Insufficient';
          } else if (av < 2 && av >= 1.4) {
            return 'Slightly Under';
          } else if (av >= 2) {
            return 'Goal Met';
          }
        }
        break;
      case 10:
        {
          if (provider.Provider.of<GlobalVariables>(context, listen: false)
                  .gender ==
              "Male") {
            if (av < 12) {
              return 'Insufficient';
            } else if (av < 15.5 && av >= 12) {
              return 'Slightly Under';
            } else if (av < 20 && av >= 15.5) {
              return 'Goal Met';
            } else if (av < 25 && av >= 20) {
              return 'Slightly Over';
            } else if (av >= 25) {
              return 'In Excess';
            }
          } else {
            if (av < 10) {
              return 'Insufficient';
            } else if (av < 13.5 && av >= 10) {
              return 'Slightly Under';
            } else if (av < 18 && av >= 13.5) {
              return 'Goal Met';
            } else if (av < 23 && av >= 18) {
              return 'Slightly Over';
            } else if (av >= 23) {
              return 'In Excess';
            }
          }
        }
        break;
      case 12:
        {
          if (av < 18) {
            return 'Insufficient';
          } else if (av < 21 && av >= 18) {
            return 'Slightly Under';
          } else if (av < 27 && av >= 21) {
            return 'Goal Met';
          } else if (av < 40 && av >= 27) {
            return 'Slightly Over';
          } else if (av >= 40) {
            return 'In Excess';
          }
        }
        break;
      case 18:
        {
          if (provider.Provider.of<GlobalVariables>(context, listen: false)
                  .gender ==
              "Male") {
            if (av < 6) {
              return 'Insufficient';
            } else if (av < 7.5 && av >= 6) {
              return 'Slightly Under';
            } else if (av < 15 && av >= 7.5) {
              return 'Goal Met';
            } else if (av < 24 && av >= 15) {
              return 'Slightly Over';
            } else if (av >= 24) {
              return 'In Excess';
            }
          } else {
            if (av < 15) {
              return 'Insufficient';
            } else if (av < 17 && av >= 15) {
              return 'Slightly Under';
            } else if (av < 22 && av >= 17) {
              return 'Goal Met';
            } else if (av < 25 && av >= 22) {
              return 'Slightly Over';
            } else if (av >= 25) {
              return 'In Excess';
            }
          }
        }
        break;
      case 19:
        {
          if (provider.Provider.of<GlobalVariables>(context, listen: false)
                  .gender ==
              "Male") {
            if (av < 1.4) {
              return 'Insufficient';
            } else if (av < 2 && av >= 1.4) {
              return 'Slightly Under';
            } else if (av < 5 && av >= 2) {
              return 'Goal Met';
            } else if (av < 8 && av >= 5) {
              return 'Slightly Over';
            } else if (av >= 8) {
              return 'In Excess';
            }
          } else {
            if (av < 1.2) {
              return 'Insufficient';
            } else if (av < 1.6 && av >= 1.2) {
              return 'Slightly Under';
            } else if (av < 4.5 && av >= 1.6) {
              return 'Goal Met';
            } else if (av < 7.5 && av >= 4.5) {
              return 'Slightly Over';
            } else if (av >= 7.5) {
              return 'In Excess';
            }
          }
        }
        break;
      case 20:
        {
          if (provider.Provider.of<GlobalVariables>(context, listen: false)
                  .gender ==
              "Male") {
            if (av < 8) {
              return 'Insufficient';
            } else if (av < 10 && av >= 8) {
              return 'Slightly Under';
            } else if (av < 14 && av >= 10) {
              return 'Goal Met';
            } else if (av < 20 && av >= 14) {
              return 'Slightly Over';
            } else if (av >= 20) {
              return 'In Excess';
            }
          } else {
            if (av < 5) {
              return 'Insufficient';
            } else if (av < 8 && av >= 5) {
              return 'Slightly Under';
            } else if (av < 12 && av >= 8) {
              return 'Goal Met';
            } else if (av < 18 && av >= 12) {
              return 'Slightly Over';
            } else if (av >= 18) {
              return 'In Excess';
            }
          }
        }
        break;

      default:
        {
          if (v / g < (-0.05)) {
            return 'Insufficient';
          } else if (v / g >= (-0.05) && v / g < (-0.02)) {
            return 'Slightly Under';
          } else if (v / g >= (-0.02) && v / g < 0.02) {
            return 'Goal Met';
          } else if (v / g >= 0.02 && v / g < 0.05) {
            return 'Slightly Over';
          } else if (v / g >= 0.05) {
            return 'In Excess';
          }
        }
        break;
    }
  }

  Color getTextColor(String status) {
    switch (status) {
      case "Insufficient":
        {
          return Color(0xFFFF0000);
        }
        break;
      case "Slightly Under":
        {
          return Color(0xFFE6AF00);
        }
        break;
      case "Goal Met":
        {
          return Color(0xFF76BB59);
        }
        break;
      case "Slightly Over":
        {
          return Color(0xFFE6AF00);
        }
        break;
      case "In Excess":
        {
          return Color(0xFFFF0000);
        }
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        widget.updateName(widget.nutrition);
      },
      child: Container(
        decoration: BoxDecoration(
          border: Border.all(
            color: Colors.grey[300],
          ),
          color: (selectedNutrition == widget.nutrition)
              ? kGreenColor
              : Colors.white,
          borderRadius: BorderRadius.circular(0),
        ),
        margin: EdgeInsets.all(0),
        padding: EdgeInsets.all(5),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Text(
                '${kNutritionsNames[widget.nutrition]} ${kNutritionsUnits[widget.nutrition]}',
                style: TextStyle(
                  fontSize: 12,
                  fontWeight: FontWeight.w600,
                  color: (selectedNutrition == widget.nutrition)
                      ? Colors.white
                      : Colors.black,
                ),
              ),
              SizedBox(height: 6),
              Text(
                '${widget.average.toStringAsFixed(1)}',
                style: TextStyle(
                    color: (selectedNutrition == widget.nutrition)
                        ? Colors.white
                        : getTextColor(getResult(
                            widget.average, widget.goal, widget.nutrition)),
                    fontSize: 25,
                    fontWeight: FontWeight.w700),
              ),
              SizedBox(height: 6),
              Text(
                '${getResult(widget.average, widget.goal, widget.nutrition)}',
                style: TextStyle(
                  fontSize: 12,
                  color: (selectedNutrition == widget.nutrition)
                      ? Colors.white
                      : Colors.black,
                ),
              ),
            ]),
      ),
    );
  }
}
