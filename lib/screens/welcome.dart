import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nutrilifeapp/const.dart';
import 'package:nutrilifeapp/icons.dart';
import 'package:nutrilifeapp/screens/login_screen.dart';
import 'package:nutrilifeapp/screens/signup_screen.dart';


class WeclomeScreen extends StatefulWidget {
  @override
  _WeclomeScreenState createState() => _WeclomeScreenState();
}

class _WeclomeScreenState extends State<WeclomeScreen> {
  void getLoginScreen(){
    Navigator.push(context, FadeRoute(
      page:LoginScreen(),
    ));
  }

  void getSignUpScreen(){
    Navigator.push(context, FadeRoute(
      page:SignUpScreen(),
    ));
  }
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
          body: Container(
            color: kGreenColor,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Expanded(child: Container()),
                  Icon(MyFlutterApp.apple_alt, size: 90, color: Colors.white),
                  SizedBox(height:15),
                  Text('Nutrilife', style: TextStyle(color: Colors.white, fontSize: 28, fontWeight: FontWeight.w300)),
                  SizedBox(height:35),
                  Button('Login', Colors.white, (){getLoginScreen();}, kGreenColor),
                  SizedBox(height:15),
                  Button('Sign Up', kGreenColor, (){getSignUpScreen();}, kGreyColor),
                  Expanded(
                    child: Container(),
                  )
                ],
              )
          ),
        )
    );
  }
}



class Button extends StatelessWidget {
  final String text;
  final Color color;
  final Function function;
  final Color textColor;
  Button(this.text, this.color, this.function, this.textColor);
  @override
  Widget build(BuildContext context) {
    return Material(
      color: color,
      borderRadius: BorderRadius.circular(10.0),
      elevation: 5.0,
      child: MaterialButton(
        onPressed: function,
        minWidth: 200.0,
        height: 42.0,
        child: Text(
          text, style: TextStyle(color: textColor,),
        ),
      ),
    );
  }
}
