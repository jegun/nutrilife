import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nutrilifeapp/icons.dart';
import 'package:nutrilifeapp/const.dart';
import 'package:nutrilifeapp/me/help.dart';
import 'package:nutrilifeapp/widgets/navigation_bar.dart';
import 'package:nutrilifeapp/me/update_basic_info.dart';
import 'package:nutrilifeapp/me/units_setting.dart';
import 'package:nutrilifeapp/me/about.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:nutrilifeapp/screens/welcome.dart';
import 'package:nutrilifeapp/services/shared_preferences.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:http/http.dart' as http;

NutrilifeSharedPreferences pref = NutrilifeSharedPreferences();

class MeScreen extends StatefulWidget {
  final double dailyCalories;
  final String userName;
  final String token;
  MeScreen(this.userName, this.dailyCalories, this.token);
  @override
  _MeScreenState createState() => _MeScreenState();
}

class _MeScreenState extends State<MeScreen> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final ProgressDialog pr = ProgressDialog(context);
    return WillPopScope(
      onWillPop: () async => false,
      child: SafeArea(
        child: Scaffold(
          appBar: AppBar(
            leading: Icon(MyFlutterApp.apple_alt, size: 30),
            backgroundColor: kGreenColor,
            title: Text('My Profile'),
          ),
          body: Column(
            children: <Widget>[
              //todo: insert 3 widgets (weight input, actions and nutrition facts)
              Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                      color: Colors.grey[300],
                    ),
                  ),
                  margin: EdgeInsets.all(5),
                  child: Column(
                    children: <Widget>[
                      GestureDetector(
                        onTap: () async {
                          String userGender = await pref.getUserGender();
                          String name = await pref.getUserName();
                          int height = await pref.getUserHeight();
                          int weight = await pref.getUserWeight();
                          String birthDateShown = await pref.getUserBirthDate();
                          int levelChoice = await pref.getUserActivityLevel();
                          int goalChoice = await pref.getWeeklyGoal();
                          String gainWeight = await pref.getUserGainWeight();
                          String loseWeight = await pref.getUserLoseWeight();
                          String token = await pref.getToken();
                          int userId = await pref.getUserID();
                          String heightUnit = await pref.getUserHeightUnit();
                          String weightUnit = await pref.getUserWeightUnit();
                          String email = await pref.getUserEmail();
                          await Navigator.push(
                              context,
                              FadeRoute(
                                page: UpdateBasicInfo(
                                    name,
                                    userId,
                                    token,
                                    userGender,
                                    height,
                                    weight,
                                    birthDateShown,
                                    levelChoice,
                                    goalChoice,
                                    gainWeight,
                                    loseWeight,
                                    heightUnit,
                                    weightUnit,
                                    email),
                              ));
                        },
                        child: ListTile(
                          title: Row(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: <Widget>[
                              Text('${widget.userName}'),
                              SizedBox(width: 10),
                              Text(
                                '(Basic Info)',
                                style:
                                    TextStyle(fontSize: 12, color: Colors.grey),
                              ),
                            ],
                          ),
                          leading: Icon(Icons.person, color: kGreenColor),
                          trailing: Container(
                            child: Icon(Icons.keyboard_arrow_right),
                          ),
                        ),
                      ),
                      ListTile(
                        title: Text('Daily Calorie Goal'),
                        leading: Icon(
                          MyFlutterApp.fire,
                          color: kGreenColor,
                        ),
                        trailing: Container(
                          child: Text('${widget.dailyCalories.round()}'),
                        ),
                      ),
                      GestureDetector(
                        onTap: () async {
                          int weightUnit;
                          int heightUnit;
                          int measureUnit;
                          List<String> unitData = [];
                          unitData.add(await pref.getUserWeightUnit());
                          unitData.add(await pref.getUserHeightUnit());
                          unitData.add(await pref.getUserMeasureUnit());
                          if (unitData[0] == 'kg') {
                            weightUnit = 1;
                          } else {
                            weightUnit = 2;
                          }

                          if (unitData[1] == 'cm') {
                            heightUnit = 1;
                          } else {
                            heightUnit = 2;
                          }

                          if (unitData[2] == 'g') {
                            measureUnit = 1;
                          } else if (unitData[2] == 'ml') {
                            measureUnit = 2;
                          } else if (unitData[2] == 'lb') {
                            measureUnit = 3;
                          } else if (unitData[2] == 'oz') {
                            measureUnit = 4;
                          } else if (unitData[2] == 'kg') {
                            measureUnit = 5;
                          }
                          String token = await pref.getToken();
                          int userId = await pref.getUserID();
                          String name = await pref.getUserName();
                          String email = await pref.getUserEmail();
                          int height = await pref.getUserHeight();
                          int weight = await pref.getUserWeight();
                          await Navigator.push(
                              context,
                              FadeRoute(
                                page: UnitSetting(
                                    weightUnit,
                                    heightUnit,
                                    measureUnit,
                                    userId,
                                    token,
                                    name,
                                    email,
                                    height,
                                    weight,
                                    unitData[1],
                                    unitData[0]),
                              ));
                        },
                        child: ListTile(
                          title: Text('Unit setting'),
                          leading: Icon(Icons.settings, color: kGreenColor),
                          trailing: Container(
                            child: Icon(Icons.keyboard_arrow_right),
                          ),
                        ),
                      ),
                    ],
                  )),
              SizedBox(height: 10),
              Container(
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Colors.grey[300],
                    ),
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10),
                  ),
                  margin: EdgeInsets.all(5),
                  child: Column(
                    children: <Widget>[
                      GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              FadeRoute(
                                page: AboutUs(),
                              ));
                        },
                        child: ListTile(
                          title: Text('About Us'),
                          leading: Icon(
                            Icons.info,
                            color: kGreenColor,
                          ),
                          trailing: Container(
                            child: Icon(Icons.keyboard_arrow_right),
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () => launch(
                            'https://nutrilifeco.com/pages/terms-of-service-1'),
                        child: ListTile(
                          title: Text('Terms of Service'),
                          leading: Icon(
                            Icons.format_list_bulleted,
                            color: kGreenColor,
                          ),
                          trailing: Container(
                            child: Icon(Icons.keyboard_arrow_right),
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () => launch(
                            'https://nutrilifeco.com/pages/privacy-policy-1'),
                        child: ListTile(
                          title: Text('Privacy Policy'),
                          leading: Icon(
                            Icons.lock,
                            color: kGreenColor,
                          ),
                          trailing: Container(
                            child: Icon(Icons.keyboard_arrow_right),
                          ),
                        ),
                      ),
                    ],
                  )),
              SizedBox(height: 10),
              Container(
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Colors.grey[300],
                    ),
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10),
                  ),
                  margin: EdgeInsets.all(5),
                  child: Column(
                    children: <Widget>[
                      GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              FadeRoute(
                                page: HelpScreen(),
                              ));
                        },
                        child: ListTile(
                          title: Text('Help'),
                          leading: Icon(
                            Icons.help,
                            color: kGreenColor,
                          ),
                          trailing: Container(
                            child: Icon(Icons.keyboard_arrow_right),
                          ),
                        ),
                      ),
                    ],
                  )),
              SizedBox(height: 10),
              Expanded(child: Container()),
              GestureDetector(
                onTap: () async {
                  pr.update(
                    message: "Signing Out",
                    progressWidget: Container(
                        padding: EdgeInsets.all(8.0),
                        child: CircularProgressIndicator(
                          valueColor:
                              AlwaysStoppedAnimation<Color>(kGreenColor),
                        )),
                  );
                  pr.show();
                  var response = await http.post('${kUrl}logout', headers: {
                    'Accept': 'application/json',
                    'Authorization': 'Bearer ${widget.token}',
                  });
                  print('Response status: ${response.statusCode}');
                  print('Response body: ${response.body}');
                  await pref.setIsLogged(false);
                  await pref.setUserID(null);
                  await pref.setToken(null);
                  await pref.setUserWeightUnit(null);
                  await pref.setUserWeight(null);
                  await pref.setUserHeight(null);
                  await pref.setUserLoseWeight(null);
                  await pref.setUserName(null);
                  await pref.setUserMeasureUnit(null);
                  await pref.setUserBirthDate(null);
                  await pref.setUserGainWeight(null);
                  await pref.setUserDailyCalories(null);
                  await pref.setUserActivityLevel(null);
                  await pref.setWeeklyGoal(null);
                  await pref.setUserHeightUnit(null);
                  await pref.setUserGender(null);
                  List<String> emptyList = [];
                  await pref.setLastChosen(emptyList);
                  await pref.setUserReceive(null);
                  await pref.setUserPolicy(null);
                  await pr.hide();
                  await Navigator.pushReplacement(
                      context,
                      FadeRoute(
                        page: WeclomeScreen(),
                      ));
                },
                child: Container(
                    height: 50,
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.grey[300],
                      ),
                      color: kGreenColor,
                      borderRadius: BorderRadius.circular(10),
                    ),
                    margin: EdgeInsets.all(15),
                    child: Center(
                        child: Text('Logout',
                            style: TextStyle(
                                fontSize: 15,
                                color: Colors.white,
                                fontWeight: FontWeight.w500)))),
              ),
              NavigationBar(3),
            ],
          ),
        ),
      ),
    );
  }
}
