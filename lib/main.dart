import 'package:flutter/material.dart';
import 'screens/splash.dart';
import 'screens/login_screen.dart';
import 'screens/signup_screen.dart';
import 'screens/welcome.dart';
import 'package:provider/provider.dart' as provider;
import 'package:nutrilifeapp/services/global.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return provider.ChangeNotifierProvider<GlobalVariables>(
        create: (context) {
          return GlobalVariables();
        },
        lazy: false,
        child: MaterialApp(
          debugShowCheckedModeBanner: true,
          routes: {
            // When navigating to the "/" route, build the FirstScreen widget.
            //        'home': (context) => HomeLoading(kInitialData, getToken()),
            'splash': (context) => Splash(),
            'login': (context) => LoginScreen(),
            'welcome': (context) => WeclomeScreen(),
            'signup': (context) => SignUpScreen(),
            // When navigating to the "/second" route, build the SecondScreen widget.
          },
          initialRoute: 'splash',
          theme: ThemeData(
            fontFamily: 'Montserrat',
          ),
        ));
  }
}
