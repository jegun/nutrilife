import 'package:shared_preferences/shared_preferences.dart';

class NutrilifeSharedPreferences {

  setIsLogged(bool isLogged) async{
      SharedPreferences preferences = await SharedPreferences.getInstance();
      await preferences.setBool('isLogged', isLogged);
  }


  getIsLogged() async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    bool isLogged = preferences.getBool('isLogged') ?? false;
    return isLogged;
  }


  setUserID(int userId) async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setInt('userID', userId);
  }

  getUserID() async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    int userId = preferences.getInt('userID');
    return userId;
  }


  setToken(String tokenValue) async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setString('token', tokenValue);
  }


  getToken() async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String token = preferences.getString('token');
    return token;
  }

  setUserName(String userName) async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setString('username', userName);
  }

  getUserName() async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String userName = preferences.getString('username');
    return userName;
  }

  setUserWeight(int userWeight) async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setInt('userweight', userWeight);
  }

  getUserWeight() async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    int userWeight = preferences.getInt('userweight');
    return userWeight;
  }


  setUserHeight(int userHeight) async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setInt('userheightcm', userHeight);
  }

  getUserHeight() async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    int userHeight = preferences.getInt('userheightcm');
    return userHeight;
  }



  setUserActivityLevel(int userActivityLevel) async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setInt('useractivitylevel', userActivityLevel);
  }

  getUserActivityLevel() async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    int userActivityLevel = preferences.getInt('useractivitylevel');
    return userActivityLevel;
  }


  setWeeklyGoal(int userWeeklyGoal) async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setInt('userweeklygoal', userWeeklyGoal);
  }

  getWeeklyGoal() async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    int userWeeklyGoal = preferences.getInt('userweeklygoal');
    return userWeeklyGoal;
  }

  setUserGender(String userGender) async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setString('usergender', userGender);
  }

  getUserGender() async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String userGender = preferences.getString('usergender');
    return userGender;
  }

  setUserGainWeight(String userGainChoice) async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setString('usergainchoice', userGainChoice);
  }

  getUserGainWeight() async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String userGainChoice = preferences.getString('usergainchoice');
    return userGainChoice;
  }

  setUserLoseWeight(String userLoseChoice) async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setString('userlosechoice', userLoseChoice);
  }

  getUserLoseWeight() async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String userLoseChoice = preferences.getString('userlosechoice');
    return userLoseChoice;
  }

  setUserDailyCalories(double userDailyCalories) async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setDouble('userdailycalories', userDailyCalories);
  }

  getUserDailyCalories() async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    double userDailyCalories = preferences.getDouble('userdailycalories');
    return userDailyCalories;
  }

  setUserBirthDate(String userBirthDate) async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setString('userbirthdate', userBirthDate);
  }

  getUserBirthDate() async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String userBirthDate = preferences.getString('userbirthdate');
    return userBirthDate;
  }

  setUserHeightUnit(String userHeightUnit) async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setString('userheightunit', userHeightUnit);
  }

  getUserHeightUnit() async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String userHeightUnit = preferences.getString('userheightunit');
    return userHeightUnit;
  }


  setUserWeightUnit(String userWeightUnit) async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setString('userweightunit', userWeightUnit);
  }

  getUserWeightUnit() async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String userWeightUnit = preferences.getString('userweightunit');
    return userWeightUnit;
  }


  setUserMeasureUnit(String userMeasureUnit) async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setString('usermeasureunit', userMeasureUnit);
  }

  getUserMeasureUnit() async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String userMeasureUnit = preferences.getString('usermeasureunit');
    return userMeasureUnit;
  }

  setUserEmail(String userEmail) async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setString('useremail', userEmail);
  }

  getUserEmail() async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String userEmail = preferences.getString('useremail');
    return userEmail;
  }

  setUserPassword(String userPassword) async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setString('userpassword', userPassword);
  }

  getUserPassword() async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String userPassword = preferences.getString('userpassword');
    return userPassword;
  }

  setUserPolicy(int userPolicy) async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setInt('userpolicy', userPolicy);
  }

  getUserPolicy() async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    int userPolicy = preferences.getInt('userpolicy');
    return userPolicy;
  }

  setUserReceive(int userReceive) async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setInt('userreceive', userReceive);
  }

  getUserReceive() async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    int userReceive = preferences.getInt('userreceive');
    return userReceive;
  }


  setLastChosen(List<String> lastChosen) async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setStringList('lastchosen', lastChosen);
  }

  getLastChosen() async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    List<String> lastChosen = preferences.getStringList('lastchosen');
    return lastChosen;
  }

}

