import 'package:flutter/material.dart';
import 'package:nutrilifeapp/const.dart';
import 'package:nutrilifeapp/screens/diary.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class GlobalVariables extends ChangeNotifier {
  List<dynamic> diaryList = [];
  List<dynamic> breakfastList = [];
  List<dynamic> lunchList = [];
  List<dynamic> dinnerList = [];
  List<dynamic> snacksList = [];
  List<dynamic> waterList = [];
  List<dynamic> exerciseList = [];
  Map<String, dynamic> dailyGoal;
  double caloriesAll = 0;
  double caloriesBreakfast = 0;
  double caloriesLunch = 0;
  double caloriesDinner = 0;
  double caloriesSnacks = 0;
  double caloriesGoal = 0;
  String token;
  DateTime chosenDate = DateTime.now();

  getWater(DateTime date, String token) async {
    waterList = [];
    var response = await http.post('${kUrl}getWater', headers: {
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    }, body: {
      'date': '${date.year}-${date.month}-${date.day}',
    });
    print(response.body);
    var data = jsonDecode(response.body);
    List<dynamic> dataList = data['data'];
    for (int i = 0; i < dataList.length; i++) {
      Map currentWater = dataList[i];
      Map singleEntry;
      singleEntry = {
        "water_amount": "${currentWater['amount']}",
        "water_id": "${currentWater['id']}",
      };
      waterList.add(singleEntry);
    }
    notifyListeners();
  }

  getExercise(DateTime date, String token) async {
    exerciseList = [];
    var response = await http.post('${kUrl}getExercise', headers: {
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    }, body: {
      'date': '${date.year}-${date.month}-${date.day}',
    });
    print(response.body);
    var data = jsonDecode(response.body);
    List<dynamic> dataList = data['data'];
    for (int i = 0; i < dataList.length; i++) {
      Map currentExercise = dataList[i];
      Map singleEntry;
      singleEntry = {
        "calories": "${currentExercise['calories']}",
        "id": "${currentExercise['id']}",
        "name": "${currentExercise['name']}",
      };
      exerciseList.add(singleEntry);
    }
    notifyListeners();
  }

  getDiaryList(DateTime date, String token) async {
    diaryList = [];
    breakfastList = [];
    lunchList = [];
    snacksList = [];
    dinnerList = [];
    var response = await http.post('${kUrl}getDiaryDate', headers: {
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    }, body: {
      'date': '${date.year}-${date.month}-${date.day}',
    });
    print(response.body);
    var data = jsonDecode(response.body);
    List<dynamic> dataList = data['data'];
    for (int x = 0; x < dataList.length; x++) {
      Map singleEntry = dataList[x];
      Map singleEnt;
      singleEnt = {
        "mealType": "${singleEntry['mealType']}",
        "date": "${singleEntry['mealDate']}",
        "diary_id": "${singleEntry['id']}",
        "food_id": "${singleEntry['food_id']}",
        "food_name": "${singleEntry['food_name']}",
        "weight": "${singleEntry['mealWeight']}",
        "calories": "${singleEntry['calories']}",
        "protein": "${singleEntry['protein']}",
        "carbohydrates": "${singleEntry['carbohydrates']}",
        "fat": "${singleEntry['fat']}",
        "transFat": "${singleEntry['transFat']}",
        "saturatedFats": "${singleEntry['saturatedFats']}",
        "sugars": "${singleEntry['sugars']}",
        "vitaminA": "${singleEntry['vitaminA']}",
        "vitaminB3": "${singleEntry['vitaminB3']}",
        "vitaminB6": "${singleEntry['vitaminB6']}",
        "vitaminB12": "${singleEntry['vitaminB12']}",
        "vitaminC": "${singleEntry['vitaminC']}",
        "vitaminE": "${singleEntry['vitaminE']}",
        "calcium": "${singleEntry['calcium']}",
        "cholesterol": "${singleEntry['cholesterol']}",
        "iron": "${singleEntry['iron']}",
        "manganese": "${singleEntry['manganese']}",
        "magnesium": "${singleEntry['magnesium']}",
        "sodium": "${singleEntry['sodium']}",
        "potassium": "${singleEntry['potassium']}",
        "zinc": "${singleEntry['zinc']}",
        "copper": "${singleEntry['copper']}",
        "selenium": "${singleEntry['selenium']}",
        "phosphorus": "${singleEntry['phosphorus']}",
        "fibre": "${singleEntry['fibre']}",
        "carotene": "${singleEntry['carotene']}",
        "unit_type": "${singleEntry['unit_type']}"
      };
      diaryList.add(singleEnt);
    }

    for (int i = 0; i < diaryList.length; i++) {
      Map singleFood = diaryList[i];
      switch (singleFood['mealType']) {
        case '0':
          {
            breakfastList.add(singleFood);
          }
          break;
        case '1':
          {
            lunchList.add(singleFood);
          }
          break;
        case '2':
          {
            dinnerList.add(singleFood);
          }
          break;
        case '3':
          {
            snacksList.add(singleFood);
          }
          break;
      }
    }
    notifyListeners();
  }

  getCalories(List<dynamic> list) {
    double x = 0;
    if (list.length != 0) {
      for (int s = 0; s < list.length; s++) {
        Map y = list[s];
        double z =
            double.parse(y["weight"]) / 100 * double.parse(y["calories"]);
        x = x + z;
      }
    }
    return x;
  }

  getCaloriesGoal() async {
    caloriesGoal = await pref.getUserDailyCalories();
    notifyListeners();
  }

  getConsumedCalories() {
    caloriesBreakfast = getCalories(breakfastList);
    caloriesLunch = getCalories(lunchList);
    caloriesDinner = getCalories(dinnerList);
    caloriesSnacks = getCalories(snacksList);
    caloriesAll =
        caloriesBreakfast + caloriesLunch + caloriesDinner + caloriesSnacks;
    notifyListeners();
  }

  getGoal(String token) async {
    var response = await http.get(
      '${kUrl}getDailyGoal',
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      },
    );
    var goals = jsonDecode(response.body);
    Map<String, dynamic> x = jsonDecode(goals['data'][0]);
    dailyGoal = x;
    notifyListeners();
  }

  updateDate(DateTime date) {
    chosenDate = date;
    notifyListeners();
  }

  updateToken(String newToken) {
    token = newToken;
    notifyListeners();
  }

  getThemAll(DateTime date, String token) async {
    await getDiaryList(date, token);
    await getCaloriesGoal();
    await getWater(date, token);
    await getExercise(date, token);
    await getGoal(token);
    getConsumedCalories();
  }

  List<dynamic> foodList = [];
  List<dynamic> waterListHistory = [];
  int period = 0;
  DateTime dateFrom = DateTime.now().subtract(Duration(days: 7));
  DateTime dateTo = DateTime.now();
  String gender;

  getGender() async {
    gender = await pref.getUserGender();
    notifyListeners();
  }

  getWaterHistory(DateTime date1, DateTime date2) async {
    waterListHistory = [];
    var response = await http.post('${kUrl}getWaterBetweenDate', headers: {
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    }, body: {
      'date1': '${date1.year}-${date1.month}-${date1.day}',
      'date2': '${date2.year}-${date2.month}-${date2.day}',
    });
    print(response.body);
    var data = jsonDecode(response.body);
    List<dynamic> dataList = data['data'];
    if (dataList != null) {
      for (int i = 0; i < dataList.length; i++) {
        Map currentWater = dataList[i];
        Map singleEntry;
        singleEntry = {
          "date": "${currentWater['date']}",
          "water_amount": "${currentWater['amount']}",
          "water_id": "${currentWater['id']}",
        };
        waterListHistory.add(singleEntry);
      }
    }
    notifyListeners();
  }

  getPeriod(DateTime date1, DateTime date2) {
    DateTime dateX = date1;
    DateTime dateY = date2;
    period = 0;
    int p = 0;
    while (
        '${dateX.subtract(Duration(days: 1)).year}-${dateX.subtract(Duration(days: 1)).month}-${dateX.subtract(Duration(days: 1)).day}' !=
            '${dateY.year}-${dateY.month}-${dateY.day}') {
      p++;
      dateX = dateX.add(Duration(days: 1));
    }
    period = p;
    notifyListeners();
  }

  getFoodList(DateTime date1, DateTime date2) async {
    foodList = [];
    var response = await http.post('${kUrl}getDiaryBetweenDate', headers: {
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    }, body: {
      'date1': '${date1.year}-${date1.month}-${date1.day}',
      'date2': '${date2.year}-${date2.month}-${date2.day}',
    });
    print(response.body);
    var data = jsonDecode(response.body);
    List<dynamic> dataList = data['data'];
    for (int x = 0; x < dataList.length; x++) {
      Map singleEntry = dataList[x];
      Map singleEnt;
      singleEnt = {
        "mealType": "${singleEntry['mealType']}",
        "date": "${singleEntry['mealDate']}",
        "diary_id": "${singleEntry['id']}",
        "food_id": "${singleEntry['food_id']}",
        "food_name": "${singleEntry['food_name']}",
        "weight": "${singleEntry['mealWeight']}",
        "calories": "${singleEntry['calories']}",
        "protein": "${singleEntry['protein']}",
        "carbohydrates": "${singleEntry['carbohydrates']}",
        "fat": "${singleEntry['fat']}",
        "transFat": "${singleEntry['transFat']}",
        "saturatedFats": "${singleEntry['saturatedFats']}",
        "sugars": "${singleEntry['sugars']}",
        "vitaminA": "${singleEntry['vitaminA']}",
        "vitaminB3": "${singleEntry['vitaminB3']}",
        "vitaminB6": "${singleEntry['vitaminB6']}",
        "vitaminB12": "${singleEntry['vitaminB12']}",
        "vitaminC": "${singleEntry['vitaminC']}",
        "vitaminE": "${singleEntry['vitaminE']}",
        "calcium": "${singleEntry['calcium']}",
        "cholesterol": "${singleEntry['cholesterol']}",
        "iron": "${singleEntry['iron']}",
        "manganese": "${singleEntry['manganese']}",
        "magnesium": "${singleEntry['magnesium']}",
        "sodium": "${singleEntry['sodium']}",
        "potassium": "${singleEntry['potassium']}",
        "zinc": "${singleEntry['zinc']}",
        "copper": "${singleEntry['copper']}",
        "selenium": "${singleEntry['selenium']}",
        "phosphorus": "${singleEntry['phosphorus']}",
        "fibre": "${singleEntry['fibre']}",
        "carotene": "${singleEntry['carotene']}",
      };
      foodList.add(singleEnt);
    }
    notifyListeners();
  }

  updateDateFrom(DateTime date) {
    dateFrom = date;
    notifyListeners();
  }

  updateDateTo(DateTime date) {
    dateTo = date;
    notifyListeners();
  }

  getHistoryAll(DateTime date1, DateTime date2) async {
    await getWaterHistory(date1, date2);
    await getFoodList(date1, date2);
    await getGender();
    getPeriod(date1, date2);
  }

  List favFood = [];
  List myFood = [];

  clearFavFood() {
    favFood = [];
    notifyListeners();
  }

  getFavFoodDa(int listIndex) async {
    // ProgressDialog pr = ProgressDialog(context);
    // pr.update(
    //   message: "Loading..",
    //   progressWidget: Container(
    //       padding: EdgeInsets.all(8.0),
    //       child: CircularProgressIndicator(
    //         valueColor: AlwaysStoppedAnimation<Color>(
    //             kGreenColor),
    //       )),
    // );
    // pr.show();
    String token = await pref.getToken();
    var response =
        await http.get('${kUrl}myFoodFavorites?page=$listIndex', headers: {
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    });

    var data = jsonDecode(response.body);
    print(data);
    List<dynamic> favItems = data['data']['data'];
    favFood.addAll(favItems);
    notifyListeners();
    // pr.hide();
  }

  getMyFoodDa() async {
    myFood = [];
    // ProgressDialog pr = ProgressDialog(context);
    // pr.update(
    //   message: "Loading..",
    //   progressWidget: Container(
    //       padding: EdgeInsets.all(8.0),
    //       child: CircularProgressIndicator(
    //         valueColor: AlwaysStoppedAnimation<Color>(
    //             kGreenColor),
    //       )),
    // );
    // pr.show();
    String token = await pref.getToken();
    var response = await http.get('${kUrl}myFood', headers: {
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    });

    var data = jsonDecode(response.body);
    print(data);
    List<dynamic> myFoodItems = data['data']['data'];
    myFood = myFoodItems;
    // pr.hide();
    notifyListeners();
  }

  List recentFood = [];

  getRecentFood(List<String> laChosen) async {
    recentFood = [];
    String token = await pref.getToken();
    if (laChosen.length != 0) {
      for (int i = 0; i < laChosen.length; i++) {
        var response = await http.get(
          '${kUrl}food_id/${laChosen[i]}',
          headers: {
            'Accept': 'application/json',
            'Authorization': 'Bearer $token',
          },
        );
        var data = jsonDecode(response.body);
        recentFood.add(data['data']);
      }
      print(recentFood);
    }
    notifyListeners();
  }

  addToRecent(String foodID) async {
    List recent = [];
    String token = await pref.getToken();
    var response = await http.get(
      '${kUrl}food_id/$foodID',
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      },
    );
    var data = jsonDecode(response.body);
    recent.add(data['data']);
    for (int i = 0; i < recentFood.length; i++) {
      recent.add(recentFood[i]);
    }
    recentFood = recent;
    notifyListeners();
  }

  clearRecent() {
    recentFood = [];
    notifyListeners();
  }
}
