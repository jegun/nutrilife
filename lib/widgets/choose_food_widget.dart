import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nutrilifeapp/const.dart';
import 'package:nutrilifeapp/screens/choose_food.dart';

String food;

class ChooseFoodWidget extends StatelessWidget {
  final dynamic foodName;
  final double weight;
  ChooseFoodWidget(this.foodName, this.weight);

Widget setLeading(){
  if (foodName == 'Select your food'){
    return Icon(Icons.add, color: Colors.white);
  } else{
    return Icon(Icons.check, color: Colors.white);
  }
}

String getFoodName(){
  if(foodName == null){
   return 'Select Your Food';
  }
  else{
  return foodName;
  }
}
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        Navigator.push(context, FadeRoute(page: ChooseFood(weight,0)));
      },
      child: Container(
          decoration: BoxDecoration(
            color: kGreenColor,
            borderRadius: BorderRadius.circular(10),
          ),
          margin: EdgeInsets.symmetric(horizontal: 15),
          padding: EdgeInsets.all(15),
          child: Row(
            children: <Widget>[
            Icon(
            Icons.fastfood,
            color: Colors.white,
              ),
            Expanded(child: Center(
                child: Text(
            '${getFoodName()}',
                style: TextStyle(color: Colors.white)
            )
            )
            ),
            setLeading(),
            ],
          )
      ),
    );
  }
}




