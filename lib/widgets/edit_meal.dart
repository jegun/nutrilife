import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:nutrilifeapp/const.dart';
import 'package:configurable_expansion_tile/configurable_expansion_tile.dart';
import 'package:nutrilifeapp/screens/diary.dart';
import 'package:nutrilifeapp/screens/diaryLoading.dart';
import 'package:nutrilifeapp/services/shared_preferences.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:nutrilifeapp/services/global.dart';
import 'package:provider/provider.dart' as provider;

NutrilifeSharedPreferences pref = NutrilifeSharedPreferences();

class EditMeal extends StatefulWidget {
  Map food;
  final DateTime date;
  final String token;
  String unitType;

  EditMeal(this.food, this.date, this.token, this.unitType);

  @override
  _EditMealState createState() => _EditMealState();
}

class _EditMealState extends State<EditMeal> {
  final myController = TextEditingController();
  bool isExpanded = false;
  double widgetHeight = 200;
  double weightGram;
  double weightOtherType = 0;
  String previousString = '';

  deleteDiary() async {
    ProgressDialog pr = ProgressDialog(context);

    pr.update(
      message: "Removing..",
      progressWidget: Container(
          padding: EdgeInsets.all(8.0),
          child: CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation<Color>(kGreenColor),
          )),
    );
    pr.show();
    var response = await http.post(
        '${kUrl}destroyDiary/diary_id/${widget.food['diary_id']}',
        headers: {
          'Accept': 'application/json',
          'Authorization': 'Bearer ${widget.token}',
        },
        body: {
          'datedate':
              "${widget.date.year}-${widget.date.month}-${widget.date.day}",
        });
    print(response.body);
    provider.Provider.of<GlobalVariables>(context, listen: false)
        .updateDateFrom(DateTime.now().subtract(Duration(days: 7)));
    provider.Provider.of<GlobalVariables>(context, listen: false)
        .updateDateTo(DateTime.now());
    await provider.Provider.of<GlobalVariables>(context, listen: false)
        .getHistoryAll(
            DateTime.now().subtract(Duration(days: 7)), DateTime.now());
    Future.delayed(Duration(milliseconds: 1000), () {
      pr.hide();
      Navigator.pushReplacement(
          context, FadeRoute(page: DiaryLoading(widget.date, widget.token)));
    });
  }

  updateMeal() async {
    ProgressDialog pr = ProgressDialog(context);
    pr.update(
      message: "Updating..",
      progressWidget: Container(
          padding: EdgeInsets.all(8.0),
          child: CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation<Color>(kGreenColor),
          )),
    );
    pr.show();
    var responses = await http.get(
      '${kUrl}food_id/${widget.food['food_id']}',
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer ${widget.token}',
      },
    );
    var foodData = jsonDecode(responses.body);
    Map<String, dynamic> foodDataEntry = foodData['data'];
    double calories = 0;
    double protein = 0;
    double fat = 0;
    double saturatedFats = 0;
    double transFats = 0;
    double carbohydrates = 0;
    double sugars = 0;
    double fibre = 0;
    double vitaminA = 0;
    double carotene = 0;
    double vitaminB6 = 0;
    double vitaminB12 = 0;
    double vitaminB3 = 0;
    double vitaminC = 0;
    double vitaminE = 0;
    double cholesterol = 0;
    double potassium = 0;
    double sodium = 0;
    double calcium = 0;
    double magnesium = 0;
    double iron = 0;
    double manganese = 0;
    double zinc = 0;
    double copper = 0;
    double phosphorus = 0;
    double selenium = 0;

    if (foodDataEntry != null) {
      List<dynamic> facts = foodDataEntry['Facts'];
      print(facts);
      for (int f = facts.length; f < 26; f++) {
        Map x = {
          "name": "default",
          "value": 0,
        };
        facts.add(x);
      }

      for (int j = 0; j < 26; j++) {
        var item = facts[j];
        switch (item['name']) {
          case 'Calories':
            {
              calories = item['value'].toDouble();
            }
            break;
          case 'Protein':
            {
              protein = item['value'].toDouble();
            }
            break;
          case 'Fat':
            {
              fat = item['value'].toDouble();
            }
            break;
          case 'Saturated Fats':
            {
              saturatedFats = item['value'].toDouble();
            }
            break;
          case 'Trans Fats':
            {
              transFats = item['value'].toDouble();
            }
            break;
          case 'Carbohydrates':
            {
              carbohydrates = item['value'].toDouble();
            }
            break;
          case 'Sugars':
            {
              sugars = item['value'].toDouble();
            }
            break;
          case 'Fibre':
            {
              fibre = item['value'].toDouble();
            }
            break;
          case 'Vitamin A':
            {
              vitaminA = item['value'].toDouble();
            }
            break;
          case 'Carotene':
            {
              carotene = item['value'].toDouble();
            }
            break;
          case 'Vitamin B6':
            {
              vitaminB6 = item['value'].toDouble();
            }
            break;
          case 'Vitamin B12':
            {
              vitaminB12 = item['value'].toDouble();
            }
            break;
          case 'Vitamin B3':
            {
              vitaminB3 = item['value'].toDouble();
            }
            break;
          case 'Vitamin C':
            {
              vitaminC = item['value'].toDouble();
            }
            break;
          case 'Vitamin E':
            {
              vitaminE = item['value'].toDouble();
            }
            break;
          case 'Cholesterol':
            {
              cholesterol = item['value'].toDouble();
            }
            break;
          case 'Potassium':
            {
              potassium = item['value'].toDouble();
            }
            break;
          case 'Sodium':
            {
              sodium = item['value'].toDouble();
            }
            break;
          case 'Calcium':
            {
              calcium = item['value'].toDouble();
            }
            break;
          case 'Magnesium':
            {
              magnesium = item['value'].toDouble();
            }
            break;
          case 'Iron':
            {
              iron = item['value'].toDouble();
            }
            break;
          case 'Manganese':
            {
              manganese = item['value'].toDouble();
            }
            break;
          case 'Zinc':
            {
              zinc = item['value'].toDouble();
            }
            break;
          case 'Copper':
            {
              copper = item['value'].toDouble();
            }
            break;
          case 'Phosphorus':
            {
              phosphorus = item['value'].toDouble();
            }
            break;
          case 'Selenium':
            {
              selenium = item['value'].toDouble();
            }
            break;
        }
      }
    }

    var response = await http.put(
//      '${kUrl}diaryEdit/diary_id/${widget.food['diary_id']}?food_id=${widget.food['food_id']}&mealWeight=${getWeightToGram().toString()}&mealType=${widget.food['mealType']}&mealDate=${widget.date}&food_name=${foodDataEntry['name']}&calories=${weightGram * calories / 100}&protein=${weightGram * protein / 100}&carbohydrates${weightGram * carbohydrates / 100}&fat=${weightGram * fat / 100}&transFat=${weightGram * transFats / 100}&saturatedFats=${weightGram * saturatedFats / 100}&sugars=${weightGram * sugars / 100}&vitaminA=${weightGram * vitaminA / 100}&vitaminB3=${weightGram * vitaminB3 / 100}&vitaminB6=${weightGram * vitaminB6 / 100}&vitaminB12=${weightGram * vitaminB12 / 100}&vitaminC=${weightGram * vitaminC / 100}&vitaminE=${weightGram * vitaminE / 100}&calcium=${weightGram * calcium / 100}&cholesterol=${weightGram * cholesterol / 100}&iron=${weightGram * iron / 100}&manganese=${weightGram * manganese / 100}&magnesium=${weightGram * magnesium / 100}&sodium=${weightGram * sodium / 100}&potassium=${weightGram * potassium / 100}&zinc=${weightGram * zinc / 100}&copper=${weightGram * copper / 100}&selenium=${weightGram * selenium / 100}&phosphorus=${weightGram * phosphorus / 100}&fibre=${weightGram * fibre / 100}&carotene=${weightGram * carotene / 100}',
      '${kUrl}diaryEdit/diary_id/${widget.food['diary_id']}?food_id=${widget.food['food_id']}&mealWeight=${getWeightToGram().toString()}&mealType=${widget.food['mealType']}&mealDate=${widget.date}&food_name=${foodDataEntry['name']}&calories=${calories.toString()}&protein=${protein.toString()}&carbohydrates${carbohydrates.toString()}&fat=${fat.toString()}&transFat=${transFats.toString()}&saturatedFats=${saturatedFats.toString()}&sugars=${sugars.toString()}&vitaminA=${vitaminA.toString()}&vitaminB3=${vitaminB3.toString()}&vitaminB6=${vitaminB6.toString()}&vitaminB12=${vitaminB12.toString()}&vitaminC=${vitaminC.toString()}&vitaminE=${vitaminE.toString()}&calcium=${calcium.toString()}&cholesterol=${cholesterol.toString()}&iron=${iron.toString()}&manganese=${manganese.toString()}&magnesium=${magnesium.toString()}&sodium=${sodium.toString()}&potassium=${potassium.toString()}&zinc=${zinc.toString()}&copper=${copper.toString()}&selenium=${selenium.toString()}&phosphorus=${phosphorus.toString()}&fibre=${fibre.toString()}&carotene=${carotene.toString()}',
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer ${widget.token}',
      },
    );
    print(response.body);
    await provider.Provider.of<GlobalVariables>(context, listen: false)
        .getThemAll(widget.date, widget.token);
    provider.Provider.of<GlobalVariables>(context, listen: false)
        .updateDateFrom(DateTime.now().subtract(Duration(days: 7)));
    provider.Provider.of<GlobalVariables>(context, listen: false)
        .updateDateTo(DateTime.now());
    await provider.Provider.of<GlobalVariables>(context, listen: false)
        .getHistoryAll(
            DateTime.now().subtract(Duration(days: 7)), DateTime.now());
    Future.delayed(Duration(milliseconds: 500), () {
      pr.hide();
      Navigator.pushReplacement(context, FadeRoute(page: DiaryScreen()));
    });
  }

  @override
  void initState() {
    super.initState();
    weightGram = double.parse(widget.food['weight']);
    if (widget.unitType == 'g') {
      myController.text = getWeightFromGram().toStringAsFixed(1);
    } else if (widget.unitType == 'lb') {
      myController.text = getWeightFromGram().toStringAsFixed(2);
    } else if (widget.unitType == 'oz') {
      myController.text = getWeightFromGram().toStringAsFixed(1);
    } else {
      int decimalCount = getWeightFromGram().toString().split(".")[1].length;
      decimalCount = decimalCount > 3 ? 3 : decimalCount;
      myController.text = getWeightFromGram().toStringAsFixed(decimalCount);
    }

    if (widget.unitType == 'lb') {
      weightOtherType = double.parse(myController.text);
      int pound = int.parse(myController.text.split(".")[0]);
      double decimalPound =
          double.parse("0." + myController.text.split(".")[1]) * 16;
      previousString = '$pound:${decimalPound.toStringAsFixed(2)}';
      myController.text = '$pound:${decimalPound.toStringAsFixed(2)}';
    } else {
      weightOtherType = double.parse(myController.text);
    }
  }

  getWeightFromGram() {
    double w;
    if (widget.unitType == 'g') {
      w = weightGram;
    } else if (widget.unitType == 'ml') {
      w = weightGram;
    } else if (widget.unitType == 'lb') {
      w = weightGram / 453.59237;
    } else if (widget.unitType == 'oz') {
      w = weightGram / 28.3495231;
    } else if (widget.unitType == 'kg') {
      w = weightGram / 1000;
    }
    return w;
  }

  getWeightToGram() {
    double w;
    if (widget.unitType == 'g') {
      w = weightOtherType;
    } else if (widget.unitType == 'ml') {
      w = weightOtherType;
    } else if (widget.unitType == 'lb') {
      w = weightOtherType * 453.59237;
    } else if (widget.unitType == 'oz') {
      w = weightOtherType * 28.3495231;
    } else if (widget.unitType == 'kg') {
      w = weightOtherType * 1000;
    }
    return w;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: widgetHeight,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
      ),
      margin: EdgeInsets.all(5),
      padding: EdgeInsets.all(20),
      child: ListView(
        children: <Widget>[
          ListTile(
            title: Text('${widget.food['food_name']}',
                style: TextStyle(fontSize: 13, fontWeight: FontWeight.w300)),
            trailing: Container(
              height: 20,
              width: 170,
              child: TextField(
                  controller: myController,
                  keyboardType: widget.unitType == 'lb'
                      ? TextInputType.text
                      : TextInputType.numberWithOptions(decimal: true),
                  onChanged: (value) {
                    print("value: " + value);
                    double w = 0;
                    if (widget.unitType == 'lb') {
                      if (previousString == "" && value == ":") {
                        myController.text = "0:";
                        myController.selection = TextSelection.collapsed(
                            offset: myController.text.length);
                        return;
                      }
                      if (!previousString.contains(":") &&
                          value.contains(".")) {
                        myController.text = "0:0.";
                        previousString = "0:0.";
                        myController.selection = TextSelection.collapsed(
                            offset: myController.text.length);
                        return;
                      }
                      if (value.isNotEmpty) {
                        List<String> lbAoz = value.split(":");
                        print("lbAoz('${value}'): " + lbAoz.length.toString());
                        if (lbAoz.length == 1) {
                          try {
                            w = double.parse(value) ?? "0";
                          } catch (e) {
                            w = 0;
                          }
                          print("lb => " + w.toString());
                        } else if (lbAoz.length == 2) {
                          double pounds = 0;
                          double convertToPoundValue = 0;
                          try {
                            pounds = double.parse(lbAoz[0]);
                          } catch (e) {
                            pounds = 0;
                          }
                          try {
                            String oz = lbAoz[1] ?? "0";
                            convertToPoundValue = double.parse(oz) * (1 / 16);
                          } catch (e) {
                            convertToPoundValue = 0;
                          }
                          w = pounds + convertToPoundValue;
                          print("lb&oz => " + w.toString());
                        } else {
                          w = 0;
                        }
                      }
                    } else {
                      try {
                        w = double.parse(value);
                      } catch (e) {
                        w = 0;
                      }
                    }
                    previousString = value;
                    if (value == "") {
                      setState(() {
                        weightOtherType = 0;
                      });
                    } else {
                      setState(() {
                        weightOtherType = w;
                      });
                    }
                    print("convertion End: " + w.toString());
                    /*setState(() {
                    try {
                      weightOtherType = double.parse(value);
                    } catch (e) {
                      weightOtherType = 0;
                    }
                  });*/
                  },
                  style: TextStyle(fontWeight: FontWeight.bold),
                  textAlign: TextAlign.center,
                  decoration: InputDecoration(
                    suffixText:
                        widget.unitType == 'lb' ? 'lb:oz' : widget.unitType,
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: kGreenColor),
                    ),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: kGreenColor),
                    ),
                    border: UnderlineInputBorder(
                      borderSide: BorderSide(color: kGreenColor),
                    ),
                    hintText: '',
                  ),
                  inputFormatters: widget.unitType == 'lb'
                      ? [FilteringTextInputFormatter.allow(RegExp(r'[0-9:.]'))]
                      : [FilteringTextInputFormatter.allow(RegExp(r'[0-9.]'))]),
            ),
          ),
          Container(
            decoration: BoxDecoration(
              color: kGreyColor,
              borderRadius: BorderRadius.circular(10),
            ),
            child: ConfigurableExpansionTile(
              header: SizedBox(
                child: Center(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Text(
                        'Nutrition Facts',
                        style: TextStyle(color: kGreenColor, fontSize: 15),
                      ),
                      Icon(Icons.keyboard_arrow_down, color: Colors.grey),
                    ],
                  ),
                ),
                height: 35,
              ),
              headerExpanded: SizedBox(
                child: Center(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Text(
                        'See Less',
                        style: TextStyle(color: Colors.grey, fontSize: 15),
                      ),
                      Icon(Icons.keyboard_arrow_up, color: Colors.grey),
                    ],
                  ),
                ),
                height: 35,
              ),
              onExpansionChanged: (bool isExpand) {
                if (isExpand) {
                  setState(() {
                    isExpanded = isExpand;
                    widgetHeight = 400;
                  });
                } else {
                  setState(() {
                    isExpanded = isExpand;
                    widgetHeight = 200;
                  });
                }
              },
              children: <Widget>[
                SizedBox(
                  height: 170,
                  child: Scrollbar(
                    child: ListView(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 35.0,
                            vertical: 6,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  'Calories (kcal)',
                                ),
                              ),
                              Text(
                                _getFormedGramsValue(widget.food['calories']),
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 35.0,
                            vertical: 6,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  'Protein (g)',
                                ),
                              ),
                              Text(
                                _getFormedGramsValue(widget.food['protein']),
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 35.0,
                            vertical: 6,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  'Fat (g)',
                                ),
                              ),
                              Text(
                                _getFormedGramsValue(widget.food['fat']),
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 35.0,
                            vertical: 6,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  '        Saturated fats (g)',
                                ),
                              ),
                              Text(
                                _getFormedGramsValue(
                                    widget.food['saturatedFats']),
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 35.0,
                            vertical: 6,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  '        Trans fats (g)',
                                ),
                              ),
                              Text(
                                _getFormedGramsValue(widget.food['transFat']),
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 35.0,
                            vertical: 6,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  'Carbohydrates (g)',
                                ),
                              ),
                              Text(
                                _getFormedGramsValue(
                                    widget.food['carbohydrates']),
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 35.0,
                            vertical: 6,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  '        Sugars (g)',
                                ),
                              ),
                              Text(_getFormedGramsValue(widget.food['sugars']))
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 35.0,
                            vertical: 6,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  'Fibre (g)',
                                ),
                              ),
                              Text(_getFormedGramsValue(widget.food['fibre']))
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 35.0,
                            vertical: 6,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  'Vitamin A (IU)',
                                ),
                              ),
                              Text(
                                  _getFormedGramsValue(widget.food['vitaminA']))
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 35.0,
                            vertical: 6,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  'Carotene (µg)',
                                ),
                              ),
                              Text(
                                  _getFormedGramsValue(widget.food['carotene']))
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 35.0,
                            vertical: 6,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  'Vitamin B6 (mg)',
                                ),
                              ),
                              Text(_getFormedGramsValue(
                                  widget.food['vitaminB6']))
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 35.0,
                            vertical: 6,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  'Vitamin B12 (µg)',
                                ),
                              ),
                              Text(_getFormedGramsValue(
                                  widget.food['vitaminB12']))
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 35.0,
                            vertical: 6,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  'Vitamin B3 (mg)',
                                ),
                              ),
                              Text(_getFormedGramsValue(
                                  widget.food['vitaminB3']))
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 35.0,
                            vertical: 6,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  'Vitamin C (mg)',
                                ),
                              ),
                              Text(
                                  _getFormedGramsValue(widget.food['vitaminC']))
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 35.0,
                            vertical: 6,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  'Vitamin E (IU)',
                                ),
                              ),
                              Text(
                                  _getFormedGramsValue(widget.food['vitaminE']))
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 35.0,
                            vertical: 6,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  'Cholesterol (mg)',
                                ),
                              ),
                              Text(_getFormedGramsValue(
                                  widget.food['cholesterol']))
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 35.0,
                            vertical: 6,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  'Potassium (mg)',
                                ),
                              ),
                              Text(_getFormedGramsValue(
                                  widget.food['potassium']))
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 35.0,
                            vertical: 6,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  'Sodium (mg)',
                                ),
                              ),
                              Text(_getFormedGramsValue(widget.food['sodium']))
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 35.0,
                            vertical: 6,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  'Calcium (mg)',
                                ),
                              ),
                              Text(_getFormedGramsValue(widget.food['calcium']))
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 35.0,
                            vertical: 6,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  'Magnesium (mg)',
                                ),
                              ),
                              Text(_getFormedGramsValue(
                                  widget.food['magnesium']))
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 35.0,
                            vertical: 6,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  'Iron (mg)',
                                ),
                              ),
                              Text(_getFormedGramsValue(widget.food['iron']))
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 35.0,
                            vertical: 6,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  'Manganese (mg)',
                                ),
                              ),
                              Text(_getFormedGramsValue(
                                  widget.food['manganese']))
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 35.0,
                            vertical: 6,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  'Zinc (mg)',
                                ),
                              ),
                              Text(
                                _getFormedGramsValue(widget.food['zinc']),
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 35.0,
                            vertical: 6,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  'Copper (mg)',
                                ),
                              ),
                              Text(_getFormedGramsValue(widget.food['copper']))
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 35.0,
                            vertical: 6,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  'Phosphorus (mg)',
                                ),
                              ),
                              Text(_getFormedGramsValue(
                                  widget.food['phosphorus']))
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 35.0,
                            vertical: 6,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  'Selenium (µg)',
                                ),
                              ),
                              Text(
                                _getFormedGramsValue(widget.food['selenium']),
                              )
                            ],
                          ),
                        ),
                        SizedBox(height: 10),
                      ],
                    ),
                  ),
                ),
              ],
              initiallyExpanded: isExpanded,
            ),
          ),
          SizedBox(height: 20),
          Row(
            children: <Widget>[
              Expanded(
                  child: Material(
                color: kGreyColor,
                borderRadius: BorderRadius.circular(10.0),
                elevation: 0,
                child: MaterialButton(
                  onPressed: () {
                    deleteDiary();
                  },
                  minWidth: 200.0,
                  height: 42.0,
                  child: Text(
                    'Remove Food',
                    style: TextStyle(
                      color: Colors.black,
                    ),
                  ),
                ),
              )),
              SizedBox(width: 5),
              Expanded(
                  child: Material(
                color: kGreenColor,
                borderRadius: BorderRadius.circular(10.0),
                elevation: 0,
                child: MaterialButton(
                  onPressed: () {
                    updateMeal();
                  },
                  minWidth: 200.0,
                  height: 42.0,
                  child: Text(
                    'Update',
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                ),
              )),
            ],
          )
        ],
      ),
    );
  }

  String _getFormedGramsValue(String grams) {
    return (getWeightToGram() / 100 * double.parse(grams)).toStringAsFixed(1);
  }
}

class Button extends StatelessWidget {
  final String text;
  final Color color;
  final Color textColor;

  Button(this.text, this.color, this.textColor);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: color,
      borderRadius: BorderRadius.circular(10.0),
      elevation: 0,
      child: MaterialButton(
        onPressed: () {},
        minWidth: 200.0,
        height: 42.0,
        child: Text(
          text,
          style: TextStyle(
            color: textColor,
          ),
        ),
      ),
    );
  }
}
