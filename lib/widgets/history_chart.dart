import 'package:flutter/material.dart';
import 'package:bezier_chart/bezier_chart.dart';
import 'package:nutrilifeapp/const.dart';

Widget historyChart(
    BuildContext context,
    String unit,
    double goal,
    DateTime dateFrom,
    DateTime dateTo,
    List<dynamic> foodList,
    List<dynamic> waterList,
    int nutrition) {
  final fromDate = dateFrom;
  final toDate = dateTo;
  Map<int, String> nutritionsNames = {
    1: "calories",
    2: "protein",
    3: "fat",
    4: "carbohydrates",
    5: "fibre",
    6: "vitaminA",
    7: "carotene",
    8: "vitaminB6",
    9: "vitaminB12",
    10: "vitaminB3",
    11: "vitaminC",
    12: "vitaminE",
    13: "cholesterol",
    14: "potassium",
    15: "sodium",
    16: "calcium",
    17: "magnesium",
    18: "iron",
    19: "manganese",
    20: "zinc",
    21: "copper",
    22: "phosphorus",
    23: "selenium",
    24: "water",
  };
//
//  final date1 = DateTime.now().subtract(Duration(days: 2));
//  final date2 = DateTime.now().subtract(Duration(days: 3));
//  final date3 = DateTime.now().subtract(Duration(days: 6));
//  final date4 = DateTime.now().subtract(Duration(days: 8));
//  final date5 = DateTime.now().subtract(Duration(days: 13));
//  final date6 = DateTime.now().subtract(Duration(days: 14));
//  final date7 = DateTime.now().subtract(Duration(days: 17));

  getFoodValue(String date) {
    double value = 0;
    if (foodList.length != 0) {
      for (int i = 0; i < foodList.length; i++) {
        Map singleFood = foodList[i];
        if (singleFood['date'] == date) {
          value = value +
              (double.parse(singleFood['weight']) /
                  100 *
                  double.parse(singleFood[nutritionsNames[nutrition]]));
        }
      }
      return value;
    } else {
      return value;
    }
  }

  getWaterValue(String date) {
    double value = 0;
    if (waterList.length != 0) {
      for (int i = 0; i < waterList.length; i++) {
        Map singleWater = waterList[i];
        if (singleWater['date'] == date) {
          double z = double.parse(singleWater["water_amount"]);
          value = value + z;
        }
      }
      return value;
    } else {
      return value;
    }
  }

  getDataPoints() {
    DateTime date1 = fromDate;
    DateTime date2 = toDate;
    List<DataPoint<DateTime>> data = [];
    if (nutrition == 24) {
      if (waterList != null) {
        while (
            '${date1.subtract(Duration(days: 1)).year}-${date1.subtract(Duration(days: 1)).month}-${date1.subtract(Duration(days: 1)).day}' !=
                '${date2.year}-${date2.month}-${date2.day}') {
          int x = 0;
          if (date1.day < 10) {
            if (date1.month < 10) {
              x = getWaterValue('${date1.year}-0${date1.month}-0${date1.day}')
                  .round();
            } else {
              x = getWaterValue('${date1.year}-${date1.month}-0${date1.day}')
                  .round();
            }
          } else {
            if (date1.month < 10) {
              x = getWaterValue('${date1.year}-0${date1.month}-${date1.day}')
                  .round();
            } else {
              x = getWaterValue('${date1.year}-${date1.month}-${date1.day}')
                  .round();
            }
          }
          double value = x.toDouble();
          data.add(DataPoint<DateTime>(value: value, xAxis: date1));
          date1 = date1.add(Duration(days: 1));
        }
      }
    } else {
      if (waterList != null) {
        while (
            '${date1.subtract(Duration(days: 1)).year}-${date1.subtract(Duration(days: 1)).month}-${date1.subtract(Duration(days: 1)).day}' !=
                '${date2.year}-${date2.month}-${date2.day}') {
          int x = 0;
          if (date1.day < 10) {
            if (date1.month < 10) {
              x = getFoodValue('${date1.year}-0${date1.month}-0${date1.day}')
                  .round();
            } else {
              x = getFoodValue('${date1.year}-${date1.month}-0${date1.day}')
                  .round();
            }
          } else {
            if (date1.month < 10) {
              x = getFoodValue('${date1.year}-0${date1.month}-${date1.day}')
                  .round();
            } else {
              x = getFoodValue('${date1.year}-${date1.month}-${date1.day}')
                  .round();
            }
          }
          double value = x.toDouble();
          data.add(DataPoint<DateTime>(value: value, xAxis: date1));
          date1 = date1.add(Duration(days: 1));
        }
      }
    }
    return data;
  }

  return Center(
    child: Container(
      color: kGreenColor,
      height: MediaQuery.of(context).size.height / 2,
      width: MediaQuery.of(context).size.width,
      child: BezierChart(
        fromDate: fromDate,
        bezierChartScale: BezierChartScale.WEEKLY,
        toDate: toDate,
        selectedDate: toDate,
        series: [
          BezierLine(
            dataPointFillColor: Colors.white,
            label: "$unit [Total]",
            onMissingValue: (dateTime) {
              return 0;
            },
            data: getDataPoints(),
//            [
//              DataPoint<DateTime>(value: 15, xAxis: date1),
//              DataPoint<DateTime>(value: 42, xAxis: date2),
//              DataPoint<DateTime>(value: 65, xAxis: date3),
//              DataPoint<DateTime>(value: 32, xAxis: date4),
//              DataPoint<DateTime>(value: 54, xAxis: date5),
//              DataPoint<DateTime>(value: 24, xAxis: date6),
//              DataPoint<DateTime>(value: 45, xAxis: date7),
//            ],
          ),
          BezierLine(
            dataPointFillColor: Colors.grey[300],
            label: "$unit [Goal]",
            onMissingValue: (dateTime) {
              return goal;
            },
            data: [
              DataPoint<DateTime>(value: goal, xAxis: DateTime.now()),
            ],
            lineColor: Colors.grey[300],
            lineStrokeWidth: 1,
          ),
        ],
        config: BezierChartConfig(
            verticalIndicatorStrokeWidth: 3.0,
            verticalIndicatorColor: kGreyColor,
            showVerticalIndicator: true,
            verticalIndicatorFixedPosition: false,
            backgroundColor: kGreenColor,
            footerHeight: 50.0,
            updatePositionOnTap: true,
            pinchZoom: true),
      ),
    ),
  );
}
//
//class SimpleTimeSeriesChart extends StatelessWidget {
//  final List<charts.Series> seriesList;
//  final bool animate;
//
//  SimpleTimeSeriesChart(this.seriesList, {this.animate});
//
//  /// Creates a [TimeSeriesChart] with sample data and no transition.
//  factory SimpleTimeSeriesChart.withSampleData() {
//    return new SimpleTimeSeriesChart(
//      _createSampleData(),
//      // Disable animations for image tests.
//      animate: true,
//    );
//  }
//
//
//  @override
//  Widget build(BuildContext context) {
//    return new charts.TimeSeriesChart(
//      seriesList,
//      animate: animate,
//      // Optionally pass in a [DateTimeFactory] used by the chart. The factory
//      // should create the same type of [DateTime] as the data provided. If none
//      // specified, the default creates local date time.
//      dateTimeFactory: const charts.LocalDateTimeFactory(),
//    );
//  }
//
//  /// Create one series with sample hard coded data.
//  static List<charts.Series<TimeSeriesSales, DateTime>> _createSampleData() {
//    final data = [
//      new TimeSeriesSales(new DateTime(2020, 6, 06), 45),
//      new TimeSeriesSales(new DateTime(2020, 6, 09), 29),
//      new TimeSeriesSales(new DateTime(2020, 6, 14), 53),
//      new TimeSeriesSales(new DateTime(2020, 6, 16), 74),
//      new TimeSeriesSales(new DateTime(2020, 6, 19), 150),
//      new TimeSeriesSales(new DateTime(2020, 6, 21), 95),
//      new TimeSeriesSales(new DateTime(2020, 6, 25), 65),
//      new TimeSeriesSales(new DateTime(2020, 7, 1), 85),
//      new TimeSeriesSales(new DateTime(2020, 7, 3), 100),
//      new TimeSeriesSales(new DateTime(2020, 7, 6), 75),
//    ];
//
//    return [
//      new charts.Series<TimeSeriesSales, DateTime>(
//        id: 'Sales',
//        colorFn: (_, __) => charts.MaterialPalette.green.shadeDefault,
//        domainFn: (TimeSeriesSales sales, _) => sales.time,
//        measureFn: (TimeSeriesSales sales, _) => sales.sales,
//        data: data,
//      )
//    ];
//  }
//}
//
///// Sample time series data type.
//class TimeSeriesSales {
//  final DateTime time;
//  final int sales;
//
//  TimeSeriesSales(this.time, this.sales);
//}
