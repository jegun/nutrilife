import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nutrilifeapp/const.dart';
import 'package:nutrilifeapp/screens/diary.dart';
import 'package:nutrilifeapp/screens/diaryLoading.dart';
import 'package:nutrilifeapp/services/shared_preferences.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:http/http.dart' as http;
import 'package:nutrilifeapp/services/global.dart';
import 'package:provider/provider.dart' as provider;

NutrilifeSharedPreferences pref = NutrilifeSharedPreferences();

class EditWater extends StatefulWidget {
  Map waterRecord;
  final DateTime date;
  final String token;
  EditWater(this.waterRecord, this.date, this.token);
  @override
  _EditWaterState createState() => _EditWaterState();
}

class _EditWaterState extends State<EditWater> {
  final myController = TextEditingController();
  double waterAmount = 0;

  deleteWater() async{
    ProgressDialog pr = ProgressDialog(context);
    pr.update(
      message: "Removing..",
      progressWidget: Container(
          padding: EdgeInsets.all(8.0),
          child: CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation<Color>(
                kGreenColor),
          )),
    );
    pr.show();
   var response = await http.post(
        '${kUrl}destroyWater/water_id/${widget.waterRecord['water_id']}',
        headers: {
          'Accept' : 'application/json',
          'Authorization' : 'Bearer ${widget.token}',
        },
        body: {
          'datedate': "${widget.date.year}-${widget.date.month}-${widget.date.day}",
        }
    );
    print(response.body);
    provider.Provider.of<GlobalVariables>(context, listen: false).updateDateFrom(DateTime.now().subtract(Duration(days: 7)));
    provider.Provider.of<GlobalVariables>(context, listen: false).updateDateTo(DateTime.now());
    await provider.Provider.of<GlobalVariables>(context, listen: false).getHistoryAll(DateTime.now().subtract(Duration(days: 7)), DateTime.now());
    Future.delayed(
        Duration(milliseconds: 500),
            () {
          pr.hide();
          Navigator.pushReplacement(context, FadeRoute(
              page: DiaryLoading(widget.date, widget.token)
          ));
        }
    );
  }

  updateWater() async{
    ProgressDialog pr = ProgressDialog(context);
    pr.update(
      message: "Updating..",
      progressWidget: Container(
          padding: EdgeInsets.all(8.0),
          child: CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation<Color>(
                kGreenColor),
          )),
    );
    pr.show();
    var response = await http.put(
      '${kUrl}editWater/water_id/${widget.waterRecord['water_id']}?amount=$waterAmount&date=${widget.date}',
      headers: {
        'Accept' : 'application/json',
        'Authorization' : 'Bearer ${widget.token}',
      },
    );
    print(response.body);
    await provider.Provider.of<GlobalVariables>(context, listen: false).getWater(widget.date, widget.token);
    provider.Provider.of<GlobalVariables>(context, listen: false).updateDateFrom(DateTime.now().subtract(Duration(days: 7)));
    provider.Provider.of<GlobalVariables>(context, listen: false).updateDateTo(DateTime.now());
    await provider.Provider.of<GlobalVariables>(context, listen: false).getHistoryAll(DateTime.now().subtract(Duration(days: 7)), DateTime.now());
    Future.delayed(
        Duration(milliseconds: 500),
            () {
          pr.hide();
          Navigator.pushReplacement(context, FadeRoute(
              page: DiaryScreen()
          ));
        }
    );
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    myController.text = widget.waterRecord['water_amount'].toString();
    waterAmount = double.parse(widget.waterRecord['water_amount'].toString());
  }
  @override
  Widget build(BuildContext context) {
    ProgressDialog pr = ProgressDialog(context);
    return Container(
      height: 200,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
      ),
      margin: EdgeInsets.all(5),
      padding: EdgeInsets.all(20),
      child: ListView(
        children: <Widget>[
          Text(
            'Water consumed:', textAlign: TextAlign.left,
          ),
          Container(
            padding: EdgeInsets.symmetric(vertical:0),
            child: TextField(
              controller: myController,
              onChanged: (x){
                setState(() {
                  waterAmount = double.parse(x);
                });
              },
              decoration: InputDecoration(
                suffixText: 'ml',
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: kGreenColor),
                ),
                focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: kGreenColor),
                ),
                border: UnderlineInputBorder(
                  borderSide: BorderSide(color: kGreenColor),
                ),
                hintText: '',
              ),
            ),
          ),
          SizedBox(height:20),
          Row(
            children: <Widget>[
              Expanded(
                child:
                 Material(
                    color: kGreyColor,
                    borderRadius: BorderRadius.circular(10.0),
                    elevation: 0,
                    child: MaterialButton(
                      onPressed: (){
                        deleteWater();
                      },
                      minWidth: 200.0,
                      height: 42.0,
                      child: Text(
                        'Remove Water', style: TextStyle(color: Colors.black,),
                     ),
                        ),
               )
              ),
              SizedBox(width:5),
              Expanded(
                  child:
                  Material(
                    color: kGreenColor,
                    borderRadius: BorderRadius.circular(10.0),
                    elevation: 0,
                    child: MaterialButton(
                      onPressed: () {
                       updateWater();
                      },
                      minWidth: 200.0,
                      height: 42.0,
                      child: Text(
                        'Update', style: TextStyle(color: Colors.white,),
                      ),
                    ),
                  )
              ),
            ],
          )
        ],
      ),
    );
  }
}
