import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:nutrilifeapp/const.dart';
import 'package:nutrilifeapp/screens/nutritional_analysis.dart';

Map<String, String> factSyn = {
  "Calories (kcal)" : "calories",
  "Protein (g)" : "protein",
  "Fat (g)" : "fat",
  "Carbohydrates (g)" : "carbohydrates",
  "Fibre (g)" : "fibre",
  "Vitamin A (IU)" : "vitaminA",
  "Carotene (µg)" : "carotene",
  "Vitamin B6 (mg)" : "vitaminB6",
  "Vitamin B12 (µg)" : "vitaminB12",
  "Vitamin B3 (mg)" : "vitaminB3",
  "Vitamin C (mg)" : "vitaminC",
  "Vitamin E (IU)" : "vitaminE",
  "Cholesterol (mg)" : "cholesterol",
  "Potassium (mg)" : "potassium",
  "Sodium (mg)" : "sodium",
  "Calcium (mg)" : "calcium",
  "Magnesium (mg)" : "magnesium",
  "Iron (mg)" : "iron",
  "Manganese (mg)" : "manganese",
  "Zinc (mg)" : "zinc",
  "Copper (mg)" : "copper",
  "Phosphorus (mg)" : "phosphorus",
  "Selenium (µg)" : "selenium",
};


Map<String, String> factSyn2 = {
  "Calories (kcal)" : "calories",
  "Protein (g)" : "protein",
  "Fat (g)" : "fat",
  "Carbohydrates (g)" : "carbohydrates",
  "Fibre (g)" : "fiber",
  "Vitamin A (IU)" : "vitamin a",
  "Carotene (µg)" : "carotene",
  "Vitamin B6 (mg)" : "vitamin b6",
  "Vitamin B12 (µg)" : "vitamin b12",
  "Vitamin B3 (mg)" : "vitamin b3",
  "Vitamin C (mg)" : "vitamin c",
  "Vitamin E (IU)" : "vitamin e",
  "Cholesterol (mg)" : "cholestrol",
  "Potassium (mg)" : "potassium",
  "Sodium (mg)" : "sodium",
  "Calcium (mg)" : "calcium",
  "Magnesium (mg)" : "magnesium",
  "Iron (mg)" : "iron",
  "Manganese (mg)" : "manganese",
  "Zinc (mg)" : "zinc",
  "Copper (mg)" : "copper",
  "Phosphorus (mg)" : "phosphorus",
  "Selenium (µg)" : "selenium",
};

class SingleNutritionChart extends StatefulWidget {
  final List<dynamic> allFood;
  Map<String, dynamic> dailyGoal;
  final String name;
  final double total;
  final double goal;
  SingleNutritionChart(this.allFood, this.dailyGoal, this.name, this.total, this.goal);
  @override
  _SingleNutritionChartState createState() => _SingleNutritionChartState();
}

class _SingleNutritionChartState extends State<SingleNutritionChart> {


  getGoal(String fact){
    String value;
    value = widget.dailyGoal['$fact'].toString();
    return value;
  }

  getValue(String fact){
    double value = 0;
    for(int i = 0; i < widget.allFood.length; i++){
      Map singleFood = widget.allFood[i];
      //value = value + double.parse(singleFood['$fact']);
      value = value +
          (double.parse(singleFood['weight']) /
              100 *
              double.parse(singleFood['$fact']));
    }
    return value;
  }


  Widget getSugar(){
    if (widget.name == 'Carbohydrates (g)'){
      return NutritionProgress(widget.allFood, widget.dailyGoal,'Sugars (g)',double.parse(getGoal('sugars')),getValue('sugars'),false);
    } else{
      return Container();
    }
  }


  Widget getFat1(){
    if (widget.name == 'Fat (g)'){
      return NutritionProgress(widget.allFood, widget.dailyGoal,'Saturated Fats (g)',double.parse(getGoal('saturated fat')),getValue('saturatedFats'),false);
    } else{
      return Container();
    }
  }

  Widget getFat2(){
    if (widget.name == 'Fat (g)'){
      return NutritionProgress(widget.allFood, widget.dailyGoal,'Trans Fats (g)',double.parse(getGoal('trans fat')),getValue('transFat'),false);
    } else{
      return Container();
    }
  }


  getMealList(String mealType){
    List<Map> food = [];
    Map singleFood;

    for(int i = 0; i < widget.allFood.length; i++){
      singleFood = widget.allFood[i];
      if(singleFood['mealType'] == mealType){
        food.add(singleFood);
      }
    }
    return food;
  }

  getFactTotal(String fact, List<Map> foodList){
    double value = 0;
    for(int i = 0; i < foodList.length; i++){
      Map singleFood = foodList[i];
      //value = value + double.parse(singleFood['$fact']);
      value = value + (double.parse(singleFood['weight']) /
          100 * double.parse(singleFood['$fact']));
    }
    return value;
  }

  List<charts.Series<MealValue, String>> dataSeries() {
    final data = [
      new MealValue('1', double.parse((getFactTotal(factSyn[widget.name],getMealList("0")).round()).toString())),
      new MealValue('2', double.parse((getFactTotal(factSyn[widget.name],getMealList("1")).round()).toString())),
      new MealValue('3', double.parse((getFactTotal(factSyn[widget.name],getMealList("2")).round()).toString())),
      new MealValue('4', double.parse((getFactTotal(factSyn[widget.name],getMealList("3")).round()).toString())),
    ];

    return [
      new charts.Series<MealValue, String>(
        id: 'Meal Values',
        domainFn: (MealValue meal, _) => meal.meal,
        measureFn: (MealValue meal, _) => meal.value,
        data: data,
        // Set a label accessor to control the text of the arc label.
        labelAccessorFn: (MealValue row, _) => '${row.meal}: ${row.value}',
        colorFn: (_, index){
          return charts.MaterialPalette.green.makeShades(4)[index];
        },

      )
    ];
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print(widget.allFood);
    print(widget.allFood.length);
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
            appBar: AppBar(
              leading: GestureDetector(
                onTap: (){Navigator.pop(context);},
                child: Icon(Icons.arrow_back, size: 30),),
              backgroundColor: kGreenColor,
            ),
            body: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
                border: Border.all(
                  color: Colors.grey[300],
                ),
              ),
              margin: EdgeInsets.all(5),
              padding: EdgeInsets.all(10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  SizedBox(height: 8,),
                  Center(child: Text('${widget.name}', style: TextStyle(fontSize: 20, fontWeight: FontWeight.w400), textAlign: TextAlign.center,)),
                  Expanded(
                    child: Scrollbar(
                      child: ListView(
                        children: <Widget>[
                          Container( height: 250, child:
                            ((getFactTotal(factSyn[widget.name],getMealList("0")) + getFactTotal(factSyn[widget.name],getMealList("1")) + getFactTotal(factSyn[widget.name],getMealList("2")) + getFactTotal(factSyn[widget.name],getMealList("3"))) == 0) ?
                            Center(child: Text('No data found!', style: TextStyle(color: Colors.grey)))
                                : DonutAutoLabelChart(dataSeries(), animate: true)

                          ),
                          NutritionDiary(),
                          Divider(),
                          NutritionProgress(widget.allFood, widget.dailyGoal,widget.name, double.parse(getGoal(factSyn2[widget.name])),getValue(factSyn[widget.name]),false),
                          getSugar(),
                          getFat1(),
                          getFat2(),
                          SizedBox(height: 5),
                          Meal('Breakfast',  widget.name, getMealList("0"), getFactTotal(factSyn[widget.name], getMealList("0"))),
                          SizedBox(height:5),
                          Meal('Lunch',  widget.name, getMealList("1"), getFactTotal(factSyn[widget.name], getMealList("1"))),
                          SizedBox(height:5),
                          Meal('Dinner',  widget.name, getMealList("2"), getFactTotal(factSyn[widget.name], getMealList("2"))),
                          SizedBox(height:5),
                          Meal('Snacks',  widget.name, getMealList("3"), getFactTotal(factSyn[widget.name], getMealList("3"))),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            )
        )
    );
  }
}



class DonutAutoLabelChart extends StatelessWidget {
  final List<charts.Series> seriesList;
  final bool animate;

  DonutAutoLabelChart(this.seriesList, {this.animate});

  /// Creates a [PieChart] with sample data and no transition.
  factory DonutAutoLabelChart.withSampleData() {
    return new DonutAutoLabelChart(
      _createSampleData(),
      // Disable animations for image tests.
      animate: true,
    );
  }


  @override
  Widget build(BuildContext context) {
    return new charts.PieChart(seriesList,
        animate: animate,
        // Configure the width of the pie slices to 60px. The remaining space in
        // the chart will be left as a hole in the center.
        //
        // [ArcLabelDecorator] will automatically position the label inside the
        // arc if the label will fit. If the label will not fit, it will draw
        // outside of the arc with a leader line. Labels can always display
        // inside or outside using [LabelPosition].
        //
        // Text style for inside / outside can be controlled independently by
        // setting [insideLabelStyleSpec] and [outsideLabelStyleSpec].
        //
        // Example configuring different styles for inside/outside:
        //       new charts.ArcLabelDecorator(
        //          insideLabelStyleSpec: new charts.TextStyleSpec(...),
        //          outsideLabelStyleSpec: new charts.TextStyleSpec(...)),
        defaultRenderer: new charts.ArcRendererConfig(
            arcWidth: 60,
            arcRendererDecorators: [new charts.ArcLabelDecorator()]));
  }

  /// Create one series with sample hard coded data.
  static List<charts.Series<MealValue, String>> _createSampleData() {
    final data = [
      new MealValue('1', 100),
      new MealValue('2', 75),
      new MealValue('3', 25),
      new MealValue('4', 5),
    ];

    return [
      new charts.Series<MealValue, String>(
        id: 'Meal Values',
        domainFn: (MealValue meal, _) => meal.meal,
        measureFn: (MealValue meal, _) => meal.value,
        data: data,
        // Set a label accessor to control the text of the arc label.
        labelAccessorFn: (MealValue row, _) => '${row.meal}: ${row.value}',
        colorFn: (_, index){
      return charts.MaterialPalette.green.makeShades(4)[index];
    },

      )
    ];
  }
}

/// Sample linear data type.
class MealValue {
  final String meal;
  final double value;

  MealValue(this.meal, this.value);
}


class NutritionDiary extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: <Widget>[
          Expanded(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(height: 8, width:8, color: kGreenColor),
                SizedBox(width:5),
                Text('1. Breakfast', style: TextStyle(fontSize: 12)),
              ],
            ),
          ),
          Expanded(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(height: 8, width:8, color: Colors.green[300]),
                SizedBox(width:5),
                Text('2. Lunch', style: TextStyle(fontSize: 12)),
              ],
            ),
          ),
          Expanded(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(height: 8, width:8, color: Colors.green[200]),
                SizedBox(width:5),
                Text('3. Dinner', style: TextStyle(fontSize: 12)),
              ],
            ),
          ),
          Expanded(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(height: 8, width:8, color: Colors.green[100]),
                SizedBox(width:5),
                Text('4. Snacks', style: TextStyle(fontSize: 12)),
              ],
            ),
          ),
        ],
      )
    );
  }
}


class Meal extends StatefulWidget {
  final String mealName;
  final String factName;
  List<Map> mealList;
  double factTotal;
  Meal(this.mealName, this.factName, this.mealList, this.factTotal);
  @override
  _MealState createState() => _MealState();
}

class _MealState extends State<Meal> {
  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          border: Border.all(
            color: Colors.grey[300],
          ),
        ),
        margin: EdgeInsets.all(5),
        child: Column(
          children: <Widget>[
            ListTile(
              title: Text(widget.mealName, style: TextStyle(fontWeight: FontWeight.w700)),
              trailing: Text('${widget.factTotal.round()}'),
            ),
            (widget.mealList.length == 0 ) ? Container() : Divider(),
            Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
              ),
              height: (widget.mealList.length == 0) ? 0 : widget.mealList.length.toDouble() * 60,
              child: ListView.builder
                (
                  physics: NeverScrollableScrollPhysics(),
                  itemCount: widget.mealList.length,
                  itemBuilder: (BuildContext context, int position) {
                    return  GestureDetector(
                      onTap:(){
                      },
                      onLongPress: (){
                      },
                      child: ListTile(
                        title: Text('${widget.mealList[position]['food_name']}'),
                        trailing: Text('${(double.parse(widget.mealList[position]['weight']) /
                            100 * double.parse(widget.mealList[position][factSyn[widget.factName]])).round()}'),
                      ),
                    );
                  }
              ),
            ),
          ],
        )
    );

  }
}
