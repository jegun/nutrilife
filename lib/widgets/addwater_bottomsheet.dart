import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nutrilifeapp/const.dart';
import 'package:nutrilifeapp/screens/diary.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:http/http.dart' as http;
import 'package:nutrilifeapp/services/global.dart';
import 'package:provider/provider.dart' as provider;


class AddWater extends StatefulWidget {
  final DateTime date;
  final String token;
  AddWater(this.date, this.token);
  @override
  _AddWaterState createState() => _AddWaterState();
}

class _AddWaterState extends State<AddWater> {
  double waterAmount = 0;

  addWater() async{
    if(waterAmount != 0) {
      ProgressDialog pr = ProgressDialog(context);
      pr.update(
        message: "Adding..",
        progressWidget: Container(
            padding: EdgeInsets.all(8.0),
            child: CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(
                  kGreenColor),
            )),
      );
      pr.show();

      var response = await http.post(
          '${kUrl}addWater',
          headers: {
            'Accept': 'application/json',
            'Authorization': 'Bearer ${widget.token}',
          },
          body: {
            'amount': '$waterAmount',
            'date': '${widget.date}',
          }
      );
      print(response.body);
      await provider.Provider.of<GlobalVariables>(context, listen: false).getWater(widget.date, widget.token);
      provider.Provider.of<GlobalVariables>(context, listen: false).updateDateFrom(DateTime.now().subtract(Duration(days: 7)));
      provider.Provider.of<GlobalVariables>(context, listen: false).updateDateTo(DateTime.now());
      await provider.Provider.of<GlobalVariables>(context, listen: false).getHistoryAll(DateTime.now().subtract(Duration(days: 7)), DateTime.now());
      Future.delayed(
          Duration(milliseconds: 500),
              () {
            pr.hide();
            Navigator.pushReplacement(context, FadeRoute(
                page: DiaryScreen()
            ));
          }
      );
    } else {
      wrongAlert(context, 'Please fill the field!');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
      ),
      margin: EdgeInsets.all(5),
      padding: EdgeInsets.all(20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          ListTile(
            title: Text('Add water to your diary', style: TextStyle(fontSize:15, fontWeight: FontWeight.w700)),
            trailing: Icon(Icons.local_drink),
          ),
          SizedBox(height: 10,),
          Container(
            decoration: BoxDecoration(
              color: kGreyColor,
              borderRadius: BorderRadius.circular(10),
            ),
            margin: EdgeInsets.all(2),
            padding: EdgeInsets.all(10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  child: Text(
                    'Water consumed', textAlign: TextAlign.center,
                  ),
                ),
                Expanded(
                  child: Container(
                    padding: EdgeInsets.symmetric(vertical:0),
                    child: TextField(
                      autofocus: true,
                      onChanged: (x){
                        setState(() {
                          waterAmount = double.parse(x);
                        });
                      },
                      decoration: InputDecoration(
                        suffixText: 'ml',
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: kGreenColor),
                        ),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: kGreenColor),
                        ),
                        border: UnderlineInputBorder(
                          borderSide: BorderSide(color: kGreenColor),
                        ),
                        hintText: '',
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height:15),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: FlatButton(
                  color: kGreyColor,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8.0),
                    side: BorderSide(color: kGreyColor),
                  ),
                  child: Text('Cancel',
                      style: TextStyle(
                        color: Colors.black,
                      )),
                  onPressed: (){
                    Navigator.pop(context);
                  },
                ),
              ),
              SizedBox(width: 10),
              Expanded(
                child: FlatButton(
                  color: kGreenColor,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8.0),
                    side: BorderSide(color: kGreenColor),
                  ),
                  child: Text('Add to Diary',
                      style: TextStyle(
                        color: Colors.white,
                      )),
                  onPressed: (){
                  addWater();
                  },
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
