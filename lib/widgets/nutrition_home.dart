import 'package:flutter/material.dart';
import 'package:nutrilifeapp/const.dart';
import 'package:configurable_expansion_tile/configurable_expansion_tile.dart';
import 'package:flutter/widgets.dart';
import 'package:nutrilifeapp/icons.dart';

double calories = 0;
double protein = 0;
double fat = 0;
double saturatedFats = 0;
double transFats = 0;
double carbohydrates = 0;
double sugars = 0;
double fibre = 0;
double vitaminA = 0;
double carotene = 0;
double vitaminB6 = 0;
double vitaminB12 = 0;
double vitaminB3 = 0;
double vitaminC = 0;
double vitaminE = 0;
double cholesterol = 0;
double potassium = 0;
double sodium = 0;
double calcium = 0;
double magnesium = 0;
double iron = 0;
double manganese = 0;
double zinc = 0;
double copper = 0;
double phosphorus = 0;
double selenium = 0;

class NutritionHome extends StatefulWidget {
  @override
  _NutritionHomeState createState() => _NutritionHomeState();
  List foodInfo;
  final double weight;
  final String userUnit;
  NutritionHome(this.foodInfo, this.weight, this.userUnit);
}

class _NutritionHomeState extends State<NutritionHome> {
  bool isExpanded = false;

  getWeightGram() {
    double weight;
    if (widget.userUnit == 'g') {
      weight = widget.weight;
    } else if (widget.userUnit == 'ml') {
      weight = widget.weight;
    } else if (widget.userUnit == 'lb') {
      weight = widget.weight * 453.59237;
    } else if (widget.userUnit == 'oz') {
      weight = widget.weight * 28.3495231;
    } else if (widget.userUnit == 'kg') {
      weight = widget.weight * 1000;
    }
    return weight;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getWeightGram();

    calories = 0;
    protein = 0;
    fat = 0;
    saturatedFats = 0;
    transFats = 0;
    carbohydrates = 0;
    sugars = 0;
    fibre = 0;
    vitaminA = 0;
    carotene = 0;
    vitaminB6 = 0;
    vitaminB12 = 0;
    vitaminB3 = 0;
    vitaminC = 0;
    vitaminE = 0;
    cholesterol = 0;
    potassium = 0;
    sodium = 0;
    calcium = 0;
    magnesium = 0;
    iron = 0;
    manganese = 0;
    zinc = 0;
    copper = 0;
    phosphorus = 0;
    selenium = 0;

    if (widget.foodInfo.isNotEmpty) {
      for (int i = widget.foodInfo.length; i < 26; i++) {
        Map x = {
          "name": "default",
          "value": 0,
        };
        widget.foodInfo.add(x);
      }
      for (int i = 0; i < 26; i++) {
        var item = widget.foodInfo[i];
        switch (item['name']) {
          case 'Calories':
            {
              calories = item['value'].toDouble();
            }
            break;
          case 'Protein':
            {
              protein = item['value'].toDouble();
            }
            break;
          case 'Fat':
            {
              fat = item['value'].toDouble();
            }
            break;
          case 'Saturated Fats':
            {
              saturatedFats = item['value'].toDouble();
            }
            break;
          case 'Trans Fats':
            {
              transFats = item['value'].toDouble();
            }
            break;
          case 'Carbohydrates':
            {
              carbohydrates = item['value'].toDouble();
            }
            break;
          case 'Sugars':
            {
              sugars = item['value'].toDouble();
            }
            break;
          case 'Fibre':
            {
              fibre = item['value'].toDouble();
            }
            break;
          case 'Vitamin A':
            {
              vitaminA = item['value'].toDouble();
            }
            break;
          case 'Carotene':
            {
              carotene = item['value'].toDouble();
            }
            break;
          case 'Vitamin B6':
            {
              vitaminB6 = item['value'].toDouble();
            }
            break;
          case 'Vitamin B12':
            {
              vitaminB12 = item['value'].toDouble();
            }
            break;
          case 'Vitamin B3':
            {
              vitaminB3 = item['value'].toDouble();
            }
            break;
          case 'Vitamin C':
            {
              vitaminC = item['value'].toDouble();
            }
            break;
          case 'Vitamin E':
            {
              vitaminE = item['value'].toDouble();
            }
            break;
          case 'Cholesterol':
            {
              cholesterol = item['value'].toDouble();
            }
            break;
          case 'Potassium':
            {
              potassium = item['value'].toDouble();
            }
            break;
          case 'Sodium':
            {
              sodium = item['value'].toDouble();
            }
            break;
          case 'Calcium':
            {
              calcium = item['value'].toDouble();
            }
            break;
          case 'Magnesium':
            {
              magnesium = item['value'].toDouble();
            }
            break;
          case 'Iron':
            {
              iron = item['value'].toDouble();
            }
            break;
          case 'Manganese':
            {
              manganese = item['value'].toDouble();
            }
            break;
          case 'Zinc':
            {
              zinc = item['value'].toDouble();
            }
            break;
          case 'Copper':
            {
              copper = item['value'].toDouble();
            }
            break;
          case 'Phosphorus':
            {
              phosphorus = item['value'].toDouble();
            }
            break;
          case 'Selenium':
            {
              selenium = item['value'].toDouble();
            }
            break;
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(7),
      decoration: BoxDecoration(
        border: Border.all(
          color: Colors.grey[300],
        ),
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
      ),
      margin: EdgeInsets.all(5),
      child: Column(
        children: <Widget>[
          isExpanded
              ? Container()
              : Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                      Expanded(
                        child: Container(
                            child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Icon(MyFlutterApp.fire,
                                size: 50, color: Colors.grey),
                            SizedBox(height: 5),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Text('Calories'),
                                Text(
                                  '(kcal)',
                                  style: TextStyle(fontSize: 10),
                                ),
                              ],
                            ),
                            Text(
                                '${(getWeightGram() / 100 * calories).toStringAsFixed(1)}',
                                style: TextStyle(
                                    color: kGreenColor, fontSize: 30)),
                          ],
                        )),
                      ),
                      Expanded(
                        child: Container(
                            child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Icon(MyFlutterApp.strong,
                                size: 50, color: Colors.grey),
                            SizedBox(height: 5),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Text('Protein'),
                                Text(
                                  '(g)',
                                  style: TextStyle(fontSize: 10),
                                ),
                              ],
                            ),
                            Text(
                                '${(getWeightGram() / 100 * protein).toStringAsFixed(1)}',
                                style: TextStyle(
                                    color: kGreenColor, fontSize: 30)),
                          ],
                        )),
                      ),
                      Expanded(
                        child: Container(
                            child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Icon(MyFlutterApp.water_drop,
                                size: 50, color: Colors.grey),
                            SizedBox(height: 5),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Text('Fat'),
                                Text(
                                  '(g)',
                                  style: TextStyle(fontSize: 10),
                                ),
                              ],
                            ),
                            Text(
                                '${(getWeightGram() / 100 * fat).toStringAsFixed(1)}',
                                style: TextStyle(
                                    color: kGreenColor, fontSize: 30)),
                          ],
                        )),
                      ),
                    ]),
          SizedBox(height: isExpanded ? 0 : 7),
          Container(
            decoration: BoxDecoration(
              color: kGreyColor,
              borderRadius: BorderRadius.circular(10),
            ),
            child: ConfigurableExpansionTile(
              header: SizedBox(
                child: Center(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Text(
                        'See More',
                        style: TextStyle(color: Colors.grey, fontSize: 15),
                      ),
                      Icon(Icons.keyboard_arrow_down, color: Colors.grey),
                    ],
                  ),
                ),
                height: 35,
              ),
              headerExpanded: SizedBox(
                child: Center(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Text(
                        'See Less',
                        style: TextStyle(color: Colors.grey, fontSize: 15),
                      ),
                      Icon(Icons.keyboard_arrow_up, color: Colors.grey),
                    ],
                  ),
                ),
                height: 35,
              ),
              onExpansionChanged: (bool isExpand) {
                setState(() {
                  isExpanded = isExpand;
                });
              },
              children: <Widget>[
                SizedBox(
                  height: 170,
                  child: Scrollbar(
                    child: ListView(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 35.0,
                            vertical: 6,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  'Calories (kcal)',
                                ),
                              ),
                              Text(
                                '${(getWeightGram() / 100 * calories).toStringAsFixed(1)}',
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 35.0,
                            vertical: 6,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  'Protein (g)',
                                ),
                              ),
                              Text(
                                '${(getWeightGram() / 100 * protein).toStringAsFixed(1)}',
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 35.0,
                            vertical: 6,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  'Fat (g)',
                                ),
                              ),
                              Text(
                                '${(getWeightGram() / 100 * fat).toStringAsFixed(1)}',
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 35.0,
                            vertical: 6,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  '        Saturated fats (g)',
                                ),
                              ),
                              Text(
                                '${(getWeightGram() / 100 * saturatedFats).toStringAsFixed(1)}',
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 35.0,
                            vertical: 6,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  '        Trans fats (g)',
                                ),
                              ),
                              Text(
                                '${(getWeightGram() / 100 * transFats).toStringAsFixed(1)}',
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 35.0,
                            vertical: 6,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  'Carbohydrates (g)',
                                ),
                              ),
                              Text(
                                '${(getWeightGram() / 100 * carbohydrates).toStringAsFixed(1)}',
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 35.0,
                            vertical: 6,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  '        Sugars (g)',
                                ),
                              ),
                              Text(
                                '${(getWeightGram() / 100 * sugars).toStringAsFixed(1)}',
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 35.0,
                            vertical: 6,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  'Fibre (g)',
                                ),
                              ),
                              Text(
                                '${(getWeightGram() / 100 * fibre).toStringAsFixed(1)}',
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 35.0,
                            vertical: 6,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  'Vitamin A (IU)',
                                ),
                              ),
                              Text(
                                '${(getWeightGram() / 100 * vitaminA).toStringAsFixed(1)}',
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 35.0,
                            vertical: 6,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  'Carotene (µg)',
                                ),
                              ),
                              Text(
                                '${(getWeightGram() / 100 * carotene).toStringAsFixed(1)}',
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 35.0,
                            vertical: 6,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  'Vitamin B6 (mg)',
                                ),
                              ),
                              Text(
                                '${(getWeightGram() / 100 * vitaminB6).toStringAsFixed(1)}',
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 35.0,
                            vertical: 6,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  'Vitamin B12 (µg)',
                                ),
                              ),
                              Text(
                                '${(getWeightGram() / 100 * vitaminB12).toStringAsFixed(1)}',
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 35.0,
                            vertical: 6,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  'Vitamin B3 (mg)',
                                ),
                              ),
                              Text(
                                '${(getWeightGram() / 100 * vitaminB3).toStringAsFixed(1)}',
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 35.0,
                            vertical: 6,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  'Vitamin C (mg)',
                                ),
                              ),
                              Text(
                                '${(getWeightGram() / 100 * vitaminC).toStringAsFixed(1)}',
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 35.0,
                            vertical: 6,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  'Vitamin E (IU)',
                                ),
                              ),
                              Text(
                                '${(getWeightGram() / 100 * vitaminE).toStringAsFixed(1)}',
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 35.0,
                            vertical: 6,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  'Cholesterol (mg)',
                                ),
                              ),
                              Text(
                                '${(getWeightGram() / 100 * cholesterol).toStringAsFixed(1)}',
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 35.0,
                            vertical: 6,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  'Potassium (mg)',
                                ),
                              ),
                              Text(
                                '${(getWeightGram() / 100 * potassium).toStringAsFixed(1)}',
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 35.0,
                            vertical: 6,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  'Sodium (mg)',
                                ),
                              ),
                              Text(
                                '${(getWeightGram() / 100 * sodium).toStringAsFixed(1)}',
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 35.0,
                            vertical: 6,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  'Calcium (mg)',
                                ),
                              ),
                              Text(
                                '${(getWeightGram() / 100 * calcium).toStringAsFixed(1)}',
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 35.0,
                            vertical: 6,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  'Magnesium (mg)',
                                ),
                              ),
                              Text(
                                '${(getWeightGram() / 100 * magnesium).toStringAsFixed(1)}',
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 35.0,
                            vertical: 6,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  'Iron (mg)',
                                ),
                              ),
                              Text(
                                '${(getWeightGram() / 100 * iron).toStringAsFixed(1)}',
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 35.0,
                            vertical: 6,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  'Manganese (mg)',
                                ),
                              ),
                              Text(
                                '${(getWeightGram() / 100 * manganese).toStringAsFixed(1)}',
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 35.0,
                            vertical: 6,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  'Zinc (mg)',
                                ),
                              ),
                              Text(
                                '${(getWeightGram() / 100 * zinc).toStringAsFixed(1)}',
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 35.0,
                            vertical: 6,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  'Copper (mg)',
                                ),
                              ),
                              Text(
                                '${(getWeightGram() / 100 * copper).toStringAsFixed(1)}',
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 35.0,
                            vertical: 6,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  'Phosphorus (mg)',
                                ),
                              ),
                              Text(
                                '${(getWeightGram() / 100 * phosphorus).toStringAsFixed(1)}',
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 35.0,
                            vertical: 6,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Expanded(
                                child: Text(
                                  'Selenium (µg)',
                                ),
                              ),
                              Text(
                                '${(getWeightGram() / 100 * selenium).toStringAsFixed(1)}',
                              )
                            ],
                          ),
                        ),
                        SizedBox(height: 10),
                      ],
                    ),
                  ),
                ),
              ],
              initiallyExpanded: isExpanded,
            ),
          )
        ],
      ),
    );
  }
}
