import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nutrilifeapp/const.dart';
import 'package:nutrilifeapp/services/shared_preferences.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:nutrilifeapp/screens/choose_food.dart';
import 'package:flutter/services.dart';
import 'dart:math' as math;
import 'package:nutrilifeapp/services/global.dart';
import 'package:provider/provider.dart' as provider;

double calories;
double protein;
double fat;
double saturatedFats;
double transFats;
double carbohydrates;
double sugars;
double fibre;
double vitaminA;
double carotene;
double vitaminB6;
double vitaminB12;
double vitaminB3;
double vitaminC;
double vitaminE;
double cholesterol;
double potassium;
double sodium;
double calcium;
double magnesium;
double iron;
double manganese;
double zinc;
double copper;
double phosphorus;
double selenium;

NutrilifeSharedPreferences pref = NutrilifeSharedPreferences();
double factVal;
List<Map> newFoodFacts = [];
String foodName;

class EditMyFood extends StatefulWidget {
  final double weight;
  Map myFood;
  EditMyFood(this.weight, this.myFood);
  @override
  _EditMyFoodState createState() => _EditMyFoodState();
}

class _EditMyFoodState extends State<EditMyFood> {

  disposePage(){
    Future.delayed(
        Duration(milliseconds: 300),
            () {
          Navigator.pushReplacement(context, FadeRoute(
            page: ChooseFood(widget.weight, 2),
          ));
        }
    );
  }

  TextEditingController _nameController = new TextEditingController();
  TextEditingController _calController = new TextEditingController();
  TextEditingController _proController = new TextEditingController();
  TextEditingController _fatController = new TextEditingController();
  TextEditingController _satFatController = new TextEditingController();
  TextEditingController _transFatController = new TextEditingController();
  TextEditingController _carboController = new TextEditingController();
  TextEditingController _sugController = new TextEditingController();
  TextEditingController _fibController = new TextEditingController();
  TextEditingController _vitAController = new TextEditingController();
  TextEditingController _caroController = new TextEditingController();
  TextEditingController _vitB6Controller = new TextEditingController();
  TextEditingController _vitB12Controller = new TextEditingController();
  TextEditingController _vitB3Controller = new TextEditingController();
  TextEditingController _vitCController = new TextEditingController();
  TextEditingController _vitEController = new TextEditingController();
  TextEditingController _cholController = new TextEditingController();
  TextEditingController _potController = new TextEditingController();
  TextEditingController _sodController = new TextEditingController();
  TextEditingController _calcController = new TextEditingController();
  TextEditingController _magController = new TextEditingController();
  TextEditingController _ironController = new TextEditingController();
  TextEditingController _mangaController = new TextEditingController();
  TextEditingController _zincController = new TextEditingController();
  TextEditingController _copperController = new TextEditingController();
  TextEditingController _phosController = new TextEditingController();
  TextEditingController _selController = new TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _nameController.text = widget.myFood['name'];
    foodName = widget.myFood['name'];
    List facts = widget.myFood['Facts'];
    print(facts);
    for(int i=0; i<26; i++){
      var item = facts[i];
      switch(item['name']) {
        case 'Calories': {
          _calController.text = item['value'].toString();
          calories = item['value'].toDouble();
        }
        break;
        case 'Protein': {
          _proController.text = item['value'].toString();
          protein = item['value'].toDouble();
        }
        break;
        case 'Fat': {
          _fatController.text = item['value'].toString();
          fat = item['value'].toDouble();
        }
        break;
        case 'Saturated Fats': {
          _satFatController.text = item['value'].toString();
          saturatedFats = item['value'].toDouble();
        }
        break;
        case 'Trans Fats': {
          _transFatController.text = item['value'].toString();
          transFats = item['value'].toDouble();
        }
        break;
        case 'Carbohydrates': {
          _carboController.text = item['value'].toString();
          carbohydrates = item['value'].toDouble();
        }
        break;
        case 'Sugars': {
          _sugController.text = item['value'].toString();
          sugars = item['value'].toDouble();
        }
        break;
        case 'Fibre': {
          _fibController.text = item['value'].toString();
          fibre = item['value'].toDouble();
        }
        break;
        case 'Vitamin A': {
          _vitAController.text = item['value'].toString();
          vitaminA = item['value'].toDouble();
        }
        break;
        case 'Carotene': {
          _caroController.text = item['value'].toString();
          carotene = item['value'].toDouble();
        }
        break;
        case 'Vitamin B6': {
          _vitB6Controller.text = item['value'].toString();
          vitaminB6 = item['value'].toDouble();
        }
        break;
        case 'Vitamin B12': {
          _vitB12Controller.text = item['value'].toString();
          vitaminB12 = item['value'].toDouble();
        }
        break;
        case 'Vitamin B3': {
          _vitB3Controller.text = item['value'].toString();
          vitaminB3 = item['value'].toDouble();
        }
        break;
        case 'Vitamin C': {
          _vitCController.text = item['value'].toString();
          vitaminC = item['value'].toDouble();
        }
        break;
        case 'Vitamin E': {
          _vitEController.text = item['value'].toString();
          vitaminE = item['value'].toDouble();
        }
        break;
        case 'Cholesterol': {
          _cholController.text = item['value'].toString();
          cholesterol = item['value'].toDouble();
        }
        break;
        case 'Potassium': {
          _potController.text = item['value'].toString();
          potassium = item['value'].toDouble();
        }
        break;
        case 'Sodium': {
          _sodController.text = item['value'].toString();
          sodium = item['value'].toDouble();
        }
        break;
        case 'Calcium': {
          _calcController.text = item['value'].toString();
          calcium = item['value'].toDouble();
        }
        break;
        case 'Magnesium': {
          _magController.text = item['value'].toString();
          magnesium = item['value'].toDouble();
        }
        break;
        case 'Iron': {
          _ironController.text = item['value'].toString();
          iron = item['value'].toDouble();
        }
        break;
        case 'Manganese': {
          _mangaController.text = item['value'].toString();
          manganese = item['value'].toDouble();
        }
        break;
        case 'Zinc': {
          _zincController.text = item['value'].toString();
          zinc = item['value'].toDouble();
        }
        break;
        case 'Copper': {
          _copperController.text = item['value'].toString();
          copper = item['value'].toDouble();
        }
        break;
        case 'Phosphorus': {
          _phosController.text = item['value'].toString();
          phosphorus = item['value'].toDouble();
        }
        break;
        case 'Selenium': {
          _selController.text = item['value'].toString();
          selenium = item['value'].toDouble();
        }
        break;
      }
    }
    newFoodFacts = [];
  }

  @override
  Widget build(BuildContext context) {
    ProgressDialog pr = ProgressDialog(context);
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
      ),
      margin: EdgeInsets.all(5),
      padding: EdgeInsets.all(20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          ListTile(
            title: Text('Edit ${widget.myFood['name']}', style: TextStyle(fontSize:15, fontWeight: FontWeight.w700)),
            trailing: GestureDetector(
              onTap:(){
                showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    // return object of type Dialog
                    return AlertDialog(
                      title: Row(children: <Widget>[
                        Icon(Icons.help, color: kGreenColor),
                        SizedBox(width:5),
                        Text("Nutrilife", style: TextStyle(color: kGreenColor)),
                      ]),
                      content: new Text('Are you sure you want to delete ${widget.myFood['name']}?'),
                      actions: <Widget>[
                        // usually buttons at the bottom of the dialog
                        FlatButton(
                          child: new Text("Yes", style: TextStyle(color: kGreenColor)),
                          onPressed: () async{
                            pr.update(
                              message: "Deleting..",
                              progressWidget: Container(
                                  padding: EdgeInsets.all(8.0),
                                  child: CircularProgressIndicator(
                                    valueColor: AlwaysStoppedAnimation<Color>(
                                        kGreenColor),
                                  )),
                            );
                            pr.show();
                            String token = await pref.getToken();
                            var response = await http.post(
                                '${kUrl}deleteFood/food_id/${widget.myFood['id']}',
                                headers: {
                                  'Accept': 'application/json',
                                  'Authorization' : 'Bearer $token',
                                },
                            );
                            print(response.body);
                            await provider.Provider.of<GlobalVariables>(context, listen: false).getMyFoodDa();
                            pr.hide();
                            int count = 0;
                            Navigator.popUntil(context, (route) { return count++ == 1; });
                            disposePage();
                          },
                        ),
                        FlatButton(
                          child: new Text("No", style: TextStyle(color: Colors.grey)),
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                        ),
                      ],
                    );
                  },
                );
              },
                child: Icon(Icons.delete_forever, size: 30, color: kGreenColor),
          ),
          ),
          SizedBox(height: 10,),
          Container(
            decoration: BoxDecoration(
              border: Border.all(
                color: Colors.grey[300],
              ),
              color: kGreenColor,
              borderRadius: BorderRadius.circular(10),
            ),
            margin: EdgeInsets.all(2),
            padding: EdgeInsets.all(10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  child: Text(
                    'Food Name', textAlign: TextAlign.center, style: TextStyle(color: Colors.white),
                  ),
                ),
                Expanded(
                  child: Container(
                    padding: EdgeInsets.symmetric(vertical:0),
                    child: TextField(
                      controller: _nameController,
                      onChanged: (x){
                        foodName = x;
                        print(foodName);
                      },
                      decoration: InputDecoration(
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                        ),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                        ),
                        border: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                        ),
                        hintText: '',
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 10),
          Center(
            child: Text('Nutrition Facts per 100g',
              style: TextStyle(color: Colors.grey),
            ),
          ),
          SizedBox(height: 10),
          Container(
            height: 300,
            child: ListView(
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                    color: kGreyColor,
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                      color: Colors.grey[300],
                    ),
                  ),
                  margin: EdgeInsets.all(2),
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: Text('Calories:', textAlign: TextAlign.center, style: TextStyle(color: kGreenColor)),
                      ),
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical:7),
                          child: TextField(
                            inputFormatters: [DecimalTextInputFormatter(decimalRange: 2)],
                            keyboardType: TextInputType.numberWithOptions(decimal: true),
                            maxLength: 6,
                            controller: _calController,
                            onChanged: (x){
                              calories = double.parse(x);
                            },
                            decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              border: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              hintText: '',
                              suffix: Text('kcal'),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ), // 1 calories
                Container(
                  decoration: BoxDecoration(
                    color: kGreyColor,
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                      color: Colors.grey[300],
                    ),
                  ),
                  margin: EdgeInsets.all(2),
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: Text('Protein:', textAlign: TextAlign.center, style: TextStyle(color: kGreenColor)),
                      ),
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical:7),
                          child: TextField(
                            inputFormatters: [DecimalTextInputFormatter(decimalRange: 2)],
                            keyboardType: TextInputType.numberWithOptions(decimal: true),
                            maxLength: 6,
                            controller: _proController,
                            onChanged: (x){
                              protein = double.parse(x);
                            },
                            decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              border: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              hintText: '',
                              suffix: Text('g'),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ), // 2 Protein
                Container(
                  decoration: BoxDecoration(
                    color: kGreyColor,
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                      color: Colors.grey[300],
                    ),
                  ),
                  margin: EdgeInsets.all(2),
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: Text('Fat:', textAlign: TextAlign.center, style: TextStyle(color: kGreenColor)),
                      ),
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical:7),
                          child: TextField(
                            inputFormatters: [DecimalTextInputFormatter(decimalRange: 2)],
                            keyboardType: TextInputType.numberWithOptions(decimal: true),
                            maxLength: 6,
                            controller: _fatController,
                            onChanged: (x){
                              fat = double.parse(x);
                            },
                            decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              border: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              hintText: '',
                              suffix: Text('g'),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ), // 3 Fat
                Container(
                  decoration: BoxDecoration(
                    color: kGreyColor,
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                      color: Colors.grey[300],
                    ),
                  ),
                  margin: EdgeInsets.all(2),
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: Text('Saturated Fats:', textAlign: TextAlign.center, style: TextStyle(color: kGreenColor)),
                      ),
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical:7),
                          child: TextField(
                            inputFormatters: [DecimalTextInputFormatter(decimalRange: 2)],
                            keyboardType: TextInputType.numberWithOptions(decimal: true),
                            maxLength: 6,
                            controller: _satFatController,
                            onChanged: (x){
                              saturatedFats = double.parse(x);
                            },
                            decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              border: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              hintText: '',
                              suffix: Text('g'),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ), // 31 Saturated Fats
                Container(
                  decoration: BoxDecoration(
                    color: kGreyColor,
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                      color: Colors.grey[300],
                    ),
                  ),
                  margin: EdgeInsets.all(2),
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: Text('Trans Fats:', textAlign: TextAlign.center, style: TextStyle(color: kGreenColor)),
                      ),
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical:7),
                          child: TextField(
                            inputFormatters: [DecimalTextInputFormatter(decimalRange: 2)],
                            keyboardType: TextInputType.numberWithOptions(decimal: true),
                            maxLength: 6,
                            controller: _transFatController,
                            onChanged: (x){
                              transFats = double.parse(x);
                            },
                            decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              border: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              hintText: '',
                              suffix: Text('g'),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ), // 32 Trans Fat
                Container(
                  decoration: BoxDecoration(
                    color: kGreyColor,
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                      color: Colors.grey[300],
                    ),
                  ),
                  margin: EdgeInsets.all(2),
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: Text('Carbohydrates:', textAlign: TextAlign.center, style: TextStyle(color: kGreenColor)),
                      ),
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical:7),
                          child: TextField(
                            inputFormatters: [DecimalTextInputFormatter(decimalRange: 2)],
                            keyboardType: TextInputType.numberWithOptions(decimal: true),
                            maxLength: 6,
                            controller: _carboController,
                            onChanged: (x){
                              carbohydrates = double.parse(x);
                            },
                            decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              border: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              hintText: '',
                              suffix: Text('g'),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ), // 4 Carbohydrates
                Container(
                  decoration: BoxDecoration(
                    color: kGreyColor,
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                      color: Colors.grey[300],
                    ),
                  ),
                  margin: EdgeInsets.all(2),
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: Text('Sugars', textAlign: TextAlign.center, style: TextStyle(color: kGreenColor)),
                      ),
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical:7),
                          child: TextField(
                            inputFormatters: [DecimalTextInputFormatter(decimalRange: 2)],
                            keyboardType: TextInputType.numberWithOptions(decimal: true),
                            maxLength: 6,
                            controller: _sugController,
                            onChanged: (x){
                              sugars = double.parse(x);
                            },
                            decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              border: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              hintText: '',
                              suffix: Text('g'),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ), // 41 Sugars
                Container(
                  decoration: BoxDecoration(
                    color: kGreyColor,
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                      color: Colors.grey[300],
                    ),
                  ),
                  margin: EdgeInsets.all(2),
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: Text('Fibre:', textAlign: TextAlign.center, style: TextStyle(color: kGreenColor)),
                      ),
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical:7),
                          child: TextField(
                            inputFormatters: [DecimalTextInputFormatter(decimalRange: 2)],
                            keyboardType: TextInputType.numberWithOptions(decimal: true),
                            maxLength: 6,
                            controller: _fibController,
                            onChanged: (x){
                              fibre = double.parse(x);
                            },
                            decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              border: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              hintText: '',
                              suffix: Text('g'),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ), // 5 Fibre
                Container(
                  decoration: BoxDecoration(
                    color: kGreyColor,
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                      color: Colors.grey[300],
                    ),
                  ),
                  margin: EdgeInsets.all(2),
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: Text('Vitamin A:', textAlign: TextAlign.center, style: TextStyle(color: kGreenColor)),
                      ),
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical:7),
                          child: TextField(
                            inputFormatters: [DecimalTextInputFormatter(decimalRange: 2)],
                            keyboardType: TextInputType.numberWithOptions(decimal: true),
                            maxLength: 6,
                            controller: _vitAController,
                            onChanged: (x){
                              vitaminA = double.parse(x);
                            },
                            decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              border: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              hintText: '',
                              suffix: Text('IU'),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),// 6 vitamin A
                Container(
                  decoration: BoxDecoration(
                    color: kGreyColor,
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                      color: Colors.grey[300],
                    ),
                  ),
                  margin: EdgeInsets.all(2),
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: Text('Carotene:', textAlign: TextAlign.center, style: TextStyle(color: kGreenColor)),
                      ),
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical:7),
                          child: TextField(
                            inputFormatters: [DecimalTextInputFormatter(decimalRange: 2)],
                            keyboardType: TextInputType.numberWithOptions(decimal: true),
                            maxLength: 6,
                            controller: _caroController,
                            onChanged: (x){
                              carotene = double.parse(x);
                            },
                            decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              border: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              hintText: '',
                              suffix: Text('µg'),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),// 7 carotene
                Container(
                  decoration: BoxDecoration(
                    color: kGreyColor,
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                      color: Colors.grey[300],
                    ),
                  ),
                  margin: EdgeInsets.all(2),
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: Text('Vitamin B6:', textAlign: TextAlign.center, style: TextStyle(color: kGreenColor)),
                      ),
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical:7),
                          child: TextField(
                            inputFormatters: [DecimalTextInputFormatter(decimalRange: 2)],
                            keyboardType: TextInputType.numberWithOptions(decimal: true),
                            maxLength: 6,
                            controller: _vitB6Controller,
                            onChanged: (x){
                              vitaminB6 = double.parse(x);
                            },
                            decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              border: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              hintText: '',
                              suffix: Text('mg'),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ), //9 vitamin b6
                Container(
                  decoration: BoxDecoration(
                    color: kGreyColor,
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                      color: Colors.grey[300],
                    ),
                  ),
                  margin: EdgeInsets.all(2),
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: Text('Vitamin B12:', textAlign: TextAlign.center, style: TextStyle(color: kGreenColor)),
                      ),
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical:7),
                          child: TextField(
                            inputFormatters: [DecimalTextInputFormatter(decimalRange: 2)],
                            keyboardType: TextInputType.numberWithOptions(decimal: true),
                            maxLength: 6,
                            controller: _vitB12Controller,
                            onChanged: (x){
                              vitaminB12 = double.parse(x);
                            },
                            decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              border: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              hintText: '',
                              suffix: Text('µg'),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),//10 vitamin b12
                Container(
                  decoration: BoxDecoration(
                    color: kGreyColor,
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                      color: Colors.grey[300],
                    ),
                  ),
                  margin: EdgeInsets.all(2),
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: Text('Vitamin B3:', textAlign: TextAlign.center, style: TextStyle(color: kGreenColor)),
                      ),
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical:7),
                          child: TextField(
                            inputFormatters: [DecimalTextInputFormatter(decimalRange: 2)],
                            keyboardType: TextInputType.numberWithOptions(decimal: true),
                            maxLength: 6,
                            controller: _vitB3Controller,
                            onChanged: (x){
                              vitaminB3 = double.parse(x);
                            },
                            decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              border: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              hintText: '',
                              suffix: Text('mg'),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),//11 vitamin b3
                Container(
                  decoration: BoxDecoration(
                    color: kGreyColor,
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                      color: Colors.grey[300],
                    ),
                  ),
                  margin: EdgeInsets.all(2),
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: Text('Vitamin C:', textAlign: TextAlign.center, style: TextStyle(color: kGreenColor)),
                      ),
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical:7),
                          child: TextField(
                            inputFormatters: [DecimalTextInputFormatter(decimalRange: 2)],
                            keyboardType: TextInputType.numberWithOptions(decimal: true),
                            maxLength: 6,
                            controller: _vitCController,
                            onChanged: (x){
                              vitaminC = double.parse(x);
                            },
                            decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              border: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              hintText: '',
                              suffix: Text('mg'),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),// 12 vitamin c
                Container(
                  decoration: BoxDecoration(
                    color: kGreyColor,
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                      color: Colors.grey[300],
                    ),
                  ),
                  margin: EdgeInsets.all(2),
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: Text('Vitamin E:', textAlign: TextAlign.center, style: TextStyle(color: kGreenColor)),
                      ),
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical:7),
                          child: TextField(
                            inputFormatters: [DecimalTextInputFormatter(decimalRange: 2)],
                            keyboardType: TextInputType.numberWithOptions(decimal: true),
                            maxLength: 6,
                            controller: _vitEController,
                            onChanged: (x){
                              vitaminE = double.parse(x);
                            },
                            decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              border: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              hintText: '',
                              suffix: Text('IU'),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ), // 13 Vitamin E
                Container(
                  decoration: BoxDecoration(
                    color: kGreyColor,
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                      color: Colors.grey[300],
                    ),
                  ),
                  margin: EdgeInsets.all(2),
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: Text('Cholesterol:', textAlign: TextAlign.center, style: TextStyle(color: kGreenColor)),
                      ),
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical:7),
                          child: TextField(
                            inputFormatters: [DecimalTextInputFormatter(decimalRange: 2)],
                            keyboardType: TextInputType.numberWithOptions(decimal: true),
                            maxLength: 6,
                            controller: _cholController,
                            onChanged: (x){
                              cholesterol = double.parse(x);
                            },
                            decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              border: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              hintText: '',
                              suffix: Text('mg'),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),// 14 cholesterol
                Container(
                  decoration: BoxDecoration(
                    color: kGreyColor,
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                      color: Colors.grey[300],
                    ),
                  ),
                  margin: EdgeInsets.all(2),
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: Text('Potassium:', textAlign: TextAlign.center, style: TextStyle(color: kGreenColor)),
                      ),
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical:7),
                          child: TextField(
                            inputFormatters: [DecimalTextInputFormatter(decimalRange: 2)],
                            keyboardType: TextInputType.numberWithOptions(decimal: true),
                            maxLength: 6,
                            controller: _potController,
                            onChanged: (x){
                              potassium = double.parse(x);
                            },
                            decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              border: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              hintText: '',
                              suffix: Text('mg'),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ), // 15 potassium
                Container(
                  decoration: BoxDecoration(
                    color: kGreyColor,
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                      color: Colors.grey[300],
                    ),
                  ),
                  margin: EdgeInsets.all(2),
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: Text('Sodium:', textAlign: TextAlign.center, style: TextStyle(color: kGreenColor)),
                      ),
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical:7),
                          child: TextField(
                            inputFormatters: [DecimalTextInputFormatter(decimalRange: 2)],
                            keyboardType: TextInputType.numberWithOptions(decimal: true),
                            maxLength: 6,
                            controller: _sodController,
                            onChanged: (x){
                              sodium = double.parse(x);
                            },
                            decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              border: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              hintText: '',
                              suffix: Text('mg'),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ), // 16 Sodium
                Container(
                  decoration: BoxDecoration(
                    color: kGreyColor,
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                      color: Colors.grey[300],
                    ),
                  ),
                  margin: EdgeInsets.all(2),
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: Text('Calcium:', textAlign: TextAlign.center, style: TextStyle(color: kGreenColor)),
                      ),
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical:7),
                          child: TextField(
                            inputFormatters: [DecimalTextInputFormatter(decimalRange: 2)],
                            keyboardType: TextInputType.numberWithOptions(decimal: true),
                            maxLength: 6,
                            controller: _calcController,
                            onChanged: (x){
                              calcium = double.parse(x);
                            },
                            decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              border: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              hintText: '',
                              suffix: Text('mg'),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ), // 17 Calcium
                Container(
                  decoration: BoxDecoration(
                    color: kGreyColor,
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                      color: Colors.grey[300],
                    ),
                  ),
                  margin: EdgeInsets.all(2),
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: Text('Magnesium:', textAlign: TextAlign.center, style: TextStyle(color: kGreenColor)),
                      ),
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical:7),
                          child: TextField(
                            inputFormatters: [DecimalTextInputFormatter(decimalRange: 2)],
                            keyboardType: TextInputType.numberWithOptions(decimal: true),
                            maxLength: 6,
                            controller: _magController,
                            onChanged: (x){
                              magnesium = double.parse(x);
                            },
                            decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              border: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              hintText: '',
                              suffix: Text('mg'),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ), // 18 Magnesium
                Container(
                  decoration: BoxDecoration(
                    color: kGreyColor,
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                      color: Colors.grey[300],
                    ),
                  ),
                  margin: EdgeInsets.all(2),
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: Text('Iron:', textAlign: TextAlign.center, style: TextStyle(color: kGreenColor)),
                      ),
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical:7),
                          child: TextField(
                            inputFormatters: [DecimalTextInputFormatter(decimalRange: 2)],
                            keyboardType: TextInputType.numberWithOptions(decimal: true),
                            maxLength: 6,
                            controller: _ironController,
                            onChanged: (x){
                              iron = double.parse(x);
                            },
                            decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              border: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              hintText: '',
                              suffix: Text('mg'),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ), // 19 Iron
                Container(
                  decoration: BoxDecoration(
                    color: kGreyColor,
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                      color: Colors.grey[300],
                    ),
                  ),
                  margin: EdgeInsets.all(2),
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: Text('Manganese:', textAlign: TextAlign.center, style: TextStyle(color: kGreenColor)),
                      ),
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical:7),
                          child: TextField(
                            inputFormatters: [DecimalTextInputFormatter(decimalRange: 2)],
                            keyboardType: TextInputType.numberWithOptions(decimal: true),
                            maxLength: 6,
                            controller: _mangaController,
                            onChanged: (x){
                              manganese = double.parse(x);
                            },
                            decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              border: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              hintText: '',
                              suffix: Text('mg'),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ), // 20 manganese
                Container(
                  decoration: BoxDecoration(
                    color: kGreyColor,
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                      color: Colors.grey[300],
                    ),
                  ),
                  margin: EdgeInsets.all(2),
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: Text('Zinc:', textAlign: TextAlign.center, style: TextStyle(color: kGreenColor)),
                      ),
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical:7),
                          child: TextField(
                            inputFormatters: [DecimalTextInputFormatter(decimalRange: 2)],
                            keyboardType: TextInputType.numberWithOptions(decimal: true),
                            maxLength: 6,
                            controller: _zincController,
                            onChanged: (x){
                              zinc = double.parse(x);
                            },
                            decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              border: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              hintText: '',
                              suffix: Text('mg'),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ), // 21 Zinc
                Container(
                  decoration: BoxDecoration(
                    color: kGreyColor,
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                      color: Colors.grey[300],
                    ),
                  ),
                  margin: EdgeInsets.all(2),
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: Text('Copper:', textAlign: TextAlign.center, style: TextStyle(color: kGreenColor)),
                      ),
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical:7),
                          child: TextField(
                            inputFormatters: [DecimalTextInputFormatter(decimalRange: 2)],
                            keyboardType: TextInputType.numberWithOptions(decimal: true),
                            maxLength: 6,
                            controller: _copperController,
                            onChanged: (x){
                              copper = double.parse(x);
                            },
                            decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              border: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              hintText: '',
                              suffix: Text('mg'),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ), // 22 copper
                Container(
                  decoration: BoxDecoration(
                    color: kGreyColor,
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                      color: Colors.grey[300],
                    ),
                  ),
                  margin: EdgeInsets.all(2),
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: Text('Phosphorus:', textAlign: TextAlign.center, style: TextStyle(color: kGreenColor)),
                      ),
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical:7),
                          child: TextField(
                            inputFormatters: [DecimalTextInputFormatter(decimalRange: 2)],
                            keyboardType: TextInputType.numberWithOptions(decimal: true),
                            maxLength: 6,
                            controller: _phosController,
                            onChanged: (x){
                              phosphorus = double.parse(x);
                            },
                            decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              border: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              hintText: '',
                              suffix: Text('mg'),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ), // phosphorus
                Container(
                  decoration: BoxDecoration(
                    color: kGreyColor,
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                      color: Colors.grey[300],
                    ),
                  ),
                  margin: EdgeInsets.all(2),
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: Text('Selenium:', textAlign: TextAlign.center, style: TextStyle(color: kGreenColor)),
                      ),
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical:7),
                          child: TextField(
                            inputFormatters: [DecimalTextInputFormatter(decimalRange: 2)],
                            keyboardType: TextInputType.numberWithOptions(decimal: true),
                            maxLength: 6,
                            controller: _selController,
                            onChanged: (x){
                              selenium = double.parse(x);
                            },
                            decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              border: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              hintText: '',
                              suffix: Text('µg'),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),// selenium
              ],
            ),
          ),
          SizedBox(height: 10),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: FlatButton(
                  color: kGreyColor,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8.0),
                    side: BorderSide(color: kGreyColor),
                  ),
                  child: Text('Cancel',
                      style: TextStyle(
                        color: Colors.black,
                      )),
                  onPressed: (){
                    Navigator.pop(context);
                  },
                ),
              ),
              SizedBox(width: 10),
              Expanded(
                child: FlatButton(
                  color: kGreenColor,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8.0),
                    side: BorderSide(color: kGreenColor),
                  ),
                  child: Text('Update Food',
                      style: TextStyle(
                        color: Colors.white,
                      )),
                  onPressed: ()async{


                    newFoodFacts = [
                      {"fact": 1008,
                        "value" : calories,
                      },
                      {"fact": 1003,
                        "value" : protein,
                      },
                      {"fact": 1004,
                        "value" : fat,
                      },
                      {"fact": 1258,
                        "value" : saturatedFats,
                      },
                      {"fact": 1257,
                        "value" : transFats,
                      },
                      {"fact": 1005,
                        "value" : carbohydrates,
                      },
                      {"fact": 2000,
                        "value" : sugars,
                      },
                      {"fact": 1079,
                        "value" : fibre,
                      },
                      {"fact": 1104,
                        "value" : vitaminA,
                      },
                      {"fact": 1107,
                        "value" : carotene,
                      },
                      {"fact": 1175,
                        "value" : vitaminB6,
                      },
                      {"fact": 1178,
                        "value" : vitaminB12,
                      },
                      {"fact": 1167,
                        "value" : vitaminB3,
                      },
                      {"fact": 1162,
                        "value" : vitaminC,
                      },
                      {"fact": 1109,
                        "value" : vitaminE,
                      },
                      {"fact": 1253,
                        "value" : cholesterol,
                      },
                      {"fact": 1092,
                        "value" : potassium,
                      },
                      {"fact": 1093,
                        "value" : sodium,
                      },
                      {"fact": 1087,
                        "value" : calcium,
                      },
                      {"fact": 1090,
                        "value" : magnesium,
                      },
                      {"fact": 1089,
                        "value" : iron,
                      },
                      {"fact": 1101,
                        "value" : manganese,
                      },
                      {"fact": 1095,
                        "value" : zinc,
                      },
                      {"fact": 1098,
                        "value" : copper,
                      },
                      {"fact": 1091,
                        "value" : phosphorus,
                      },
                      {"fact": 1103,
                        "value" : selenium,
                      },
                    ];

                    if(foodName.length > 3) {
                      pr.update(
                        message: "Updating..",
                        progressWidget: Container(
                            padding: EdgeInsets.all(8.0),
                            child: CircularProgressIndicator(
                              valueColor: AlwaysStoppedAnimation<Color>(
                                  kGreenColor),
                            )),
                      );
                      pr.show();
                      print(newFoodFacts);
                      print(foodName);
                      String token = await pref.getToken();
                      var response = await http.put(
                          '${kUrl}foodEdit/food_id/${widget.myFood['id']}?facts=${jsonEncode(newFoodFacts)}&name=$foodName',
                          headers: {
                            'Accept': 'application/json',
                            'Authorization' : 'Bearer $token',
                          },
                      );
                      var data = jsonDecode(response.body);
                      print(data);
                      await provider.Provider.of<GlobalVariables>(context, listen: false).getMyFoodDa();
                      pr.hide();
                      // Navigator.pushReplacement(context, FadeRoute(
                      //     page: ChooseFood(widget.weight, 0)
                      // ));
                      int count = 0;
                      Navigator.popUntil(context, (route) { return count++ == 1; });
                      disposePage();
                    } else {
                      wrongAlert(context, 'Food name must be at least 3 letters!');
                    }
                  },
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class DecimalTextInputFormatter extends TextInputFormatter {
  DecimalTextInputFormatter({this.decimalRange})
      : assert(decimalRange == null || decimalRange > 0);

  final int decimalRange;

  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, // unused.
      TextEditingValue newValue,
      ) {
    TextSelection newSelection = newValue.selection;
    String truncated = newValue.text;

    if (decimalRange != null) {
      String value = newValue.text;

      if (value.contains(".") &&
          value.substring(value.indexOf(".") + 1).length > decimalRange) {
        truncated = oldValue.text;
        newSelection = oldValue.selection;
      } else if (value == ".") {
        truncated = "0.";

        newSelection = newValue.selection.copyWith(
          baseOffset: math.min(truncated.length, truncated.length + 1),
          extentOffset: math.min(truncated.length, truncated.length + 1),
        );
      }

      return TextEditingValue(
        text: truncated,
        selection: newSelection,
        composing: TextRange.empty,
      );
    }
    return newValue;
  }
}