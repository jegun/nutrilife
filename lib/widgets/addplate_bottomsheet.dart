import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nutrilifeapp/const.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:nutrilifeapp/screens/diary.dart';
import 'package:nutrilifeapp/services/shared_preferences.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:nutrilifeapp/services/global.dart';
import 'package:provider/provider.dart' as provider;

List<String> meals = ['Breakfast', 'Lunch', 'Dinner', 'Snacks'];

NutrilifeSharedPreferences pref = NutrilifeSharedPreferences();

class AddPlate extends StatefulWidget {
  final String weightLabel;
  final double weight;
  final int foodID;
  final String foodName;
  final String userUnit;
  List facts;
  AddPlate(this.foodID, this.foodName, this.weight, this.userUnit, this.facts, this.weightLabel);
  @override
  _AddPlateState createState() => _AddPlateState();
}

class _AddPlateState extends State<AddPlate> {
  String token;
  String mealChoice = "Breakfast";
  DateTime chosenDate = DateTime.now();

  int getMealType(String meal) {
    int mealType;
    if (meal == "Breakfast") {
      mealType = 0;
    } else if (meal == "Lunch") {
      mealType = 1;
    } else if (meal == "Dinner") {
      mealType = 2;
    } else if (meal == "Snacks") {
      mealType = 3;
    }
    return mealType;
  }

  getWeightGram() {
    double weight;
    if (widget.userUnit == 'g') {
      weight = widget.weight;
    } else if (widget.userUnit == 'ml') {
      weight = widget.weight;
    } else if (widget.userUnit == 'lb') {
      weight = widget.weight * 453.59237;
    } else if (widget.userUnit == 'oz') {
      weight = widget.weight * 28.3495231;
    } else if (widget.userUnit == 'kg') {
      weight = widget.weight * 1000;
    }
    return weight;
  }

  getToken() async {
    token = await pref.getToken();
  }

  addMeal() async {
    ProgressDialog pr = ProgressDialog(context);
    pr.update(
      message: "Adding..",
      progressWidget: Container(
          padding: EdgeInsets.all(8.0),
          child: CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation<Color>(kGreenColor),
          )),
    );
    pr.show();

    var responses = await http.get(
      '${kUrl}food_id/${widget.foodID}',
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      },
    );
    var foodData = jsonDecode(responses.body);
    print('foodData is ${foodData['data']}');
    Map<String, dynamic> foodDataEntry = foodData['data'];
    print('fooddataentry is $foodDataEntry');
    double calories = 0;
    double protein = 0;
    double fat = 0;
    double saturatedFats = 0;
    double transFats = 0;
    double carbohydrates = 0;
    double sugars = 0;
    double fibre = 0;
    double vitaminA = 0;
    double carotene = 0;
    double vitaminB6 = 0;
    double vitaminB12 = 0;
    double vitaminB3 = 0;
    double vitaminC = 0;
    double vitaminE = 0;
    double cholesterol = 0;
    double potassium = 0;
    double sodium = 0;
    double calcium = 0;
    double magnesium = 0;
    double iron = 0;
    double manganese = 0;
    double zinc = 0;
    double copper = 0;
    double phosphorus = 0;
    double selenium = 0;
    if (foodDataEntry != null) {
      List<dynamic> facts = foodDataEntry['Facts'];
      print(facts);
      for (int f = facts.length; f < 26; f++) {
        Map x = {
          "name": "default",
          "value": 0,
        };
        facts.add(x);
      }

      for (int j = 0; j < 26; j++) {
        var item = facts[j];
        switch (item['name']) {
          case 'Calories':
            {
              calories = item['value'].toDouble();
            }
            break;
          case 'Protein':
            {
              protein = item['value'].toDouble();
            }
            break;
          case 'Fat':
            {
              fat = item['value'].toDouble();
            }
            break;
          case 'Saturated Fats':
            {
              saturatedFats = item['value'].toDouble();
            }
            break;
          case 'Trans Fats':
            {
              transFats = item['value'].toDouble();
            }
            break;
          case 'Carbohydrates':
            {
              carbohydrates = item['value'].toDouble();
            }
            break;
          case 'Sugars':
            {
              sugars = item['value'].toDouble();
            }
            break;
          case 'Fibre':
            {
              fibre = item['value'].toDouble();
            }
            break;
          case 'Vitamin A':
            {
              vitaminA = item['value'].toDouble();
            }
            break;
          case 'Carotene':
            {
              carotene = item['value'].toDouble();
            }
            break;
          case 'Vitamin B6':
            {
              vitaminB6 = item['value'].toDouble();
            }
            break;
          case 'Vitamin B12':
            {
              vitaminB12 = item['value'].toDouble();
            }
            break;
          case 'Vitamin B3':
            {
              vitaminB3 = item['value'].toDouble();
            }
            break;
          case 'Vitamin C':
            {
              vitaminC = item['value'].toDouble();
            }
            break;
          case 'Vitamin E':
            {
              vitaminE = item['value'].toDouble();
            }
            break;
          case 'Cholesterol':
            {
              cholesterol = item['value'].toDouble();
            }
            break;
          case 'Potassium':
            {
              potassium = item['value'].toDouble();
            }
            break;
          case 'Sodium':
            {
              sodium = item['value'].toDouble();
            }
            break;
          case 'Calcium':
            {
              calcium = item['value'].toDouble();
            }
            break;
          case 'Magnesium':
            {
              magnesium = item['value'].toDouble();
            }
            break;
          case 'Iron':
            {
              iron = item['value'].toDouble();
            }
            break;
          case 'Manganese':
            {
              manganese = item['value'].toDouble();
            }
            break;
          case 'Zinc':
            {
              zinc = item['value'].toDouble();
            }
            break;
          case 'Copper':
            {
              copper = item['value'].toDouble();
            }
            break;
          case 'Phosphorus':
            {
              phosphorus = item['value'].toDouble();
            }
            break;
          case 'Selenium':
            {
              selenium = item['value'].toDouble();
            }
            break;
        }
      }
    }

    var response = await http.post('${kUrl}addDiary', headers: {
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    }, body: {
      'food_id': '${widget.foodID}',
      'mealWeight': '${getWeightGram().toString()}',
      'mealType': '${getMealType(mealChoice)}',
      'unit_type': 'g',
      'mealDate': '$chosenDate',
      "food_name": "${foodDataEntry['name'] ?? ''}",
      "calories": "${calories.toString()}",
      "protein": "${protein.toString()}",
      "carbohydrates": "${carbohydrates.toString()}",
      "fat": "${fat.toString()}",
      "transFat": "${transFats.toString()}",
      "saturatedFats": "${saturatedFats.toString()}",
      "sugars": "${sugars.toString()}",
      "vitaminA": "${vitaminA.toString()}",
      "vitaminB3": "${vitaminB3.toString()}",
      "vitaminB6": "${vitaminB6.toString()}",
      "vitaminB12": "${vitaminB12.toString()}",
      "vitaminC": "${vitaminC.toString()}",
      "vitaminE": "${vitaminE.toString()}",
      "calcium": "${calcium.toString()}",
      "cholesterol": "${cholesterol.toString()}",
      "iron": "${iron.toString()}",
      "manganese": "${manganese.toString()}",
      "magnesium": "${magnesium.toString()}",
      "sodium": "${sodium.toString()}",
      "potassium": "${potassium.toString()}",
      "zinc": "${zinc.toString()}",
      "copper": "${copper.toString()}",
      "selenium": "${selenium.toString()}",
      "phosphorus": "${phosphorus.toString()}",
      "fibre": "${fibre.toString()}",
      "carotene": "${carotene.toString()}",
    });
    /*var response = await http.post('${kUrl}addDiary', headers: {
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    }, body: {
      'food_id': '${widget.foodID}',
      'mealWeight': '${widget.weight}',
      'mealType': '${getMealType(mealChoice)}',
      'unit_type': widget.userUnit,
      'mealDate': '$chosenDate',
      "food_name": "${foodDataEntry['name'] ?? ''}",
      "calories": "${widget.weight * calories / 100}",
      "protein": "${widget.weight * protein / 100}",
      "carbohydrates": "${widget.weight * carbohydrates / 100}",
      "fat": "${widget.weight * fat / 100}",
      "transFat": "${widget.weight * transFats / 100}",
      "saturatedFats": "${widget.weight * saturatedFats / 100}",
      "sugars": "${widget.weight * sugars / 100}",
      "vitaminA": "${widget.weight * vitaminA / 100}",
      "vitaminB3": "${widget.weight * vitaminB3 / 100}",
      "vitaminB6": "${widget.weight * vitaminB6 / 100}",
      "vitaminB12": "${widget.weight * vitaminB12 / 100}",
      "vitaminC": "${widget.weight * vitaminC / 100}",
      "vitaminE": "${widget.weight * vitaminE / 100}",
      "calcium": "${widget.weight * calcium / 100}",
      "cholesterol": "${widget.weight * cholesterol / 100}",
      "iron": "${widget.weight * iron / 100}",
      "manganese": "${widget.weight * manganese / 100}",
      "magnesium": "${widget.weight * magnesium / 100}",
      "sodium": "${widget.weight * sodium / 100}",
      "potassium": "${widget.weight * potassium / 100}",
      "zinc": "${widget.weight * zinc / 100}",
      "copper": "${widget.weight * copper / 100}",
      "selenium": "${widget.weight * selenium / 100}",
      "phosphorus": "${widget.weight * phosphorus / 100}",
      "fibre": "${widget.weight * fibre / 100}",
      "carotene": "${widget.weight * carotene / 100}",
    });*/
    print(response.body);
    provider.Provider.of<GlobalVariables>(context, listen: false)
        .getThemAll(chosenDate, token);
    provider.Provider.of<GlobalVariables>(context, listen: false)
        .updateDateFrom(DateTime.now().subtract(Duration(days: 7)));
    provider.Provider.of<GlobalVariables>(context, listen: false)
        .updateDateTo(DateTime.now());
    await provider.Provider.of<GlobalVariables>(context, listen: false)
        .getHistoryAll(
            DateTime.now().subtract(Duration(days: 7)), DateTime.now());
    Future.delayed(Duration(milliseconds: 500), () {
      pr.hide();
      Navigator.pushReplacement(context, FadeRoute(page: DiaryScreen()));
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getToken();
  }

  @override
  Widget build(BuildContext context) {
    final ProgressDialog pr = ProgressDialog(context);
    final String unitType = widget.userUnit == 'lb' ? 'lb:oz' : widget.userUnit;
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
      ),
      margin: EdgeInsets.all(5),
      padding: EdgeInsets.all(20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          ListTile(
            title: Text('${widget.foodName}',
                style: TextStyle(fontSize: 15, fontWeight: FontWeight.w700)),
            trailing: Text(
              '${widget.weightLabel} $unitType',
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.w400),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Container(
            decoration: BoxDecoration(
              color: kGreyColor,
              borderRadius: BorderRadius.circular(10),
            ),
            margin: EdgeInsets.all(2),
            padding: EdgeInsets.all(10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  child: Text(
                    'Choose Date:',
                    textAlign: TextAlign.center,
                  ),
                ),
                Expanded(
                  child: GestureDetector(
                    child: Row(
                      children: <Widget>[
                        Text(
                          (chosenDate.day == DateTime.now().day &&
                                  chosenDate.month == DateTime.now().month &&
                                  chosenDate.year == DateTime.now().year)
                              ? 'Today'
                              : '${chosenDate.year}-${chosenDate.month}-${chosenDate.day}',
                          style: TextStyle(
                            color: kGreenColor,
                          ),
                        ),
                        Icon(Icons.keyboard_arrow_down, color: kGreenColor),
                      ],
                    ),
                    onTap: () {
                      DatePicker.showDatePicker(context,
                          showTitleActions: true,
                          minTime: DateTime(2000, 9, 1),
                          maxTime: DateTime(2100, 12, 31), onChanged: (date) {
                        //todo: add date variable to have the value
                      }, onConfirm: (date) {
                        setState(() {
                          chosenDate = date;
                        });
                        //todo: return date value
                      },
                          currentTime: (chosenDate != null)
                              ? chosenDate
                              : DateTime.now(),
                          locale: LocaleType.en,
                          theme: DatePickerTheme(
                            doneStyle: TextStyle(color: kGreenColor),
                            itemStyle: TextStyle(color: Colors.black54),
                          ));
                    },
                  ),
                ),
              ],
            ),
          ),
          Container(
            decoration: BoxDecoration(
              color: kGreyColor,
              borderRadius: BorderRadius.circular(10),
            ),
            margin: EdgeInsets.all(2),
            padding: EdgeInsets.symmetric(horizontal: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Expanded(
                    child: Text(
                  'Choose Meal:',
                  textAlign: TextAlign.center,
                )),
                Expanded(
                  child: DropdownButton<String>(
                    value: mealChoice,
                    icon: Icon(Icons.keyboard_arrow_down, color: kGreenColor),
                    iconSize: 30,
                    elevation: 16,
                    style: TextStyle(
                      color: kGreenColor,
                      fontSize: 15,
                    ),
                    underline: Container(),
                    onChanged: (String newValue) {
                      setState(() {
                        mealChoice = newValue;
                      });
                    },
                    items: meals.map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 15),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: FlatButton(
                  color: kGreyColor,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8.0),
                    side: BorderSide(color: kGreyColor),
                  ),
                  child: Text('Cancel',
                      style: TextStyle(
                        color: Colors.black,
                      )),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
              ),
              SizedBox(width: 10),
              Expanded(
                child: FlatButton(
                  color: kGreenColor,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8.0),
                    side: BorderSide(color: kGreenColor),
                  ),
                  child: Text('Add to Diary',
                      style: TextStyle(
                        color: Colors.white,
                      )),
                  onPressed: () async {
                    await addMeal();
                  },
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
