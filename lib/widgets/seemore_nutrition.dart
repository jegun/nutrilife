import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nutrilifeapp/const.dart';

class SeeMoreNutrition extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
      ),
      margin: EdgeInsets.all(15),
      padding: EdgeInsets.all(20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Expanded(
              child: Scrollbar(
                controller: ScrollController(initialScrollOffset: 0.0, keepScrollOffset: true),
                child: ListView(
                  children: <Widget>[
                   ListTile(
                     title: Text('Fact Number 1'),
                     leading: Icon(Icons.fastfood),
                     trailing: Text('32.5'),
                   ),
                    ListTile(
                      title: Text('Fact Number 2'),
                      leading: Icon(Icons.fastfood),
                      trailing: Text('32.5'),
                    ),
                    ListTile(
                      title: Text('Fact Number 3'),
                      leading: Icon(Icons.fastfood),
                      trailing: Text('32.5'),
                    ),
                    ListTile(
                      title: Text('Fact Number 4'),
                      leading: Icon(Icons.fastfood),
                      trailing: Text('32.5'),
                    ),
                    ListTile(
                      title: Text('Fact Number 5'),
                      leading: Icon(Icons.fastfood),
                      trailing: Text('32.5'),
                    ),
                    ListTile(
                      title: Text('Fact Number 6'),
                      leading: Icon(Icons.fastfood),
                      trailing: Text('32.5'),
                    ),
                    ListTile(
                      title: Text('Fact Number 7'),
                      leading: Icon(Icons.fastfood),
                      trailing: Text('32.5'),
                    ),
                    ListTile(
                      title: Text('Fact Number 8'),
                      leading: Icon(Icons.fastfood),
                      trailing: Text('32.5'),
                    ),
                    ListTile(
                      title: Text('Fact Number 9'),
                      leading: Icon(Icons.fastfood),
                      trailing: Text('32.5'),
                    ),
                    ListTile(
                      title: Text('Fact Number 10'),
                      leading: Icon(Icons.fastfood),
                      trailing: Text('32.5'),
                    ),
                    ListTile(
                      title: Text('Fact Number 11'),
                      leading: Icon(Icons.fastfood),
                      trailing: Text('32.5'),
                    ),
                    ListTile(
                      title: Text('Fact Number 12'),
                      leading: Icon(Icons.fastfood),
                      trailing: Text('32.5'),
                    ),
                    ListTile(
                      title: Text('Fact Number 13'),
                      leading: Icon(Icons.fastfood),
                      trailing: Text('32.5'),
                    ),
                    ListTile(
                      title: Text('Fact Number 14'),
                      leading: Icon(Icons.fastfood),
                      trailing: Text('32.5'),
                    ),
                  ],
                ),
              )
          ),
        ],
      ),
    );
  }
}
