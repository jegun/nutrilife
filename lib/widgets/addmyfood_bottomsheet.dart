import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nutrilifeapp/const.dart';
import 'package:nutrilifeapp/services/shared_preferences.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:flutter/services.dart';
import 'package:nutrilifeapp/screens/choose_food.dart';
import 'dart:math' as math;
import 'package:nutrilifeapp/services/global.dart';
import 'package:provider/provider.dart' as provider;

double calories = 0;
double protein = 0;
double fat = 0;
double saturatedFats = 0;
double transFats = 0;
double carbohydrates = 0;
double sugars = 0;
double fibre = 0;
double vitaminA = 0;
double carotene = 0;
double vitaminB6 = 0;
double vitaminB12 = 0;
double vitaminB3 = 0;
double vitaminC = 0;
double vitaminE = 0;
double cholesterol = 0;
double potassium = 0;
double sodium = 0;
double calcium = 0;
double magnesium = 0;
double iron = 0;
double manganese = 0;
double zinc = 0;
double copper =0;
double phosphorus = 0;
double selenium = 0;
double perUnit = 0;
String userGender;



NutrilifeSharedPreferences pref = NutrilifeSharedPreferences();
double factVal;
List<Map> newFoodFacts = [];
String foodName = 'nothing';

class AddMyFood extends StatefulWidget {
  final double weight;
  AddMyFood(this.weight);
  @override
  _AddMyFoodState createState() => _AddMyFoodState();
}

class _AddMyFoodState extends State<AddMyFood> {


  getUserGender()async{
    userGender = await pref.getUserGender();
  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getUserGender();
    newFoodFacts = [];
    calories = 0;
    protein = 0;
    fat = 0;
    saturatedFats = 0;
    transFats = 0;
    carbohydrates = 0;
    sugars = 0;
    fibre = 0;
    vitaminA = 0;
    carotene = 0;
    vitaminB6 = 0;
    vitaminB12 = 0;
    vitaminB3 = 0;
    vitaminC = 0;
    vitaminE = 0;
    cholesterol = 0;
    potassium = 0;
    sodium = 0;
    calcium = 0;
    magnesium = 0;
    iron = 0;
    manganese = 0;
    zinc = 0;
    copper =0;
    phosphorus = 0;
    selenium = 0;
    perUnit =0;
  }

  @override
  Widget build(BuildContext context) {
    ProgressDialog pr = ProgressDialog(context);
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
      ),
      margin: EdgeInsets.all(5),
      padding: EdgeInsets.all(20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          ListTile(
            title: Text('Add Custom Food', style: TextStyle(fontSize:15, fontWeight: FontWeight.w700)),
            trailing: Icon(Icons.fastfood),
          ),
          SizedBox(height: 10,),
          Container(
            decoration: BoxDecoration(
              border: Border.all(
                color: Colors.grey[300],
              ),
              color: kGreenColor,
              borderRadius: BorderRadius.circular(10),
            ),
            margin: EdgeInsets.all(2),
            padding: EdgeInsets.all(10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  child: Text(
                    'Food Name', textAlign: TextAlign.center, style: TextStyle(color: Colors.white),
                  ),
                ),
                Expanded(
                  child: Container(
                    padding: EdgeInsets.symmetric(vertical:0),
                    child: TextField(
                      autofocus: true,
                      onChanged: (x){
                        foodName = x;
                      },
                      decoration: InputDecoration(
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                        ),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                        ),
                        border: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                        ),
                        hintText: '',
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 10),
          Container(
            decoration: BoxDecoration(
              color: kGreenColor,
              borderRadius: BorderRadius.circular(10),
              border: Border.all(
                color: Colors.grey[300],
              ),
            ),
            margin: EdgeInsets.all(2),
            padding: EdgeInsets.symmetric(horizontal: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  child: Text('Serving Size:', textAlign: TextAlign.center, style: TextStyle(color: Colors.white)),
                ),
                Expanded(
                  child: Container(
                    padding: EdgeInsets.symmetric(vertical:7),
                    child: TextField(
                      keyboardType: TextInputType.number,
                      inputFormatters: [WhitelistingTextInputFormatter.digitsOnly],
                      maxLength: 3,
                      onChanged: (x){
                        perUnit = double.parse(x);
                      },
                      decoration: InputDecoration(
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                        ),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                        ),
                        border: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                        ),
                        hintText: '',
                        suffix: Text('g'),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ), // 1 calories
          SizedBox(height: 10),
          Container(
            height: 300,
            child: ListView(
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                    color: kGreyColor,
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                      color: Colors.grey[300],
                    ),
                  ),
                  margin: EdgeInsets.all(2),
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: Text('Calories:', textAlign: TextAlign.center, style: TextStyle(color: kGreenColor)),
                      ),
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical:7),
                          child: TextField(
                            inputFormatters: [DecimalTextInputFormatter(decimalRange: 2)],
                            keyboardType: TextInputType.numberWithOptions(decimal: true),
                            maxLength: 6,
                            onChanged: (x){
                              calories = double.parse(x);
                            },
                            decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              border: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              hintText: '',
                              suffix: Text('kcal'),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ), // 1 calories
                Container(
                  decoration: BoxDecoration(
                    color: kGreyColor,
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                      color: Colors.grey[300],
                    ),
                  ),
                  margin: EdgeInsets.all(2),
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: Text('Protein:', textAlign: TextAlign.center, style: TextStyle(color: kGreenColor)),
                      ),
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical:7),
                          child: TextField(
                            inputFormatters: [DecimalTextInputFormatter(decimalRange: 2)],
                            keyboardType: TextInputType.numberWithOptions(decimal: true),
                            maxLength: 6,
                            onChanged: (x){
                              protein = double.parse(x);
                            },
                            decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              border: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              hintText: '',
                              suffix: Text('g'),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ), // 2 Protein
                Container(
                  decoration: BoxDecoration(
                    color: kGreyColor,
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                      color: Colors.grey[300],
                    ),
                  ),
                  margin: EdgeInsets.all(2),
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: Text('Fat:', textAlign: TextAlign.center, style: TextStyle(color: kGreenColor)),
                      ),
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical:7),
                          child: TextField(
                            inputFormatters: [DecimalTextInputFormatter(decimalRange: 2)],
                            keyboardType: TextInputType.numberWithOptions(decimal: true),
                            maxLength: 6,
                            onChanged: (x){
                              fat = double.parse(x);
                            },
                            decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              border: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              hintText: '',
                              suffix: Text('g'),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ), // 3 Fat
                Container(
                  decoration: BoxDecoration(
                    color: kGreyColor,
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                      color: Colors.grey[300],
                    ),
                  ),
                  margin: EdgeInsets.all(2),
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: Text('Saturated Fats:', textAlign: TextAlign.center, style: TextStyle(color: kGreenColor)),
                      ),
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical:7),
                          child: TextField(
                            inputFormatters: [DecimalTextInputFormatter(decimalRange: 2)],
                            keyboardType: TextInputType.numberWithOptions(decimal: true),
                            maxLength: 6,
                            onChanged: (x){
                              saturatedFats = double.parse(x);
                            },
                            decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              border: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              hintText: '',
                              suffix: Text('g'),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ), // 31 Saturated Fats
                Container(
                  decoration: BoxDecoration(
                    color: kGreyColor,
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                      color: Colors.grey[300],
                    ),
                  ),
                  margin: EdgeInsets.all(2),
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: Text('Trans Fats:', textAlign: TextAlign.center, style: TextStyle(color: kGreenColor)),
                      ),
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical:7),
                          child: TextField(
                            inputFormatters: [DecimalTextInputFormatter(decimalRange: 2)],
                            keyboardType: TextInputType.numberWithOptions(decimal: true),
                            maxLength: 6,
                            onChanged: (x){
                              transFats = double.parse(x);
                            },
                            decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              border: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              hintText: '',
                              suffix: Text('g'),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ), // 32 Trans Fat
                Container(
                  decoration: BoxDecoration(
                    color: kGreyColor,
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                      color: Colors.grey[300],
                    ),
                  ),
                  margin: EdgeInsets.all(2),
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: Text('Carbohydrates:', textAlign: TextAlign.center, style: TextStyle(color: kGreenColor)),
                      ),
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical:7),
                          child: TextField(
                            inputFormatters: [DecimalTextInputFormatter(decimalRange: 2)],
                            keyboardType: TextInputType.numberWithOptions(decimal: true),
                            maxLength: 6,
                            onChanged: (x){
                              carbohydrates = double.parse(x);
                            },
                            decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              border: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              hintText: '',
                              suffix: Text('g'),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ), // 4 Carbohydrates
                Container(
                  decoration: BoxDecoration(
                    color: kGreyColor,
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                      color: Colors.grey[300],
                    ),
                  ),
                  margin: EdgeInsets.all(2),
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: Text('Sugars', textAlign: TextAlign.center, style: TextStyle(color: kGreenColor)),
                      ),
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical:7),
                          child: TextField(
                            inputFormatters: [DecimalTextInputFormatter(decimalRange: 2)],
                            keyboardType: TextInputType.numberWithOptions(decimal: true),
                            maxLength: 6,
                            onChanged: (x){
                              sugars = double.parse(x);
                            },
                            decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              border: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              hintText: '',
                              suffix: Text('g'),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ), // 41 Sugars
                Container(
                  decoration: BoxDecoration(
                    color: kGreyColor,
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                      color: Colors.grey[300],
                    ),
                  ),
                  margin: EdgeInsets.all(2),
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: Text('Fibre:', textAlign: TextAlign.center, style: TextStyle(color: kGreenColor)),
                      ),
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical:7),
                          child: TextField(
                            inputFormatters: [DecimalTextInputFormatter(decimalRange: 2)],
                            keyboardType: TextInputType.numberWithOptions(decimal: true),
                            maxLength: 6,
                            onChanged: (x){
                              fibre = double.parse(x);
                            },
                            decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              border: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              hintText: '',
                              suffix: Text('g'),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ), // 5 Fibre
                Container(
                  decoration: BoxDecoration(
                    color: kGreyColor,
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                      color: Colors.grey[300],
                    ),
                  ),
                  margin: EdgeInsets.all(2),
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: Text('Vitamin A:', textAlign: TextAlign.center, style: TextStyle(color: kGreenColor)),
                      ),
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical:7),
                          child: TextField(
                            inputFormatters: [DecimalTextInputFormatter(decimalRange: 2)],
                            keyboardType: TextInputType.numberWithOptions(decimal: true),
                            maxLength: 6,
                            onChanged: (x){
                              vitaminA = double.parse(x);
                            },
                            decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              border: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              hintText: '',
                              suffix: Text('%'),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),// 6 vitamin A
                Container(
                  decoration: BoxDecoration(
                    color: kGreyColor,
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                      color: Colors.grey[300],
                    ),
                  ),
                  margin: EdgeInsets.all(2),
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: Text('Carotene:', textAlign: TextAlign.center, style: TextStyle(color: kGreenColor)),
                      ),
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical:7),
                          child: TextField(
                            inputFormatters: [DecimalTextInputFormatter(decimalRange: 2)],
                            keyboardType: TextInputType.numberWithOptions(decimal: true),
                            maxLength: 6,
                            onChanged: (x){
                              carotene = double.parse(x);
                            },
                            decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              border: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              hintText: '',
                              suffix: Text('%'),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),// 7 carotene
                Container(
                  decoration: BoxDecoration(
                    color: kGreyColor,
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                      color: Colors.grey[300],
                    ),
                  ),
                  margin: EdgeInsets.all(2),
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: Text('Vitamin B6:', textAlign: TextAlign.center, style: TextStyle(color: kGreenColor)),
                      ),
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical:7),
                          child: TextField(
                            inputFormatters: [DecimalTextInputFormatter(decimalRange: 2)],
                            keyboardType: TextInputType.numberWithOptions(decimal: true),
                            maxLength: 6,
                            onChanged: (x){
                              vitaminB6 = double.parse(x);
                            },
                            decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              border: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              hintText: '',
                              suffix: Text('%'),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ), //9 vitamin b6
                Container(
                  decoration: BoxDecoration(
                    color: kGreyColor,
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                      color: Colors.grey[300],
                    ),
                  ),
                  margin: EdgeInsets.all(2),
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: Text('Vitamin B12:', textAlign: TextAlign.center, style: TextStyle(color: kGreenColor)),
                      ),
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical:7),
                          child: TextField(
                            inputFormatters: [DecimalTextInputFormatter(decimalRange: 2)],
                            keyboardType: TextInputType.numberWithOptions(decimal: true),
                            maxLength: 6,
                            onChanged: (x){
                              vitaminB12 = double.parse(x);
                            },
                            decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              border: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              hintText: '',
                              suffix: Text('%'),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),//10 vitamin b12
                Container(
                  decoration: BoxDecoration(
                    color: kGreyColor,
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                      color: Colors.grey[300],
                    ),
                  ),
                  margin: EdgeInsets.all(2),
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: Text('Vitamin B3:', textAlign: TextAlign.center, style: TextStyle(color: kGreenColor)),
                      ),
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical:7),
                          child: TextField(
                            inputFormatters: [DecimalTextInputFormatter(decimalRange: 2)],
                            keyboardType: TextInputType.numberWithOptions(decimal: true),
                            maxLength: 6,
                            onChanged: (x){
                              vitaminB3 = double.parse(x);
                            },
                            decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              border: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              hintText: '',
                              suffix: Text('%'),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),//11 vitamin b3
                Container(
                  decoration: BoxDecoration(
                    color: kGreyColor,
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                      color: Colors.grey[300],
                    ),
                  ),
                  margin: EdgeInsets.all(2),
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: Text('Vitamin C:', textAlign: TextAlign.center, style: TextStyle(color: kGreenColor)),
                      ),
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical:7),
                          child: TextField(
                            inputFormatters: [DecimalTextInputFormatter(decimalRange: 2)],
                            keyboardType: TextInputType.numberWithOptions(decimal: true),
                            maxLength: 6,
                            onChanged: (x){
                              vitaminC = double.parse(x);
                            },
                            decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              border: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              hintText: '',
                              suffix: Text('%'),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),// 12 vitamin c
                Container(
                  decoration: BoxDecoration(
                    color: kGreyColor,
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                      color: Colors.grey[300],
                    ),
                  ),
                  margin: EdgeInsets.all(2),
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: Text('Vitamin E:', textAlign: TextAlign.center, style: TextStyle(color: kGreenColor)),
                      ),
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical:7),
                          child: TextField(
                            inputFormatters: [DecimalTextInputFormatter(decimalRange: 2)],
                            keyboardType: TextInputType.numberWithOptions(decimal: true),
                            maxLength: 6,
                            onChanged: (x){
                              vitaminE = double.parse(x);
                            },
                            decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              border: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              hintText: '',
                              suffix: Text('%'),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ), // 13 Vitamin E
                Container(
                  decoration: BoxDecoration(
                    color: kGreyColor,
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                      color: Colors.grey[300],
                    ),
                  ),
                  margin: EdgeInsets.all(2),
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: Text('Cholesterol:', textAlign: TextAlign.center, style: TextStyle(color: kGreenColor)),
                      ),
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical:7),
                          child: TextField(
                            inputFormatters: [DecimalTextInputFormatter(decimalRange: 2)],
                            keyboardType: TextInputType.numberWithOptions(decimal: true),
                            maxLength: 6,
                            onChanged: (x){
                              cholesterol = double.parse(x);
                            },
                            decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              border: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              hintText: '',
                              suffix: Text('mg'),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),// 14 cholesterol
                Container(
                  decoration: BoxDecoration(
                    color: kGreyColor,
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                      color: Colors.grey[300],
                    ),
                  ),
                  margin: EdgeInsets.all(2),
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: Text('Potassium:', textAlign: TextAlign.center, style: TextStyle(color: kGreenColor)),
                      ),
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical:7),
                          child: TextField(
                            inputFormatters: [DecimalTextInputFormatter(decimalRange: 2)],
                            keyboardType: TextInputType.numberWithOptions(decimal: true),
                            maxLength: 6,
                            onChanged: (x){
                              potassium = double.parse(x);
                            },
                            decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              border: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              hintText: '',
                              suffix: Text('mg'),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ), // 15 potassium
                Container(
                  decoration: BoxDecoration(
                    color: kGreyColor,
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                      color: Colors.grey[300],
                    ),
                  ),
                  margin: EdgeInsets.all(2),
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: Text('Sodium:', textAlign: TextAlign.center, style: TextStyle(color: kGreenColor)),
                      ),
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical:7),
                          child: TextField(
                            inputFormatters: [DecimalTextInputFormatter(decimalRange: 2)],
                            keyboardType: TextInputType.numberWithOptions(decimal: true),
                            maxLength: 6,
                            onChanged: (x){
                              sodium = double.parse(x);
                            },
                            decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              border: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              hintText: '',
                              suffix: Text('mg'),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ), // 16 Sodium
                Container(
                  decoration: BoxDecoration(
                    color: kGreyColor,
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                      color: Colors.grey[300],
                    ),
                  ),
                  margin: EdgeInsets.all(2),
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: Text('Calcium:', textAlign: TextAlign.center, style: TextStyle(color: kGreenColor)),
                      ),
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical:7),
                          child: TextField(
                            inputFormatters: [DecimalTextInputFormatter(decimalRange: 2)],
                            keyboardType: TextInputType.numberWithOptions(decimal: true),
                            maxLength: 6,
                            onChanged: (x){
                              calcium = double.parse(x);
                            },
                            decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              border: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              hintText: '',
                              suffix: Text('%'),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ), // 17 Calcium
                Container(
                  decoration: BoxDecoration(
                    color: kGreyColor,
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                      color: Colors.grey[300],
                    ),
                  ),
                  margin: EdgeInsets.all(2),
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: Text('Magnesium:', textAlign: TextAlign.center, style: TextStyle(color: kGreenColor)),
                      ),
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical:7),
                          child: TextField(
                            inputFormatters: [DecimalTextInputFormatter(decimalRange: 2)],
                            keyboardType: TextInputType.numberWithOptions(decimal: true),
                            maxLength: 6,
                            onChanged: (x){
                              magnesium = double.parse(x);
                            },
                            decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              border: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              hintText: '',
                              suffix: Text('%'),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ), // 18 Magnesium
                Container(
                  decoration: BoxDecoration(
                    color: kGreyColor,
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                      color: Colors.grey[300],
                    ),
                  ),
                  margin: EdgeInsets.all(2),
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: Text('Iron:', textAlign: TextAlign.center, style: TextStyle(color: kGreenColor)),
                      ),
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical:7),
                          child: TextField(
                            inputFormatters: [DecimalTextInputFormatter(decimalRange: 2)],
                            keyboardType: TextInputType.numberWithOptions(decimal: true),
                            maxLength: 6,
                            onChanged: (x){
                              iron = double.parse(x);
                            },
                            decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              border: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              hintText: '',
                              suffix: Text('%'),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ), // 19 Iron
                Container(
                  decoration: BoxDecoration(
                    color: kGreyColor,
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                      color: Colors.grey[300],
                    ),
                  ),
                  margin: EdgeInsets.all(2),
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: Text('Manganese:', textAlign: TextAlign.center, style: TextStyle(color: kGreenColor)),
                      ),
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical:7),
                          child: TextField(
                            inputFormatters: [DecimalTextInputFormatter(decimalRange: 2)],
                            keyboardType: TextInputType.numberWithOptions(decimal: true),
                            maxLength: 6,
                            onChanged: (x){
                              manganese = double.parse(x);
                            },
                            decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              border: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              hintText: '',
                              suffix: Text('%'),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ), // 20 manganese
                Container(
                  decoration: BoxDecoration(
                    color: kGreyColor,
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                      color: Colors.grey[300],
                    ),
                  ),
                  margin: EdgeInsets.all(2),
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: Text('Zinc:', textAlign: TextAlign.center, style: TextStyle(color: kGreenColor)),
                      ),
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical:7),
                          child: TextField(
                            inputFormatters: [DecimalTextInputFormatter(decimalRange: 2)],
                            keyboardType: TextInputType.numberWithOptions(decimal: true),
                            maxLength: 6,
                            onChanged: (x){
                              zinc = double.parse(x);
                            },
                            decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              border: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              hintText: '',
                              suffix: Text('%'),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ), // 21 Zinc
                Container(
                  decoration: BoxDecoration(
                    color: kGreyColor,
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                      color: Colors.grey[300],
                    ),
                  ),
                  margin: EdgeInsets.all(2),
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: Text('Copper:', textAlign: TextAlign.center, style: TextStyle(color: kGreenColor)),
                      ),
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical:7),
                          child: TextField(
                            inputFormatters: [DecimalTextInputFormatter(decimalRange: 2)],
                            keyboardType: TextInputType.numberWithOptions(decimal: true),
                            maxLength: 6,
                            onChanged: (x){
                              copper = double.parse(x);
                            },
                            decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              border: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              hintText: '',
                              suffix: Text('%'),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ), // 22 copper
                Container(
                  decoration: BoxDecoration(
                    color: kGreyColor,
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                      color: Colors.grey[300],
                    ),
                  ),
                  margin: EdgeInsets.all(2),
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: Text('Phosphorus:', textAlign: TextAlign.center, style: TextStyle(color: kGreenColor)),
                      ),
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical:7),
                          child: TextField(
                            inputFormatters: [DecimalTextInputFormatter(decimalRange: 2)],
                            keyboardType: TextInputType.numberWithOptions(decimal: true),
                            maxLength: 6,
                            onChanged: (x){
                              phosphorus = double.parse(x);
                            },
                            decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              border: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              hintText: '',
                              suffix: Text('%'),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ), // phosphorus
                Container(
                  decoration: BoxDecoration(
                    color: kGreyColor,
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                      color: Colors.grey[300],
                    ),
                  ),
                  margin: EdgeInsets.all(2),
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: Text('Selenium:', textAlign: TextAlign.center, style: TextStyle(color: kGreenColor)),
                      ),
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical:7),
                          child: TextField(
                            inputFormatters: [DecimalTextInputFormatter(decimalRange: 2)],
                            keyboardType: TextInputType.numberWithOptions(decimal: true),
                            maxLength: 6,
                            onChanged: (x){
                              selenium = double.parse(x);
                            },
                            decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              border: UnderlineInputBorder(
                                borderSide: BorderSide(color: kGreenColor),
                              ),
                              hintText: '',
                              suffix: Text('%'),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),// selenium
              ],
            ),
          ),
          SizedBox(height: 10),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: FlatButton(
                  color: kGreyColor,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8.0),
                    side: BorderSide(color: kGreyColor),
                  ),
                  child: Text('Cancel',
                      style: TextStyle(
                        color: Colors.black,
                      )),
                  onPressed: (){
                    Navigator.pop(context);
                  },
                ),
              ),
              SizedBox(width: 10),
              Expanded(
                child: FlatButton(
                  color: kGreenColor,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8.0),
                    side: BorderSide(color: kGreenColor),
                  ),
                  child: Text('Add Food',
                      style: TextStyle(
                        color: Colors.white,
                      )),
                  onPressed: ()async{
                    Map dailyGoalVal = {
                      'vitamin a' : (userGender == "Male") ? 3000 : 2333,
                      'carotene' : 6000,
                      'vitamin b6' : 1.3,
                      'vitamin b12' : 2.4,
                      'vitamin b3': (userGender == "Male") ? 16 : 14,
                      'vitamin c' : (userGender == "Male") ? 90 : 75,
                      'vitamin e' : 22,
                      'calcium' : 1000,
                      'magnesium' : (userGender == "Male") ? 420 : 320,
                      'iron' : (userGender == "Male") ? 8 : 18,
                      'manganese' : (userGender == "Male") ? 2.3 : 1.8,
                      'zinc' : (userGender == "Male") ? 11 : 8,
                      'copper' : 0.9,
                      'phosphorus' : 700,
                      'selenium' : 55,
                    };
                    newFoodFacts = [
                      {"fact": 1008,
                        "value" : double.parse((calories / perUnit * 100).toStringAsFixed(2)),
                      },
                      {"fact": 1003,
                        "value" : double.parse((protein / perUnit * 100).toStringAsFixed(2)),
                      },
                      {"fact": 1004,
                        "value" : double.parse((fat / perUnit * 100).toStringAsFixed(2)),
                      },
                      {"fact": 1258,
                        "value" : double.parse((saturatedFats / perUnit * 100).toStringAsFixed(2)),
                      },
                      {"fact": 1257,
                        "value" : double.parse((transFats / perUnit * 100).toStringAsFixed(2)),
                      },
                      {"fact": 1005,
                        "value" : double.parse((carbohydrates / perUnit * 100).toStringAsFixed(2)),
                      },
                      {"fact": 2000,
                        "value" : double.parse((sugars / perUnit * 100).toStringAsFixed(2)),
                      },
                      {"fact": 1079,
                        "value" : double.parse((fibre / perUnit * 100).toStringAsFixed(2)),
                      },
                      {"fact": 1104,
                        "value" : double.parse((((vitaminA/100) * dailyGoalVal['vitamin a'] / perUnit) * 100).toStringAsFixed(2)),
                      },
                      {"fact": 1107,
                        "value" : double.parse((((carotene/100) * dailyGoalVal['carotene'] / perUnit) * 100).toStringAsFixed(2)),
                      },
                      {"fact": 1175,
                        "value" : double.parse((((vitaminB6/100) * dailyGoalVal['vitamin b6'] / perUnit) * 100).toStringAsFixed(2)),
                      },
                      {"fact": 1178,
                      "value" : double.parse((((vitaminB12/100) * dailyGoalVal['vitamin b12'] / perUnit) * 100).toStringAsFixed(2)),
                      },
                      {"fact": 1167,
                      "value" : double.parse((((vitaminB3/100) * dailyGoalVal['vitamin b3'] / perUnit) * 100).toStringAsFixed(2)),
                      },
                      {"fact": 1162,
                      "value" : double.parse((((vitaminC/100) * dailyGoalVal['vitamin c'] / perUnit) * 100).toStringAsFixed(2)),
                      },
                      {"fact": 1109,
                      "value" : double.parse((((vitaminE/100) * dailyGoalVal['vitamin e'] / perUnit) * 100).toStringAsFixed(2)),
                      },
                      {"fact": 1253,
                        "value" : double.parse((cholesterol / perUnit * 100).toStringAsFixed(2)),
                      },
                      {"fact": 1092,
                        "value" : double.parse((potassium / perUnit * 100).toStringAsFixed(2)),
                      },
                      {"fact": 1093,
                        "value" : double.parse((sodium / perUnit * 100).toStringAsFixed(2)),
                      },
                      {"fact": 1087,
                      "value" : double.parse((((calcium/100) * dailyGoalVal['calcium'] / perUnit) * 100).toStringAsFixed(2)),
                      },
                      {"fact": 1090,
                      "value" : double.parse((((magnesium/100) * dailyGoalVal['magnesium'] / perUnit) * 100).toStringAsFixed(2)),
                      },
                      {"fact": 1089,
                      "value" : double.parse((((iron/100) * dailyGoalVal['iron'] / perUnit) * 100).toStringAsFixed(2)),
                      },
                      {"fact": 1101,
                      "value" : double.parse((((manganese/100) * dailyGoalVal['manganese'] / perUnit) * 100).toStringAsFixed(2)),
                      },
                      {"fact": 1095,
                      "value" : double.parse((((zinc/100) * dailyGoalVal['zinc'] / perUnit) * 100).toStringAsFixed(2)),
                      },
                      {"fact": 1098,
                        "value" : double.parse((((copper/100) * dailyGoalVal['copper'] / perUnit) * 100).toStringAsFixed(2)),
                      },
                      {"fact": 1091,
                        "value" : double.parse((((phosphorus/100) * dailyGoalVal['phosphorus'] / perUnit) * 100).toStringAsFixed(2)),
                      },
                      {"fact": 1103,
                        "value" : double.parse((((selenium/100) * dailyGoalVal['selenium'] / perUnit) * 100).toStringAsFixed(2)),
                      },
                    ];

                    if(foodName.length > 3) {
                      if(perUnit != 0) {
                        pr.update(
                          message: "Adding..",
                          progressWidget: Container(
                              padding: EdgeInsets.all(8.0),
                              child: CircularProgressIndicator(
                                valueColor: AlwaysStoppedAnimation<Color>(
                                    kGreenColor),
                              )),
                        );
                        pr.show();
                        print(newFoodFacts);
                        String token = await pref.getToken();
                        var response = await http.post(
                            '${kUrl}addFood',
                            headers: {
                              'Accept': 'application/json',
                              'Authorization': 'Bearer $token',
                            },
                            body: {
                              'name': '$foodName',
                              'facts': '${jsonEncode(newFoodFacts)}',
                            }
                        );
                        var data = jsonDecode(response.body);
                        print(data);
                        await provider.Provider.of<GlobalVariables>(context, listen: false).getMyFoodDa();
                        pr.hide();
                        Navigator.pushReplacement(context, FadeRoute(
                            page: ChooseFood(widget.weight, 2)
                        ));
                      } else {
                        wrongAlert(context, 'You must enter the serving size!');
                      }
                    } else {
                      wrongAlert(context, 'Food name must be at least 3 letters!');
                    }
                  },
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class DecimalTextInputFormatter extends TextInputFormatter {
  DecimalTextInputFormatter({this.decimalRange})
      : assert(decimalRange == null || decimalRange > 0);

  final int decimalRange;

  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, // unused.
      TextEditingValue newValue,
      ) {
    TextSelection newSelection = newValue.selection;
    String truncated = newValue.text;

    if (decimalRange != null) {
      String value = newValue.text;

      if (value.contains(".") &&
          value.substring(value.indexOf(".") + 1).length > decimalRange) {
        truncated = oldValue.text;
        newSelection = oldValue.selection;
      } else if (value == ".") {
        truncated = "0.";

        newSelection = newValue.selection.copyWith(
          baseOffset: math.min(truncated.length, truncated.length + 1),
          extentOffset: math.min(truncated.length, truncated.length + 1),
        );
      }

      return TextEditingValue(
        text: truncated,
        selection: newSelection,
        composing: TextRange.empty,
      );
    }
    return newValue;
  }
}
