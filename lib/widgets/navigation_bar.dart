import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nutrilifeapp/screens/diary.dart';
import 'package:nutrilifeapp/const.dart';
import 'package:nutrilifeapp/screens/history.dart';
import 'package:nutrilifeapp/screens/home_loading.dart';
import 'package:nutrilifeapp/screens/me_loading.dart';
import 'package:nutrilifeapp/services/shared_preferences.dart';

NutrilifeSharedPreferences pref = NutrilifeSharedPreferences();

int scrNum;

class NavigationBar extends StatefulWidget {
  final int screenNum;
  NavigationBar(this.screenNum);
  @override
  _NavigationBarState createState() => _NavigationBarState();
}

class _NavigationBarState extends State<NavigationBar> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    scrNum = widget.screenNum;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(7),
      decoration: BoxDecoration(
        border: Border.all(
          color: Colors.grey[300],
        ),
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
      ),
      margin: EdgeInsets.all(5),
      child: Row(
        //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Expanded(
            child: GestureDetector(
              onTap: () async {
                String token = await pref.getToken();
                if (scrNum != 0) {
                  Navigator.pushReplacement(
                      context,
                      FadeRoute(
                        page: HomeLoading(kInitialData, token, 0),
                      ));
                  setState(() {
                    scrNum = 0;
                  });
                }
              },
              child: Container(
                child: Column(
                    // crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Icon(Icons.home,
                          color: (scrNum == 0) ? Colors.black : Colors.grey),
                      Text('Home',
                          style: TextStyle(
                              color:
                                  (scrNum == 0) ? Colors.black : Colors.grey)),
                    ]),
              ),
            ),
          ),
          Expanded(
            child: GestureDetector(
              onTap: () async {
                if (scrNum != 1) {
                  Navigator.pushReplacement(
                      context,
                      FadeRoute(
                        page: DiaryScreen(),
                      ));
                  setState(() {
                    scrNum = 1;
                  });
                }
              },
              child: Container(
                child: Column(
                    // crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Icon(Icons.calendar_today,
                          color: (scrNum == 1) ? Colors.black : Colors.grey),
                      Text('Diary',
                          style: TextStyle(
                              color:
                                  (scrNum == 1) ? Colors.black : Colors.grey)),
                    ]),
              ),
            ),
          ),
          Expanded(
            child: GestureDetector(
              onTap: () async {
                String token = await pref.getToken();
                DateTime date1 = DateTime.now().subtract(Duration(days: 7));
                DateTime date2 = DateTime.now();
                if (scrNum != 2) {
                  Navigator.pushReplacement(
                      context,
                      FadeRoute(
                        page: HistoryScreen(),
                      ));
                  setState(() {
                    scrNum = 2;
                  });
                }
              },
              child: Container(
                child: Column(
                    //  crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Icon(Icons.history,
                          color: (scrNum == 2) ? Colors.black : Colors.grey),
                      Text('History',
                          style: TextStyle(
                              color:
                                  (scrNum == 2) ? Colors.black : Colors.grey)),
                    ]),
              ),
            ),
          ),
          Expanded(
            child: GestureDetector(
              onTap: () {
                if (scrNum != 3) {
                  Navigator.pushReplacement(
                      context,
                      FadeRoute(
                        page: MeLoading(),
                      ));
                  setState(() {
                    scrNum = 3;
                  });
                }
              },
              child: Container(
                child: Column(
                    // crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Icon(Icons.person,
                          color: (scrNum == 3) ? Colors.black : Colors.grey),
                      Text('Me',
                          style: TextStyle(
                              color:
                                  (scrNum == 3) ? Colors.black : Colors.grey)),
                    ]),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
