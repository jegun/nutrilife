import UIKit
import Flutter
import iFreshSDK

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
  
  private var mainCoordinator: AppCoordinator?
  var isCheckBluetooth: Int = 0
  var weightValue: String = ""
  
  override func application( _ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    
    GeneratedPluginRegistrant.register(with: self)
    
    isCheckBluetooth = 1
    
    let flutterViewController: FlutterViewController = window?.rootViewController as! FlutterViewController
    
    let tstChannel = FlutterMethodChannel(name: "com.ble.support/testChannel", binaryMessenger: flutterViewController.binaryMessenger)
    
    tstChannel.setMethodCallHandler({ [self] (call: FlutterMethodCall, result: FlutterResult) -> Void in
      
      print(call.arguments)
      
      if call.method == "getBleSupportStatus" {
        
        if self.isCheckBluetooth == 0 {
          
          let someDict:[Int:String] = [0:"1"]
          result(someDict)
          
        }else if self.isCheckBluetooth == 1 {
          let someDict:[Int:String] = [0:"2"]
          result(someDict)
        }
        
      }else if (call.method == "bleTurnOn") {
        
        if isCheckBluetooth == 0 {
          let someDict:[Int:String] = [0:"1"]
          result(someDict)
          
        }else if isCheckBluetooth == 1 {
          let someDict:[Int:String] = [0:"2"]
          result(someDict)
        }
        
      }else if(call.method == "getBLEDeviceList") {
        self.mainCoordinator?.start()
        
      }
      
    })
    
    let navigationController = UINavigationController(rootViewController: flutterViewController)
    navigationController.isNavigationBarHidden = true
    window?.rootViewController = navigationController
    mainCoordinator = AppCoordinator(navigationController: navigationController)
    window?.makeKeyAndVisible()
    
    return super.application(application, didFinishLaunchingWithOptions: launchOptions)
    
  }
}
