//
//  ViewController.swift
//  iFreshDemoSwift
//
//  Created by CIPL0419 on 30/03/21.
//

import UIKit
import iFreshSDK

class ViewController: UIViewController {
  
  @IBOutlet weak var labelStatus: UILabel!
  @IBOutlet weak var labelWeight: UILabel!
  var isAddPeripheraling: Bool = true
  var peripheralArray : [iFreshDevice] = []
  
  //MARK:- ViewController LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
   
    
    NSLog("********************************************")
    NSLog("ViewController ==> viewDidLoad Methode")
    NSLog("********************************************")
    
    iFreshSDK.shareManager()?.bleDisconnect()
    iFreshSDK.shareManager()?.setbleReturnValueDelegate(self)
    iFreshSDK.shareManager()?.bleDoScan()
    
  }
  
}

//MARK:- BleReturnValueDelegate Methode
extension ViewController: BleReturnValueDelegate {
  
  func bluetoothManagerReturePenetrateData(_ data: Data!) {
    print(data.description)
    
    NSLog("********************************************")
    NSLog("description ==> \(data.description)")
    NSLog("********************************************")
  }
  
  func bleStatusupdate(_ bleStatus: GN_BleStatus) {
    
    if bleStatus == bleOpen || bleStatus == bleBreak {
      
      print("Not connected")
      labelStatus.text = "Not connected"
      NSLog("********************************************")
      NSLog("BleReturnValueDelegate ==> Not Connected")
      NSLog("********************************************")
      
    }else if bleStatus == bleOff {
      print("Not turned on")
      labelStatus.text = "Not turned on"
      NSLog("********************************************")
      NSLog("BleReturnValueDelegate ==> Not turned on")
      NSLog("********************************************")
      
    }else if bleStatus == bleConnect {
      print("Connected")
      labelStatus.text = "Connected"
      NSLog("********************************************")
      NSLog("BleReturnValueDelegate ==> Connected")
      NSLog("********************************************")
      
    }
  }
  
  func bleReturnScannedDevice(_ device: iFreshDevice!) {
    
    if isAddPeripheraling == true {
      return
    }
    
    isAddPeripheraling = true
    
    var willAdd = true
    for model in peripheralArray {
      //print(model.mac)
      if model.mac == device.mac {
        willAdd = false
      }
    }
    
    if willAdd {
      peripheralArray.append(device)
      
      iFreshSDK.shareManager()?.setbleReturnValueDelegate(self)
      iFreshSDK.shareManager()?.connect(peripheralArray.first)
      
    }
    
    isAddPeripheraling = false
  }
  
  
  func bleReturnValueModel(_ model: iFreshModel!) {
    
    print("Weight", model.value)
    print("Model.gValue", model.gValue)
    labelWeight.text = model.value
    
    NSLog("********************************************")
    NSLog("Weight ==> \(model.value) ")
    NSLog("********************************************")
    
    NSLog("********************************************")
    NSLog("Model.gValue ==> \(model.gValue) ")
    NSLog("********************************************")
  }
  
}
