//
//  iFreshModel.h
//  iFreshSDK
//
//  Created by zhang on 16/9/9.
//  Copyright © 2016年 taolei. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface iFreshModel : NSObject


//weight (重量)
@property (nonatomic, copy) NSString *value;
//Data in "g"(以“g”为单位的数据)
@property (nonatomic, assign) NSInteger gValue;
//
@property (nonatomic,copy) NSString *Unit;


/**
 * g to lb method
 * Weightvalue: the weight string passed in g
 * Returned data separated by ":"
 */
+ (NSString *)gTolb:(NSString *)weightValue;
/**
 * g to oz method
 * Weightvalue: the weight string passed in g
 */
+ (NSString *)gTooz:(NSString *)weightVlaue;
/**
 * Lb to g method
 * Weightvalue: the weight string passed in lb, please pass the ":" separated data
 */
+ (NSString *)lbTog:(NSString *)weightVlaue;
/**
 * Oz to g method
 * weightvalue：Incoming weight string in oz
 */
+ (NSString *)ozTog:(NSString *) weightValue;
/**
 * g to kg method
 * weightvalue：Incoming weight string in g
 */
+ (NSString *)gToKg:(NSString *)weightValue;
/**
 * g to kg method
 * weightvalue：Incoming weight string in g
 */
+ (NSString *)gToJin:(NSString *)weightValue;
/**
 * Kg to g method
 * weightvalue：Incoming weight string in kg
 */
+ (NSString *)kgTog:(NSString *)weightValue;
/**
 * Pinch to g method
 * weightvalue：Incoming weight string in kilograms
 */
+ (NSString *)jinTog:(NSString *)weightValue;
/**
 * Kg to lb method
 * weightvalue：Incoming weight string in kg
 */
+ (NSString *)kgTolb:(NSString *)weightValue;

+ (NSString *)LBTog:(NSString *)weightValue;
+ (NSString *)gToLB:(NSString *)weightVlaue;
@end
