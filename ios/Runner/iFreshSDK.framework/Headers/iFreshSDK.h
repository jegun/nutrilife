//
//  iFreshSDK.h
//  iFreshSDK
//
//  Created by zhang on 16/9/9.
//  Copyright © 2016年 taolei. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "iFreshModel.h"
#import "AiFreshHeader.h"
#import "iFreshDevice.h"

@protocol BleReturnValueDelegate <NSObject>
@optional

/**
 ble Penetrate Data(蓝牙透传数据)

 @param data Penetrate data（透传数据）
 */
-(void)bluetoothManagerReturePenetrateData:(NSData *)data;

/**
 *  Model model: value ble returns the value (model模型：value ble返还数值)
 *
 *  @param model Global Bluetooth model data(全局蓝牙模型数据)
 */
- (void)bleReturnValueModel:(iFreshModel*)model;

/**
 * Proxy method triggered by end switching unit(称端切换单位触发的代理方法)
 * GN_UnitEnum Unit enumeration(单位枚举)
 * unitChang   Switched unit(切换后的单位)
 */

- (void)changeUnitWithBle:(GN_UnitEnum)unitChange;

/**
 * Proxy method for changing Bluetooth connection status(蓝牙连接状态改变的代理方法)
 * GN_BleStatus Bluetooth status enumeration(蓝牙状态枚举)
 * bleStatus   Current Bluetooth status(当前蓝牙状态)
 */
- (void)bleStatusupdate:(GN_BleStatus)bleStatus;
/**
 *Callback Bluetooth device type（回调蓝牙设备类型）
 *deviceType:Bluetooth device type, default AiFrushDeviceTypeUnknown when no device is connected（蓝牙设备类型,未连接设备时，默认AiFrushDeviceTypeUnknown）
 */
- (void)bleReturnDeviceType:(AiFrushDeviceType)deviceType;


/**
 *Callback Bluetooth  scanned devices（回调扫描到的设备）
 */
- (void)bleReturnScannedDevice:(iFreshDevice *)device;



#pragma mark ============ Coffee scale callback（咖啡秤回调） ==============
/**
 *Coffee scale support agent callback（咖啡秤支持的代理回调）
 *workType:Coffee scale operation instructions（咖啡秤的操作指令）
 */
-(void)bluetoothManagerCoffeeTimingWork:(BleCoffeeTimingWorkType)workType;
/**
 *Coffee scale return time（咖啡秤返回的时间）
 *min:minute(分)
 *sec:second(秒)
 *countdown:Whether it is countdown, YES: countdown, NO: timing(是否是倒计时，YES:倒计时，NO：正计时)
 */
-(void)bluetoothManagerCoffeeTime:(int )min second:(int)sec isCountdown:(BOOL)countdown;
/**
 *Coffee scale countdown starts（咖啡秤倒计时开始）
 *min:minute（分）
 *sec:second（秒）
 */
-(void)bluetoothManagerCoffeeCountdownStartTime:(int )min second:(int)sec;

/**
 Bluetooth pause timing callback(蓝牙暂停计时的回调)
 
 @param type Pause type(暂停类型)
 @param min minute(分钟)
 @param sec second(秒)
 */
-(void)bluetoothManagerCoffeePauseType:(BleCoffeeTimingWorkType)type minute:(int)min second:(int)sec;

/**
 设备发送停止报警指令
 */
-(void)bluetoothManagerCoffeeReceiveStopAlarm;

@end

@interface iFreshSDK : NSObject

/// Whether to link to Bluetooth(是否链接上蓝牙)
@property (nonatomic, assign) BOOL isBle_Link;



@property (nonatomic, assign, readonly) GN_UnitEnum unitEnum;


@property (nonatomic, assign , readonly) AiFrushDeviceType deviceType;


////
//+(void)setAppKey:(NSString *)key appSecret:(NSString *)secret;
/**
 *  shareManager
 */
+ (instancetype)shareManager;
/*
 * write data to ble(写入数据)
 */

- (void)sendDataToBle:(NSData *)data;
/**
 *  start scaning(开始扫描)
 */
- (void)bleDoScan;
/**
 *  stop scaning(停止扫描)
 */
- (void)bleStopScan;

/**
 *  stop connect device(停止连接设备)
 */
- (void)bleDisconnect;

/**
 * Follow the protocol call(遵循协议的调用)
 */
- (void)setbleReturnValueDelegate:(id<BleReturnValueDelegate>)delegate;

/**
 * setting unit(设置单位)
 *
 * @param unit Write units according to GN_UnitEnum enumeration(根据GN_UnitEnum枚举写入单位)
 */
- (void)insertTheUnit:(GN_UnitEnum)unit;

/**
 * Return to zero(归零)
 */
- (void)zeroWriteBle;


/*
 * Shutdown command（关机指令）
 *
 */
- (void)turnOffDevice;

/*
 * The SDK is no longer automatically connected, so you should call the following methods to connect
 * (SDK不再自动连接，需要App调用如下方法连接设备）
 *
 */
- (void)connectDevice:(iFreshDevice *)device;

#pragma mark ============ No longer use（不再使用） ==============

/*
 Setting weight(设置重量)
 */
- (void)setWeight:(NSInteger)weight;

/*
 *  Set a single calorie（设置单项卡路里）
 */
- (void)setCalorie:(NSInteger)calories;

/*
 *  Set total calories（设置总卡路里）
 */
- (void)setTotalCalorie:(NSInteger)toatlCalories;


/*
 *  Set total fat（设置总脂肪）
 */
- (void)setTotalFat:(NSInteger)fat;

/*
 *  Set total protein（设置总蛋白质）
 */
- (void)setTotalProtein:(NSInteger)protein;

/*
 *  Set total carbohydrates（设置总碳水化合物）
 */
- (void)setTotalCarbohydrates:(NSInteger)carbohydertes;
/*
 *  Set total fat fiber（设置总脂肪纤维）
 */
- (void)setTotalFiber:(NSInteger)fiber;
/*
*  Set total cholesterol（设置总胆固醇）
*/
- (void)setTotalCholesterd:(NSInteger)cholesterd;
/*
*  Set total sodium content（设置总钠含量）
*/
- (void)setTotalSodium:(NSInteger)sodium;

/*
 *  Set total sugar content（设置总糖含量）
 */
- (void)setTotalSugar:(NSInteger)sugar;

/*
 *Setting fat（设置脂肪）
 */
- (void)setFat:(NSInteger)fat;

/*
 *Set protein（设置蛋白质）
 */
- (void)setProtein:(NSInteger)protein;
/*
 *Set carbohydrates（设置碳水化合物）
 */

- (void)setCarbohydrates:(NSInteger)carbohydertes;
/*
 *Setting dietary fiber（设置膳食纤维）
 */

- (void)setFiber:(NSInteger)fiber;
/*
 *Set cholesterol（设置胆固醇）
 */

- (void)setCholesterd:(NSInteger)cholesterd;
/*
 *Set sodium content（设置钠含量）
 */

- (void)setSodium:(NSInteger)sodium;
/*
 *Setting the sugar content（设置糖含量）
 */
- (void)setSugar:(NSInteger)sugar;


#pragma mark ============ Coffee scale（咖啡秤） ==============
//start the timer（开始计时）
-(void)bluetoothCoffeeStartTime;
//pause(暂停)
-(void)bluetoothCoffeePauseType:(BleCoffeeTimingWorkType)type minute:(int)min second:(int)sec;
//reset(重置)
-(void)bluetoothCoffeeResetTime;
//Countdown starts(倒计时开始)
-(void)bluetoothCoffeeCountdownStartTime:(int)min second:(int)sec;

//APP reply stop alarm command（APP 回复停止报警指令)
-(void)bluetoothManagerSendStopAlarm;


@end
