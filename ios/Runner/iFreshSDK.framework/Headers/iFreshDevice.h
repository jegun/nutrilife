//
//  iFreshDevice.h
//  iFreshSDK
//
//  Created by steven wu on 2021/6/23.
//  Copyright © 2021 IOT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AiFreshHeader.h"
#import <CoreBluetooth/CoreBluetooth.h>

NS_ASSUME_NONNULL_BEGIN

@interface iFreshDevice : NSObject

@property (nonatomic, assign) AiFrushDeviceType deviceType;
@property (nonatomic,copy)  NSString  *mac;      //蓝牙地址
@property (nonatomic, strong) CBPeripheral  *peripheral;
@property (nonatomic, strong) NSDictionary *advertisementData;
@property (nonatomic, strong) NSNumber *RSSI;

@end

NS_ASSUME_NONNULL_END
