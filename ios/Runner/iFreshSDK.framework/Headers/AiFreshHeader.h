
//
//  AiFreshHeader.h
//  iFreshSDK
//
//  Created by iot_user on 2019/4/4.
//  Copyright © 2019年 IOT. All rights reserved.
//

#ifndef AiFreshHeader_h
#define AiFreshHeader_h

typedef NS_ENUM(NSUInteger, AiFrushDeviceType) {
    AiFrushDeviceTypeUnknown ,    //When not connected（未连接时）
    AiFrushDeviceTypeAC04    ,    //AC04 scale does not support KG（AC04的秤不支持KG）
    AiFrushDeviceTypeAC05    ,    //Some AC05 scales support KG（部分AC05的秤支持KG）
};

//咖啡定时功能
typedef NS_ENUM(NSUInteger, BleCoffeeTimingWorkType) {
    BleCoffeeTimingWorkTypeStart          = 1      , //Starting time（正计时开始）
    BleCoffeeTimingWorkTypePositivePause  = 2      , //Positive timing pause（正计时暂停）
    BleCoffeeTimingWorkTypeReset          = 3      , //reset(重置)
    BleCoffeeTimingWorkTypeCountdownPause = 4      , //Countdown pause(倒计时暂停)
};

typedef enum : NSUInteger {
    
    UNIT_g = 0,  //g
    UNIT_ml = 1, //
    UNIT_lb = 2, //lb:oz
    UNIT_oz = 3, //oz
    UNIT_kg = 4, //kg
    UNIT_jin = 5, //斤
    UNIT_LB = 10,//lb
    
} GN_UnitEnum;


typedef enum : NSUInteger {
    
    bleOpen  = 0,
    bleOff,
    bleConnect,
    bleBreak
    
} GN_BleStatus;

/**
 000 = 0 位小数(不带小数点); 001 = 1 位小数
 010 = 2 位小数; 011 = 3 位小数; 100 = 4 位小数 101 = 5 位小数; 110 = 6 位小数; 111 = 7 位小数
 */
typedef NS_ENUM(NSUInteger, PointType) {
    PointType_0 = 0,
    PointType_1 = 1,
    PointType_2 = 2,
    PointType_3 = 3,
    PointType_4 = 4,
    PointType_5 = 5,
    PointType_6 = 6,
    PointType_7 = 7,
};

//SDK 内部沙盒
#define AiFreshlastUnit         @"AiFreshlastUnit"
#define AiBrushFLAG             @"AiBrushFLAG"
#define AiBrushPointCount       @"PointCount"
#define AiBrushScalesType       @"ScalesTypeAC"
#define ScalesTypeAC04          @"ac04"
#define ScalesTypeAC05          @"ac05"


#endif /* AiFreshHeader_h */
