
import UIKit
import iFreshSDK

class NewsViewController: UIViewController {
  
  @IBOutlet weak var labelFoodName: UILabel!
  @IBOutlet weak var labelWeight: UILabel!
  var isAddPeriPheraling: Bool = false
  var peripheralArray : [iFreshDevice] = []
  
  var coordinatorDelegate: NewsCoordinatorDelegate?
  
  //MARK:- ViewController LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    
    NSLog("********************************************")
    NSLog("ViewController ==> viewDidLoad Methode")
    NSLog("********************************************")
    
    iFreshSDK.shareManager()?.bleDisconnect()
    iFreshSDK.shareManager()?.setbleReturnValueDelegate(self)
    iFreshSDK.shareManager()?.bleDoScan()
  }
  
  @IBAction func submitButtonAction(_ sender: Any) {
    coordinatorDelegate?.navigateToFlutter()
  }
  
}

//MARK:- BleReturnValueDelegate Methode
extension NewsViewController: BleReturnValueDelegate {
  
  func bluetoothManagerReturePenetrateData(_ data: Data!) {
    //    print(data.description)
    //    NSLog("********************************************")
    //    NSLog("description ==> \(data.description)")
    //    NSLog("********************************************")
  }
  
  func bleStatusupdate(_ bleStatus: GN_BleStatus) {
    
    if bleStatus == bleOpen || bleStatus == bleBreak {
      
      print("Not connected")
      //labelStatus.text = "Not connected"
      NSLog("********************************************")
      NSLog("BleReturnValueDelegate ==> Not Connected")
      NSLog("********************************************")
      
    }else if bleStatus == bleOff {
      print("Not turned on")
      //labelStatus.text = "Not turned on"
      NSLog("********************************************")
      NSLog("BleReturnValueDelegate ==> Not turned on")
      NSLog("********************************************")
      
    }else if bleStatus == bleConnect {
      print("Connected")
     //labelStatus.text = "Connected"
      NSLog("********************************************")
      NSLog("BleReturnValueDelegate ==> Connected")
      NSLog("********************************************")
      
    }
  }
  
  func bleReturnScannedDevice(_ device: iFreshDevice!) {
    
    if isAddPeriPheraling == true {
      return
    }
    
    isAddPeriPheraling = true
    
    var willAdd = true
    for model in peripheralArray {
      
      if model.mac == device.mac {
        willAdd = false
      }
    }
    
    if willAdd {
      peripheralArray.append(device)
      
      iFreshSDK.shareManager()?.setbleReturnValueDelegate(self)
      iFreshSDK.shareManager()?.connect(peripheralArray.first)
      
    }
    
    isAddPeriPheraling = false
  }
  
  
  func bleReturnValueModel(_ model: iFreshModel!) {
    
    print("Weight", model.value)
    print("Model.gValue", model.gValue)
    labelWeight.text = model.value
    
    NSLog("********************************************")
    NSLog("Weight ==> \(model.value) ")
    NSLog("********************************************")
    
    NSLog("********************************************")
    NSLog("Model.gValue ==> \(model.gValue) ")
    NSLog("********************************************")
  }
}
